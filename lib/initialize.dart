import 'package:com_tarazgroup/desktop.dart';
import 'package:com_tarazgroup/login.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/modules/future/webservice.dart';
import 'package:com_tarazgroup/modules/rest/restful.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart';
import 'package:another_flushbar/flushbar.dart';
import 'models/application_info.dart';

class Initialize {
  final BuildContext context;
  const Initialize(this.context);
  Future<void> getActiveApplicationsAndGoOn(
      GlobalKey<ScaffoldState> scaffoldKey) async {
    WebService.callModalMode<ApplicationInfo>(
      context,
      value: ApplicationInfo(),
      futureResponse: _futureResponseActiveInfo,
      waitingMsg: "Get Active Applications",
      doneMsg: "Get Active Applications",
      on200object: (applicationInfo) {
        if (applicationInfo.apps.length == 0) {
          Flushbar(
            leftBarIndicatorColor: Colors.green[300],
            margin: EdgeInsets.all(8),
            messageText: Text(
                "اعتبار استفاده از `نرم افزار جامع همراه تراز سامانه` به پایان رسیده است" +
                    "\n" +
                    "\n" +
                    "لطفا با پشتیبانی تراز سامانه تماس حاصل فرمایید" +
                    "\n" +
                    "\n" +
                    "021-88662021",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'IRANSansBlack',
                  color: Colors.white,
                  fontSize: 12.0,
                )),
            duration: Duration(seconds: 30),
          )..show(context);
        } else {
          Setting setting = TIW.getSetting(context);
          setting.apps = applicationInfo.apps;
          TIW.setSetting(context, setting);
          if (TIW.getSetting(context).token == null) {
            Navigator.of(context).push<Response>(
              PageRouteBuilder(
                transitionDuration: Duration(milliseconds: 0),
                // settings: RouteSettings(isInitialRoute: true),
                pageBuilder: (context, animation1, animation2) {
                  return Login();
                },
              ),
            );
          } else {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => Desktop()),
            );
          }
        }
      },
    );
  }

  Future<Response> _futureResponseActiveInfo() {
    Setting setting = TIW.getSetting(context);
    return TRestful.get("applicationinfo",
        setting: setting, timeout: 30, needToken: false);
  }
}
