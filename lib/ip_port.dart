import 'package:com_tarazgroup/initialize.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class IpPort extends StatefulWidget {
  @override
  _IpPortState createState() => _IpPortState();
}

class _IpPortState extends State<IpPort> {
  final formKey = GlobalKey<FormState>();
  // final textEditIPController = TextEditingController();
  // final textEditPortController = TextEditingController();
  String ip, port;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: FutureBuilder<SharedPreferences>(
        future: SharedPreferences.getInstance(),
        builder: (BuildContext context,
            AsyncSnapshot<SharedPreferences> asyncSnapshot) {
          return Center(
              child: Form(
            key: formKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                ListTileTheme(
                  iconColor: Theme.of(context).primaryColor,
                  child: ListTile(
                    leading: Icon(FontAwesomeIcons.server),
                  ),
                ),
                ListTile(
                  title: TextFormField(
                    // initialValue: "172.16.1.53",
                    controller: TextEditingController(
                        text: asyncSnapshot.data == null
                            ? ""
                            : asyncSnapshot.data.getString("ip")),
                    decoration: new InputDecoration(
                      labelText: "IP",
                      contentPadding: EdgeInsets.all(4.0),
                    ),
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      Pattern pattern =
                          r'^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$';
                      RegExp regex = new RegExp(pattern);
                      if (!regex.hasMatch(value))
                        return 'آی پی معتبر نیست';
                      else
                        return null;
                    },
                    onSaved: (String val) {
                      ip = val;
                    },
                  ),
                ),
                ListTile(
                  title: TextFormField(
                    // initialValue: "8080",
                    maxLength: 4,
                    controller: TextEditingController(
                        text: asyncSnapshot.data == null
                            ? ""
                            : asyncSnapshot.data.getString("port")),
                    decoration: new InputDecoration(
                      labelText: "Port",
                      contentPadding: EdgeInsets.all(4.0),
                    ),
                    keyboardType: TextInputType.number,
                    validator: (value) {
                      Pattern pattern = r'^\d{4}$';
                      RegExp regex = new RegExp(pattern);
                      if (!regex.hasMatch(value))
                        return 'پورت معتبر نیست';
                      else
                        return null;
                    },
                    onSaved: (String val) {
                      port = val;
                    },
                  ),
                ),
                ListTile(
                  title: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Theme.of(context).primaryColor,
                    ),
                    child: Text("ادامه",
                        style:
                            TextStyle(fontFamily: 'IRANSans', fontSize: 16.0)),
                    onPressed: () {
                      if (formKey.currentState.validate()) {
                        formKey.currentState.save();
                        SharedPreferences.getInstance().then((sp) {
                          sp.setString("ip", ip);
                          sp.setString("port", port);
                          Setting setting = TIW.getSetting(context);
                          setting.ip = ip;
                          setting.port = port;
                          TIW.setSetting(context, setting);
                          Initialize(context)
                              .getActiveApplicationsAndGoOn(scaffoldKey);
                        });
                      }
                    },
                  ),
                )
              ],
            ),
          ));
        },
      ),
    );
  }
}
