class SaleSettingServer {
  final List<SaleDefaultVoucher> saleDefaultVouchers;
  final SystemSetup systemSetup;

  const SaleSettingServer({
    this.saleDefaultVouchers,
    this.systemSetup,
  });
  SaleSettingServer fromMap(Map map) {
    List saledefaultvouchers = map['voucherDefaults'];
    Map systemsetup = map['sysSetup'];
    return SaleSettingServer(
        saleDefaultVouchers: List.generate(saledefaultvouchers.length, (index) {
          return SaleDefaultVoucher.fromMap(saledefaultvouchers[index]);
        }),
        systemSetup: SystemSetup.fromMap(systemsetup));
  }
}

class SaleSettingClient {
  bool mustSendLocation;
  bool hasRemain;
  bool hasPrice;

  SaleSettingClient({
    this.mustSendLocation,
    this.hasRemain,
    this.hasPrice,
  });
}

class SaleDefaultVoucher {
  final int voucherTypeID;
  final int storeID;
  final bool controlInventory;
  final bool isFixedFee;
  final int saleCenterID;
  final int deliverCenterID;
  final int saleTypeID;
  final String paymentWayIDs;
  final int center1ID;
  final int center2ID;
  final int center3ID;

  const SaleDefaultVoucher({
    this.voucherTypeID,
    this.storeID,
    this.controlInventory,
    this.isFixedFee,
    this.saleCenterID,
    this.deliverCenterID,
    this.saleTypeID,
    this.paymentWayIDs,
    this.center1ID,
    this.center2ID,
    this.center3ID,
  });

  static SaleDefaultVoucher fromMap(Map map) {
    return SaleDefaultVoucher(
      voucherTypeID: map["voucherTypeID"],
      storeID: map["storeID"],
      controlInventory: map["controlInventory"],
      isFixedFee: map["isFixedFee"],
      saleCenterID: map["saleCenterID"],
      deliverCenterID: map["deliverCenterID"],
      saleTypeID: map["saleTypeID"],
      paymentWayIDs: map["paymentWayIDs"],
      center1ID: map["center1ID"],
      center2ID: map["center2ID"],
      center3ID: map["center3ID"],
    );
  }
}

class SystemSetup {
  final bool isByStore;
  final bool isFeeAgrToFeeInRefer;
  final bool isStoreInDet;
  final bool canCreateCustomerInAndroid;
  final bool isShowSaleTypeInVoucherHeaderInAndroid;

  const SystemSetup({
    this.isByStore,
    this.isFeeAgrToFeeInRefer,
    this.isStoreInDet,
    this.canCreateCustomerInAndroid,
    this.isShowSaleTypeInVoucherHeaderInAndroid,
  });

  static SystemSetup fromMap(Map map) {
    return SystemSetup(
      isByStore: map["isByStore"],
      isFeeAgrToFeeInRefer: map["isFeeAgrToFeeInRefer"],
      isStoreInDet: map["isStoreInDet"],
      canCreateCustomerInAndroid: map["canCreateCustomerInAndroid"],
      isShowSaleTypeInVoucherHeaderInAndroid: map["isShowSaleTypeInVoucherHeaderInAndroid"],
    );
  }
}
