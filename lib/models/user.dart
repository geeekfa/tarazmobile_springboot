class User {
  final int perComID;
  final String name;
  final String family;
  final String fatherName;
  final String latinName;
  final bool gender;
  final String address;
  final bool isAdmin;
  final int serverID;
  final String serverIDs;
  final int activeYearID;

  User(
      {this.perComID,
      this.name,
      this.family,
      this.fatherName,
      this.latinName,
      this.gender,
      this.address,
      this.serverID,
      this.serverIDs,
      this.activeYearID,
      this.isAdmin});

  User fromMap(Map map) {
    return User(
        perComID: map["perComID"],
        name: map["name"],
        family: map["family"],
        fatherName: map["fatherName"],
        latinName: map["latinName"],
        gender: map["isMale"],
        address: map["address"],
        serverID: map["serverID"],
        serverIDs: map["serverIDs"],
        activeYearID: map["activeYearID"],
        isAdmin: map["isAdmin"]);
  }
}
