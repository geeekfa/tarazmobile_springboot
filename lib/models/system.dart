import 'package:com_tarazgroup/models/sale_setting.dart';
import 'package:flutter/material.dart';

enum SystemType { SALE, INV }

class System {
  final String id;
  final int systemID;
  final String title;
  bool isActive;
  final ThemeData themeData;
  final IconData iconData;
  SaleSettingServer saleSettingServer;
  SaleSettingClient saleSettingClient;
  Function onTap;
  System({
    this.id,
    this.systemID,
    this.title,
    this.isActive,
    this.themeData,
    this.iconData,
    this.onTap,
    this.saleSettingClient,
  });
}
