class Tafsili {
  final int tafsiliID;
  final String tafsiliDesc;
  final String tafsiliCode;

  Tafsili({
    this.tafsiliID,
    this.tafsiliDesc,
    this.tafsiliCode,
  });

  int get id {
    return tafsiliID;
  }

  String get desc {
    return tafsiliDesc;
  }

  Tafsili fromMap(Map map) {
    return Tafsili(
      tafsiliID: map['tafsiliID'],
      tafsiliDesc: map['tafsiliDesc'],
      tafsiliCode: map['tafsiliCode'],
    );
  }
}
