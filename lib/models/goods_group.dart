class GoodsGroup {
  int groupID;
  final int fatherID;
  final String groupDesc;
  final bool isLeaf;
  final String imageGroup;
  final String parentIDs;

  GoodsGroup({
     this.groupID,
     this.fatherID,
     this.groupDesc,
     this.isLeaf,
     this.imageGroup,
     this.parentIDs
  });

  int get id {
    return groupID;
  }

  String get desc {
    return groupDesc;
  }

  GoodsGroup fromMap(Map map) {
    return GoodsGroup(
      groupID: map['groupID'],
      fatherID: map['fatherID'],
      groupDesc: map['groupDesc'],
      isLeaf: map['isLeaf'],
      imageGroup: map['imageGroup'],
      parentIDs: map['parentIDs'],
    );
  }
}
