import 'package:com_tarazgroup/models/promotion.dart';

class PromotionElement {
  List<TPromotion> tPromotions;
  List<TElement> tElements;

  PromotionElement({
    this.tPromotions,
    this.tElements,
  });

  PromotionElement fromMap(Map map) {
    List ps = map['promotions'];
    List es = map['elements'];
    List<TPromotion> tps = ps.map<TPromotion>((p) {
      return TPromotion.fromMap(p);
    }).toList();

    List<TElement> tes = es.map<TElement>((e) {
      return TElement.fromMap(e);
    }).toList();

    return PromotionElement(
      tPromotions: tps,
      tElements: tes,
    );
  }
}

class TElement {
  final int goodsID;
  final int elementID;
  final String elementDesc;
  final double elementMab;

  TElement({
    this.goodsID,
    this.elementID,
    this.elementDesc,
    this.elementMab,
  });

  // static List<TElement> fromPromotionElements(
  //     List<PromotionElement> promotionElements) {
  //   List<PromotionElement> promotionElementsTemp =
  //       promotionElements.where((promotionElement) {
  //     return promotionElement.isGoods == null;
  //   }).to[]

  //   List<TElement> tElements = []

  //   for (PromotionElement item in promotionElementsTemp) {
  //     tElements.add(_fromPromotionElement(item));
  //   }

  //   return tElements;
  // }

  // static TElement _fromPromotionElement(PromotionElement promotionElement) {
  //   return TElement(
  //       elementID: promotionElement.elementID,
  //       elementDesc: promotionElement.elementDesc,
  //       pRMVAL: promotionElement.pRMVAL);
  // }

  static TElement fromMap(Map map) {
    return TElement(
      elementDesc: map['elementDesc'],
      elementID: map['elementID'],
      elementMab: map['elementMab'],
      goodsID: map['goodsID'],
    );
  }

  static Map toMap(TElement tElement) {
    return {
      "goodsID": tElement.goodsID,
      "elementDesc": tElement.elementDesc,
      "elementMab": tElement.elementMab,
      "elementID": tElement.elementID,
    };
  }

  static List<Map> toListMap(List<TElement> tElements) {
    return tElements.map((tElement) {
      return toMap(tElement);
    }).toList();
  }
}
