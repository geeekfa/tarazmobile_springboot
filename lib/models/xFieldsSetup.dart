class XFieldsSetup {
  final int fieldNumber;
  final int voucherTypeID;
  final String customCaption;
  final bool isShowFee;
  final int fieldType;

  XFieldsSetup(
      {this.fieldNumber,
      this.voucherTypeID,
      this.customCaption,
      this.isShowFee,
      this.fieldType});

  XFieldsSetup fromMap(Map map) {
    return XFieldsSetup(
      fieldNumber: map['fieldNumber'],
      voucherTypeID: map['voucherTypeID'],
      customCaption: map['customCaption'],
      isShowFee: map['isShowFee'],
      fieldType: map['fieldType'],
    );
  }
}
