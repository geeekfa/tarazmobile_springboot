class SaleType {
  final int saleTypeID;
  final String saleTypeDesc;

  const SaleType({
    this.saleTypeID,
    this.saleTypeDesc,
  });

  int get id {
    return saleTypeID;
  }

  String get desc {
    return saleTypeDesc;
  }

  SaleType fromMap(Map map) {
    return SaleType(
      saleTypeID: map['saleTypeID'],
      saleTypeDesc: map['saleTypeDesc'],
    );
  }
}
