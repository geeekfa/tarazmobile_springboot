import 'package:com_tarazgroup/models/customer.dart';
import 'package:com_tarazgroup/models/provider.dart';
import 'package:com_tarazgroup/models/sale_type.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/xFieldsSetup.dart';
import 'package:com_tarazgroup/models/store.dart';
import 'package:com_tarazgroup/models/tafsili.dart';
import 'package:com_tarazgroup/models/voucher.dart';
import 'package:com_tarazgroup/models/voucher_header.dart';
import 'package:com_tarazgroup/models/voucher_type.dart';
import 'package:flutter/material.dart';

class TIW extends StatefulWidget {
  final Widget child;

  TIW({
    @required this.child,
  });

  static TIWState _of(BuildContext context, {bool initState}) {
    if (initState != null && initState) {
      var data = context
          .getElementForInheritedWidgetOfExactType<_InheritedState>()
          ?.widget;
      return (data as _InheritedState).data;
    }
    return context.dependOnInheritedWidgetOfExactType<_InheritedState>().data;
  }

  static Setting getSetting(BuildContext context, {bool initState}) {
    TIWState _tIWState = _of(context, initState: initState);
    if (_tIWState.setting == null) {
      return Setting();
    }
    return _tIWState.setting;
  }

  static void setSetting(BuildContext context, Setting setting,
      {bool initState}) {
    _of(context, initState: initState).updateSetting(setting);
  }

  //Voucher
  static Voucher getVoucher(BuildContext context, {bool initState}) {
    TIWState _tIWState = _of(context, initState: initState);
    if (_tIWState.voucher == null) {
      return Voucher(
          voucherHeader: VoucherHeader(
              voucherNumber: null,
              voucherType: VoucherType(),
              saleType: SaleType(),
              voucherTypeRefer: VoucherType(),
              store: Store(),
              customer: Customer(),
              provider: Provider(),
              tafsili: Tafsili(),
              voucherDesc: null,
              voucherDate: null,
              salesManID: null,
              marketingManID: null,
              saleCenterID: null,
              deliverCenterID: null,
              saleTypeID: null,
              isManualPromotion: null,
              center1ID: null,
              center2ID: null,
              center3ID: null,
              paymentWayID: null,
              voucherHeaderID: null),
          voucherDetails: [],
          tElements: [],
          tPromotions: []);
    }
    return _tIWState.voucher;
  }

  static void setVoucher(BuildContext context, Voucher voucher,
      {bool initState}) {
    _of(context, initState: initState).updateVoucher(voucher);
  }

  //XFieldsSetup
  static List<XFieldsSetup> getXFieldsSetups(BuildContext context, {bool initState}) {
    TIWState _tIWState = _of(context, initState: initState);
    if (_tIWState.xFieldsSetups == null) {
      return [];
    }
    return _tIWState.xFieldsSetups;
  }

  static void setXFieldsSetups(BuildContext context, List<XFieldsSetup> xFieldsSetups,
      {bool initState}) {
    _of(context, initState: initState).updateXFieldsSetups(xFieldsSetups);
  }

  @override
  TIWState createState() => new TIWState();
}

class TIWState extends State<TIW> {
  Setting setting;
  Voucher voucher;
  // زمانی که نوع سند انتخاب میشود وب سرویس فیلد های اختاری سند کال میشود
  List<XFieldsSetup> xFieldsSetups;

  //Setting
  void updateSetting(setting) {
    setState(() {
      this.setting = setting;
    });
  }

  //Voucher
  void updateVoucher(voucher) {
    setState(() {
      this.voucher = voucher;
    });
  }

  //XFieldsSetups
  void updateXFieldsSetups(xFieldsSetups) {
    setState(() {
      this.xFieldsSetups = xFieldsSetups;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new _InheritedState(
      data: this,
      child: widget.child,
    );
  }
}

class _InheritedState extends InheritedWidget {
  final TIWState data;

  _InheritedState({
    Key key,
    @required this.data,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_InheritedState old) => true;
}
