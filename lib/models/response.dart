class TResponse {
  final int newID;
  final int result;
  final String errMsg;
  const TResponse({this.newID, this.result, this.errMsg});

  TResponse fromMap(Map map){
    return TResponse(
        newID: map['NEWID'],
        result: map['RESULT'],
        errMsg: map['ERRMSG']);
  }
}