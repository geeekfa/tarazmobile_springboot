class Token {
  final String token;
  final int expire;
  final String type;
  const Token({
    this.token,
    this.expire,
    this.type,
  });

  Token fromMap(Map map) {
    return Token(
      token: map["token"],
      expire: map["expire"],
      type: map["type"],
    );
  }
}
