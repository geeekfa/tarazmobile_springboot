class ActiveApplication {
  final String companyName;
  final List<String> names;

  ActiveApplication({
     this.companyName,
     this.names,
  });

  ActiveApplication fromMap(Map map) {
    return ActiveApplication(
        companyName: map["company_name"],
        names:
            List<String>.generate(map["active_applications"].length, (index) {
          return map["active_applications"][index]["application_name"];
        }));
  }
}
