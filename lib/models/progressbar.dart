import 'package:flutter/material.dart';

class ProgressBar extends StatelessWidget {
  final String label;
  final double value;
  final double size;

  const ProgressBar({
     this.label,
     this.value,
     this.size,
  });
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            child: CircularProgressIndicator(
              value: value,
              strokeWidth: 1.5,
              valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor),
            ),
            height: size,
            width: size,
          ),
          Text(label,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 12.0,
              ))
        ],
      ),
    );
  }
}