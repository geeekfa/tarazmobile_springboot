import 'dart:convert';

import 'package:com_tarazgroup/models/promotion.dart';
import 'package:com_tarazgroup/models/promotion_element.dart';
import 'package:com_tarazgroup/models/voucher_detail.dart';
import 'package:com_tarazgroup/models/voucher_header.dart';

class Voucher {
  VoucherHeader voucherHeader;
  List<VoucherDetail> voucherDetails;
  List<TPromotion> tPromotions;
  List<TElement> tElements;

  Voucher({
    this.voucherHeader,
    this.voucherDetails,
    this.tPromotions,
    this.tElements,
  });

  Voucher fromMap(Map map) {
    VoucherHeader voucherHeader = VoucherHeader.fromMap1(map["header"]);
    int index = -1;
    List<VoucherDetail> voucherDetails =
        map["details"].map<VoucherDetail>((voucherDetail) {
      index++;
      return VoucherDetail.fromMap1(voucherDetail, index);
    }).toList();
    return Voucher(
        voucherHeader: voucherHeader, voucherDetails: voucherDetails);
  }

  static Map _toMapSale(Voucher voucher) {
    return {
      "header": VoucherHeader.toMapSale(voucher.voucherHeader),
      "details": VoucherDetail.toListMapSale(voucher.voucherDetails),
      "promotions": voucher.tPromotions == null
          ? []
          : TPromotion.toListMap(voucher.tPromotions),
      "elements": voucher.tElements == null
          ? []
          : TElement.toListMap(voucher.tElements),
      "other": {},
    };
  }

  static Map _toMapInv(Voucher voucher) {
    return {
      "header": VoucherHeader.toMapInv(voucher.voucherHeader),
      "details": VoucherDetail.toListMapInv(voucher.voucherDetails),
    };
  }

  static String jsonEncodeSale(Voucher voucher) {
    return jsonEncode(_toMapSale(voucher));
  }

  static String jsonEncodeInv(Voucher voucher) {
    return jsonEncode(_toMapInv(voucher));
  }
}
