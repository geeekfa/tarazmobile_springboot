class DeliverCenter {
  final int deliverCenterID;
  final String deliverCenterDesc;

  const DeliverCenter({
     this.deliverCenterID,
     this.deliverCenterDesc,
  });

  int get id {
    return deliverCenterID;
  }

  String get desc {
    return deliverCenterDesc;
  }

  DeliverCenter fromMap(Map map) {
    return DeliverCenter(
      deliverCenterID: map['DELIVERCENTERID'],
      deliverCenterDesc: map['DELIVERCENTERDESC'],
    );
  }
}
