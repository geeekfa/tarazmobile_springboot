import 'package:com_tarazgroup/models/goods_unit.dart';

class Goods {
  int goodsID;
  String barCode;
  int groupID;
  double remain;
  double secRemain;
  double price;
  double price1;
  double price2;
  String goodsDesc;
  String goodsCode;
  List<String> imageUrls;
  int serialType;
  String serialDesc;
  GoodsUnit goodsUnit;
  String techInfo;
  int storeID;
  String storeName;

  Goods({
    this.goodsID,
    this.barCode,
    this.groupID,
    this.remain,
    this.secRemain,
    this.price,
    this.price1,
    this.price2,
    this.goodsDesc,
    this.goodsCode,
    this.serialType,
    this.serialDesc,
    this.imageUrls,
    this.goodsUnit,
    this.techInfo,
    this.storeID,
    this.storeName,
  });

  Goods.clone(Goods goods)
      : this(
          goodsID: goods.goodsID,
          barCode: goods.barCode,
          groupID: goods.groupID,
          remain: goods.remain,
          secRemain: goods.secRemain,
          price: goods.price,
          price1: goods.price1,
          price2: goods.price2,
          goodsDesc: goods.goodsDesc,
          goodsCode: goods.goodsCode,
          serialType: goods.serialType,
          serialDesc: goods.serialDesc,
          imageUrls: goods.imageUrls,
          goodsUnit: goods.goodsUnit,
          storeID: goods.storeID,
          storeName: goods.storeName,
        );

  Goods fromMap(Map jsonObject) {
    return Goods(
      goodsID: jsonObject['goodsID'],
      barCode: jsonObject['barCode'],
      groupID: jsonObject['groupID'],
      remain: jsonObject['remain'],
      secRemain: jsonObject['secRemain'],
      price: jsonObject['price'],
      price1: jsonObject['price1'],
      price2: jsonObject['price2'],
      goodsDesc: jsonObject['goodsDesc'],
      goodsCode: jsonObject['goodsCode'],
      imageUrls: jsonObject['imageUrls'] == null
          ? null
          : List<String>.generate(jsonObject['imageUrls'].length, (index) {
              return jsonObject['imageUrls'][index];
            }),
      serialType: jsonObject['serialType'],
      serialDesc: jsonObject['serialDesc'],
      goodsUnit: GoodsUnit(
        unitID: jsonObject['unitID'],
        unitDesc: jsonObject['unitDesc'],
        secUnitID: jsonObject['secUnitID'],
        secUnitDesc: jsonObject['secUnitDesc'],
        defaultSecUnitID: jsonObject['defaultSecUnitID'],
        computeValue: jsonObject['computeValue'],
        isQuantitative: jsonObject['isQuantitative'],
        isQuantitativeDesc: jsonObject['isQuantitativeDesc'],
      ),
      techInfo: jsonObject['techInfo'],
      storeID: jsonObject['storeID'],
      storeName: jsonObject['storeName'],
    );
  }

  Map toMap1(Goods goods) {
    return {
      "barCode": goods.barCode,
      "goodsCode": goods.goodsCode,
      "goodsDesc": goods.goodsDesc,
      "goodsID": goods.goodsID,
      "groupID": goods.groupID,
      "imageUrls": goods.imageUrls,
      "price": goods.price,
      "price1": goods.price1,
      "remain": goods.remain,
      "secRemain": goods.secRemain,
      "serialDesc": goods.serialDesc,
      "serialType": goods.serialType,
      "goodsUnit": {
        "unitID": goods.goodsUnit.unitID,
        "unitDesc": goods.goodsUnit.unitDesc,
        "secUnitID": goods.goodsUnit.secUnitID,
        "secUnitDesc": goods.goodsUnit.secUnitDesc,
        "defaultSecUnitID": goods.goodsUnit.defaultSecUnitID,
        "computeValue": goods.goodsUnit.computeValue,
      },
      "techInfo": goods.techInfo,
      "storeID": goods.storeID,
      "storeName": goods.storeName,
    };
  }

  static Map toMap(Goods goods) {
    return {
      "barCode": goods.barCode,
      "goodsCode": goods.goodsCode,
      "goodsDesc": goods.goodsDesc,
      "goodsID": goods.goodsID,
      "groupID": goods.groupID,
      "imageUrls": goods.imageUrls,
      "price": goods.price,
      "price1": goods.price1,
      "remain": goods.remain,
      "secRemain": goods.secRemain,
      "serialDesc": goods.serialDesc,
      "serialType": goods.serialType,
      "goodsUnit": {
        "unitID": goods.goodsUnit.unitID,
        "unitDesc": goods.goodsUnit.unitDesc,
        "secUnitID": goods.goodsUnit.secUnitID,
        "secUnitDesc": goods.goodsUnit.secUnitDesc,
        "defaultSecUnitID": goods.goodsUnit.defaultSecUnitID,
        "computeValue": goods.goodsUnit.computeValue,
        "isQuantitative": goods.goodsUnit.isQuantitative,
        "isQuantitativeDesc": goods.goodsUnit.isQuantitativeDesc,
      },
      "techInfo": goods.techInfo,
      "storeID": goods.storeID,
      "storeName": goods.storeName,
    };
  }
}
