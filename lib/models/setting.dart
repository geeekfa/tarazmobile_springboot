import 'package:com_tarazgroup/models/system.dart';

class Setting {
  int perComID;
  String userName;
  String password;
  String name;
  String family;
  String ip;
  String port;
  String androidId;
  String token;
  List<String> apps;
  System sale;
  System inv;
  System currentSystem;
  bool isAdmin;
  int serverID;
  String serverIDs;
  int activeYearID;

  Setting({
    this.perComID,
    this.userName,
    this.password,
    this.name,
    this.family,
    this.ip,
    this.port,
    this.androidId,
    this.token,
    this.apps,
    this.sale,
    this.inv,
    this.isAdmin,
    this.serverID,
    this.serverIDs,
    this.activeYearID,
  });

  System getSystem(SystemType systemType) {
    switch (systemType) {
      case SystemType.SALE:
        return sale;
      case SystemType.INV:
        return inv;
      default:
        return null;
    }
  }
}
