class VoucherType {
  final int voucherTypeID;
  final String voucherTypeDesc;
  final SerialType serialType;
  final int systemID;
  final bool isUnlockRefByBarcode;

  const VoucherType({
    this.voucherTypeID,
    this.voucherTypeDesc,
    this.serialType,
    this.systemID,
    this.isUnlockRefByBarcode,
  });

  int get id {
    return voucherTypeID;
  }

  String get desc {
    return voucherTypeDesc;
  }

  VoucherType fromMap(Map map) {
    return VoucherType(
        voucherTypeID: map['voucherTypeID'],
        voucherTypeDesc: map['voucherTypeDesc'],
        systemID: map['systemID'],
        isUnlockRefByBarcode: map['isUnlockRefByBarcode'],
        serialType: SerialType(
          selSerialType: map['selSerialType'],
          selSerialTypeDesc: map['selSerialTypeDesc'],
        ));
  }
}

class SerialType {
  final int selSerialType;
  final String selSerialTypeDesc;

  const SerialType({
    this.selSerialType,
    this.selSerialTypeDesc,
  });

  SerialType fromMap(Map map) {
    return SerialType(
        selSerialType: map['selSerialType'],
        selSerialTypeDesc: map['selSerialTypeDesc']);
  }
}
