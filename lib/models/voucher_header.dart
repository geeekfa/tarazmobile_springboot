import 'package:com_tarazgroup/models/provider.dart';
import 'package:com_tarazgroup/models/sale_type.dart';
import 'package:com_tarazgroup/models/store.dart';
import 'package:com_tarazgroup/models/tafsili.dart';
import 'package:com_tarazgroup/models/voucher_type.dart';
import 'package:flutter/foundation.dart';
import 'customer.dart';

class VoucherHeader {
  int voucherHeaderID;
  int voucherNumber;
  String voucherDate;
  int referNumber;
  String referDate;
  VoucherType voucherType;
  VoucherType voucherTypeRefer;
  Store store;
  Customer customer;
  Provider provider;
  Tafsili tafsili;
  String voucherDesc;

  int salesManID;
  int marketingManID;
  int saleCenterID;
  int deliverCenterID;
  int saleTypeID;
  SaleType saleType;
  bool isManualPromotion;

  int paymentWayID;
  int center1ID;
  int center2ID;
  int center3ID;

  // SaleType saleType;
  // SaleCenter saleCenter;
  // DeliverCenter deliverCenter;

  VoucherHeader(
      {this.voucherHeaderID,
      @required this.voucherNumber,
      @required this.voucherType,
      @required this.voucherTypeRefer,
      @required this.store,
      @required this.customer,
      @required this.provider,
      @required this.tafsili,
      @required this.voucherDesc,
      @required this.voucherDate,
      @required this.salesManID,
      @required this.marketingManID,
      @required this.saleCenterID,
      @required this.deliverCenterID,
      @required this.saleTypeID,
      @required this.isManualPromotion,
      @required this.paymentWayID,
      @required this.center1ID,
      @required this.center2ID,
      @required this.center3ID,
      @required this.saleType
      // this.saleCenter,
      // this.deliverCenter,
      });

  static Map toMapSale(VoucherHeader voucherHeader) {
    return {
      "voucherTypeID": voucherHeader.voucherType.voucherTypeID,
      "customerID": voucherHeader.customer.customerID,
      "salesManID": voucherHeader.salesManID,
      "marketingManID": voucherHeader.marketingManID,
      "storeID": voucherHeader.store.storeID,
      "saleCenterID": voucherHeader.saleCenterID,
      "deliverCenterID": voucherHeader.deliverCenterID,
      "saleTypeID": voucherHeader.saleTypeID,
      // "saleType": voucherHeader.saleType,
      "voucherDesc": voucherHeader.voucherDesc,
      "voucherDate": voucherHeader.voucherDate,
      "refTypeID": voucherHeader.voucherTypeRefer.voucherTypeID,
      "referID": voucherHeader.voucherHeaderID,
      "referDate": voucherHeader.referDate,
      "referNumber": voucherHeader.referNumber,
      "isManualPromotion": voucherHeader.isManualPromotion,
      "paymentWayID": voucherHeader.paymentWayID,
      "center1ID": voucherHeader.center1ID,
      "center2ID": voucherHeader.center2ID,
      "center3ID": voucherHeader.center3ID,
      // "SaleTypeID": voucherHeader.saleType.saleTypeID,
      // "SaleCenterID": voucherHeader.saleCenter.saleCenterID,
      // "DeliverCenterID": voucherHeader.deliverCenter.deliverCenterID
    };
  }

  static Map toMapBuy(VoucherHeader voucherHeader) {
    return {
      "voucherTypeID": voucherHeader.voucherType.voucherTypeID,
      "providerID": voucherHeader.provider.providerID,
      "salesManID": voucherHeader.salesManID,
      "marketingManID": voucherHeader.marketingManID,
      "storeID": voucherHeader.store.storeID,
      "saleCenterID": voucherHeader.saleCenterID,
      "deliverCenterID": voucherHeader.deliverCenterID,
      "saleTypeID": voucherHeader.saleTypeID,
      "saleType": voucherHeader.saleType,
      "voucherDesc": voucherHeader.voucherDesc,
      "voucherDate": voucherHeader.voucherDate,
      "refTypeID": voucherHeader.voucherTypeRefer.voucherTypeID,
      "referID": voucherHeader.voucherHeaderID,
      "referDate": voucherHeader.referDate,
      "referNumber": voucherHeader.referNumber,
      "isManualPromotion": voucherHeader.isManualPromotion,
      "paymentWayID": voucherHeader.paymentWayID,
      "center1ID": voucherHeader.center1ID,
      "center2ID": voucherHeader.center2ID,
      "center3ID": voucherHeader.center3ID,
    };
  }

  static Map toMapInv(VoucherHeader voucherHeader) {
    return {
      "voucherTypeID": voucherHeader.voucherType.voucherTypeID,
      "customerID": voucherHeader.customer.customerID,
      "storeID": voucherHeader.store.storeID,
      "tafsiliID": voucherHeader.tafsili.tafsiliID,
      "voucherDesc": voucherHeader.voucherDesc,
      "voucherDate": voucherHeader.voucherDate,
      "refTypeID": voucherHeader.voucherTypeRefer.voucherTypeID,
      "referID": voucherHeader.voucherHeaderID,
      "referDate": voucherHeader.referDate,
      "referNumber": voucherHeader.referNumber,
      // "SaleTypeID": voucherHeader.saleType.saleTypeID,
      // "SaleCenterID": voucherHeader.saleCenter.saleCenterID,
      // "DeliverCenterID": voucherHeader.deliverCenter.deliverCenterID
    };
  }

  static VoucherHeader fromMap1(Map map) {
    return _fromMap(map);
  }

  VoucherHeader fromMap(Map map) {
    return _fromMap(map);
  }

  static VoucherHeader _fromMap(Map map) {
    return VoucherHeader(
      voucherHeaderID: map["voucherHeaderID"],
      voucherTypeRefer: VoucherType(
        voucherTypeID: map["voucherTypeID"],
        voucherTypeDesc: map["voucherTypeDesc"],
      ),
      voucherNumber: map["voucherNumber"],
      customer: Customer(
        customerID: map["customerID"],
        customerName: map["customerName"],
      ),
      provider: Provider(
        providerID: map["customerID"],
        providerCode: map["providerCode"],
        providerName: map["providerName"],
      ),
      tafsili: Tafsili(
        tafsiliID: map["tafsiliID"],
        tafsiliCode: map["tafsiliCode"],
        tafsiliDesc: map["tafsiliDesc"],
      ),
      store: Store(
        storeID: map["storeID"],
        storeName: map["storeName"],
      ),
      voucherType: VoucherType(
          voucherTypeID: map["voucherTypeID"],
          voucherTypeDesc: map["voucherTypeDesc"]),
      voucherDesc: map["voucherDesc"],
      voucherDate: map["voucherDate"],
      salesManID: map["salesManID"],
      marketingManID: map["marketingManID"],
      saleCenterID: map["saleCenterID"],
      deliverCenterID: map["deliverCenterID"],
      saleTypeID: map["saleTypeID"],
      saleType: SaleType(
        saleTypeID: map["saleTypeID"],
        saleTypeDesc: map["saleTypeDesc"],
      ),
      isManualPromotion: map["isManualPromotion"],
      paymentWayID: map["paymentWayID"],
      center1ID: map["center1ID"],
      center2ID: map["center2ID"],
      center3ID: map["center3ID"],
      // saleType: SaleType(
      //   saleTypeID: map["SALETYPEID"],
      //   saleTypeDesc: map["SALETYPEDESC"],
      // ),
      // saleCenter: SaleCenter(
      //   saleCenterID: map["SALECENTERID"],
      //   saleCenterDesc: map["SALECENTERDESC"],
      // ),
      // deliverCenter: DeliverCenter(
      //     deliverCenterID: map["DELIVERCENTERID"],
      //     deliverCenterDesc: map["DELIVERCENTERDESC"]),
    );
  }
}
