class GoodsUnit {
  final int goodsID;
  final bool computeType;
  final int unitID;
  final double computeValue;
  final String unitCode;
  final String unitType;
  int secUnitID;
  final String secUnitDesc;
  final int defaultSecUnitID;
  final String unitDesc;
  final bool isMainUnitID;
  final bool isDefaultSecUnitID;
  bool isQuantitative;
  String isQuantitativeDesc;

  GoodsUnit({
     this.goodsID,
     this.computeType,
     this.unitID,
     this.computeValue,
     this.unitCode,
     this.unitType,
     this.secUnitID,
     this.secUnitDesc,
     this.defaultSecUnitID,
     this.unitDesc,
     this.isMainUnitID,
     this.isDefaultSecUnitID,
     this.isQuantitative,
     this.isQuantitativeDesc,
  });
  GoodsUnit fromMap(Map jsonObject) {
    return GoodsUnit(
      goodsID: jsonObject['goodsID'],
      computeType: jsonObject['computeType'],
      unitID: jsonObject['unitID'],
      computeValue: jsonObject['computeValue'],
      unitCode: jsonObject['unitCode'],
      unitType: jsonObject['unitType'],
      secUnitID: jsonObject['secUnitID'],
      secUnitDesc: jsonObject['unitDesc'],
      defaultSecUnitID: jsonObject['defaultSecUnitID'],
      unitDesc: jsonObject['unitDesc'],
      isMainUnitID: jsonObject['isMainUnitID'] ,
      isQuantitative: jsonObject['isQuantitative'],
      isDefaultSecUnitID: jsonObject['isDefaultSecUnitID'],
      isQuantitativeDesc: jsonObject['isQuantitativeDesc'],
    );
  }

  GoodsUnit.clone(GoodsUnit goodsUnit)
      : this(
          goodsID: goodsUnit.goodsID,
          computeType: goodsUnit.computeType,
          unitID: goodsUnit.unitID,
          computeValue: goodsUnit.computeValue,
          unitCode: goodsUnit.unitCode,
          unitType: goodsUnit.unitType,
          secUnitID: goodsUnit.secUnitID,
          secUnitDesc: goodsUnit.secUnitDesc,
          defaultSecUnitID: goodsUnit.defaultSecUnitID,
          unitDesc: goodsUnit.unitDesc,
          isMainUnitID: goodsUnit.isMainUnitID,
          isDefaultSecUnitID: goodsUnit.isDefaultSecUnitID,
          isQuantitative:goodsUnit.isQuantitative,
          isQuantitativeDesc:goodsUnit.isQuantitativeDesc,
        );
}
