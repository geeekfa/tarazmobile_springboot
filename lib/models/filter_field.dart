class FilterField {
  final String name;
  final String title;
  final FilterFieldType filterFieldType;

  const FilterField({
     this.name,
     this.title,
     this.filterFieldType,
  });
}

enum FilterFieldType { NUMBER, TEXT, DATE }
