class SaleCenter {
  final int saleCenterID;
  final String saleCenterDesc;

  const SaleCenter({
    this.saleCenterID,
    this.saleCenterDesc,
  });

  int get id {
    return saleCenterID;
  }

  String get desc {
    return saleCenterDesc;
  }

  SaleCenter fromMap(Map map) {
    return SaleCenter(
      saleCenterID: map['SALECENTERID'],
      saleCenterDesc: map['SALECENTERDESC'],
    );
  }
}
