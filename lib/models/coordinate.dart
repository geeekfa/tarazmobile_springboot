// import 'package:latlong/latlong.dart';
import 'package:latlng/latlng.dart';

class Coordinate {
  int routeID;
  int marketingManID;
  int customerID;
  String customerName;
  LatLng latLng;
  String startTime;

  Coordinate({
    this.routeID,
    this.marketingManID,
    this.customerID,
    this.customerName,
    this.latLng,
    this.startTime,
  });

  Coordinate fromMap(Map jsonObject) {
    return Coordinate(
      routeID: jsonObject['routeID'],
      marketingManID: jsonObject['marketingManID'],
      customerID: jsonObject['customerID'],
      customerName: jsonObject['customerName'],
      latLng: LatLng(jsonObject['latitude'], jsonObject['longitude']),
      startTime: jsonObject['startTime'],
    );
  }
}
