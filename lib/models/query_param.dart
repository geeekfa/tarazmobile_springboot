enum QueryParamType { TEXT, DATE, NUMBER ,BOOL}

class QueryParam {
  final QueryParamType queryParamType;
  final String name;
  final String title;
  String value;
  QueryParam({
    this.queryParamType,
    this.name,
    this.title,
    this.value,
  });
}
