// این کلاس برای
// BuyShowPurchaseHeader
// ساخته شده است
class Provider {
  int providerID;
  String providerCode;
  String providerName;

  Provider({
    this.providerID,
    this.providerCode,
    this.providerName,
  });

  int get id {
    return providerID;
  }

  String get desc {
    return providerName;
  }

  Provider fromMap(Map map) {
    return Provider(
      providerID: map['providerID'],
      providerCode: map['providerCode'],
      providerName: map['providerName'],
    );
  }
}
