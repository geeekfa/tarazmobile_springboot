class Customer {
  int customerID;
  final String customerCode;
  String customerName;
  String perComAddress;
  final String perComMobile;
  final String perComTel;
  final String percomFName;
  final String percomLName;
  final double remainCHQ;
  final double remainCash;

  Customer(
      {this.customerID,
      this.customerCode,
      this.customerName,
      this.perComAddress,
      this.perComMobile,
      this.perComTel,
      this.percomFName,
      this.percomLName,
      this.remainCHQ,
      this.remainCash});

  int get id {
    return customerID;
  }

  String get desc {
    return customerName;
  }

  Customer fromMap(Map map) {
    return Customer(
        customerID: map['customerID'],
        customerCode: map['customerCode'],
        customerName: map['customerName'],
        perComAddress: map['perComAddress'],
        perComMobile: map['perComMobile'],
        perComTel: map['perComTel'],
        percomFName: map['percomFName'],
        percomLName: map['percomLName'],
        remainCHQ: map['remainChq']==null?null:double.parse(map['remainChq'].toString()),
        remainCash: map['remainCash']==null?null:double.parse(map['remainCash'].toString()));
  }
  // static Future<Customer> fromSharedPreferences() async {
  //   SharedPreferences sp = await SharedPreferences.getInstance();
  //   String customerStr = sp.getString("customer");
  //   if (customerStr != null) {
  //     Customer customer1 = Customer.fromJsonObject(json.decode(customerStr));
  //     return customer1;
  //   }
  //   return null;
  // }
}
