class Store {
  final int storeID;
  final String storeCode;
  final String storeName;

  const Store({
    this.storeID,
    this.storeCode,
    this.storeName,
  });

  int get id {
    return storeID;
  }

  String get desc {
    return storeName;
  }

  Store fromMap(Map map) {
    return Store(
        storeID: map['storeID'],
        storeCode: map['storeCode'],
        storeName: map['storeName']);
  }
}
