class TPromotion {
  final int goodsID;
  final String goodsDesc;
  final int secUnitID;
  final String unitDesc;
  final double quantity;
  final String type;

  TPromotion({
    this.goodsID,
    this.goodsDesc,
    this.secUnitID,
    this.unitDesc,
    this.quantity,
    this.type,
  });

  // static List<TPromotion> fromPromotionElements(
  //     List<PromotionElement> promotionElements) {
  //   List<PromotionElement> promotionElementsTemp =
  //       promotionElements.where((promotionElement) {
  //     return promotionElement.isGoods == true;
  //   }).to[]

  //   List<TPromotion> tPromotions = []

  //   for (PromotionElement item in promotionElementsTemp) {
  //     tPromotions.add(_fromPromotionElement(item));
  //   }

  //   return tPromotions;
  // }

  // static TPromotion _fromPromotionElement(PromotionElement promotionElement) {
  //   return TPromotion(
  //       goodsDesc: promotionElement.goodsDesc,
  //       goodsID: promotionElement.goodsID,
  //       quantity: promotionElement.quantity,
  //       secUnitID: promotionElement.secUnitID,
  //       unitDesc: promotionElement.unitDesc);
  // }

  static TPromotion fromMap(Map map) {
    return TPromotion(
      goodsDesc: map['goodsDesc'],
      goodsID: map['goodsID'],
      quantity: map['quantity'],
      secUnitID: map['secUnitID'],
      unitDesc: map['unitDesc'],
      type: map['type'] ?? "خودکار",
    );
  }

  static Map toMap(TPromotion tPromotion) {
    return {
      "goodsID": tPromotion.goodsID,
      "quantity": tPromotion.quantity,
      "secUnitID": tPromotion.secUnitID,
      "type": tPromotion.type,
    };
  }

  static List<Map> toListMap(List<TPromotion> tPromotions) {
    return tPromotions.map((tPromotion) {
      return toMap(tPromotion);
    }).toList();
  }
}
