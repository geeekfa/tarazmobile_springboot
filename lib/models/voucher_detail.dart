import 'package:com_tarazgroup/models/goods.dart';
import 'package:com_tarazgroup/models/goods_unit.dart';

enum VoucherDetailStatus {
  DEFAULT_ACTIVE, //این دیتیل دیفالت است و در سند حضور فعال دارد و هنوز به سند اضافه نشده است
  REFER_ACTIVE,
  REFER_DEACTIVE,
}

class VoucherDetail {
  int index;
  VoucherDetailStatus status;
  int voucherDetailID;
  int voucherHeaderID;
  double quantity;
  double
      quantityReserved; // برای زمانی که کالاهای سند مرجع دار با بارکد اضافه میشوند
  double rowPrice;
  double fee;
  double feeAgreement;
  Goods goods;
  double remainQ;
  double invRemain;
  GoodsUnit goodsUnit;
  List<TSerial> tSerials;
  int dReferID;
  int baseDReferID;
  String detailXDesc;
  String customField1;
  String customField2;
  String customField3;
  String customField4;
  String customField5;
  String customField6;
  String customField7;
  String customField8;
  String customField9;
  String customField10;

  String customField11;
  String customField12;
  String customField13;
  String customField14;
  String customField15;
  String customField16;
  String customField17;
  String customField18;
  String customField19;
  String customField20;

  String customField21;
  String customField22;
  String customField23;
  String customField24;
  String customField25;

  VoucherDetail(
    this.index, {
    this.status,
    this.voucherDetailID,
    this.voucherHeaderID,
    this.quantity,
    this.rowPrice,
    this.fee,
    this.feeAgreement,
    this.goods,
    this.goodsUnit,
    this.remainQ,
    this.invRemain,
    this.tSerials,
    this.dReferID,
    this.baseDReferID,
    this.detailXDesc,
    this.customField1,
    this.customField2,
    this.customField3,
    this.customField4,
    this.customField5,
    this.customField6,
    this.customField7,
    this.customField8,
    this.customField9,
    this.customField10,
    this.customField11,
    this.customField12,
    this.customField13,
    this.customField14,
    this.customField15,
    this.customField16,
    this.customField17,
    this.customField18,
    this.customField19,
    this.customField20,
    this.customField21,
    this.customField22,
    this.customField23,
    this.customField24,
    this.customField25,
  });

  static Map _toMapSale(VoucherDetail voucherDetail) {
    return {
      "goodsID": voucherDetail.goods.goodsID,
      "secUnitID": voucherDetail.goodsUnit.secUnitID,
      "quantity": voucherDetail.quantity,
      "remainQ": voucherDetail.remainQ,
      "invRemain": voucherDetail.invRemain,
      "rowPrice": voucherDetail.rowPrice,
      "fee": voucherDetail.fee,
      "feeAgreement": voucherDetail.feeAgreement,
      "detailXDesc": voucherDetail.detailXDesc,
      "dReferID": voucherDetail.dReferID,
      "baseDReferID": voucherDetail.baseDReferID,
      "storeID": voucherDetail.goods.storeID,
      "isQuantitative": voucherDetail.goodsUnit.isQuantitative,
      "isQuantitativeDesc": voucherDetail.goodsUnit.isQuantitativeDesc,
      "customField1": voucherDetail.customField1,
      "customField2": voucherDetail.customField2,
      "customField3": voucherDetail.customField3,
      "customField4": voucherDetail.customField4,
      "customField5": voucherDetail.customField5,
      "customField6": voucherDetail.customField6,
      "customField7": voucherDetail.customField7,
      "customField8": voucherDetail.customField8,
      "customField9": voucherDetail.customField9,
      "customField10": voucherDetail.customField10,
      "customField11": voucherDetail.customField11,
      "customField12": voucherDetail.customField12,
      "customField13": voucherDetail.customField13,
      "customField14": voucherDetail.customField14,
      "customField15": voucherDetail.customField15,
      "customField16": voucherDetail.customField16,
      "customField17": voucherDetail.customField17,
      "customField18": voucherDetail.customField18,
      "customField19": voucherDetail.customField19,
      "customField20": voucherDetail.customField20,
      "customField21": voucherDetail.customField21,
      "customField22": voucherDetail.customField22,
      "customField23": voucherDetail.customField23,
      "customField24": voucherDetail.customField24,
      "customField25": voucherDetail.customField25,
    };
  }

  static Map _toMapInv(VoucherDetail voucherDetail) {
    if (voucherDetail.tSerials == null) {
      return {
        "goodsID": voucherDetail.goods.goodsID,
        "secUnitID": voucherDetail.goodsUnit.secUnitID,
        "quantity": voucherDetail.quantity,
        "remainQ": voucherDetail.remainQ,
        "invRemain": voucherDetail.invRemain,
        "dReferID": voucherDetail.voucherDetailID,
        "baseDReferID": voucherDetail.baseDReferID,
        "isQuantitative": voucherDetail.goodsUnit.isQuantitative,
        "isQuantitativeDesc": voucherDetail.goodsUnit.isQuantitativeDesc
      };
    } else {
      return {
        "goodsID": voucherDetail.goods.goodsID,
        "secUnitID": voucherDetail.goodsUnit.secUnitID,
        "quantity": voucherDetail.quantity,
        "remainQ": voucherDetail.remainQ,
        "invRemain": voucherDetail.invRemain,
        "dReferID": voucherDetail.voucherDetailID,
        "baseDReferID": voucherDetail.baseDReferID,
        "isQuantitative": voucherDetail.goodsUnit.isQuantitative,
        "isQuantitativeDesc": voucherDetail.goodsUnit.isQuantitativeDesc,
        "tSerials": voucherDetail.tSerials.map((tSerial) {
          return TSerial.toMap(tSerial);
        }).toList()
      };
    }
  }

  static VoucherDetail fromMap1(Map map, int index) {
    return _fromMap(map, index);
  }

  VoucherDetail fromMap(Map map) {
    return _fromMap(map, -1);
  }

  static VoucherDetail _fromMap(Map map, int index) {
    return VoucherDetail(
      index,
      voucherHeaderID: map["voucherHeaderID"],
      voucherDetailID: map["voucherDetailID"],
      goodsUnit: GoodsUnit(
        unitID: map["unitID"],
        computeValue: map["computeValue"],
        unitDesc: map["unitName"],
        defaultSecUnitID: map["secUnitID"],
        secUnitID: map["secUnitID"],
        isQuantitative: map["isQuantitative"],
        isQuantitativeDesc: map["isQuantitativeDesc"],
      ),
      rowPrice: map["rowPrice"],
      fee: map["fee"],
      feeAgreement: map["feeAgreement"],
      remainQ: map["remainQ"] == null
          ? null
          : double.parse(map["remainQ"].toString()),
      invRemain: map["invRemain"] == null
          ? null
          : double.parse(map["invRemain"].toString()),
      quantity: map["quantity"],
      goods: Goods(
        imageUrls: map['imageUrls'] == null
            ? null
            : List<String>.generate(map['imageUrls'].length, (index) {
                return map['imageUrls'][index];
              }),
        barCode: map["barCode"],
        price: map["fee"],
        goodsDesc: map["goodsDesc"],
        goodsID: map["goodsID"],
        goodsCode: map["goodsCode"],
        goodsUnit: GoodsUnit(
          unitDesc: map["defUnitName"],
          computeValue: map["computeValue"],
          isQuantitative: map["isQuantitative"],
          isQuantitativeDesc: map["isQuantitativeDesc"],
        ),
        storeID: map["storeID"],
        storeName: map["storeName"],
        serialType: map["serialType"],
        serialDesc: map["serialDesc"],
      ),
      status: VoucherDetailStatus.DEFAULT_ACTIVE,
      tSerials: null,
      dReferID: map["dreferID"],
      baseDReferID: map["baseDReferID"],
      customField1: map["customField1"],
      customField2: map["customField2"],
      customField3: map["customField3"],
      customField4: map["customField4"],
      customField5: map["customField5"],
      customField6: map["customField6"],
      customField7: map["customField7"],
      customField8: map["customField8"],
      customField9: map["customField9"],
      customField10: map["customField10"],
      customField11: map["customField11"],
      customField12: map["customField12"],
      customField13: map["customField13"],
      customField14: map["customField14"],
      customField15: map["customField15"],
      customField16: map["customField16"],
      customField17: map["customField17"],
      customField18: map["customField18"],
      customField19: map["customField19"],
      customField20: map["customField20"],
      customField21: map["customField21"],
      customField22: map["customField22"],
      customField23: map["customField23"],
      customField24: map["customField24"],
      customField25: map["customField25"],
    );
  }

  static List<Map> toListMapSale(List<VoucherDetail> voucherDetails) {
    return voucherDetails.map((voucherDetail) {
      return _toMapSale(voucherDetail);
    }).toList();
  }

  static List<Map> toListMapInv(List<VoucherDetail> voucherDetails) {
    return voucherDetails.map((voucherDetail) {
      return _toMapInv(voucherDetail);
    }).toList();
  }
}

class TSerial {
  String serialGoodsDesc;
  double serialQuantity;
  int refSerialGoodsID;
  int serialGoodsID;
  int baseSerialGoodsID;
  String field1;
  String field2;
  String field3;
  String field4;
  String field5;
  String field6;
  String field7;
  String field8;
  String field9;
  String field10;
  String rowID;
  String iGUID;
  int vDID;

  TSerial({
    this.serialGoodsDesc,
    this.serialQuantity,
    this.refSerialGoodsID,
    this.serialGoodsID,
    this.baseSerialGoodsID,
    this.field1,
    this.field2,
    this.field3,
    this.field4,
    this.field5,
    this.field6,
    this.field7,
    this.field8,
    this.field9,
    this.field10,
    this.rowID,
    this.iGUID,
    this.vDID,
  });

  int get id {
    return serialGoodsID;
  }

  String get desc {
    return serialGoodsDesc;
  }

  TSerial fromMap(Map map) {
    return TSerial(
      serialGoodsDesc: map['serialGoodsDesc'],
      serialQuantity: map['serialQuantity'],
      refSerialGoodsID: map['refSerialGoodsID'],
      serialGoodsID: map['serialGoodsID'],
      baseSerialGoodsID: map['baseSerialGoodsID'],
      field1: map['field1'],
      field2: map['field2'],
      field3: map['field3'],
      field4: map['field4'],
      field5: map['field5'],
      field6: map['field6'],
      field7: map['field7'],
      field8: map['field8'],
      field9: map['field9'],
      field10: map['field10'],
      rowID: map['rowID'],
      iGUID: map['iGUID'],
      vDID: map['vDID'],
    );
  }

  static Map toMap(TSerial tSerial) {
    return {
      "serialGoodsDesc": tSerial.serialGoodsDesc,
      "serialQuantity": tSerial.serialQuantity,
      "refSerialGoodsID": tSerial.baseSerialGoodsID,
      "serialGoodsID": 0,
      "baseSerialGoodsID": tSerial.baseSerialGoodsID,
      "field1": tSerial.field1,
      "field2": tSerial.field2,
      "field3": tSerial.field3,
      "field4": tSerial.field4,
      "field5": tSerial.field5,
      "field6": tSerial.field6,
      "field7": tSerial.field7,
      "field8": tSerial.field8,
      "field9": tSerial.field9,
      "field10": tSerial.field10,
      "rowID": tSerial.rowID,
      // "IGUID": tSerial.iGUID,
      "vDID": tSerial.vDID,
    };
  }
}
