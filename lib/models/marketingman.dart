class MarketingMan {
  int marketingManID;
  String marketingManName;

  MarketingMan({
     this.marketingManID,
     this.marketingManName,
  });

  int get id {
    return marketingManID;
  }

  String get desc {
    return marketingManName;
  }

  MarketingMan fromMap(Map jsonObject) {
    return MarketingMan(
      marketingManID: jsonObject['MARKETINGMANID'],
      marketingManName: jsonObject['MARKETINGMANNAME'],
    );
  }
}
