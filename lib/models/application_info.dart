class ApplicationInfo {
  final String companyName;
  final List<String> apps;

  ApplicationInfo({
     this.companyName,
     this.apps,
  });

  ApplicationInfo fromMap(Map map) {
    return ApplicationInfo(
        companyName: map["companyName"],
        apps:
            List<String>.generate(map["apps"].length, (index) {
          return map["apps"][index];
        }));
  }
}
