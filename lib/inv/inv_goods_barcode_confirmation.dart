// import 'package:audioplayers/audio_cache.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher_detail.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class InvGoodsBarcodeConfirmation extends StatefulWidget {
  final VoucherDetail voucherDetail;

  const InvGoodsBarcodeConfirmation({this.voucherDetail});
  @override
  _InvGoodsBarcodeConfirmationState createState() =>
      _InvGoodsBarcodeConfirmationState();
}

class _InvGoodsBarcodeConfirmationState
    extends State<InvGoodsBarcodeConfirmation> {
  Setting setting;
  int scanned = 0;
  // AudioCache audioCache;
  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    // audioCache = AudioCache();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: setting.currentSystem.themeData,
      child: Scaffold(
        appBar: AppBar(
          title: ListTile(
            subtitle: Text(widget.voucherDetail.goods.goodsDesc,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.right,
                style: TextStyle(
                  fontFamily: 'IRANSansUltraLight',
                  fontSize: 10.0,
                  color: Colors.white,
                )),
            title: Text("تایید رکورد با اسکن بارکد",
                textAlign: TextAlign.right,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                  color: Colors.white,
                )),
          ),
        ),
        body: Container(
            padding: EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                ButtonBar(
                  alignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                        widget.voucherDetail.quantity.toString() +
                            " " +
                            widget.voucherDetail.goodsUnit.unitDesc,
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                          fontFamily: 'IRANSans',
                          fontSize: 18.0,
                        )),
                    Text("تعداد",
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          fontFamily: 'IRANSans',
                          fontSize: 18.0,
                        )),
                  ],
                ),
                ButtonBar(
                  alignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(widget.voucherDetail.goodsUnit.isQuantitativeDesc,
                        style: TextStyle(
                          fontFamily: 'IRANSans',
                          fontSize: 18.0,
                        )),
                    Text("ماهیت شمارش",
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          fontFamily: 'IRANSans',
                          fontSize: 18.0,
                        )),
                  ],
                ),
                Padding(padding: EdgeInsets.only(top: 40)),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: setting.currentSystem.themeData.primaryColor,
                        padding: EdgeInsets.all(20)),
                    child: Icon(
                      FontAwesomeIcons.qrcode,
                      size: 80,
                    ),
                    onPressed: !returnButtonEnability()
                        ? null
                        : () {
                            BarcodeScanner.scan().then((value) {
                              if (widget.voucherDetail.goods.barCode != null &&
                                  widget.voucherDetail.goods.barCode == value) {
                                // audioCache.play("sounds/beep-07.mp3",
                                //     volume: 1.0);
                                scanned++;
                              } else {
                                // audioCache.play("sounds/beep-09.mp3",
                                //     volume: 1.0);
                              }
                              setState(() {});
                            }).catchError((err) {});
                          }),
                Padding(padding: EdgeInsets.only(top: 40)),
                ButtonBar(
                  alignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("اسکن نشده : " + notScannedNumber().toString(),
                        style: TextStyle(
                          fontFamily: 'IRANSans',
                          fontSize: 16.0,
                        )),
                    Text("اسکن شده : " + scanned.toString(),
                        style: TextStyle(
                          fontFamily: 'IRANSans',
                          fontSize: 16.0,
                        )),
                  ],
                ),
                ElevatedButton(
                    child: Text("بازگشت",
                        style: TextStyle(
                          fontFamily: 'IRANSans',
                          fontSize: 14.0,
                        )),
                    onPressed: returnButtonEnability()
                        ? null
                        : () {
                            Navigator.pop(context);
                          })
              ],
            )),
      ),
    );
  }

  bool returnButtonEnability() {
    if (widget.voucherDetail.goodsUnit.isQuantitative) {
      if (widget.voucherDetail.quantity.toInt() == scanned) {
        return false;
      }
      return true;
    } else {
      if (scanned == 1) {
        return false;
      }
      return true;
    }
  }

  int notScannedNumber() {
    if (widget.voucherDetail.goodsUnit.isQuantitative) {
      return widget.voucherDetail.quantity.toInt() - scanned;
    } else {
      if (scanned == 0) {
        return 1;
      }

      return 0;
    }
  }
}
