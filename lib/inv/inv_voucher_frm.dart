// import 'package:audioplayers/audio_cache.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:com_tarazgroup/inv/inv_goods.dart';
import 'package:com_tarazgroup/models/goods_group.dart';
import 'package:com_tarazgroup/models/query_param.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/store.dart';
import 'package:com_tarazgroup/models/tafsili.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher.dart';
import 'package:com_tarazgroup/models/voucher_detail.dart';
import 'package:com_tarazgroup/models/voucher_type.dart';
import 'package:com_tarazgroup/modules/dialog/alert_dialog.dart';
import 'package:com_tarazgroup/modules/dialog/confirm_dialog.dart';
import 'package:com_tarazgroup/modules/future/webservice.dart';
import 'package:com_tarazgroup/modules/qkey/qkey_filter.dart';
import 'package:com_tarazgroup/modules/rest/restful.dart';
import 'package:com_tarazgroup/modules/textfield/ttextfield.dart';
import 'package:com_tarazgroup/modules/tlib/tlib.dart';
import 'package:com_tarazgroup/modules/view/list_tile_view.dart';
import 'package:com_tarazgroup/modules/voucher/voucher_frm.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'inv_voucher_header_refer.dart';

//وارده
class InvVoucherFrm extends StatefulWidget {
  final int initialIndex;
  final int
      voucherType; // voucherType==1 -> وارده  voucherType==2 -> صادره   voucherType==3 -> انبار به انبار

  const InvVoucherFrm({
    @required this.initialIndex,
    @required this.voucherType,
  });
  @override
  _InvVoucherFrmState createState() => _InvVoucherFrmState();
}

class _InvVoucherFrmState extends State<InvVoucherFrm> {
  Setting setting;
  Voucher voucher;
  // AudioCache audioCache;
  //Filter
  String voucherTypeDesc;
  String voucherTypeDescRefer;
  String storeName;
  String tafsiliDesc;

  String goodsDesc;

  GoodsGroup goodsGroup = GoodsGroup();
  String groupDesc;

  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    // audioCache = AudioCache();
    super.initState();
  }

  String _title() {
    if (widget.voucherType == 1) {
      return "وارده";
    } else if (widget.voucherType == 2) {
      return "صادره";
    } else if (widget.voucherType == 3) {
      return "انبار به انبار";
    }
    return "";
  }

  @override
  Widget build(BuildContext context) {
    voucher = TIW.getVoucher(context);
    return VoucherFrm(
      voucherType: widget.voucherType,
      routeName: '/invHome',
      initialIndex: widget.initialIndex,
      title: _title(),
      onSend: () async {
        bool yesNo = true;
        bool isUnlockRefByBarcode =
            voucher.voucherHeader.voucherType.isUnlockRefByBarcode;
        if (isUnlockRefByBarcode != null && isUnlockRefByBarcode) {
          List<VoucherDetail> vd =
              voucher.voucherDetails.where((voucherDetail) {
            return voucherDetail.quantity > 0;
          }).toList();
          if (vd.length == 0) {
            TAlertDialog(context: context).show(
                title: "عدم اسکن کالا", content: "هیچ کالایی اسکن نشده است!");
            return;
          } else if (vd.length < voucher.voucherDetails.length) {
            yesNo = await TConfirmDialog(
              context: context,
            ).show("اسکن نشدن برخی کالا ها",
                "برخی از کالا ها اسکن نشده اند و در نتیجه از سند حذف می شوند . آیا ادامه می دهید؟");
            if (yesNo) {
              voucher.voucherDetails.retainWhere((voucherDetail) {
                return voucherDetail.quantity > 0;
              });
            } else {
              return;
            }
          }
        }
        insertVoucher();
      },
      onDelete: () {
        Navigator.popUntil(
          context,
          ModalRoute.withName('/invHome'),
        );
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => InvVoucherFrm(
                    initialIndex: 0,
                    voucherType: widget.voucherType,
                  )),
        );
      },
      defaultTab: <Widget>[
        QKeyFilter<VoucherType>(
          onValue: () {
            return voucher.voucherHeader.voucherType;
          },
          title: "نوع سند",
          resource: "pub/vouchertypes",
          // recCnt: 50,
          queryParams: <QueryParam>[
            QueryParam(
                queryParamType: QueryParamType.NUMBER,
                name: "systemID",
                value: setting.currentSystem.systemID.toString()),
            QueryParam(
                queryParamType: QueryParamType.NUMBER,
                name: "voucherType",
                value: widget.voucherType.toString())
          ],
          itemBuilder: (item, index) {
            return ListTileView.view1_1(
              index,
              item.voucherTypeDesc,
            );
          },
          onChangePermission: () async {
            if (voucher.voucherDetails.length > 0) {
              bool yesNo = await TConfirmDialog(
                context: context,
              ).show("حذف کالاهای فعلی سند",
                  "با تغییر نوع سند ، کالاهای فعلی سند حذف خواهند شد . آیا ادامه می دهید؟");
              if (yesNo) {
                return true;
              } else {
                return false;
              }
            }
            return true;
          },
          onSelect: (voucherType) {
            voucher.voucherHeader.voucherType = voucherType;
            voucher.voucherHeader.voucherTypeRefer = VoucherType();
            voucher.voucherHeader.referNumber = null;
            voucher.voucherHeader.referDate = null;
            voucher.voucherDetails = [];
            TIW.setVoucher(context, voucher);
            Navigator.popUntil(
              context,
              ModalRoute.withName('/invHome'),
            );
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => InvVoucherFrm(
                        initialIndex: 0,
                        voucherType: widget.voucherType,
                      )),
            );
          },
          filterWidgets: <Widget>[
            TTextField(
              title: "نوع سند",
              onValue: () {
                return voucherTypeDesc;
              },
              onChanged: (txt) {
                voucherTypeDesc = txt;
              },
            ),
          ],
          onFiltered: () {
            List<QueryParam> queryParams = [];
            if (this.voucherTypeDesc != null && this.voucherTypeDesc != "") {
              queryParams.add(QueryParam(
                  queryParamType: QueryParamType.TEXT,
                  name: "voucherTypeDesc",
                  value: this.voucherTypeDesc));
            }
            return queryParams;
          },
          onDeleteFilter: () {
            voucherTypeDesc = null;
          },
        ),
        QKeyFilter<Store>(
          onValue: () {
            return voucher.voucherHeader.store;
          },
          title: "انبار",
          resource: "inv/stores",
          // recCnt: 15,
          queryParams: <QueryParam>[],
          itemBuilder: (item, index) {
            return ListTileView.view1_1(
              index,
              item.storeName,
            );
          },
          onChangePermission: () async {
            // قبلا اجازه نمیدادیم که وقتی مرجع انتخاب شده کاربر بتواند نام انبار را تغییر دهد
            // اما این امکان موقتا برداشته شده است
            // به صورت اصولی باید این اجازه از دیتابیس خوانده شود و نام اس پی مربوطه
            // از آقای باغین زاده پرسیده شود

            // if (voucher.voucherDetails.length > 0) {
            //   bool yesNo = await TConfirmDialog(
            //     context: context,
            //   ).show("حذف کالاهای فعلی سند",
            //       "با تغییر انبار ، کالاهای فعلی سند حذف خواهند شد . آیا ادامه می دهید؟");
            //   if (yesNo) {
            //     return true;
            //   } else {
            //     return false;
            //   }
            // }
            return true;
          },
          onSelect: (store) {
            // قبلا اجازه نمیدادیم که وقتی مرجع انتخاب شده کاربر بتواند نام انبار را تغییر دهد
            // اما این امکان موقتا برداشته شده است
            // به صورت اصولی باید این اجازه از دیتابیس خوانده شود و نام اس پی مربوطه
            // از آقای باغین زاده پرسیده شود

            voucher.voucherHeader.store = store;
            // voucher.voucherHeader.voucherTypeRefer = VoucherType();
            // voucher.voucherHeader.referNumber = null;
            // voucher.voucherHeader.referDate = null;
            // voucher.voucherDetails = [];
            TIW.setVoucher(context, voucher);
            Navigator.popUntil(
              context,
              ModalRoute.withName('/invHome'),
            );
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => InvVoucherFrm(
                        initialIndex: 0,
                        voucherType: widget.voucherType,
                      )),
            );
          },
          filterWidgets: <Widget>[
            TTextField(
              title: "انبار",
              onValue: () {
                return storeName;
              },
              onChanged: (txt) {
                storeName = txt;
              },
            ),
          ],
          onFiltered: () {
            List<QueryParam> queryParams = [];
            if (this.storeName != null && this.storeName != "") {
              queryParams.add(QueryParam(
                  queryParamType: QueryParamType.TEXT,
                  name: "storeName",
                  value: this.storeName));
            }
            return queryParams;
          },
          onDeleteFilter: () {
            storeName = null;
          },
        ),
        QKeyFilter<Tafsili>(
          onValue: () {
            return voucher.voucherHeader.tafsili;
          },
          title: "طرف مقابل",
          resource: "acc/tafsilidescs1",
          // recCnt: 15,
          queryParams: <QueryParam>[
            QueryParam(
              queryParamType: QueryParamType.NUMBER,
              name: "voucherTypeID",
              value: voucher.voucherHeader.voucherType.voucherTypeID.toString(),
            )
          ],
          itemBuilder: (item, index) {
            return ListTileView.view1_1(
              index,
              item.tafsiliDesc,
            );
          },
          onChangePermission: () async {
            if (voucher.voucherHeader.voucherType.voucherTypeID == null) {
              TAlertDialog(context: context).show(
                  title: "نوع سند انتخاب نشده است",
                  content: "برای انتخاب طرف مقابل ، نوع سند را انتخاب کنید");
              return false;
            }
            // قبلا اجازه نمیدادیم که وقتی مرجع انتخاب شده کاربر بتواند طرف مقابل را تغییر دهد
            // اما این امکان موقتا برداشته شده است
            // به صورت اصولی باید این اجازه از دیتابیس خوانده شود و نام اس پی مربوطه
            // از آقای باغین زاده پرسیده شود

            // if (voucher.voucherDetails.length > 0) {
            //   bool yesNo = await TConfirmDialog(
            //     context: context,
            //   ).show("حذف کالاهای فعلی سند",
            //       "با تغییر طرف مقابل ، کالاهای فعلی سند حذف خواهند شد . آیا ادامه می دهید؟");
            //   if (yesNo) {
            //     return true;
            //   } else {
            //     return false;
            //   }
            // }
            return true;
          },
          onSelect: (tafsili) {
            // قبلا اجازه نمیدادیم که وقتی مرجع انتخاب شده کاربر بتواند طرف مقابل را تغییر دهد
            // اما این امکان موقتا برداشته شده است
            // به صورت اصولی باید این اجازه از دیتابیس خوانده شود و نام اس پی مربوطه
            // از آقای باغین زاده پرسیده شود

            voucher.voucherHeader.tafsili = tafsili;
            // voucher.voucherHeader.voucherTypeRefer = VoucherType();
            // voucher.voucherHeader.referNumber = null;
            // voucher.voucherHeader.referDate = null;
            // voucher.voucherDetails = [];
            TIW.setVoucher(context, voucher);
            Navigator.popUntil(
              context,
              ModalRoute.withName('/invHome'),
            );
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => InvVoucherFrm(
                        initialIndex: 0,
                        voucherType: widget.voucherType,
                      )),
            );
          },
          filterWidgets: <Widget>[
            TTextField(
              title: "طرف مقابل",
              onValue: () {
                return tafsiliDesc;
              },
              onChanged: (txt) {
                tafsiliDesc = txt;
              },
            ),
          ],
          onFiltered: () {
            List<QueryParam> queryParams = [];
            if (this.tafsiliDesc != null && this.tafsiliDesc != "") {
              queryParams.add(QueryParam(
                  queryParamType: QueryParamType.TEXT,
                  name: "tafsiliDesc",
                  value: this.tafsiliDesc));
            }
            return queryParams;
          },
          onDeleteFilter: () {
            tafsiliDesc = null;
          },
        )
      ],
      referTab: <Widget>[
        QKeyFilter<VoucherType>(
          onValue: () {
            return voucher.voucherHeader.voucherTypeRefer;
          },
          title: "نوع مرجع",
          resource: "pub/vouchertypes/" +
              voucher.voucherHeader.voucherType.voucherTypeID.toString() +
              "/refers",
          // recCnt: 50,
          queryParams: <QueryParam>[
            // QueryParam(
            //     queryParamType: QueryParamType.NUMBER,
            //     name: "systemID",
            //     value: setting.currentSystem.systemID.toString()),
          ],
          itemBuilder: (item, index) {
            return ListTileView.view1_1(
              index,
              item.voucherTypeDesc,
            );
          },
          onChangePermission: () async {
            if (voucher.voucherHeader.voucherType.voucherTypeID == null) {
              TAlertDialog(context: context).show(
                  title: "نوع سند انتخاب نشده است",
                  content: "برای انتخاب نوع مرجع ، نوع سند را انتخاب کنید");
              return false;
            }
            if (voucher.voucherDetails.length > 0) {
              bool yesNo = await TConfirmDialog(
                context: context,
              ).show("حذف کالاهای فعلی سند",
                  "با تغییر نوع مرجع ، کالاهای فعلی سند حذف خواهند شد . آیا ادامه می دهید؟");
              if (yesNo) {
                return true;
              } else {
                return false;
              }
            }
            return true;
          },
          onSelect: (voucherType) {
            voucher.voucherHeader.voucherTypeRefer = voucherType;
            voucher.voucherHeader.store = Store();
            voucher.voucherHeader.tafsili = Tafsili();
            voucher.voucherHeader.referNumber = null;
            voucher.voucherHeader.referDate = null;
            voucher.voucherDetails = [];
            TIW.setVoucher(context, voucher);
            Navigator.popUntil(
              context,
              ModalRoute.withName('/invHome'),
            );
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => InvVoucherFrm(
                        initialIndex: 1,
                        voucherType: widget.voucherType,
                      )),
            );
          },
          filterWidgets: <Widget>[
            TTextField(
              title: "نوع مرجع",
              onValue: () {
                return voucherTypeDescRefer;
              },
              onChanged: (txt) {
                voucherTypeDescRefer = txt;
              },
            ),
          ],
          onFiltered: () {
            List<QueryParam> queryParams = [];
            if (this.voucherTypeDescRefer != null &&
                this.voucherTypeDescRefer != "") {
              queryParams.add(QueryParam(
                  queryParamType: QueryParamType.TEXT,
                  name: "voucherTypeDesc",
                  value: this.voucherTypeDescRefer));
            }
            return queryParams;
          },
          onDeleteFilter: () {
            voucherTypeDescRefer = null;
          },
        ),
        SizedBox(
          height: 50,
          child: ListTile(
            isThreeLine: true,
            dense: true,
            title: Text(
              "شماره مرجع",
              textAlign: TextAlign.right,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 11.0,
                color: Colors.black,
              ),
            ),
            subtitle: Text(
              voucher.voucherHeader.referNumber == null
                  ? ""
                  : voucher.voucherHeader.referNumber.toString(),
              textAlign: TextAlign.right,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 10.0,
                color: Colors.black,
              ),
            ),
          ),
        ),
        SizedBox(
          height: 50,
          child: ListTile(
            isThreeLine: true,
            dense: true,
            title: Text(
              "تاریخ مرجع",
              textAlign: TextAlign.right,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 11.0,
                color: Colors.black,
              ),
            ),
            subtitle: Text(
              voucher.voucherHeader.referDate == null
                  ? ""
                  : voucher.voucherHeader.referDate.toString(),
              textAlign: TextAlign.right,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 10.0,
                color: Colors.black,
              ),
            ),
          ),
        ),
      ],
      otherTab: <Widget>[
        TTextField(
          title: "توضیحات",
          maxLines: 3,
          onValue: () {
            return voucher.voucherHeader.voucherDesc;
          },
          onChanged: (voucherDesc) {
            voucher.voucherHeader.voucherDesc = voucherDesc;
            TIW.setVoucher(context, voucher);
          },
        ),
      ],
      sliverPersistentHeaderDelegateActions: <Widget>[
        IconButton(
          icon: Icon(Icons.link),
          tooltip: "سند های مرجع",
          onPressed: () {
            if (voucher.voucherHeader.voucherTypeRefer.systemID == 39) {
              TAlertDialog(context: context).show(
                  title: "نوع مرجع بدون مرجع است",
                  content:
                      "برای نمایش سند های مرجع ، نوع مرجع نباید بدون مرجع باشد");
            } else if (voucher.voucherHeader.voucherTypeRefer.voucherTypeID ==
                null) {
              TAlertDialog(context: context).show(
                  title: "نوع مرجع انتخاب نشده است",
                  content: "برای نمایش سند های مرجع ، نوع مرجع را انتخاب کنید");
            } else {
              showGeneralDialog(
                context: context,
                pageBuilder: (BuildContext context, Animation<double> animation,
                    Animation<double> secondaryAnimation) {
                  return InvVoucherHeaderRefer(
                    parentWidget: () {
                      return InvVoucherFrm(
                        initialIndex: 1,
                        voucherType: widget.voucherType,
                      );
                    },
                  );
                },
                barrierColor: Color.fromRGBO(255, 255, 255, 0.9),
                barrierDismissible: true,
                barrierLabel:
                    MaterialLocalizations.of(context).modalBarrierDismissLabel,
                transitionDuration: const Duration(milliseconds: 200),
              );
            }
          },
        )
      ],
      buildVoucherDetails: () {
        if (voucher.voucherHeader.voucherTypeRefer.voucherTypeID == null) {
          // یعنی سند مرجع دار نیست
          return List.generate(voucher.voucherDetails.length, (index) {
            return ListTileView.voucherDetailListTileInv(
                context: context,
                index: index,
                voucherDetail: voucher.voucherDetails[index],
                parentWidget: () {
                  return InvVoucherFrm(
                    initialIndex: 0,
                    voucherType: widget.voucherType,
                  );
                });
          });
        } else {
          // سند مرجع دار است
          // اگر متغیر زیر ترو باشد باید کالاهای سند با اسکن اضافه شود
          bool isUnlockRefByBarcode =
              voucher.voucherHeader.voucherType.isUnlockRefByBarcode;
          if (isUnlockRefByBarcode != null && isUnlockRefByBarcode) {
            return List.generate(voucher.voucherDetails.length, (index) {
              return ListTileView.voucherDetailListTileInvUnlockByBarcode(
                  context: context,
                  index: index,
                  voucherDetail: voucher.voucherDetails[index],
                  parentWidget: () {
                    return InvVoucherFrm(
                      initialIndex: 0,
                      voucherType: widget.voucherType,
                    );
                  });
            });
          } else {
            return List.generate(voucher.voucherDetails.length, (index) {
              return ListTileView.voucherDetailListTileInv(
                  context: context,
                  index: index,
                  voucherDetail: voucher.voucherDetails[index],
                  parentWidget: () {
                    return InvVoucherFrm(
                      initialIndex: 0,
                      voucherType: widget.voucherType,
                    );
                  });
            });
          }
        }
      },
      onFooter: () {
        double total = 0;
        List<VoucherDetail> voucherDetails =
            TIW.getVoucher(context).voucherDetails;
        for (VoucherDetail voucherDetail in voucherDetails) {
          total +=
              voucherDetail.quantity * voucherDetail.goodsUnit.computeValue;
        }
        return ButtonBar(
          alignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text("Σ " + TLib.commaSeparate(total.toString()),
                style: TextStyle(
                  fontFamily: 'IRANSansBlack',
                  fontSize: 16.0,
                )),
            Text(voucher.voucherDetails.length.toString() + "  #",
                style: TextStyle(
                  fontFamily: 'IRANSansBlack',
                  fontSize: 16.0,
                ))
          ],
        );
      },
      onPermissionOpenGoodsList: () {
        if (voucher.voucherHeader.voucherType.voucherTypeID == null) {
          TAlertDialog(context: context).show(
              title: "نوع سند انتخاب نشده است",
              content: "برای نمایش لیست کالاها ، نوع سند را انتخاب کنید");
          return false;
        }

        if (voucher.voucherHeader.store.storeID == null) {
          TAlertDialog(context: context).show(
              title: "انبار انتخاب نشده است",
              content: "برای نمایش لیست کالاها ، انبار را انتخاب کنید");
          return false;
        }

        if (voucher.voucherHeader.tafsili.tafsiliID == null) {
          TAlertDialog(context: context).show(
              title: "طرف مقابل انتخاب نشده است",
              content: "برای نمایش لیست کالاها ، طرف مقابل را انتخاب کنید");
          return false;
        }
        return true;
      },
      onTapGoodsList: () {
        showGoodsList(null);
      },
      onSelectGoodsGroup: (goodsGroup) {
        // در خود فرم گروه کالا وقتی برگ کلیک میشه کالاها باز میشه نیاز به کد زیر نیست
        // this.goodsGroup = goodsGroup;
        // showGoodsList(null);
      },
      onBarcodeScanned: (barcode) {
        if (voucher.voucherHeader.voucherTypeRefer.voucherTypeID != null) {
          // یعنی مرجع ردیفی است
          bool isUnlockRefByBarcode =
              voucher.voucherHeader.voucherType.isUnlockRefByBarcode;
          if (isUnlockRefByBarcode != null && isUnlockRefByBarcode) {
            // یعنی در بین کالاهای فعلی سند دنبال این بارکد بگرد
            // VoucherDetail vd = voucher.voucherDetails.firstWhere(
            //     (voucherDetail) => voucherDetail.goods.barCode == barcode,
            //     orElse: () => null);

// همه ردیف های این کالا در سند فعلی را بیرون میکشیم
            List<VoucherDetail> vds = voucher.voucherDetails
                .where(
                    (voucherDetail) => voucherDetail.goods.barCode == barcode)
                .toList();

            if (vds.length == 0) {
              // این کالای اسکن شده در بین کالاهای مرجع نیست
              showFlushBar(
                  "کالاهای این سند شامل کالای اسکن شده نیستند یا برای این کالا بارکدی در سیستم تعریف نشده است",
                  Colors.red[300],
                  5, () {
                Navigator.popUntil(
                  context,
                  ModalRoute.withName('/invHome'),
                );
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => InvVoucherFrm(
                            initialIndex: 0,
                            voucherType: widget.voucherType,
                          )),
                );
              });
            } else {
              for (var i = 0; i < vds.length; i++) {
                VoucherDetail vd = vds[i];
                double newQuantity = vd.quantity + 1;
                if (newQuantity > vd.quantityReserved) {
                  if (i == vds.length - 1) {
                    // یعنی تعداد اسکن بیشتر از مانده مرجع است
                    // و به انتهای لیست رسیدیم و همه کالا ها اسکن شده پس ارور نمایش یابد
                    showFlushBar("تعداد اسکن بیشتر از مانده مرجع است",
                        Colors.red[300], 5, () {
                      Navigator.popUntil(
                        context,
                        ModalRoute.withName('/invHome'),
                      );
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => InvVoucherFrm(
                                  initialIndex: 0,
                                  voucherType: widget.voucherType,
                                )),
                      );
                    });
                    break;
                  } else {
                    // یعنی تعداد اسکن بیشتر از مانده مرجع است
                    // ولی هنوز امید داریم در این لیست کالای اسکن نشده وجود داشته باشد پس ادامه میدهیم
                    continue;
                  }
                } else {
                  // اسکن مجاز است
                  // همه چیز اوکی است و تعداد کالا یکی بالا میرود
                  showFlushBar(
                      vd.goods.goodsDesc +
                          " به تعداد " +
                          newQuantity.toString() +
                          " بار اسکن شد",
                      Colors.green[300],
                      1, () {
                    vd.quantity = newQuantity;
                    Navigator.popUntil(
                      context,
                      ModalRoute.withName('/invHome'),
                    );
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => InvVoucherFrm(
                                initialIndex: 0,
                                voucherType: widget.voucherType,
                              )),
                    );
                  });
                  break;
                }
              }
              // if (newQuantity > vd.quantityReserved) {
              //   showFlushBar(
              //       "تعداد اسکن بیشتر از مانده مرجع است", Colors.red[300], 5,
              //       () {
              //     Navigator.popUntil(
              //       context,
              //       ModalRoute.withName('/invHome'),
              //     );
              //     Navigator.push(
              //       context,
              //       MaterialPageRoute(
              //           builder: (BuildContext context) => InvVoucherFrm(
              //                 initialIndex: 0,
              //                 voucherType: widget.voucherType,
              //               )),
              //     );
              //   });
              // } else {
              //   // همه چیز اوکی است و تعداد کالا یکی بالا میرود
              //   showFlushBar(
              //       vd.goods.goodsDesc +
              //           " به تعداد " +
              //           newQuantity.toString() +
              //           " بار اسکن شد",
              //       Colors.green[300],
              //       1, () {
              //     vd.quantity = newQuantity;
              //     Navigator.popUntil(
              //       context,
              //       ModalRoute.withName('/invHome'),
              //     );
              //     Navigator.push(
              //       context,
              //       MaterialPageRoute(
              //           builder: (BuildContext context) => InvVoucherFrm(
              //                 initialIndex: 0,
              //                 voucherType: widget.voucherType,
              //               )),
              //     );
              //   });
              // }
            }
          } else {
            showGoodsList(barcode);
          }
        } else {
          // یعنی مرجع ردیفی نیست
          showGoodsList(barcode);
        }
      },
    );
  }

  showFlushBar(
      String messageText, Color color, int time, Function onDISMISSED) {
    Flushbar(
      leftBarIndicatorColor: color,
      margin: EdgeInsets.all(8),
      // borderRadius: 8,
      messageText: Text(messageText,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'IRANSansBlack',
            color: Colors.white,
            fontSize: 14.0,
          )),
      duration: Duration(seconds: time),
    )..show(context).then((result) {
        onDISMISSED();
      });
  }

  void showGoodsList(String barcode) {
    Navigator.push(
      context,
      MaterialPageRoute(
          settings: RouteSettings(name: "/goods"),
          builder: (BuildContext context) => InvGoods(
                barcode: barcode,
                goodsGroup: null,
                storeID: voucher.voucherHeader.store.storeID,
                voucherTypeID: voucher.voucherHeader.voucherType.voucherTypeID,
                voucherType: widget.voucherType,
              )),
    );
  }

  void insertVoucher() {
    WebService.callModalMode<Voucher>(
      context,
      value: Voucher(),
      futureResponse: _futureResponseInsertVoucher,
      waitingMsg: "ذخیره سند",
      doneMsg: "ذخیره سند",
      on200object: (voucher) {
        TIW.setVoucher(context, null);
        TAlertDialog(
                barrierDismissible: false,
                context: context,
                close: () {
                  Navigator.popUntil(
                    context,
                    ModalRoute.withName('/invHome'),
                  );
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => InvVoucherFrm(
                              initialIndex: 0,
                              voucherType: widget.voucherType,
                            )),
                  );
                })
            .show(
                title: voucher.voucherHeader.voucherNumber.toString(),
                content: "سند با موفقیت ذخیره شد!");
      },
    );
  }

  Future<Response> _futureResponseInsertVoucher() {
    Voucher voucher = TIW.getVoucher(context);
    return TRestful.post(
      "inv/vouchers",
      setting: setting,
      body: Voucher.jsonEncodeInv(voucher),
      timeout: 200,
    );
  }
}
