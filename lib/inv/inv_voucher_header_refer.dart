import 'package:com_tarazgroup/models/query_param.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher.dart';
import 'package:com_tarazgroup/models/voucher_detail.dart';
import 'package:com_tarazgroup/models/voucher_header.dart';
import 'package:com_tarazgroup/models/voucher_type.dart';
import 'package:com_tarazgroup/modules/future/webservice.dart';
import 'package:com_tarazgroup/modules/multipleview/multiple_view.dart';
import 'package:com_tarazgroup/modules/rest/restful.dart';
import 'package:com_tarazgroup/modules/tableview/tcolumn.dart';
import 'package:com_tarazgroup/modules/textfield/ttextfield.dart';
import 'package:com_tarazgroup/modules/textfield/ttextfield_date.dart';
import 'package:com_tarazgroup/modules/tlib/tlib.dart';
import 'package:flutter/material.dart';

class InvVoucherHeaderRefer extends StatefulWidget {
  final Widget Function() parentWidget;

  const InvVoucherHeaderRefer({
    @required this.parentWidget,
  }); //یعنی وقتی کار افزودن سند مرجع به سند تمام شد به  کدام صفحه بر گردیم
  @override
  _InvVoucherHeaderReferState createState() => _InvVoucherHeaderReferState();
}

class _InvVoucherHeaderReferState extends State<InvVoucherHeaderRefer> {
  Setting setting;
  Voucher voucher;
  String tafsiliDesc;
  String voucherNumber;
  String voucherDate;

  void deleteFilters() {
    tafsiliDesc = null;
    voucherNumber = null;
    voucherDate = null;
  }

  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    voucher = TIW.getVoucher(context, initState: true);
    deleteFilters();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultipleView<VoucherHeader>(
      initialIndex: 0,
      title: "اسناد مرجع",
      subtitle: voucher.voucherHeader.voucherTypeRefer.voucherTypeDesc,
      value: VoucherHeader(
          voucherNumber: null,
          tafsili: null,
          customer: null,
          provider: null,
          store: null,
          voucherType: null,
          voucherTypeRefer: null,
          voucherDesc: null,
          voucherDate: null,
          deliverCenterID: null,
          saleTypeID: null,
          marketingManID: null,
          saleCenterID: null,
          salesManID: null,
          isManualPromotion: null,
          center1ID: null,
          center2ID: null,
          center3ID: null,
          paymentWayID: null,
          voucherHeaderID: null,
          saleType: null
          ),
      filterWidgets: <Widget>[
        TTextField(
          title: "شماره سند",
          keyboardType: TextInputType.number,
          onValue: () {
            return voucherNumber;
          },
          onChanged: (txt) {
            voucherNumber = txt;
          },
        ),
        TTextFieldDate(
          title: "تاریخ سند",
          onValue: () {
            return voucherDate;
          },
          onChanged: (txt) {
            voucherDate = txt;
          },
        ),
        TTextField(
          title: "طرف مقابل",
          keyboardType: TextInputType.text,
          onValue: () {
            return tafsiliDesc;
          },
          onChanged: (txt) {
            tafsiliDesc = txt;
          },
        ),
      ],
      onFiltered: () {
        List<QueryParam> queryParams = [];
        if (this.tafsiliDesc != null && this.tafsiliDesc != "") {
          queryParams.add(QueryParam(
            queryParamType: QueryParamType.TEXT,
            name: "tafsiliDesc",
            value: this.tafsiliDesc,
          ));
        }
        if (this.voucherDate != null && this.voucherDate != "") {
          queryParams.add(QueryParam(
            queryParamType: QueryParamType.TEXT,
            name: "voucherDate",
            value: this.voucherDate,
          ));
        }
        if (this.voucherNumber != null && this.voucherNumber != "") {
          queryParams.add(QueryParam(
            queryParamType: QueryParamType.NUMBER,
            name: "voucherNumber",
            value: this.voucherNumber,
          ));
        }
        return queryParams;
      },
      onDeleteFilter: () {
        deleteFilters();
      },
      on204: () {},
      onTabChanged: (tabIndex) {
        deleteFilters();
      },
      queryParams: <QueryParam>[
        QueryParam(
          queryParamType: QueryParamType.NUMBER,
          name: "voucherTypeID",
          value: voucher.voucherHeader.voucherType.voucherTypeID.toString(),
        ),
        QueryParam(
          queryParamType: QueryParamType.NUMBER,
          name: "refTypeID",
          value:
              voucher.voucherHeader.voucherTypeRefer.voucherTypeID.toString(),
        ),
        QueryParam(
          queryParamType: QueryParamType.NUMBER,
          name: "yearID",
          value: setting.activeYearID.toString(),
        )
      ],
      resource: _resourceHeader(voucher.voucherHeader.voucherTypeRefer),
      listViewItemBuilder: null,
      detailSectionBuilder: null,
      mainSectionBuilder: null,
      tableViewCellBuilder: (tColumn, voucherHeader) {
        switch (tColumn.name) {
          case "VOUCHERNUMBER":
            return Text(
                TLib.commaSeparate(voucherHeader.voucherNumber.toString()),
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));
            break;
          case "VOUCHERTYPEDESC":
            return Text(voucherHeader.voucherType.voucherTypeDesc,
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));
            break;
          case "STORENAME":
            return Text(voucherHeader.store.storeName ?? "",
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));

            break;
          case "VOUCHERDATE":
            return Text(voucherHeader.voucherDate,
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                textDirection: TextDirection.ltr,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));

            break;
          case "TAFSILIDESC":
            return Text(voucherHeader.tafsili.tafsiliDesc,
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));

            break;
          case "VOUCHERDESC":
            return Text(voucherHeader.voucherDesc ?? "",
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));

            break;
          default:
            return Text("");
        }
      },
      tableViewColumns: <TColumn>[
        TColumn(
          name: "VOUCHERNUMBER",
          title: "شماره سند",
          width: 100,
        ),
        TColumn(
          name: "VOUCHERTYPEDESC",
          title: "نوع سند",
          width: 200,
        ),
        TColumn(
          name: "STORENAME",
          title: "انبار",
          width: 200,
        ),
        TColumn(
          name: "VOUCHERDATE",
          title: "تاریخ",
          width: 80,
        ),
        TColumn(
          name: "TAFSILIDESC",
          title: "طرف مقابل",
          width: 300,
        ),
        TColumn(
          name: "VOUCHERDESC",
          title: "توضیحات",
          width: 500,
        ),
      ],
      onTap: (voucherHeaderRefer) {
        voucher.voucherHeader.voucherHeaderID =
            voucherHeaderRefer.voucherHeaderID;
        voucher.voucherHeader.store = voucherHeaderRefer.store;
        voucher.voucherHeader.tafsili = voucherHeaderRefer.tafsili;
        voucher.voucherHeader.referNumber = voucherHeaderRefer.voucherNumber;
        voucher.voucherHeader.referDate = voucherHeaderRefer.voucherDate;
        WebService.callModalMode<VoucherDetail>(context,
            value: VoucherDetail(0),
            futureResponse: () {
              return TRestful.get(
                _resourceDetail(voucher.voucherHeader.voucherTypeRefer,
                    voucherHeaderRefer.voucherHeaderID),
                setting: setting,
                queryParams: <QueryParam>[
                  QueryParam(
                    queryParamType: QueryParamType.BOOL,
                    name: "showRemain",
                    value: "true",
                  ),
                  QueryParam(
                    queryParamType: QueryParamType.NUMBER,
                    name: "voucherTypeID",
                    value: voucher.voucherHeader.voucherType.voucherTypeID
                        .toString(),
                  ),
                  QueryParam(
                    queryParamType: QueryParamType.NUMBER,
                    name: "refTypeID",
                    value: voucher.voucherHeader.voucherTypeRefer.voucherTypeID
                        .toString(),
                  )
                ],
                timeout: 30,
              );
            },
            waitingMsg: "کالاهای سند مرجع",
            doneMsg: "کالاهای سند مرجع",
            on200list: (voucherDetails) {
              bool isUnlockRefByBarcode =
                  voucher.voucherHeader.voucherType.isUnlockRefByBarcode;
              if (isUnlockRefByBarcode != null && isUnlockRefByBarcode) {
                voucherDetails.forEach((voucherDetail) {
                  voucherDetail.quantityReserved =
                      voucherDetail.remainQ.toDouble();
                  voucherDetail.quantity = 0;
                  voucherDetail.dReferID = voucherDetail.voucherDetailID;
                });
              } else {
                voucherDetails.forEach((voucherDetail) {
                  voucherDetail.quantity = voucherDetail.remainQ.toDouble();
                  voucherDetail.dReferID = voucherDetail.voucherDetailID;
                });
              }
              voucher.voucherDetails = voucherDetails;
              TIW.setVoucher(context, voucher);
              Navigator.popUntil(
                context,
                ModalRoute.withName('/invHome'),
              );
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => widget.parentWidget()),
              );
            });
      },
    );
  }

  String _resourceHeader(VoucherType voucherTypeRefer) {
    switch (voucherTypeRefer.systemID) {
      case 6:
        return "sale/vouchers/headers/reference";
        break;
      case 12:
        return "inv/vouchers/headers/reference";
        break;
      default:
        return null;
    }
  }

  String _resourceDetail(VoucherType voucherTypeRefer, int voucherHeaderID) {
    switch (voucherTypeRefer.systemID) {
      case 6:
        return "sale/vouchers/headers/" +
            voucherHeaderID.toString() +
            "/details";
        break;
      case 12:
        return "inv/vouchers/headers/" +
            voucherHeaderID.toString() +
            "/details";
        break;
      default:
        return null;
    }
  }
}
