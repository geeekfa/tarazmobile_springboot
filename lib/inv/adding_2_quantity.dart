import 'package:com_tarazgroup/models/goods.dart';
import 'package:com_tarazgroup/models/goods_unit.dart';
import 'package:com_tarazgroup/models/system.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher.dart';
import 'package:com_tarazgroup/models/voucher_detail.dart';
import 'package:com_tarazgroup/modules/dialog/alert_dialog.dart';
import 'package:com_tarazgroup/modules/numpad.dart/numpad.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';

class AddingQuantity extends StatefulWidget {
  final int index;
  final Goods goods;
  final GoodsUnit goodsUnit;
  final double quantity;
  final Widget Function()
      parentWidget; //یعنی وقتی کار افزودن یا ویرایش تعداد کالا به سند تمام شد به  کدام صفحه بر گردیم

  const AddingQuantity({
    @required this.index,
    @required this.goods,
    @required this.goodsUnit,
    @required this.quantity,
    @required this.parentWidget,
  });
  @override
  _AddingQuantityState createState() => _AddingQuantityState();
}

class _AddingQuantityState extends State<AddingQuantity> {
  System currentSystem;
  double quantity;

  @override
  void initState() {
    quantity = widget.quantity;
    currentSystem = TIW.getSetting(context, initState: true).currentSystem;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: currentSystem.themeData,
      child: Scaffold(
        appBar: AppBar(
            title: ListTile(
          subtitle: Text(widget.goods.goodsDesc,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              softWrap: true,
              textAlign: TextAlign.right,
              style: TextStyle(
                fontFamily: 'IRANSansUltraLight',
                fontSize: 10.0,
                color: Colors.white,
              )),
          title: Text("تعداد",
              textAlign: TextAlign.right,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 12.0,
                color: Colors.white,
              )),
        )),
        body: Numpad<double>(
          value: widget.quantity,
          onChanged: (quantity) {
            this.quantity = quantity;
          },
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: _bottomNavigationBar(),
      ),
    );
  }

  Widget _bottomNavigationBar() {
    Function() onPressed = () {
      if (this.quantity == null || this.quantity == 0) {
        TAlertDialog(context: context).show(
            title: "تعداد مجاز نیست",
            content: "تعداد اقلام باید بیشتر از صفر باشد");
      } else {
        Voucher voucher = TIW.getVoucher(context);
        VoucherDetail voucherDetail = VoucherDetail(
          widget.index,
          status: VoucherDetailStatus.DEFAULT_ACTIVE,
          goods: widget.goods,
          goodsUnit: widget.goodsUnit,
          quantity: this.quantity,
          fee: widget.goods.price,
          voucherHeaderID: voucher.voucherDetails[widget.index].voucherHeaderID,
          voucherDetailID: voucher.voucherDetails[widget.index].voucherDetailID,
        );
        switch (widget.index) {
          case -1: //افزودن کالا به سند
            voucher.voucherDetails.add(voucherDetail);
            TIW.setVoucher(context, voucher);
            showFlushBar("کالا به سند اضافه شد", () {
              Navigator.popUntil(
                context,
                ModalRoute.withName("/goods"),
              );
            });
            break;
          default: //ویرایش کالای فعلی سند
            voucher.voucherDetails[widget.index] = voucherDetail;
            TIW.setVoucher(context, voucher);
            showFlushBar("کالا در سند ویرایش شد", () {
              Navigator.popUntil(
                context,
                ModalRoute.withName("/invHome"),
              );
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => widget.parentWidget()),
              );
            });
        }
      }
    };
    return FloatingActionButton(
        tooltip: "ادامه", child: Icon(Icons.done), onPressed: onPressed);
  }

  showFlushBar(String messageText, Function onDISMISSED) {
    Flushbar(
      leftBarIndicatorColor: Colors.green[300],
      margin: EdgeInsets.all(8),
      messageText: Text(messageText,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'IRANSansBlack',
            color: Colors.white,
            fontSize: 14.0,
          )),
      duration: Duration(seconds: 2),
    )..show(context).then((result) {
        onDISMISSED();
      });
  }
}
