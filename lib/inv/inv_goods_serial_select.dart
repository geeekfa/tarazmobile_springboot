// import 'package:audioplayers/audio_cache.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:com_tarazgroup/models/query_param.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher.dart';
import 'package:com_tarazgroup/models/voucher_detail.dart';
import 'package:com_tarazgroup/modules/dialog/confirm_dialog.dart';
import 'package:com_tarazgroup/modules/qkey/qkey_filter.dart';
import 'package:com_tarazgroup/modules/textfield/ttextfield.dart';
import 'package:com_tarazgroup/modules/view/list_tile_view.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class InvGoodsSerialSelect extends StatefulWidget {
  final int rowIndex;

  const InvGoodsSerialSelect({
    @required this.rowIndex,
  });
  @override
  _InvGoodsSerialSelectState createState() => _InvGoodsSerialSelectState();
}

class _InvGoodsSerialSelectState extends State<InvGoodsSerialSelect> {
  Setting setting;
  Voucher voucher;
  // final AudioCache audioCache = AudioCache();
  final formKey = GlobalKey<FormState>();
  // final List<String> values = []
  final List<TSerial> tSerials = [];
  String serialGoodsDesc;

  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    voucher = TIW.getVoucher(context, initState: true);
    VoucherDetail voucherDetail = voucher.voucherDetails[widget.rowIndex];
    for (var i = 0; i < voucherDetail.quantity.toInt(); i++) {
      if (voucherDetail.tSerials == null) {
        tSerials.add(TSerial(serialQuantity: 1.0, serialGoodsDesc: null));
        // values.add(null);
      } else {
        tSerials.add(TSerial(
            serialQuantity: 1.0,
            serialGoodsDesc: voucherDetail.tSerials[i].serialGoodsDesc));
        // values.add(voucherDetail.tSerials[i].serialGoodsDesc);
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        for (var i = 0; i < tSerials.length; i++) {
          if (tSerials[i].serialGoodsDesc == null ||
              tSerials[i].serialGoodsDesc == "") {
            bool yesNo = await TConfirmDialog(
              context: context,
            ).show("برخی سریال ها خالی است",
                "برخی از سریال ها انتخاب نشده است. با خروج از این صفحه ، سریال های جاری نیز پاک خواهند شد . آیا ادامه می دهید ؟");

            if (yesNo) {
              voucher.voucherDetails[widget.rowIndex].tSerials = null;
              TIW.setVoucher(context, voucher);
              return new Future.value(true);
            } else {
              return new Future.value(false);
            }
          }
          // tSerials[i].serialGoodsDesc = values[i];
        }
        voucher.voucherDetails[widget.rowIndex].tSerials = tSerials;
        TIW.setVoucher(context, voucher);
        return new Future.value(true);
      },
      child: Theme(
        data: setting.currentSystem.themeData,
        child: Scaffold(
          appBar: AppBar(
            title: ListTile(
              subtitle:
                  Text(voucher.voucherDetails[widget.rowIndex].goods.goodsDesc,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      softWrap: true,
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontFamily: 'IRANSansUltraLight',
                        fontSize: 10.0,
                        color: Colors.white,
                      )),
              title: Text("انتخاب سریال",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                    color: Colors.white,
                  )),
            ),
          ),
          body: Container(
            padding: EdgeInsets.all(10),
            child: Form(
              key: formKey,
              child: ListView.separated(
                  separatorBuilder: (context, index) => Divider(
                        thickness: .25,
                      ),
                  itemCount:
                      voucher.voucherDetails[widget.rowIndex].quantity.toInt(),
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      trailing: Text(
                        (index + 1).toString(),
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontFamily: 'IRANSansUltraLight',
                          fontSize: 20.0,
                        ),
                      ),
                      leading: IconButton(
                          icon: Icon(FontAwesomeIcons.qrcode),
                          onPressed: () {
                            BarcodeScanner.scan().then((value) {
                              // audioCache.play("sounds/beep-07.mp3",
                              //     volume: 1.0);
                              tSerials[index].serialGoodsDesc = value;
                              setState(() {});
                              // values[index] = value;
                            }).catchError((err) {});
                          }),
                      title: QKeyFilter<TSerial>(
                        onValue: () {
                          return this.tSerials[index];
                        },
                        title: "سریال",
                        resource: "inv/serials",
                        // recCnt: 50,
                        queryParams: <QueryParam>[
                          QueryParam(
                              queryParamType: QueryParamType.NUMBER,
                              name: "goodsID",
                              value: voucher
                                  .voucherDetails[widget.rowIndex].goods.goodsID
                                  .toString()),
                          QueryParam(
                              queryParamType: QueryParamType.NUMBER,
                              name: "refTypeID",
                              value: voucher.voucherHeader.voucherTypeRefer.voucherTypeID.toString()),
                          QueryParam(
                              queryParamType: QueryParamType.NUMBER,
                              name: "voucherTypeID",
                              value: voucher
                                  .voucherHeader.voucherType.voucherTypeID
                                  .toString()),
                        ],
                        itemBuilder: (item, index) {
                          return ListTileView.view1_1(
                            index,
                            item.serialGoodsDesc,
                          );
                        },
                        onChangePermission: () {
                          return Future.value(true);
                        },
                        onSelect: (tSerial) {
                          this.tSerials[index] = tSerial;
                        },
                        filterWidgets: <Widget>[
                          TTextField(
                            title: "سریال",
                            onValue: () {
                              return serialGoodsDesc;
                            },
                            onChanged: (txt) {
                              serialGoodsDesc = txt;
                            },
                          ),
                        ],
                        onFiltered: () {
                          List<QueryParam> queryParams = [];
                          if (this.serialGoodsDesc != null &&
                              this.serialGoodsDesc != "") {
                            queryParams.add(QueryParam(
                                queryParamType: QueryParamType.TEXT,
                                name: "serialDesc",
                                value: this.serialGoodsDesc));
                          }
                          return queryParams;
                        },
                        onDeleteFilter: () {
                          serialGoodsDesc = null;
                        },
                      ),

                      // RaisedButton(
                      //     child:
                      //         (values[index] == null || values[index] == "")
                      //             ? Text(
                      //                 "انتخاب سریال",
                      //                 style: TextStyle(
                      //                   fontFamily: 'IRANSansLight',
                      //                   fontSize: 12.0,
                      //                   color: Colors.black,
                      //                 ),
                      //               )
                      //             : Text("ff"),
                      //     onPressed: () {

                      //     })
                    );
                  }),
            ),
          ),
        ),
      ),
    );
  }
}
