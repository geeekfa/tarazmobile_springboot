import 'package:com_tarazgroup/models/query_param.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher_detail.dart';
import 'package:com_tarazgroup/models/voucher_header.dart';
import 'package:com_tarazgroup/modules/multipleview/multiple_view.dart';
import 'package:com_tarazgroup/modules/tableview/tcolumn.dart';
import 'package:com_tarazgroup/modules/textfield/ttextfield.dart';
import 'package:com_tarazgroup/modules/tlib/tlib.dart';
import 'package:flutter/material.dart';

class InvVoucherDetailsFrm extends StatefulWidget {
  final VoucherHeader voucherHeader;

  const InvVoucherDetailsFrm({
    @required this.voucherHeader,
  });
  @override
  _InvVoucherDetailsFrmState createState() => _InvVoucherDetailsFrmState();
}

class _InvVoucherDetailsFrmState extends State<InvVoucherDetailsFrm> {
  Setting setting;
  String goodsDesc;

  @override
  void initState() {
    deleteFilters();
    setting = TIW.getSetting(context, initState: true);
    super.initState();
  }

  void deleteFilters() {
    goodsDesc = null;
  }

  @override
  Widget build(BuildContext context) {
    return MultipleView<VoucherDetail>(
      initialIndex: 0,
      title: "جزییات سند",
      subtitle: widget.voucherHeader.voucherNumber.toString(),
      value: VoucherDetail(
        0,
        goods: null,
        goodsUnit: null,
        quantity: null,
        status: null,
        tSerials: null,
        voucherDetailID: null,
        voucherHeaderID: null,
      ),
      filterWidgets: <Widget>[
        TTextField(
          title: "نام کالا",
          keyboardType: TextInputType.text,
          onValue: () {
            return goodsDesc;
          },
          onChanged: (txt) {
            goodsDesc = txt;
          },
        ),
      ],
      onFiltered: () {
        List<QueryParam> queryParams = [];
        return queryParams;
      },
      onDeleteFilter: () {},
      on204: () {},
      onTabChanged: (tabIndex) {
        deleteFilters();
      },
      queryParams: <QueryParam>[
        QueryParam(
          queryParamType: QueryParamType.NUMBER,
          name: "voucherHeaderID",
          value: widget.voucherHeader.voucherHeaderID.toString(),
        )
      ],
      resource: "inv/voucher/detail",
      listViewItemBuilder: null,
      detailSectionBuilder: null,
      mainSectionBuilder: null,
      tableViewCellBuilder: (tColumn, voucherDetail) {
        switch (tColumn.name) {
          case "GOODSDESC":
            return Text(voucherDetail.goods.goodsDesc,
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));
            break;
          case "QUANTITY":
            return Text(TLib.commaSeparate(voucherDetail.quantity.toString()),
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));
            break;
          case "UNITNAME":
            return Text(voucherDetail.goodsUnit.unitDesc,
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                textDirection: TextDirection.ltr,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));
            break;
          case "COMPUTEVALUE":
            return Text(
                TLib.commaSeparate(
                    voucherDetail.goodsUnit.computeValue.toString()),
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));
            break;
          default:
            return Text("");
        }
      },
      tableViewColumns: <TColumn>[
        TColumn(
          name: "GOODSDESC",
          title: "کالا",
          width: 120,
        ),
        TColumn(
          name: "QUANTITY",
          title: "تعداد",
          width: 80,
        ),
        TColumn(
          name: "UNITNAME",
          title: "واحد",
          width: 60,
        ),
        TColumn(
          name: "COMPUTEVALUE",
          title: "ضریب",
          width: 50,
        ),
      ],
      onTap: (voucherDetail) {},
    );
  }
}
