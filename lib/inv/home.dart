import 'package:com_tarazgroup/inv/inv_voucher_frm.dart';
import 'package:com_tarazgroup/inv/inv_voucher_headers_frm.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/modules/view/grid_tile_view.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final PageController pageController = PageController();
  Setting setting;
  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    setting.currentSystem = setting.inv;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: setting.currentSystem.themeData,
      child: WillPopScope(
        onWillPop: () {
          TIW.setVoucher(context, null);
          return new Future.value(true);
        },
        child: Scaffold(
          appBar: AppBar(
            title: ListTile(
              title: Text(setting.currentSystem.title,
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                    color: Colors.white,
                  )),
            ),
          ),
          drawer: _Drawer(
            setting: setting,
          ),
          body: _Body(),
        ),
      ),
    );
  }
}

class _Drawer extends StatelessWidget {
  final Setting setting;

  const _Drawer({this.setting});
  @override
  Widget build(BuildContext context) {
    String name = setting.name;
    String family = setting.family;
    return Drawer(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: ListTile(
            title: Text(name + " " + family,
                textAlign: TextAlign.right,
                style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                    color: Colors.white)),
          ),
        ),
        body: ListView(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.settings),
              title: Text("تنظیمات",
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  )),
            ),
          ],
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Setting setting = TIW.getSetting(context);
    return GridView(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 20.0,
        mainAxisSpacing: 20.0,
      ),
      children: <Widget>[
        // GridTileView.defaultView(Icons.call_made, "اول دوره",
        //     setting.currentSystem.themeData.primaryColor, () {
        //   // Navigator.of(context).push(
        //   //   PageRouteBuilder(
        //   //     transitionDuration: Duration(milliseconds: 0),
        //   //     pageBuilder: (context, animation1, animation2) {
        //   //
        //   //     },
        //   //   ),
        //   // );
        // }),
        GridTileView.defaultView(Icons.vertical_align_bottom, "وارده",
            setting.currentSystem.themeData.primaryColor, () {
          Navigator.of(context).push(
            PageRouteBuilder(
              transitionDuration: Duration(milliseconds: 0),
              pageBuilder: (context, animation1, animation2) {
                return InvVoucherFrm(
                  initialIndex: 0,
                  voucherType: 1,
                );
              },
            ),
          );
        }),
        GridTileView.defaultView(Icons.vertical_align_top, "صادره",
            setting.currentSystem.themeData.primaryColor, () {
          Navigator.of(context).push(
            PageRouteBuilder(
              transitionDuration: Duration(milliseconds: 0),
              pageBuilder: (context, animation1, animation2) {
                return InvVoucherFrm(
                  initialIndex: 0,
                   voucherType: 2,
                );
              },
            ),
          );
        }),
        // GridTileView.defaultView(Icons.vertical_align_center, "انبار به انبار",
        //     setting.currentSystem.themeData.primaryColor, () {
        //   // Navigator.of(context).push(
        //   //   PageRouteBuilder(
        //   //     transitionDuration: Duration(milliseconds: 0),
        //   //     pageBuilder: (context, animation1, animation2) {
        //   //       return InvVoucherFrm();
        //   //     },
        //   //   ),
        //   // );
        // }),
        // GridTileView.defaultView(Icons.swap_calls, "درخواست کالا از انبار",
        //     setting.currentSystem.themeData.primaryColor, () {
        //   // Navigator.of(context).push(
        //   //   PageRouteBuilder(
        //   //     transitionDuration: Duration(milliseconds: 0),
        //   //     pageBuilder: (context, animation1, animation2) {
        //   //       return InvVoucherFrm();
        //   //     },
        //   //   ),
        //   // );
        // }),
        GridTileView.defaultView(Icons.description, "اسناد وارده ثبت شده",
            setting.currentSystem.themeData.primaryColor, () {
          Navigator.of(context).push(
            PageRouteBuilder(
              transitionDuration: Duration(milliseconds: 0),
              pageBuilder: (context, animation1, animation2) {
                return InvVoucherHeadersFrm(
                  vouchertype: 1,
                );
              },
            ),
          );
        }),
        GridTileView.defaultView(Icons.description, "اسناد صادره ثبت شده",
            setting.currentSystem.themeData.primaryColor, () {
          Navigator.of(context).push(
            PageRouteBuilder(
              transitionDuration: Duration(milliseconds: 0),
              pageBuilder: (context, animation1, animation2) {
                return InvVoucherHeadersFrm(
                  vouchertype: 2,
                );
              },
            ),
          );
        })
      ],
    );
  }
}
