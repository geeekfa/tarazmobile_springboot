// import 'package:audioplayers/audio_cache.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher.dart';
import 'package:com_tarazgroup/models/voucher_detail.dart';
import 'package:com_tarazgroup/modules/dialog/confirm_dialog.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class InvGoodsSerialEntrance extends StatefulWidget {
  final int rowIndex;

  const InvGoodsSerialEntrance({
    @required this.rowIndex,
  });
  @override
  _InvGoodsSerialEntranceState createState() => _InvGoodsSerialEntranceState();
}

class _InvGoodsSerialEntranceState extends State<InvGoodsSerialEntrance> {
  Setting setting;
  Voucher voucher;
  // final AudioCache audioCache = AudioCache();
  final formKey = GlobalKey<FormState>();
  final List<TextEditingController> textEditingControllers = [];
  final List<TSerial> tSerials = [];

  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    voucher = TIW.getVoucher(context, initState: true);
    VoucherDetail voucherDetail = voucher.voucherDetails[widget.rowIndex];
    for (var i = 0; i < voucherDetail.quantity.toInt(); i++) {
      if (voucherDetail.tSerials == null) {
        tSerials.add(TSerial(serialQuantity: 1.0, serialGoodsDesc: null));
        textEditingControllers.add(TextEditingController(text: null));
      } else {
        tSerials.add(TSerial(
            serialQuantity: 1.0,
            serialGoodsDesc: voucherDetail.tSerials[i].serialGoodsDesc));
        textEditingControllers.add(TextEditingController(
            text: voucherDetail.tSerials[i].serialGoodsDesc));
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        for (var i = 0; i < textEditingControllers.length; i++) {
          if (textEditingControllers[i].text == null ||
              textEditingControllers[i].text == "") {
            bool yesNo = await TConfirmDialog(
              context: context,
            ).show("برخی سریال ها خالی است",
                "برخی از سریال ها وارد نشده است. با خروج از این صفحه ، سریال های جاری نیز پاک خواهند شد . آیا ادامه می دهید ؟");

            if (yesNo) {
              voucher.voucherDetails[widget.rowIndex].tSerials = null;
              TIW.setVoucher(context, voucher);
              return new Future.value(true);
            } else {
              return new Future.value(false);
            }
          }
          tSerials[i].serialGoodsDesc = textEditingControllers[i].text;
        }
        voucher.voucherDetails[widget.rowIndex].tSerials = tSerials;
        TIW.setVoucher(context, voucher);
        return new Future.value(true);
      },
      child: Theme(
        data: setting.currentSystem.themeData,
        child: Scaffold(
          appBar: AppBar(
            title: ListTile(
              subtitle:
                  Text(voucher.voucherDetails[widget.rowIndex].goods.goodsDesc,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      softWrap: true,
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontFamily: 'IRANSansUltraLight',
                        fontSize: 10.0,
                        color: Colors.white,
                      )),
              title: Text("افزودن سریال",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                    color: Colors.white,
                  )),
            ),
          ),
          body: Container(
            padding: EdgeInsets.all(10),
            child: Form(
              key: formKey,
              child: ListView.separated(
                  separatorBuilder: (context, index) => Divider(
                        thickness: .25,
                      ),
                  itemCount:
                      voucher.voucherDetails[widget.rowIndex].quantity.toInt(),
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                        trailing: Text(
                          (index + 1).toString(),
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontFamily: 'IRANSansUltraLight',
                            fontSize: 20.0,
                          ),
                        ),
                        leading: IconButton(
                            icon: Icon(FontAwesomeIcons.qrcode),
                            onPressed: () {
                              BarcodeScanner.scan().then((value) {
                                // audioCache.play("sounds/beep-07.mp3",
                                //     volume: 1.0);
                                textEditingControllers[index].text = value;
                              }).catchError((err) {});
                            }),
                        title: TextFormField(
                          controller: textEditingControllers[index],
                        ));
                  }),
            ),
          ),
        ),
      ),
    );
  }
}
