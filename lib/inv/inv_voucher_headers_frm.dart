import 'package:com_tarazgroup/inv/inv_voucher_details.frm.dart';
import 'package:com_tarazgroup/models/query_param.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher_header.dart';
import 'package:com_tarazgroup/models/voucher_type.dart';
import 'package:com_tarazgroup/modules/multipleview/multiple_view.dart';
import 'package:com_tarazgroup/modules/qkey/qkey_filter.dart';
import 'package:com_tarazgroup/modules/tableview/tcolumn.dart';
import 'package:com_tarazgroup/modules/textfield/ttextfield.dart';
import 'package:com_tarazgroup/modules/textfield/ttextfield_date.dart';
import 'package:com_tarazgroup/modules/tlib/tlib.dart';
import 'package:com_tarazgroup/modules/view/list_tile_view.dart';
import 'package:flutter/material.dart';

class InvVoucherHeadersFrm extends StatefulWidget {
  final int vouchertype;

  const InvVoucherHeadersFrm({
    @required this.vouchertype,
  });
  @override
  _InvVoucherHeadersFrmState createState() => _InvVoucherHeadersFrmState();
}

class _InvVoucherHeadersFrmState extends State<InvVoucherHeadersFrm> {
  Setting setting;
  String voucherNumber;
  VoucherType voucherType;
  List<QueryParam> queryParamsVoucherType = [];
  String subtitle;
  //Filter
  String voucherTypeDesc;
  String fromDate;
  String toDate;

  @override
  void initState() {
    deleteFilters();
    setting = TIW.getSetting(context, initState: true);
    switch (widget.vouchertype) {
      case 1:
        subtitle = "وارده";
        break;
      case 2:
        subtitle = "صادره";
        break;
      default:
    }
    super.initState();
  }

  void deleteFilters() {
    voucherNumber = null;
    voucherType = VoucherType();
    fromDate = null;
    toDate = null;
  }

  @override
  Widget build(BuildContext context) {
    return MultipleView<VoucherHeader>(
      initialIndex: 0,
      title: "اسناد ثبت شده",
      subtitle: subtitle,
      value: VoucherHeader(
          voucherNumber: null,
          tafsili: null,
          customer: null,
          provider: null,
          store: null,
          voucherType: null,
          voucherTypeRefer: null,
          voucherDesc: null,
          voucherDate: null,
          deliverCenterID: null,
          saleTypeID: null,
          marketingManID: null,
          saleCenterID: null,
          salesManID: null,
          isManualPromotion: null,
          center1ID: null,
          center2ID: null,
          center3ID: null,
          paymentWayID: null,
          voucherHeaderID: null,
          saleType: null),
      filterWidgets: <Widget>[
        TTextField(
          title: "شماره سند",
          keyboardType: TextInputType.number,
          onValue: () {
            return voucherNumber;
          },
          onChanged: (txt) {
            voucherNumber = txt;
          },
        ),
        TTextFieldDate(
          title: "از تاریخ",
          onValue: () {
            return fromDate;
          },
          onChanged: (txt) {
            fromDate = txt;
          },
        ),
        TTextFieldDate(
          title: "تا تاریخ",
          onValue: () {
            return toDate;
          },
          onChanged: (txt) {
            toDate = txt;
          },
        ),
        QKeyFilter<VoucherType>(
          onValue: () {
            return this.voucherType;
          },
          title: "نوع سند",
          resource: "pub/vouchertypes",
          // recCnt: 50,
          queryParams: <QueryParam>[
            QueryParam(
              queryParamType: QueryParamType.NUMBER,
              name: "systemID",
              value: setting.currentSystem.systemID.toString(),
            ),
            QueryParam(
              queryParamType: QueryParamType.NUMBER,
              name: "voucherType",
              value: widget.vouchertype.toString(),
            )
          ],
          itemBuilder: (item, index) {
            return ListTileView.view1_1(
              index,
              item.voucherTypeDesc,
            );
          },
          onChangePermission: () {
            return Future.value(true);
          },
          onSelect: (voucherType) {
            this.voucherType = voucherType;
          },
          filterWidgets: <Widget>[
            TTextField(
              title: "نوع سند",
              onValue: () {
                return voucherTypeDesc;
              },
              onChanged: (txt) {
                voucherTypeDesc = txt;
              },
            ),
          ],
          onFiltered: () {
            List<QueryParam> queryParams = [];
            if (this.voucherTypeDesc != null && this.voucherTypeDesc != "") {
              queryParams.add(QueryParam(
                  queryParamType: QueryParamType.TEXT,
                  name: "voucherTypeDesc",
                  value: this.voucherTypeDesc));
            }
            return queryParams;
          },
          onDeleteFilter: () {
            voucherTypeDesc = null;
          },
        ),
      ],
      onFiltered: () {
        List<QueryParam> queryParams = [];
        if (this.voucherNumber != null && this.voucherNumber != "") {
          queryParams.add(QueryParam(
            queryParamType: QueryParamType.NUMBER,
            name: "voucherNumber",
            value: this.voucherNumber,
          ));
        }
        if (this.voucherType.voucherTypeID != null) {
          queryParams.add(QueryParam(
            queryParamType: QueryParamType.NUMBER,
            name: "voucherTypeID",
            value: this.voucherType.voucherTypeID.toString(),
          ));
        }

        if (this.fromDate != null &&
            this.fromDate != "" &&
            this.toDate != null &&
            this.toDate != "") {
          Pattern pattern =
              r'([12]\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\d|3[01]))';
          RegExp regex = new RegExp(pattern);
          if (regex.hasMatch(fromDate) && regex.hasMatch(toDate)) {
            queryParams.add(QueryParam(
              queryParamType: QueryParamType.DATE,
              name: "fromDate",
              value: this.fromDate,
            ));
            queryParams.add(QueryParam(
              queryParamType: QueryParamType.DATE,
              name: "toDate",
              value: this.toDate,
            ));
          }
        }
        return queryParams;
      },
      onDeleteFilter: () {
        deleteFilters();
      },
      on204: () {},
      onTabChanged: (tabIndex) {
        deleteFilters();
      },
      queryParams: <QueryParam>[
        QueryParam(
            queryParamType: QueryParamType.NUMBER,
            name: "voucherType",
            value: widget.vouchertype.toString())
      ],
      resource: "inv/vouchers/headers",
      listViewItemBuilder: null,
      detailSectionBuilder: null,
      mainSectionBuilder: null,
      tableViewCellBuilder: (tColumn, voucherHeader) {
        switch (tColumn.name) {
          case "VOUCHERNUMBER":
            return Text(
                TLib.commaSeparate(voucherHeader.voucherNumber.toString()),
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));
            break;
          case "VOUCHERTYPEDESC":
            return Text(voucherHeader.voucherType.voucherTypeDesc,
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));
            break;
          case "STORENAME":
            return Text(voucherHeader.store.storeName,
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));

            break;
          case "VOUCHERDATE":
            return Text(voucherHeader.voucherDate,
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                textDirection: TextDirection.ltr,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));

            break;
          case "TAFSILIDESC":
            return Text(voucherHeader.tafsili.tafsiliDesc,
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));

            break;
          case "VOUCHERDESC":
            return Text(voucherHeader.voucherDesc ?? "",
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));

            break;
          default:
            return Text("");
        }
      },
      tableViewColumns: <TColumn>[
        TColumn(
          name: "VOUCHERNUMBER",
          title: "شماره سند",
          width: 100,
        ),
        TColumn(
          name: "VOUCHERTYPEDESC",
          title: "نوع سند",
          width: 200,
        ),
        TColumn(
          name: "STORENAME",
          title: "انبار",
          width: 200,
        ),
        TColumn(
          name: "VOUCHERDATE",
          title: "تاریخ",
          width: 80,
        ),
        TColumn(
          name: "TAFSILIDESC",
          title: "تفصیلی",
          width: 300,
        ),
        TColumn(
          name: "VOUCHERDESC",
          title: "توضیحات",
          width: 500,
        ),
      ],
      onTap: (voucherHeader) {
        Navigator.of(context).push(
          PageRouteBuilder(
            transitionDuration: Duration(milliseconds: 0),
            pageBuilder: (context, animation1, animation2) {
              return InvVoucherDetailsFrm(
                voucherHeader: voucherHeader,
              );
            },
          ),
        );
      },
    );
  }
}
