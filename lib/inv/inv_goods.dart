import 'package:com_tarazgroup/inv/inv_voucher_frm.dart';
import 'package:com_tarazgroup/models/goods.dart';
import 'package:com_tarazgroup/models/goods_group.dart';
import 'package:com_tarazgroup/models/goods_unit.dart';
import 'package:com_tarazgroup/models/query_param.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher.dart';
import 'package:com_tarazgroup/models/voucher_detail.dart';
import 'package:com_tarazgroup/modules/dialog/alert_dialog.dart';
import 'package:com_tarazgroup/modules/goods/goods_details_frm.dart';
import 'package:com_tarazgroup/modules/multipleview/multiple_view.dart';
import 'package:com_tarazgroup/modules/tableview/tcolumn.dart';
import 'package:com_tarazgroup/modules/textfield/ttextfield.dart';
import 'package:com_tarazgroup/modules/tlib/tlib.dart';
import 'package:com_tarazgroup/modules/view/goods_grid_tile_plus_minus.dart';
import 'package:com_tarazgroup/modules/view/goods_list_tile_plus_minus_inv.dart';
import 'package:com_tarazgroup/inv/adding_1_goods_unit.dart';
import 'package:flutter/material.dart';
import 'package:shamsi_date/shamsi_date.dart';

class InvGoods extends StatefulWidget {
  final int voucherTypeID;
  final int storeID;
  final String barcode;
  final GoodsGroup goodsGroup;
  final int voucherType;// voucherType==1 -> وارده  voucherType==1 -> صادره   voucherType==1 -> انبار به انبار
  const InvGoods({
    @required this.voucherTypeID,
    @required this.storeID,
    @required this.barcode,
    @required this.goodsGroup,
    @required this.voucherType,
  });

  @override
  _InvGoodsState createState() => _InvGoodsState();
}

class _InvGoodsState extends State<InvGoods> {
  Setting setting;
  List<QueryParam> queryParams = [];
  String goodsDesc;
  String goodsCode;
  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    queryParams.add(QueryParam(
        queryParamType: QueryParamType.TEXT,
        name: "voucherDate",
        value: Jalali.now().formatter.y +
            "/" +
            Jalali.now().formatter.mm +
            "/" +
            Jalali.now().formatter.dd));
    queryParams.add(QueryParam(
        queryParamType: QueryParamType.NUMBER,
        name: "voucherTypeID",
        value: widget.voucherTypeID.toString()));
    if (widget.barcode == null) {
      queryParams.add(QueryParam(
        queryParamType: QueryParamType.NUMBER,
        name: "groupID",
        value: (widget.goodsGroup == null || widget.goodsGroup.groupID == null)
            ? ""
            : widget.goodsGroup.groupID.toString(),
      ));
    } else {
      queryParams.add(QueryParam(
          queryParamType: QueryParamType.NUMBER,
          name: "barCode",
          value: widget.barcode));
    }

    queryParams.add(QueryParam(
        queryParamType: QueryParamType.NUMBER,
        name: "storeID",
        value: widget.storeID == null ? "" : widget.storeID.toString()));

    queryParams.add(QueryParam(
        queryParamType: QueryParamType.BOOL, name: "hasPrice", value: "false"));

    queryParams.add(QueryParam(
        queryParamType: QueryParamType.BOOL,
        name: "isOnlyRemain",
        value: "false"));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
        data: setting.currentSystem.themeData,
        child: Scaffold(
            body: WillPopScope(
                child: MultipleView<Goods>(
                  initialIndex: 0,
                  title: "کالاها",
                  subtitle: (widget.goodsGroup == null ||
                          widget.goodsGroup.groupID == null)
                      ? "همه گروه کالا ها"
                      : widget.goodsGroup.groupDesc,
                  value: Goods(),
                  filterWidgets: <Widget>[
                    TTextField(
                      title: "نام کالا",
                      onValue: () {
                        return goodsDesc;
                      },
                      onChanged: (txt) {
                        goodsDesc = txt;
                      },
                    ),
                    TTextField(
                      title: "کد کالا",
                      onValue: () {
                        return goodsCode;
                      },
                      onChanged: (txt) {
                        goodsCode = txt;
                      },
                    ),
                  ],
                  onFiltered: () {
                    List<QueryParam> queryParams = [];
                    if (this.goodsDesc != null && this.goodsDesc != "") {
                      queryParams.add(QueryParam(
                          queryParamType: QueryParamType.TEXT,
                          name: "goodsDesc",
                          value: this.goodsDesc));
                    }
                    if (this.goodsCode != null && this.goodsCode != "") {
                      queryParams.add(QueryParam(
                          queryParamType: QueryParamType.TEXT,
                          name: "goodsCode",
                          value: this.goodsCode));
                    }
                    // if (this.goodsGroup.groupID != null) {
                    //   queryParams.add(QueryParam(
                    //     queryParamType: QueryParamType.NUMBER,
                    //     name: "groupID",
                    //     value: this.goodsGroup.groupID.toString(),
                    //   ));
                    // }
                    return queryParams;
                  },
                  onDeleteFilter: () {
                    goodsDesc = null;
                    goodsCode = null;
                  },
                  on204: () {},
                  onTabChanged: (tabIndex) {
                    goodsDesc = null;
                    goodsCode = null;
                  },
                  queryParams: queryParams,
                  resource: "sale/goods",
                  listViewItemBuilder: (item, index) {
                    return GoodsListTilePlusMinusInv(
                        goods: item,
                        onTapPlus: () {
                          // audioCache.play("sounds/plus.mp3", volume: 1.0);
                          onTapPlus(item);
                        },
                        onTapMinus: () {
                          // audioCache.play("sounds/minus.mp3", volume: 1.0);
                          onTapMinus(item);
                        });
                  },
                  mainSectionBuilder: (item, index) {
                    return GoodsGridTilePlusMinus(
                        goods: item,
                        onTapPlus: () {
                          // audioCache.play("sounds/plus.mp3", volume: 1.0);
                          onTapPlus(item);
                        },
                        onTapMinus: () {
                          // audioCache.play("sounds/minus.mp3", volume: 1.0);
                          onTapMinus(item);
                        });
                  },
                  detailSectionBuilder: (item, index) {
                    return GridTile(
                        child: ClipRect(
                      child: Container(
                          color: Colors.white10,
                          child: Column(
                            children: <Widget>[
                              ButtonBar(
                                alignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    item.goodsDesc,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontFamily: 'IRANSansBlack',
                                      fontSize: 15.0,
                                      color: Colors.black38,
                                    ),
                                  ),
                                ],
                              ),
                              ButtonBar(
                                alignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    item.goodsCode,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontFamily: 'IRANSans',
                                      fontSize: 14.0,
                                      color: Colors.black38,
                                    ),
                                  ),
                                  Text(
                                    "کد کالا",
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      fontFamily: 'IRANSans',
                                      fontSize: 14.0,
                                      color: Colors.black38,
                                    ),
                                  ),
                                ],
                              ),
                              ButtonBar(
                                alignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    item.storeName ?? "",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontFamily: 'IRANSans',
                                      fontSize: 14.0,
                                      color: Colors.black38,
                                    ),
                                  ),
                                  Text(
                                    "انبار",
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      fontFamily: 'IRANSans',
                                      fontSize: 14.0,
                                      color: Colors.black38,
                                    ),
                                  ),
                                  // Icon(
                                  //   Icons.store,
                                  //   color: Colors.black38,
                                  //   size: 15,
                                  // ),
                                ],
                              ),
                              ButtonBar(
                                alignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    item.goodsUnit.defaultSecUnitID == null
                                        ? item.goodsUnit.unitDesc
                                        : item.goodsUnit.secUnitDesc,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontFamily: 'IRANSans',
                                      fontSize: 14.0,
                                      color: Colors.black38,
                                    ),
                                  ),
                                  Text(
                                    "واحد پیش فرض",
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      fontFamily: 'IRANSans',
                                      fontSize: 14.0,
                                      color: Colors.black38,
                                    ),
                                  ),
                                  // Icon(
                                  //   Icons.account_balance_wallet,
                                  //   color: Colors.black38,
                                  //   size: 15,
                                  // ),
                                ],
                              ),
                              ButtonBar(
                                alignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    item.goodsUnit.computeValue == null
                                        ? ""
                                        : item.goodsUnit.computeValue
                                            .toString(),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontFamily: 'IRANSans',
                                      fontSize: 14.0,
                                      color: Colors.black38,
                                    ),
                                  ),
                                  Text(
                                    "ضریب",
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      fontFamily: 'IRANSans',
                                      fontSize: 14.0,
                                      color: Colors.black38,
                                    ),
                                  ),
                                  // Icon(
                                  //   Icons.hdr_weak,
                                  //   color: Colors.black38,
                                  //   size: 15,
                                  // ),
                                ],
                              ),
                              ButtonBar(
                                alignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    item.techInfo ?? "",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontFamily: 'IRANSans',
                                      fontSize: 14.0,
                                      color: Colors.black38,
                                    ),
                                  ),
                                  Text(
                                    "اطلاعات فنی",
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      fontFamily: 'IRANSans',
                                      fontSize: 14.0,
                                      color: Colors.black38,
                                    ),
                                  ),
                                  // Icon(
                                  //   Icons.info,
                                  //   color: Colors.black38,
                                  //   size: 15,
                                  // ),
                                ],
                              ),
                            ],
                          )),
                    ));
                  },
                  tableViewCellBuilder: (tColumn, goods) {
                    switch (tColumn.name) {
                      case "goodsDesc":
                        return Text(goods.goodsDesc,
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              fontFamily: 'IRANSans',
                              fontSize: 12.0,
                            ));
                        break;
                      case "goodsCode":
                        return Text(goods.goodsCode,
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              fontFamily: 'IRANSans',
                              fontSize: 12.0,
                            ));
                        break;
                      case "storeName":
                        return Text(goods.storeName ?? "",
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              fontFamily: 'IRANSans',
                              fontSize: 12.0,
                            ));
                        break;
                      case "price":
                        return Text(
                            goods.price == null
                                ? "-"
                                : TLib.commaSeparate(goods.price.toString()),
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              fontFamily: 'IRANSans',
                              fontSize: 12.0,
                            ));

                        break;
                      case "remain":
                        return Text(
                            goods.remain == null
                                ? "-"
                                : TLib.commaSeparate(goods.remain.toString()),
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            textAlign: TextAlign.end,
                            textDirection: TextDirection.ltr,
                            style: TextStyle(
                              fontFamily: 'IRANSans',
                              fontSize: 12.0,
                            ));

                        break;
                      case "secUnitDesc":
                        return Text(
                            goods.goodsUnit.defaultSecUnitID == null
                                ? goods.goodsUnit.unitDesc
                                : goods.goodsUnit.secUnitDesc,
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              fontFamily: 'IRANSans',
                              fontSize: 12.0,
                            ));

                        break;
                      case "techInfo":
                        return Text(goods.techInfo ?? "",
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              fontFamily: 'IRANSans',
                              fontSize: 12.0,
                            ));
                        break;
                      default:
                        return Text("");
                    }
                  },
                  tableViewColumns: <TColumn>[
                    TColumn(
                      name: "goodsDesc",
                      title: "نام کالا",
                      width: 150,
                    ),
                    TColumn(
                      name: "goodsCode",
                      title: "کد کالا",
                      width: 80,
                    ),
                    TColumn(
                      name: "storeName",
                      title: "انبار",
                      width: 170,
                    ),
                    TColumn(
                      name: "secUnitDesc",
                      title: "واحد پیش فرض",
                      width: 120,
                    ),
                    TColumn(
                      name: "techInfo",
                      title: "مشخصات فنی",
                      width: 120,
                    )
                  ],
                  onTap: (goods) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => GoodsDetailsFrm(
                                showPrice: false,
                                showRemain: false,
                                goods: goods,
                                onPressedPlusButton: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            AddingGoodsUnit(
                                              index: -1,
                                              goods: goods,
                                              goodsUnit: null,
                                              quantity: 1,
                                              parentWidget: () {
                                                return InvVoucherFrm(
                                                  initialIndex: 0,
                                                  voucherType:widget.voucherType ,
                                                );
                                              },
                                            )),
                                  );
                                },
                              )),
                    );
                  },
                ),
                onWillPop: () {
                  if (widget.goodsGroup == null) {
                    //یعنی از جانب خود فرم سند به اینجا رسیدیم
                    Navigator.popUntil(
                      context,
                      ModalRoute.withName('/invHome'),
                    );
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => InvVoucherFrm(
                                initialIndex: 0,
                                voucherType: widget.voucherType,
                              )),
                    );
                  } else {
                    // یعنی از جانب فرم گروه کالا به اینجا رسیدیم
                  }
                  return new Future.value(true);
                })));
  }

  void onTapPlus(Goods goods) {
    if (goods.goodsUnit.computeValue == null ||
        goods.goodsUnit.computeValue == 0) {
      TAlertDialog(context: context).show(
          title: "ضریب واحد کالا مجاز نیست",
          content: "ضریب واحد کالا باید بیشتر از صفر باشد");
    } else {
      Voucher voucher = TIW.getVoucher(context);
      bool isExistInVoucherDetail =
          false; //آیا این کالا در حال حاضر وجود دارد در ووچر دیتیل ؟
      for (var i = 0; i < voucher.voucherDetails.length; i++) {
        if (goods.goodsID == voucher.voucherDetails[i].goods.goodsID &&
            goods.goodsUnit.unitID ==
                voucher.voucherDetails[i].goodsUnit.unitID &&
            goods.storeID == voucher.voucherDetails[i].goods.storeID) {
          isExistInVoucherDetail = true;
          voucher.voucherDetails[i].quantity =
              voucher.voucherDetails[i].quantity + 1;
          break;
        }
      }
      if (!isExistInVoucherDetail) {
        GoodsUnit goodsUnit;
        if (goods.goodsUnit.computeValue.toInt() == 1) {
          //یعنی واحد پیش فرض همان واحد اصلی است
          goodsUnit = GoodsUnit(
            computeValue: goods.goodsUnit.computeValue,
            secUnitID: goods.goodsUnit.secUnitID,
            secUnitDesc: goods.goodsUnit.secUnitDesc,
            unitDesc: goods.goodsUnit.unitDesc,
          );
        } else {
          //یعنی واحد پیش فرض  واحد فرعی است
          goodsUnit = GoodsUnit(
            computeValue: goods.goodsUnit.computeValue,
            secUnitID: goods.goodsUnit.defaultSecUnitID,
            secUnitDesc: goods.goodsUnit.secUnitDesc,
            unitDesc: goods.goodsUnit.secUnitDesc,
          );
        }
        goods.goodsUnit.secUnitID = goods.goodsUnit.defaultSecUnitID;
        voucher.voucherDetails.add(VoucherDetail(
          -1,
          status: VoucherDetailStatus.DEFAULT_ACTIVE,
          goods: goods,
          goodsUnit: goodsUnit,
          quantity: 1,
          fee: goods.price,
        ));
      }
      TIW.setVoucher(context, voucher);
    }
  }

  void onTapMinus(Goods goods) {
    if (goods.goodsUnit.computeValue == null ||
        goods.goodsUnit.computeValue == 0) {
      TAlertDialog(context: context).show(
          title: "ضریب واحد کالا مجاز نیست",
          content: "ضریب واحد کالا باید بیشتر از صفر باشد");
    } else {
      Voucher voucher = TIW.getVoucher(context);
      for (var i = 0; i < voucher.voucherDetails.length; i++) {
        if (goods.goodsID == voucher.voucherDetails[i].goods.goodsID &&
            goods.goodsUnit.unitID ==
                voucher.voucherDetails[i].goodsUnit.unitID &&
            goods.storeID == voucher.voucherDetails[i].goods.storeID &&
            voucher.voucherDetails[i].quantity > 1) {
          voucher.voucherDetails[i].quantity =
              voucher.voucherDetails[i].quantity - 1;
          break;
        }
      }
      TIW.setVoucher(context, voucher);
    }
  }
}
