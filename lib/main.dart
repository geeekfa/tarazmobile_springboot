import 'dart:async';
import 'package:com_tarazgroup/initialize.dart';
import 'package:com_tarazgroup/ip_port.dart';
import 'package:com_tarazgroup/models/sale_setting.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/system.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  // SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
  //     .then((_) {

  // });

  runApp(TIW(
    child: MaterialApp(
      home: MainPage(),
      theme: ThemeData(
        primaryColor: Colors.lightBlue[900],
      ),
    ),
  ));
}

class MainPage extends StatefulWidget {
  @override
  MainPageState createState() {
    return MainPageState();
  }
}

class MainPageState extends State<MainPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  void initState() {
    super.initState();
    System sale = System(
      id: "sale",
      systemID: 6,
      title: "فروش",
      themeData: ThemeData(
        primaryColor: Colors.blueGrey,
        primaryColorLight: Colors.blueGrey[300],
        primaryColorDark: Colors.blueGrey[800],
        accentColor: Colors.blueGrey[100],
      ),
      iconData: Icons.change_history,
    );
    System inv = System(
      id: "inv",
      systemID: 12,
      title: "انبار",
      themeData: ThemeData(
        primaryColor: Colors.deepPurple,
        primaryColorLight: Colors.deepPurple[300],
        primaryColorDark: Colors.deepPurple[800],
        accentColor: Colors.deepPurple[100],
      ),
      iconData: Icons.hdr_strong,
    );
    SharedPreferences.getInstance().then((sp) {
      bool mustSendLocation = sp.getBool("mustSendLocation") ?? false;
      bool hasPrice = sp.getBool("hasPrice") ?? true;
      bool hasRemain = sp.getBool("hasRemain") ?? true;
      sale.saleSettingClient = SaleSettingClient(
        mustSendLocation: mustSendLocation,
        hasPrice: hasPrice,
        hasRemain: hasRemain,
      );
      Setting setting = Setting(
          perComID: sp.getInt("perComID"),
          token: sp.getString("token"),
          family: sp.getString("family"),
          ip: sp.getString("ip"),
          name: sp.getString("name"),
          password: sp.getString("password"),
          port: sp.getString("port"),
          userName: sp.getString("userName"),
          isAdmin: sp.getBool("isAdmin"),
          serverID: sp.getInt("serverID"),
          serverIDs: sp.getString("serverIDs"),
          activeYearID: sp.getInt("activeYearID"),
          sale: sale,
          inv: inv);
      TIW.setSetting(context, setting, initState: true);
      if (TIW.getSetting(context).token == null) {
        Timer(const Duration(milliseconds: 2000), () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => IpPort()),
          );
        });
      } else {
        Timer(const Duration(milliseconds: 2000), () async {
          Initialize(context).getActiveApplicationsAndGoOn(scaffoldKey);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                "assets/images/launcher.png",
                fit: BoxFit.fill,
              ),
              Padding(padding: EdgeInsets.only(bottom: 3.0)),
              Text("نرم افزار جامع همراه تراز سامانه",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'IRANSansBlack',
                    color: Theme.of(context).primaryColor,
                    fontSize: 20.0,
                  )),
            ],
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: FloatingActionButton(
          heroTag: "refresh_app",
          disabledElevation: 0,
          // backgroundColor: Theme.of(context).buttonColor,
          child: Icon(
            Icons.refresh,
            size: 35,
          ),
          onPressed: () async {
            SharedPreferences sp = await SharedPreferences.getInstance();
            await sp.clear();
            Navigator.pushNamedAndRemoveUntil(context, '/', (_) => false);
          },
        ));
  }
}
