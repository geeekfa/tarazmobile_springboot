import 'package:com_tarazgroup/models/progressbar.dart';
import 'package:com_tarazgroup/models/promotion.dart';
import 'package:com_tarazgroup/models/promotion_element.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher.dart';
import 'package:com_tarazgroup/modules/future/webservice.dart';
import 'package:com_tarazgroup/modules/rest/restful.dart';
import 'package:com_tarazgroup/modules/view/list_tile_view.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

class PromotionElementFrm extends StatefulWidget {
  @override
  _PromotionState createState() => _PromotionState();
}

class _PromotionState extends State<PromotionElementFrm> {
  Setting setting;
  Voucher voucher;

  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    voucher = TIW.getVoucher(context, initState: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: setting.currentSystem.themeData,
      child: Scaffold(
        appBar: AppBar(
          title: ListTile(
            title: Text("جایزه و عامل",
                textAlign: TextAlign.right,
                style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                    color: Colors.white)),
          ),
        ),
        body: WebService.callInlineMode<PromotionElement>(
          value: PromotionElement(),
          futureResponse: _futureResponseSaleComputePromotion,
          onWaiting: () {
            return Center(
              child: SingleChildScrollView(
                child: ProgressBar(
                  label: "... " + "جایزه و عامل",
                  value: null,
                  size: 100,
                ),
              ),
            );
          },
          on200objectWidget: (pe) {
            List<TElement> tElements = pe.tElements;
            List<TPromotion> tPromotions = pe.tPromotions;
            List<TPromotion> tPromotionsManual =
                voucher.tPromotions.where((tPromotion) {
              return tPromotion.type == 'دستی';
            }).toList();
            tPromotions.addAll(tPromotionsManual);
            return DefaultTabController(
                length: 2,
                child: Scaffold(
                  appBar: PreferredSize(
                    preferredSize: Size.fromHeight(47.0),
                    child: AppBar(
                      automaticallyImplyLeading: false,
                      // backgroundColor: setting.currentSystem.color400,
                      title: TabBar(
                        indicatorWeight: 2,
                        indicatorColor: Colors.white,
                        tabs: <Widget>[
                          Tab(
                            child: Text(
                              "عامل",
                              style: TextStyle(
                                fontFamily: 'IRANSans',
                                fontSize: 10.0,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          Tab(
                            child: Text(
                              "جایزه",
                              style: TextStyle(
                                fontFamily: 'IRANSans',
                                fontSize: 10.0,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  body: TabBarView(
                    children: <Widget>[
                      ListView.separated(
                          separatorBuilder: (context, index) => Divider(
                                // color: setting.currentSystem.color900,
                                thickness: .25,
                              ),
                          itemCount: tElements.length,
                          itemBuilder: (BuildContext context, int index) {
                            return InkWell(
                              borderRadius: BorderRadius.circular(0.0),
                              child: ListTileView.view1_2(
                                  index,
                                  tElements[index].elementDesc,
                                  tElements[index].elementMab.toString()),
                              onTap: () {},
                            );
                          }),
                      ListView.separated(
                          separatorBuilder: (context, index) => Divider(
                                // color: setting.currentSystem.color900,
                                thickness: .25,
                              ),
                          itemCount: tPromotions.length,
                          itemBuilder: (BuildContext context, int index) {
                            return InkWell(
                              borderRadius: BorderRadius.circular(0.0),
                              child: ListTileView.view1_2(
                                  index,
                                  tPromotions[index].goodsDesc,
                                  tPromotions[index].unitDesc == null
                                      ? tPromotions[index].quantity.toString()
                                      : tPromotions[index].quantity.toString() +
                                          "  " +
                                          tPromotions[index].unitDesc +
                                          " (ماهیت : " +
                                          tPromotions[index].type +
                                          ") "),
                              onTap: () {},
                            );
                          }),
                    ],
                  ),
                ));
          },
          on200listWidget: (list) {
            return null;
          },
          on204Widget: () {
            List<TPromotion> tPromotions = [];
            List<TPromotion> tPromotionsManual =
                voucher.tPromotions.where((tPromotion) {
              return tPromotion.type == 'دستی';
            }).toList();
            tPromotions.addAll(tPromotionsManual);
            return DefaultTabController(
                length: 2,
                child: Scaffold(
                  appBar: PreferredSize(
                    preferredSize: Size.fromHeight(47.0),
                    child: AppBar(
                      automaticallyImplyLeading: false,
                      // backgroundColor: setting.currentSystem.color400,
                      title: TabBar(
                        indicatorWeight: 2,
                        indicatorColor: Colors.white,
                        tabs: <Widget>[
                          Tab(
                            child: Text(
                              "عامل",
                              style: TextStyle(
                                fontFamily: 'IRANSans',
                                fontSize: 10.0,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          Tab(
                            child: Text(
                              "جایزه",
                              style: TextStyle(
                                fontFamily: 'IRANSans',
                                fontSize: 10.0,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  body: TabBarView(
                    children: <Widget>[
                      Center(
                          child: Text(
                        "به این سندعامل تعلق نمی گیرد",
                        style: TextStyle(
                          fontFamily: 'IRANSansUltraLight',
                          fontSize: 14.0,
                          color: Colors.black26,
                        ),
                      )),
                      ListView.separated(
                          separatorBuilder: (context, index) => Divider(
                                // color: setting.currentSystem.color900,
                                thickness: .25,
                              ),
                          itemCount: tPromotions.length,
                          itemBuilder: (BuildContext context, int index) {
                            return InkWell(
                              borderRadius: BorderRadius.circular(0.0),
                              child: ListTileView.view1_2(
                                  index,
                                  tPromotions[index].goodsDesc,
                                  tPromotions[index].unitDesc == null
                                      ? tPromotions[index].quantity.toString()
                                      : tPromotions[index].quantity.toString() +
                                          "  " +
                                          tPromotions[index].unitDesc +
                                          " (ماهیت : " +
                                          tPromotions[index].type +
                                          ") "),
                              onTap: () {},
                            );
                          }),
                    ],
                  ),
                ));
          },
        ),
      ),
    );
  }

  Future<Response> _futureResponseSaleComputePromotion() {
    return TRestful.post(
      "sale/promotionelements",
      setting: setting,
      body: Voucher.jsonEncodeSale(voucher),
      timeout: 300,
    );
  }
}
