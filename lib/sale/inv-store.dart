import 'package:com_tarazgroup/models/query_param.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/store.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher.dart';
import 'package:com_tarazgroup/modules/multipleview/multiple_view.dart';
import 'package:com_tarazgroup/modules/textfield/ttextfield.dart';
import 'package:com_tarazgroup/modules/view/list_tile_view.dart';
import 'package:com_tarazgroup/sale/sale_goods.dart';
import 'package:flutter/material.dart';

// وقتی انبار ردیفی است باید قبل از انتخاب کالا اول نام انبار برای هر ردیف مشخص شود
// این کلاس برای این منظور است
class InvStore extends StatefulWidget {
  const InvStore();
  @override
  _InvStoreState createState() => _InvStoreState();
}

class _InvStoreState extends State<InvStore> {
  ThemeData themeData;
  int perComID;
  Store store;
  String storeName;
  @override
  void initState() {
    Setting setting = TIW.getSetting(context, initState: true);
    perComID = setting.perComID;
    themeData = setting.currentSystem.themeData;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: themeData,
      child: Scaffold(
        body: MultipleView<Store>(
          initialIndex: 0,
          title: "انبار در ردیف",
          subtitle: "",
          value: Store(),
          resource: "inv/getstoreuserwebs",
          queryParams: <QueryParam>[
            QueryParam(
              queryParamType: QueryParamType.NUMBER,
              name: "userID",
              value: perComID.toString(),
            )
          ],
          onTap: null,
          onTabChanged: (tabIndex) {},
          listViewItemBuilder: (item, index) {
            bool value = false;
            String secondary = "";
            if (this.store == null) {
            } else if (this.store.storeID == item.storeID) {
              value = true;
            }
            return ListTileView.checkboxListTile(
                value: value,
                title: item.storeName,
                secondary: secondary,
                subTitle: "",
                onChanged: (value1) {
                  this.store = item;
                  setState(() {});
                });
          },
          tableViewCellBuilder: null,
          tableViewColumns: null,
          mainSectionBuilder: null,
          detailSectionBuilder: null,
          on204: () {
            this.store = null;
            setState(() {});
          },
          filterWidgets: <Widget>[
            TTextField(
              title: "نام انبار",
              onValue: () {
                return storeName;
              },
              onChanged: (txt) {
                storeName = txt;
              },
            ),
          ],
          onFiltered: () {
            this.storeName = null;
            setState(() {});
            List<QueryParam> queryParams = [];
            if (this.storeName != null && this.storeName != "") {
              queryParams.add(QueryParam(
                  queryParamType: QueryParamType.TEXT,
                  name: "storeName",
                  value: this.storeName));
            }
            return queryParams;
          },
          onDeleteFilter: () {
            this.store = null;
            storeName = null;
            setState(() {});
          },
        ),
        floatingActionButton: this.store != null
            ? FloatingActionButton(
                tooltip: "ادامه",
                child: Icon(
                  Icons.arrow_forward,
                  color: themeData.primaryColorDark,
                ),
                onPressed: () {
                  // GoodsUnit goodsUnit = GoodsUnit(
                  //   unitID: this.goodsUnit.unitID,
                  //   unitDesc: this.goodsUnit.unitDesc,
                  //   secUnitID: this.goodsUnit.secUnitID,
                  //   defaultSecUnitID: this.goodsUnit.defaultSecUnitID,
                  //   secUnitDesc: this.goodsUnit.unitDesc,
                  //   computeValue: this.goodsUnit.computeValue,
                  //   isQuantitative: this.goodsUnit.isQuantitative,
                  //   isQuantitativeDesc: this.goodsUnit.isQuantitativeDesc,
                  // );
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //       builder: (BuildContext context) => AddingQuantity(
                  //             addingGoodsDestination:
                  //                 widget.addingGoodsDestination,
                  //             index: widget.index,
                  //             goods: widget.goods,
                  //             goodsUnit: goodsUnit,
                  //             quantity: widget.quantity,
                  //             price: widget.price,
                  //             parentWidget: widget.parentWidget,
                  //           )),
                  // );
                  Voucher voucher;
                  voucher = TIW.getVoucher(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        settings: RouteSettings(name: "/goods"),
                        builder: (BuildContext context) => SaleGoods(
                              barcode: null,
                              customerID:
                                  voucher.voucherHeader.customer.customerID,
                              goodsGroup: null,
                              storeID: this.store.storeID,
                              voucherTypeID: voucher
                                  .voucherHeader.voucherType.voucherTypeID,
                            )),
                  );
                })
            : Container(),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      ),
    );
  }
}
