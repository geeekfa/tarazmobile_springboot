import 'dart:convert';

import 'package:com_tarazgroup/models/coordinate.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/modules/future/location/tlocation.dart';
import 'package:com_tarazgroup/modules/future/webservice.dart';
import 'package:com_tarazgroup/modules/rest/restful.dart';
import 'package:com_tarazgroup/modules/view/grid_tile_view.dart';
import 'package:com_tarazgroup/sale/sale_voucher_frm.dart';
import 'package:com_tarazgroup/sale/sale_voucher_headers_frm.dart';
import 'package:com_tarazgroup/sale/tsetting_frm.dart';
import 'package:flutter/material.dart';

import 'customer-add-frm.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final PageController pageController = PageController();
  Setting setting;

  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    setting.currentSystem = setting.sale;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: setting.currentSystem.themeData,
      child: WillPopScope(
        onWillPop: () {
          TIW.setVoucher(context, null);
          return new Future.value(true);
        },
        child: Scaffold(
          appBar: AppBar(
            title: ListTile(
              title: Text(setting.currentSystem.title,
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                    color: Colors.white,
                  )),
            ),
          ),
          drawer: _Drawer(
            setting: setting,
          ),
          body: _Body(),
        ),
      ),
    );
  }
}

class _Drawer extends StatelessWidget {
  final Setting setting;

  const _Drawer({this.setting});
  @override
  Widget build(BuildContext context) {
    String name = setting.name;
    String family = setting.family;
    return Drawer(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: ListTile(
            title: Text(name + " " + family,
                textAlign: TextAlign.right,
                style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                    color: Colors.white)),
          ),
        ),
        body: ListView(
          children: <Widget>[
            // ListTile(
            //   leading: Icon(Icons.map),
            //   title: Text("نقشه",
            //       style: TextStyle(
            //         fontFamily: 'IRANSans',
            //         fontSize: 12.0,
            //       )),
            //   onTap: () {
            //     Navigator.of(context).push(
            //       PageRouteBuilder(
            //         transitionDuration: Duration(milliseconds: 0),
            //         pageBuilder: (context, animation1, animation2) {
            //           return TCoordinateFrm();
            //           // return TMap(
            //           //   coordinates: ,
            //           //   latLngs: <LatLng>[
            //           //     LatLng(35.754180, 51.409707),
            //           //     LatLng(35.755373, 51.409063),
            //           //   ],
            //           //   onTap: (latLng) {},
            //           // );
            //         },
            //       ),
            //     );
            //   },
            // ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text("تنظیمات",
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  )),
              onTap: () {
                Navigator.of(context).push(
                  PageRouteBuilder(
                    transitionDuration: Duration(milliseconds: 0),
                    pageBuilder: (context, animation1, animation2) {
                      return TSettingFrm();
                    },
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.location_on),
              title: Text("ارسال لوکیشن به سرور",
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  )),
              onTap: () {
                TLocation.callLocationDataModalMode(context,
                    onDoneLocationData: (locationData) {
                  WebService.callModalMode<Coordinate>(
                    context,
                    value: Coordinate(),
                    futureResponse: () {
                      return TRestful.post(
                        "sale/coordinates",
                        setting: setting,
                        body: json.encode({
                          "customerID": null,
                          "latitude": locationData.latitude,
                          "longitude": locationData.longitude,
                        }),
                        timeout: 25,
                      );
                    },
                    waitingMsg: "ذخیره موقعیت جغرافیایی",
                    doneMsg: "ذخیره موقعیت جغرافیایی",
                    on200object: (coordinate) {},
                  );
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Setting setting = TIW.getSetting(context);
    return GridView(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 20.0,
        mainAxisSpacing: 20.0,
      ),
      children: <Widget>[
        GridTileView.defaultView(Icons.note_add, "ایجاد سند",
            setting.currentSystem.themeData.primaryColor, () {
          Navigator.of(context).push(
            PageRouteBuilder(
              transitionDuration: Duration(milliseconds: 0),
              settings: RouteSettings(name: "saleVoucher"),
              pageBuilder: (context, animation1, animation2) {
                return SaleVoucherFrm(
                  initialIndex: 0,
                );
              },
            ),
          );
        }),
        GridTileView.defaultView(Icons.description, "اسناد ثبت شده",
            setting.currentSystem.themeData.primaryColor, () {
          Navigator.of(context).push(
            PageRouteBuilder(
              transitionDuration: Duration(milliseconds: 0),
              pageBuilder: (context, animation1, animation2) {
                return SaleVoucherHeadersFrm();
              },
            ),
          );
        }),
        setting.sale.saleSettingServer.systemSetup.canCreateCustomerInAndroid?
        GridTileView.defaultView(Icons.person_add, "افزودن مشتری",
            setting.currentSystem.themeData.primaryColor, () {
          Navigator.of(context).push(
            PageRouteBuilder(
              transitionDuration: Duration(milliseconds: 0),
              pageBuilder: (context, animation1, animation2) {
                return CustomerAddFrm();
              },
            ),
          );
        }):Container()
      ],
    );
  }
}
