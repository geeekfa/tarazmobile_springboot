import 'package:com_tarazgroup/models/crystal_pdf_type.dart';
import 'package:com_tarazgroup/models/goods_group.dart';
import 'package:com_tarazgroup/models/query_param.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher_detail.dart';
import 'package:com_tarazgroup/modules/dialog/alert_dialog.dart';
import 'package:com_tarazgroup/modules/dialog/input_dialog.dart';
import 'package:com_tarazgroup/modules/dialog/radiolist_dialog.dart';
import 'package:com_tarazgroup/modules/dialog/report_dialog.dart';
import 'package:com_tarazgroup/modules/multipleview/multiple_view.dart';
import 'package:com_tarazgroup/modules/tableview/tcolumn.dart';
import 'package:com_tarazgroup/modules/textfield/ttextfield.dart';
import 'package:com_tarazgroup/modules/tlib/tlib.dart';
import 'package:com_tarazgroup/modules/view/voucher_detail_list_tile_plus_minus.dart';
import 'package:com_tarazgroup/sale/adding_1_quantity_refer.dart';
import 'package:com_tarazgroup/sale/sale_voucher_frm.dart';
import 'package:flutter/material.dart';

// این فرم برای نمایش کلیه ردیف های یک نوع مرجع خاص است
// برای حالت مرجع ردیفی

class DetailReferGoods extends StatefulWidget {
  final int systemID;
  final int storeID;
  final int voucherTypeID;
  final int refTypeID;
  final String voucherTypeDesc;
  final GoodsGroup goodsGroup;
  const DetailReferGoods({
    @required this.systemID,
    @required this.storeID,
    @required this.voucherTypeID,
    @required this.refTypeID,
    @required this.voucherTypeDesc,
    @required this.goodsGroup,
  });

  @override
  _DetailReferGoodsState createState() => _DetailReferGoodsState();
}

class _DetailReferGoodsState extends State<DetailReferGoods> {
  Setting setting;
  List<QueryParam> queryParams = [];
  String goodsDesc;
  String goodsCode;
  CrystalPdfType crystalPdfType = CrystalPdfType.ImageFeeRemain;

  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);

    queryParams.add(QueryParam(
        queryParamType: QueryParamType.NUMBER,
        name: "sortField.name",
        value: "goodsDesc"));

    queryParams.add(QueryParam(
        queryParamType: QueryParamType.NUMBER,
        name: "voucherTypeID",
        value: widget.voucherTypeID.toString()));
    queryParams.add(QueryParam(
        queryParamType: QueryParamType.NUMBER,
        name: "refTypeID",
        value: widget.refTypeID.toString()));
    queryParams.add(QueryParam(
        queryParamType: QueryParamType.NUMBER,
        name: "storeID",
        value: widget.storeID.toString()));
    if (widget.goodsGroup != null) {
      queryParams.add(QueryParam(
          queryParamType: QueryParamType.TEXT,
          name: "groupID",
          value: widget.goodsGroup.groupID.toString()));
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
        data: setting.currentSystem.themeData,
        child: Scaffold(
            body: WillPopScope(
                child: MultipleView<VoucherDetail>(
                  initialIndex: 0,
                  title: "کالاهای مرجع",
                  subtitle: widget.voucherTypeDesc,
                  value: VoucherDetail(
                    0,
                    goods: null,
                    goodsUnit: null,
                    quantity: null,
                    status: null,
                    tSerials: null,
                    voucherDetailID: null,
                    voucherHeaderID: null,
                  ),
                  filterWidgets: <Widget>[
                    TTextField(
                      title: "نام کالا",
                      onValue: () {
                        return goodsDesc;
                      },
                      onChanged: (txt) {
                        goodsDesc = txt;
                      },
                    ),
                    TTextField(
                      title: "کد کالا",
                      onValue: () {
                        return goodsCode;
                      },
                      onChanged: (txt) {
                        goodsCode = txt;
                      },
                    ),
                  ],
                  onFiltered: () {
                    List<QueryParam> queryParams = [];

                    if (this.goodsDesc != null && this.goodsDesc != "") {
                      queryParams.add(QueryParam(
                          queryParamType: QueryParamType.TEXT,
                          name: "goodsDesc",
                          value: this.goodsDesc));
                    }
                    if (this.goodsCode != null && this.goodsCode != "") {
                      queryParams.add(QueryParam(
                          queryParamType: QueryParamType.TEXT,
                          name: "goodsCode",
                          value: this.goodsCode));
                    }
                    return queryParams;
                  },
                  onDeleteFilter: () {
                    goodsDesc = null;
                    goodsCode = null;
                  },
                  onReport: () {
                    List<QueryParam> queryParams1 = [...queryParams];
                    if (this.goodsDesc != null && this.goodsDesc != "") {
                      queryParams1.add(QueryParam(
                          queryParamType: QueryParamType.TEXT,
                          name: "goodsDesc",
                          value: this.goodsDesc));
                    }
                    if (this.goodsCode != null && this.goodsCode != "") {
                      queryParams1.add(QueryParam(
                          queryParamType: QueryParamType.TEXT,
                          name: "goodsCode",
                          value: this.goodsCode));
                    }
                    List<TRadioItem<CrystalPdfType>> tRadioItems = [];
                    tRadioItems.add(TRadioItem<CrystalPdfType>(
                        desc: "عکس - فی - موجودی",
                        value: CrystalPdfType.ImageFeeRemain));
                    tRadioItems.add(TRadioItem<CrystalPdfType>(
                        desc: "عکس - فی", value: CrystalPdfType.ImageFee));
                    tRadioItems.add(TRadioItem<CrystalPdfType>(
                        desc: "فی - موجودی", value: CrystalPdfType.FeeRemain));
                    tRadioItems.add(TRadioItem<CrystalPdfType>(
                        desc: "فی", value: CrystalPdfType.Fee));

                    TRadioListDialog<CrystalPdfType>(
                            context: context,
                            value: crystalPdfType,
                            tRadioItems: tRadioItems)
                        .show("نوع گزارش")
                        .then((crystalPdfType) {
                      queryParams1.add(QueryParam(
                          queryParamType: QueryParamType.TEXT,
                          name: "type",
                          value: crystalPdfType.toString().split('.').last));
                      TReportDialog(
                              context: context,
                              title: 'گزارش لیست کالاها',
                              pdfSetting: PdfSetting(
                                  resource: resource(widget.systemID) + "/pdf",
                                  queryParams: queryParams1),
                              excelSetting: ExcelSetting(
                                  resource:
                                      resource(widget.systemID) + "/excel",
                                  queryParams: queryParams1))
                          .show();
                    });
                  },
                  on204: () {},
                  onTabChanged: (tabIndex) {
                    goodsDesc = null;
                    goodsCode = null;
                  },
                  queryParams: queryParams,
                  resource: resource(widget.systemID),
                  listViewItemBuilder: (item, index) {
                    return VoucherDetailListTilePlusMinus(
                        voucherDetail: item,
                        onTapPlus: () {
                          // audioCache.play("sounds/plus.mp3", volume: 1.0);
                          onTapPlus(item);
                        },
                        onTapMinus: () {
                          // audioCache.play("sounds/minus.mp3", volume: 1.0);
                          onTapMinus(item);
                        });
                  },
                  mainSectionBuilder: null,
                  detailSectionBuilder: null,
                  tableViewCellBuilder: (tColumn, voucherDetail) {
                    switch (tColumn.name) {
                      case "goodsDesc":
                        return Text(voucherDetail.goods.goodsDesc,
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              fontFamily: 'IRANSans',
                              fontSize: 12.0,
                            ));
                        break;
                      case "goodsCode":
                        return Text(voucherDetail.goods.goodsCode,
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              fontFamily: 'IRANSans',
                              fontSize: 12.0,
                            ));
                        break;
// بخاطر فارمد فیلد زیر برداشته شده است وگرنه باید درست شود در آینده
                      // case "fee":
                      //   return Text(
                      //       voucherDetail.fee == null
                      //           ? "-"
                      //           : TLib.commaSeparate(
                      //               voucherDetail.fee.toString()),
                      //       overflow: TextOverflow.ellipsis,
                      //       softWrap: true,
                      //       textAlign: TextAlign.end,
                      //       style: TextStyle(
                      //         fontFamily: 'IRANSans',
                      //         fontSize: 12.0,
                      //       ));

                      //   break;
                      case "feeAgreement":
                        return Text(
                            voucherDetail.feeAgreement == null
                                ? "-"
                                : TLib.commaSeparate(
                                    voucherDetail.feeAgreement.toString()),
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              fontFamily: 'IRANSans',
                              fontSize: 12.0,
                            ));

                        break;
                      case "invRemain":
                        return Text(
                            voucherDetail.invRemain == null
                                ? "-"
                                : TLib.commaSeparate(
                                    voucherDetail.invRemain.toString()),
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            textAlign: TextAlign.end,
                            textDirection: TextDirection.ltr,
                            style: TextStyle(
                              fontFamily: 'IRANSans',
                              fontSize: 12.0,
                            ));

                        break;
                      case "remainQ":
                        return Text(
                            voucherDetail.remainQ == null
                                ? "-"
                                : TLib.commaSeparate(
                                    voucherDetail.remainQ.toString()),
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            textAlign: TextAlign.end,
                            textDirection: TextDirection.ltr,
                            style: TextStyle(
                              fontFamily: 'IRANSans',
                              fontSize: 12.0,
                            ));

                        break;
                      case "unitDesc":
                        return Text(voucherDetail.goodsUnit.unitDesc,
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              fontFamily: 'IRANSans',
                              fontSize: 12.0,
                            ));

                        break;
                      case "storename":
                        return Text(voucherDetail.goods.storeName ?? "",
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              fontFamily: 'IRANSans',
                              fontSize: 12.0,
                            ));
                        break;
                      default:
                        return Text("");
                    }
                  },
                  tableViewColumns: <TColumn>[
                    TColumn(
                      name: "goodsDesc",
                      title: "نام کالا",
                      width: 150,
                    ),
                    TColumn(
                      name: "goodsCode",
                      title: "کد کالا",
                      width: 80,
                    ),
                    // بخاطر فارمد فیلد زیر برداشته شده است وگرنه باید درست شود در آینده
                    // TColumn(
                    //   name: "fee",
                    //   title: "فی",
                    //   width: 70,
                    // ),
                    TColumn(
                      name: "feeAgreement",
                      title: "فی برآوردی",
                      width: 100,
                    ),
                    TColumn(
                      name: "invRemain",
                      title: "موجودی",
                      width: 70,
                    ),
                    TColumn(
                      name: "remainQ",
                      title: "مانده مرجع",
                      width: 70,
                    ),
                    TColumn(
                      name: "unitDesc",
                      title: "واحد",
                      width: 120,
                    ),
                    TColumn(
                      name: "storename",
                      title: "انبار",
                      width: 170,
                    ),
                  ],
                  onTap: (voucherDetail) {
                    bool isFeeAgrToFeeInRefer = setting.sale.saleSettingServer
                        .systemSetup.isFeeAgrToFeeInRefer;
                    if (isFeeAgrToFeeInRefer &&
                        (voucherDetail.feeAgreement == null ||
                            voucherDetail.feeAgreement == 0)) {
                      TAlertDialog(context: context).show(
                          title: "فی برآوردی صفر است",
                          content: "فی برآوردی باید بیشتر از صفر باشد");
                    } else if (!isFeeAgrToFeeInRefer &&
                        (voucherDetail.fee == null || voucherDetail.fee == 0)) {
                      TAlertDialog(context: context).show(
                          title: "فی صفر است",
                          content: "فی باید بیشتر از صفر باشد");
                    } else {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                AddingQuantityRefer(
                                  index: -1,
                                  fee: isFeeAgrToFeeInRefer
                                      ? voucherDetail.feeAgreement
                                      : voucherDetail.fee,
                                  goods: voucherDetail.goods,
                                  goodsUnit: voucherDetail.goodsUnit,
                                  quantity: voucherDetail.remainQ,
                                  voucherDetailID:
                                      voucherDetail.voucherDetailID,
                                  parentWidget: () {
                                    return SaleVoucherFrm(
                                      initialIndex: 0,
                                    );
                                  },
                                )),
                      );
                    }
                  },
                ),
                onWillPop: () {
                  if (widget.goodsGroup == null) {
                    //یعنی از جانب خود فرم سند به اینجا رسیدیم
                    Navigator.popUntil(
                      context,
                      ModalRoute.withName('/saleHome'),
                    );
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => SaleVoucherFrm(
                                initialIndex: 0,
                              )),
                    );
                  } else {
                    // یعنی از جانب فرم گروه کالا به اینجا رسیدیم
                  }
                  return new Future.value(true);
                })));
  }

  void onTapPlus(VoucherDetail voucherDetail) {
    // if (goods.price == null || goods.price == 0) {
    //   TAlertDialog(context: context).show(
    //       title: "قیمت مجاز نیست", content: "قیمت باید بیشتر از صفر باشد");
    // } else if (goods.goodsUnit.computeValue == null ||
    //     goods.goodsUnit.computeValue == 0) {
    //   TAlertDialog(context: context).show(
    //       title: "ضریب واحد کالا مجاز نیست",
    //       content: "ضریب واحد کالا باید بیشتر از صفر باشد");
    // } else {
    //   Voucher voucher = TIW.getVoucher(context);
    //   bool isExistInVoucherDetail =
    //       false; //آیا این کالا در حال حاضر وجود دارد در ووچر دیتیل ؟
    //   for (var i = 0; i < voucher.voucherDetails.length; i++) {
    //     if (goods.goodsID == voucher.voucherDetails[i].goods.goodsID &&
    //         goods.goodsUnit.unitID ==
    //             voucher.voucherDetails[i].goodsUnit.unitID &&
    //         goods.storeID == voucher.voucherDetails[i].goods.storeID) {
    //       isExistInVoucherDetail = true;
    //       voucher.voucherDetails[i].quantity =
    //           voucher.voucherDetails[i].quantity + 1;
    //       break;
    //     }
    //   }
    //   if (!isExistInVoucherDetail) {
    //     GoodsUnit goodsUnit;
    //     if (goods.goodsUnit.computeValue.toInt() == 1) {
    //       //یعنی واحد پیش فرض همان واحد اصلی است
    //       goodsUnit = GoodsUnit(
    //         computeValue: goods.goodsUnit.computeValue,
    //         secUnitID: goods.goodsUnit.secUnitID,
    //         secUnitDesc: goods.goodsUnit.secUnitDesc,
    //         unitDesc: goods.goodsUnit.unitDesc,
    //       );
    //     } else {
    //       //یعنی واحد پیش فرض  واحد فرعی است
    //       goodsUnit = GoodsUnit(
    //         computeValue: goods.goodsUnit.computeValue,
    //         secUnitID: goods.goodsUnit.defaultSecUnitID,
    //         secUnitDesc: goods.goodsUnit.secUnitDesc,
    //         unitDesc: goods.goodsUnit.secUnitDesc,
    //       );
    //     }
    //     goods.goodsUnit.secUnitID = goods.goodsUnit.defaultSecUnitID;
    //     voucher.voucherDetails.add(VoucherDetail(
    //       -1,
    //       status: VoucherDetailStatus.DEFAULT_ACTIVE,
    //       goods: goods,
    //       goodsUnit: goodsUnit,
    //       quantity: 1,
    //       fee: goods.price,
    //     ));
    //   }
    //   TIW.setVoucher(context, voucher);
    // }
  }

  void onTapMinus(VoucherDetail voucherDetail) {
    // if (goods.price == null || goods.price == 0) {
    //   TAlertDialog(context: context).show(
    //       title: "قیمت مجاز نیست", content: "قیمت باید بیشتر از صفر باشد");
    // } else if (goods.goodsUnit.computeValue == null ||
    //     goods.goodsUnit.computeValue == 0) {
    //   TAlertDialog(context: context).show(
    //       title: "ضریب واحد کالا مجاز نیست",
    //       content: "ضریب واحد کالا باید بیشتر از صفر باشد");
    // } else {
    //   Voucher voucher = TIW.getVoucher(context);
    //   for (var i = 0; i < voucher.voucherDetails.length; i++) {
    //     if (goods.goodsID == voucher.voucherDetails[i].goods.goodsID &&
    //         goods.goodsUnit.unitID ==
    //             voucher.voucherDetails[i].goodsUnit.unitID &&
    //         goods.storeID == voucher.voucherDetails[i].goods.storeID &&
    //         voucher.voucherDetails[i].quantity > 1) {
    //       voucher.voucherDetails[i].quantity =
    //           voucher.voucherDetails[i].quantity - 1;
    //       break;
    //     }
    //   }
    //   TIW.setVoucher(context, voucher);
    // }
  }
  String resource(int systemID) {
    switch (systemID) {
      case 6:
        return "sale/vouchers/details/reference";
      case 12:
        return "inv/vouchers/details/reference";
      case 14:
        return "buy/purchase/details/reference";
    }
    return null;
  }
}
