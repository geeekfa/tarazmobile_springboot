import 'dart:convert';

// import 'package:audioplayers/audio_cache.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:com_tarazgroup/models/adding_goods_destination.dart';
import 'package:com_tarazgroup/models/coordinate.dart';
import 'package:com_tarazgroup/models/customer.dart';
import 'package:com_tarazgroup/models/goods.dart';
import 'package:com_tarazgroup/models/goods_group.dart';
import 'package:com_tarazgroup/models/goods_unit.dart';
import 'package:com_tarazgroup/models/promotion.dart';
import 'package:com_tarazgroup/models/promotion_element.dart';
import 'package:com_tarazgroup/models/query_param.dart';
import 'package:com_tarazgroup/models/sale_setting.dart';
import 'package:com_tarazgroup/models/sale_type.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/store.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher.dart';
import 'package:com_tarazgroup/models/voucher_detail.dart';
import 'package:com_tarazgroup/models/voucher_type.dart';
import 'package:com_tarazgroup/models/xFieldsSetup.dart';
import 'package:com_tarazgroup/modules/dialog/alert_dialog.dart';
import 'package:com_tarazgroup/modules/dialog/confirm_dialog.dart';
import 'package:com_tarazgroup/modules/future/location/tlocation.dart';
import 'package:com_tarazgroup/modules/future/webservice.dart';
import 'package:com_tarazgroup/modules/goods/goods_details_frm.dart';
import 'package:com_tarazgroup/modules/multipleview/multiple_view.dart';
import 'package:com_tarazgroup/modules/qkey/qkey_filter.dart';
import 'package:com_tarazgroup/modules/rest/restful.dart';
import 'package:com_tarazgroup/modules/tableview/tcolumn.dart';
import 'package:com_tarazgroup/modules/textfield/ttextfield.dart';
import 'package:com_tarazgroup/modules/tlib/tlib.dart';
import 'package:com_tarazgroup/modules/view/goods_grid_tile_plus_minus.dart';
import 'package:com_tarazgroup/modules/view/goods_list_tile_plus_minus_sale.dart';
import 'package:com_tarazgroup/modules/view/list_tile_view.dart';
import 'package:com_tarazgroup/modules/voucher/voucher_frm.dart';
import 'package:com_tarazgroup/sale/adding_1_goods_unit.dart';
import 'package:com_tarazgroup/sale/detail_refer_goods.dart';
import 'package:com_tarazgroup/sale/inv-store.dart';
import 'package:com_tarazgroup/sale/promotion_element_frm.dart';
import 'package:com_tarazgroup/sale/sale_goods.dart';
import 'package:com_tarazgroup/sale/sale_voucher_header_refer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart';
import 'package:location/location.dart';
import 'package:shamsi_date/shamsi_date.dart';

class SaleVoucherFrm extends StatefulWidget {
  final int initialIndex;

  const SaleVoucherFrm({
    @required this.initialIndex,
  });
  @override
  _SaleVoucherFrmState createState() => _SaleVoucherFrmState();
}

class _SaleVoucherFrmState extends State<SaleVoucherFrm> {
  Setting setting;
  Voucher voucher;
  // AudioCache audioCache;
  //Filter
  String voucherTypeDesc;
  String voucherTypeDescRefer;
  String storeName;
  String customerName;

  String goodsDesc;
  String goodsCode;

  GoodsGroup goodsGroup = GoodsGroup();
  String groupDesc;

  LocationData locationData;

  String saleTypeDesc;

  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    // audioCache = AudioCache();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    voucher = TIW.getVoucher(context);
    return VoucherFrm(
      voucherType:
          -1, // در حال حاضر چون برای سیستم فروش مرجع نداریم منهای یک دادم
      routeName: '/saleHome',
      initialIndex: widget.initialIndex,
      title: "فروش",
      onSend: () async {
        bool yesNo = true;
        bool isUnlockRefByBarcode =
            voucher.voucherHeader.voucherType.isUnlockRefByBarcode;
        if (isUnlockRefByBarcode != null && isUnlockRefByBarcode) {
          List<VoucherDetail> vd =
              voucher.voucherDetails.where((voucherDetail) {
            return voucherDetail.quantity > 0;
          }).toList();
          if (vd.length == 0) {
            TAlertDialog(context: context).show(
                title: "عدم اسکن کالا", content: "هیچ کالایی اسکن نشده است!");
            return;
          } else if (vd.length < voucher.voucherDetails.length) {
            yesNo = await TConfirmDialog(
              context: context,
            ).show("اسکن نشدن برخی کالا ها",
                "برخی از کالا ها اسکن نشده اند و در نتیجه از سند حذف می شوند . آیا ادامه می دهید؟");
            if (yesNo) {
              voucher.voucherDetails.retainWhere((voucherDetail) {
                return voucherDetail.quantity > 0;
              });
            } else {
              return;
            }
          }
        }

        if (TIW
            .getSetting(context)
            .currentSystem
            .saleSettingClient
            .mustSendLocation) {
          TLocation.callLocationDataModalMode(context,
              onDoneLocationData: (locationData) {
            this.locationData = locationData;
            insertCoordinate();
          });
        } else {
          computePromotionAndInsertVoucher();
        }
      },
      onDelete: () {
        Navigator.popUntil(
          context,
          ModalRoute.withName('/saleHome'),
        );
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => SaleVoucherFrm(
                    initialIndex: 0,
                  )),
        );
      },
      defaultTab: <Widget>[
        QKeyFilter<VoucherType>(
          onValue: () {
            return voucher.voucherHeader.voucherType;
          },
          title: "نوع سند",
          resource: "pub/vouchertypes",
          // recCnt: 50,
          queryParams: List<QueryParam>.generate(2, (index) {
            if (index == 0) {
              return QueryParam(
                  queryParamType: QueryParamType.NUMBER,
                  name: "systemID",
                  value: setting.currentSystem.systemID.toString());
            } else {
              String voucherTypeIDs = "";
              setting.currentSystem.saleSettingServer.saleDefaultVouchers
                  .forEach((saleDefaultVoucher) {
                voucherTypeIDs +=
                    saleDefaultVoucher.voucherTypeID.toString() + ",";
              });
              voucherTypeIDs =
                  voucherTypeIDs.substring(0, voucherTypeIDs.length - 1);
              return QueryParam(
                  queryParamType: QueryParamType.TEXT,
                  name: "voucherTypeIDs",
                  value: voucherTypeIDs);
            }
          }),
          itemBuilder: (item, index) {
            return ListTileView.view1_1(
              index,
              item.voucherTypeDesc,
            );
          },
          onChangePermission: () async {
            if (voucher.voucherDetails.length > 0) {
              bool yesNo = await TConfirmDialog(
                context: context,
              ).show("حذف کالاهای فعلی سند",
                  "با تغییر نوع سند ، کالاهای فعلی سند حذف خواهند شد . آیا ادامه می دهید؟");
              if (yesNo) {
                return true;
              } else {
                return false;
              }
            }
            return true;
          },
          onSelect: (voucherType) {
            List<SaleDefaultVoucher> saleDefaultVouchers = setting
                .currentSystem.saleSettingServer.saleDefaultVouchers
                .where((saleDefaultVoucher) {
              return saleDefaultVoucher.voucherTypeID ==
                  voucherType.voucherTypeID;
            }).toList();
            SaleDefaultVoucher saleDefaultVoucher = saleDefaultVouchers[0];
            voucher.voucherHeader.voucherType = voucherType;
            voucher.voucherHeader.deliverCenterID =
                saleDefaultVoucher.deliverCenterID;
            voucher.voucherHeader.saleCenterID =
                saleDefaultVoucher.saleCenterID;
            voucher.voucherHeader.saleTypeID = saleDefaultVoucher.saleTypeID;
            voucher.voucherHeader.marketingManID =
                setting.perComID; // آی دی بازاریاب
            voucher.voucherHeader.salesManID =
                setting.perComID; // آی دی بازاریاب

            if (saleDefaultVoucher.paymentWayIDs == null ||
                saleDefaultVoucher.paymentWayIDs == "") {
              voucher.voucherHeader.paymentWayID = null;
            } else {
              List<String> paymentWayIDs =
                  saleDefaultVoucher.paymentWayIDs.split(",");
              voucher.voucherHeader.paymentWayID = int.parse(paymentWayIDs[0]);
            }

            voucher.voucherHeader.center1ID = saleDefaultVoucher.center1ID;
            voucher.voucherHeader.center2ID = saleDefaultVoucher.center2ID;
            voucher.voucherHeader.center3ID = saleDefaultVoucher.center3ID;

            voucher.voucherHeader.voucherTypeRefer = VoucherType();
            voucher.voucherHeader.referNumber = null;
            voucher.voucherHeader.referDate = null;
            voucher.voucherDetails = [];
            TIW.setVoucher(context, voucher);

            TIW.setXFieldsSetups(context, []);
            WebService.callModalMode<XFieldsSetup>(context,
                value: XFieldsSetup(),
                futureResponse: _futureResponseXfieldssetups,
                waitingMsg: "فیلد های اختیاری",
                doneMsg: "فیلد های اختیاری", on204: () {
              Navigator.popUntil(
                context,
                ModalRoute.withName('/saleHome'),
              );
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => SaleVoucherFrm(
                          initialIndex: 0,
                        )),
              );
            }, on200list: (xFieldsSetups) {
              TIW.setXFieldsSetups(context, xFieldsSetups);
              Navigator.popUntil(
                context,
                ModalRoute.withName('/saleHome'),
              );
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => SaleVoucherFrm(
                          initialIndex: 0,
                        )),
              );
            });
          },
          filterWidgets: <Widget>[
            TTextField(
              title: "نوع سند",
              onValue: () {
                return voucherTypeDesc;
              },
              onChanged: (txt) {
                voucherTypeDesc = txt;
              },
            ),
          ],
          onFiltered: () {
            List<QueryParam> queryParams = [];
            if (this.voucherTypeDesc != null && this.voucherTypeDesc != "") {
              queryParams.add(QueryParam(
                  queryParamType: QueryParamType.TEXT,
                  name: "voucherTypeDesc",
                  value: this.voucherTypeDesc));
            }
            return queryParams;
          },
          onDeleteFilter: () {
            voucherTypeDesc = null;
          },
        ),
        setting.sale.saleSettingServer.systemSetup.isStoreInDet
            ? Container()
            : QKeyFilter<Store>(
                onValue: () {
                  return voucher.voucherHeader.store;
                },
                title: "انبار",
                resource: "inv/getstoreuserwebs",
                // recCnt: 15,
                queryParams: <QueryParam>[
                  QueryParam(
                    queryParamType: QueryParamType.NUMBER,
                    name: "userID",
                    value: setting.perComID.toString(),
                  )
                ],
                itemBuilder: (item, index) {
                  return ListTileView.view1_1(
                    index,
                    item.storeName,
                  );
                },
                onChangePermission: () async {
                  if (voucher.voucherDetails.length > 0) {
                    bool yesNo = await TConfirmDialog(
                      context: context,
                    ).show("حذف کالاهای فعلی سند",
                        "با تغییر انبار ، کالاهای فعلی سند حذف خواهند شد . آیا ادامه می دهید؟");
                    if (yesNo) {
                      return true;
                    } else {
                      return false;
                    }
                  }
                  return true;
                },
                onSelect: (store) {
                  voucher.voucherHeader.store = store;
                  voucher.voucherHeader.voucherTypeRefer = VoucherType();
                  voucher.voucherHeader.referNumber = null;
                  voucher.voucherHeader.referDate = null;
                  voucher.voucherDetails = [];
                  TIW.setVoucher(context, voucher);
                  Navigator.popUntil(
                    context,
                    ModalRoute.withName('/saleHome'),
                  );
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => SaleVoucherFrm(
                              initialIndex: 0,
                            )),
                  );
                },
                filterWidgets: <Widget>[
                  TTextField(
                    title: "انبار",
                    onValue: () {
                      return storeName;
                    },
                    onChanged: (txt) {
                      storeName = txt;
                    },
                  ),
                ],
                onFiltered: () {
                  List<QueryParam> queryParams = [];
                  if (this.storeName != null && this.storeName != "") {
                    queryParams.add(QueryParam(
                        queryParamType: QueryParamType.TEXT,
                        name: "storeName",
                        value: this.storeName));
                  }
                  return queryParams;
                },
                onDeleteFilter: () {
                  storeName = null;
                },
              ),
        QKeyFilter<Customer>(
          onValue: () {
            return voucher.voucherHeader.customer;
          },
          title: "مشتری",
          resource: "sale/customers1",
          // recCnt: 15,
          queryParams: <QueryParam>[
            QueryParam(
              queryParamType: QueryParamType.NUMBER,
              name: "voucherTypeID",
              value: voucher.voucherHeader.voucherType.voucherTypeID.toString(),
            ),
            QueryParam(
              queryParamType: QueryParamType.NUMBER,
              name: "storeID",
              value: voucher.voucherHeader.store.storeID == null
                  ? ""
                  : voucher.voucherHeader.store.storeID.toString(),
            )
          ],
          itemBuilder: (item, index) {
            return ListTileView.view1_2_1(
                index, item.customerName, item.perComAddress ?? "");
          },
          onChangePermission: () async {
            if (voucher.voucherHeader.voucherType.voucherTypeID == null) {
              TAlertDialog(context: context).show(
                  title: "نوع سند انتخاب نشده است",
                  content: "برای انتخاب مشتری ، نوع سند را انتخاب کنید");
              return false;
            }
            if (voucher.voucherDetails.length > 0) {
              bool yesNo = await TConfirmDialog(
                context: context,
              ).show("حذف کالاهای فعلی سند",
                  "با تغییر مشتری ، کالاهای فعلی سند حذف خواهند شد . آیا ادامه می دهید؟");
              if (yesNo) {
                return true;
              } else {
                return false;
              }
            }
            return true;
          },
          onSelect: (customer) {
            voucher.voucherHeader.customer = customer;
            voucher.voucherHeader.voucherTypeRefer = VoucherType();
            voucher.voucherHeader.referNumber = null;
            voucher.voucherHeader.referDate = null;
            voucher.voucherDetails = [];
            TIW.setVoucher(context, voucher);
            Navigator.popUntil(
              context,
              ModalRoute.withName('/saleHome'),
            );
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => SaleVoucherFrm(
                        initialIndex: 0,
                      )),
            );
          },
          filterWidgets: <Widget>[
            TTextField(
              title: "مشتری",
              onValue: () {
                return customerName;
              },
              onChanged: (txt) {
                customerName = txt;
              },
            ),
          ],
          onFiltered: () {
            List<QueryParam> queryParams = [];
            if (this.customerName != null && this.customerName != "") {
              queryParams.add(QueryParam(
                  queryParamType: QueryParamType.TEXT,
                  name: "customerName",
                  value: this.customerName));
            }
            return queryParams;
          },
          onDeleteFilter: () {
            customerName = null;
          },
        ),
        !setting.sale.saleSettingServer.systemSetup
                .isShowSaleTypeInVoucherHeaderInAndroid
            ? Container()
            : QKeyFilter<SaleType>(
                onValue: () {
                  return voucher.voucherHeader.saleType;
                },
                title: "نوع فروش",
                resource: "sale/saletypes",
                // recCnt: 15,
                queryParams: <QueryParam>[
                  // QueryParam(
                  //   queryParamType: QueryParamType.TEXT,
                  //   name: "saleTypeDesc",
                  //   value: voucher.voucherHeader.saleType.saleTypeDesc,
                  // )
                ],
                itemBuilder: (item, index) {
                  return ListTileView.view1_1(
                    index,
                    item.saleTypeDesc,
                  );
                },
                onChangePermission: () async {
                  if (voucher.voucherDetails.length > 0) {
                    bool yesNo = await TConfirmDialog(
                      context: context,
                    ).show("حذف کالاهای فعلی سند",
                        "با تغییر نوع سند ، کالاهای فعلی سند حذف خواهند شد . آیا ادامه می دهید؟");
                    if (yesNo) {
                      return true;
                    } else {
                      return false;
                    }
                  }
                  return true;
                },
                onSelect: (saleType) {
                  voucher.voucherHeader.saleType = saleType;
                  voucher.voucherDetails = [];
                  TIW.setVoucher(context, voucher);
                  Navigator.popUntil(
                    context,
                    ModalRoute.withName('/saleHome'),
                  );
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => SaleVoucherFrm(
                              initialIndex: 0,
                            )),
                  );
                },
                filterWidgets: <Widget>[
                  TTextField(
                    title: "نوع فروش",
                    onValue: () {
                      return saleTypeDesc;
                    },
                    onChanged: (txt) {
                      saleTypeDesc = txt;
                    },
                  ),
                ],
                onFiltered: () {
                  List<QueryParam> queryParams = [];
                  if (this.saleTypeDesc != null && this.saleTypeDesc != "") {
                    queryParams.add(QueryParam(
                        queryParamType: QueryParamType.TEXT,
                        name: "saleTypeDesc",
                        value: this.saleTypeDesc));
                  }
                  return queryParams;
                },
                onDeleteFilter: () {
                  saleTypeDesc = null;
                },
              )
      ],
      referTab: <Widget>[
        QKeyFilter<VoucherType>(
          onValue: () {
            return voucher.voucherHeader.voucherTypeRefer;
          },
          title: "نوع مرجع",
          resource: "pub/vouchertypes/" +
              voucher.voucherHeader.voucherType.voucherTypeID.toString() +
              "/refers",
          // recCnt: 50,
          queryParams: <QueryParam>[
            // QueryParam(
            //     queryParamType: QueryParamType.NUMBER,
            //     name: "systemID",
            //     value: setting.currentSystem.systemID.toString()),
          ],
          itemBuilder: (item, index) {
            return ListTileView.view1_1(
              index,
              item.voucherTypeDesc,
            );
          },
          onChangePermission: () async {
            if (voucher.voucherHeader.voucherType.voucherTypeID == null) {
              TAlertDialog(context: context).show(
                  title: "نوع سند انتخاب نشده است",
                  content: "برای انتخاب نوع مرجع ، نوع سند را انتخاب کنید");
              return false;
            }
            if (voucher.voucherDetails.length > 0) {
              bool yesNo = await TConfirmDialog(
                context: context,
              ).show("حذف کالاهای فعلی سند",
                  "با تغییر نوع مرجع ، کالاهای فعلی سند حذف خواهند شد . آیا ادامه می دهید؟");
              if (yesNo) {
                return true;
              } else {
                return false;
              }
            }
            return true;
          },
          onSelect: (voucherType) {
            voucher.voucherHeader.voucherTypeRefer = voucherType;
            // voucher.voucherHeader.store = Store();
            // voucher.voucherHeader.customer = Customer();
            voucher.voucherHeader.referNumber = null;
            voucher.voucherHeader.referDate = null;
            voucher.voucherDetails = [];
            TIW.setVoucher(context, voucher);
            Navigator.popUntil(
              context,
              ModalRoute.withName('/saleHome'),
            );
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => SaleVoucherFrm(
                        initialIndex: 1,
                      )),
            );
          },
          filterWidgets: <Widget>[
            TTextField(
              title: "نوع مرجع",
              onValue: () {
                return voucherTypeDescRefer;
              },
              onChanged: (txt) {
                voucherTypeDescRefer = txt;
              },
            ),
          ],
          onFiltered: () {
            List<QueryParam> queryParams = [];
            if (this.voucherTypeDescRefer != null &&
                this.voucherTypeDescRefer != "") {
              queryParams.add(QueryParam(
                  queryParamType: QueryParamType.TEXT,
                  name: "voucherTypeDesc",
                  value: this.voucherTypeDescRefer));
            }
            return queryParams;
          },
          onDeleteFilter: () {
            voucherTypeDescRefer = null;
          },
        ),
        SizedBox(
          height: 50,
          child: ListTile(
            isThreeLine: true,
            dense: true,
            title: Text(
              "شماره مرجع",
              textAlign: TextAlign.right,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 11.0,
                color: Colors.black,
              ),
            ),
            subtitle: Text(
              voucher.voucherHeader.referNumber == null
                  ? ""
                  : voucher.voucherHeader.referNumber.toString(),
              textAlign: TextAlign.right,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 10.0,
                color: Colors.black,
              ),
            ),
          ),
        ),
        SizedBox(
          height: 50,
          child: ListTile(
            isThreeLine: true,
            dense: true,
            title: Text(
              "تاریخ مرجع",
              textAlign: TextAlign.right,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 11.0,
                color: Colors.black,
              ),
            ),
            subtitle: Text(
              voucher.voucherHeader.referDate == null
                  ? ""
                  : voucher.voucherHeader.referDate.toString(),
              textAlign: TextAlign.right,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 10.0,
                color: Colors.black,
              ),
            ),
          ),
        ),
      ],
      otherTab: <Widget>[
        TTextField(
          title: "توضیحات",
          maxLines: 5,
          onValue: () {
            return voucher.voucherHeader.voucherDesc;
          },
          onChanged: (voucherDesc) {
            voucher.voucherHeader.voucherDesc = voucherDesc;
            TIW.setVoucher(context, voucher);
          },
        ),
      ],
      sliverPersistentHeaderDelegateActions: <Widget>[
        ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: setting.currentSystem.themeData.primaryColor,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 0)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Icon(
                Icons.spa,
                size: 24,
                color: Colors.white,
              ),
              Text(
                "جایزه دستی",
                style: TextStyle(
                  fontFamily: 'IRANSansUltraLight',
                  fontSize: 12.0,
                  color: Colors.white,
                ),
              )
            ],
          ),
          onPressed: () {
            List<VoucherDetail> voucherDetails =
                TIW.getVoucher(context).voucherDetails;
            if (voucherDetails == null || voucherDetails.length == 0) {
              TAlertDialog(context: context).show(
                  title: "کالایی به این سند اضافه نشده است",
                  content:
                      "برای افزودن جایزه دستی ، به این سند کالا اضافه کنید");
            } else {
              showGoodsListForPromotion();
            }
          },
        ),
        ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: setting.currentSystem.themeData.primaryColor,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 0)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Icon(
                Icons.card_giftcard,
                size: 24,
                color: Colors.white,
              ),
              Text(
                "جایزه و عامل",
                style: TextStyle(
                  fontFamily: 'IRANSansUltraLight',
                  fontSize: 12.0,
                  color: Colors.white,
                ),
              )
            ],
          ),
          onPressed: () {
            List<VoucherDetail> voucherDetails =
                TIW.getVoucher(context).voucherDetails;
            if (voucherDetails == null || voucherDetails.length == 0) {
              TAlertDialog(context: context).show(
                  title: "کالایی به این سند اضافه نشده است",
                  content:
                      "برای محاسبه جایزه و عامل خودکار ، به این سند کالا اضافه کنید");
            } else {
              showGeneralDialog(
                context: context,
                pageBuilder: (BuildContext context, Animation<double> animation,
                    Animation<double> secondaryAnimation) {
                  return PromotionElementFrm();
                },
                barrierColor: Color.fromRGBO(255, 255, 255, 0.9),
                barrierDismissible: true,
                barrierLabel:
                    MaterialLocalizations.of(context).modalBarrierDismissLabel,
                transitionDuration: const Duration(milliseconds: 200),
              );
            }
          },
        ),
        ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: setting.currentSystem.themeData.primaryColor,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 0)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Icon(
                Icons.link,
                size: 24,
                color: Colors.white,
              ),
              Text(
                "سند های مرجع",
                style: TextStyle(
                  fontFamily: 'IRANSansUltraLight',
                  fontSize: 12.0,
                  color: Colors.white,
                ),
              )
            ],
          ),
          onPressed: () {
            if (voucher.voucherHeader.voucherTypeRefer.voucherTypeID == null) {
              TAlertDialog(context: context).show(
                  title: "نوع مرجع انتخاب نشده است",
                  content: "برای نمایش سند های مرجع ، نوع مرجع را انتخاب کنید");
            } else {
              showGeneralDialog(
                context: context,
                pageBuilder: (BuildContext context, Animation<double> animation,
                    Animation<double> secondaryAnimation) {
                  return SaleVoucherHeaderRefer();
                },
                barrierColor: Color.fromRGBO(255, 255, 255, 0.9),
                barrierDismissible: true,
                barrierLabel:
                    MaterialLocalizations.of(context).modalBarrierDismissLabel,
                transitionDuration: const Duration(milliseconds: 200),
              );
            }
          },
        ),
      ],
      buildVoucherDetails: () {
        if (voucher.voucherHeader.voucherTypeRefer.voucherTypeID == null) {
          // یعنی سند مرجع دار نیست
          return List.generate(voucher.voucherDetails.length, (index) {
            return ListTileView.voucherDetailListTileSale(
                context: context,
                index: index,
                voucherDetail: voucher.voucherDetails[index],
                parentWidget: () {
                  return SaleVoucherFrm(
                    initialIndex: 0,
                  );
                });
          });
        } else {
          // سند مرجع دار است
          // اگر متغیر زیر ترو باشد باید کالاهای سند با اسکن اضافه شود
          bool isUnlockRefByBarcode =
              voucher.voucherHeader.voucherType.isUnlockRefByBarcode;
          if (isUnlockRefByBarcode != null && isUnlockRefByBarcode) {
            return List.generate(voucher.voucherDetails.length, (index) {
              return ListTileView.voucherDetailListTileSaleUnlockByBarcode(
                  context: context,
                  index: index,
                  voucherDetail: voucher.voucherDetails[index],
                  parentWidget: () {
                    return SaleVoucherFrm(
                      initialIndex: 0,
                    );
                  });
            });
          } else {
            return List.generate(voucher.voucherDetails.length, (index) {
              return ListTileView.voucherDetailListTileSale(
                  context: context,
                  index: index,
                  voucherDetail: voucher.voucherDetails[index],
                  parentWidget: () {
                    return SaleVoucherFrm(
                      initialIndex: 0,
                    );
                  });
            });
          }
        }
      },
      onFooter: () {
        double totalPrice = 0;
        double totalGoods = 0;
        List<VoucherDetail> voucherDetails =
            TIW.getVoucher(context).voucherDetails;
        for (VoucherDetail voucherDetail in voucherDetails) {
          totalPrice += voucherDetail.quantity *
              voucherDetail.goodsUnit.computeValue *
              voucherDetail.fee;

          totalGoods += voucherDetail.quantity;
        }

        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Icon(
                    FontAwesomeIcons.dollarSign,
                    size: 16,
                  ),
                  Text(TLib.commaSeparate(totalPrice.toString()),
                      style: TextStyle(
                        fontFamily: 'IRANSansBlack',
                        fontSize: 14.0,
                      )),
                ],
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Icon(
                    FontAwesomeIcons.inbox,
                    size: 16,
                  ),
                  Text(TLib.commaSeparate(totalGoods.toString()),
                      style: TextStyle(
                        fontFamily: 'IRANSansBlack',
                        fontSize: 14.0,
                      )),
                ],
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Icon(
                    FontAwesomeIcons.hashtag,
                    size: 16,
                  ),
                  Text(voucher.voucherDetails.length.toString(),
                      style: TextStyle(
                        fontFamily: 'IRANSansBlack',
                        fontSize: 14.0,
                      )),
                ],
              ),
            )
          ],
        );

        // return ListTile(
        //   leading: Icon(Icons.access_alarms),
        // );
        // return ButtonBar(
        //   alignment: MainAxisAlignment.spaceBetween,
        //   children: <Widget>[
        //     Text("\$  " + TLib.commaSeparate(totalPrice.toString()),
        //         style: TextStyle(
        //           fontFamily: 'IRANSansBlack',
        //           fontSize: 16.0,
        //         )),
        //     Text(voucher.voucherDetails.length.toString() + "  #",
        //         style: TextStyle(
        //           fontFamily: 'IRANSansBlack',
        //           fontSize: 16.0,
        //         )),
        //     Text(voucher.voucherDetails.length.toString() + "  #",
        //         style: TextStyle(
        //           fontFamily: 'IRANSansBlack',
        //           fontSize: 16.0,
        //         ))
        //   ],
        // );
      },
      onPermissionOpenGoodsList: () {
        if (voucher.voucherHeader.voucherType.voucherTypeID == null) {
          TAlertDialog(context: context).show(
              title: "نوع سند انتخاب نشده است",
              content: "برای نمایش لیست کالاها ، نوع سند را انتخاب کنید");
          return false;
        }

        SaleSettingServer saleSettingServer = setting.sale.saleSettingServer;
        if (!saleSettingServer.systemSetup.isStoreInDet) {
          // یعنی انبار ردیفی نیست و باید حتما در هدر سند انتخاب شود
          if (saleSettingServer.systemSetup.isByStore == null ||
              !saleSettingServer.systemSetup.isByStore) {
            if (voucher.voucherHeader.store.storeID == null) {
              TAlertDialog(context: context).show(
                  title: "انبار انتخاب نشده است",
                  content: "برای نمایش لیست کالاها ، انبار را انتخاب کنید");
              return false;
            }
          }
        }

        if (voucher.voucherHeader.customer.customerID == null) {
          TAlertDialog(context: context).show(
              title: "مشتری انتخاب نشده است",
              content: "برای نمایش لیست کالاها ، مشتری را انتخاب کنید");
          return false;
        }

        return true;
      },
      onTapGoodsList: () {
        if (voucher.voucherHeader.voucherTypeRefer.voucherTypeID != null) {
          // یعنی مرجع ردیفی است و باید اس پی
          // BuyShowPurchaseDetail
          // یا
          // SaleShowVoucherDetail
          // یا
          // InvShowVoucherDetail
          // فراخوانی شود
          showDetailsReferGoodsList();
        } else {
          // یعنی مرجع ردیفی نیست و باید

          SaleSettingServer saleSettingServer = setting.sale.saleSettingServer;
          if (saleSettingServer.systemSetup.isStoreInDet == null ||
              !saleSettingServer.systemSetup.isStoreInDet) {
            // اس پی
            // salegetprice
            // فراخوانی شود
            showGoodsList(null);
          } else {
            // لیست انبار ها
            // نمایش داده شود
            showStoreList();
          }
        }
      },
      onSelectGoodsGroup: (goodsGroup) {
        // در خود فرم گروه کالا وقتی برگ کلیک میشه کالاها باز میشه نیاز به کد زیر نیست
        // this.goodsGroup = goodsGroup;
        // showGoodsList(null);
      },
      onBarcodeScanned: (barcode) {
        // audioCache.play("sounds/beep-07.mp3", volume: 1.0);

        if (voucher.voucherHeader.voucherTypeRefer.voucherTypeID != null) {
          // یعنی مرجع ردیفی است
          bool isUnlockRefByBarcode =
              voucher.voucherHeader.voucherType.isUnlockRefByBarcode;
          if (isUnlockRefByBarcode != null && isUnlockRefByBarcode) {
            // یعنی در بین کالاهای فعلی سند دنبال این بارکد بگرد
            VoucherDetail vd = voucher.voucherDetails.firstWhere(
                (voucherDetail) => voucherDetail.goods.barCode == barcode,
                orElse: () => null);

            if (vd == null) {
              // این کالای اسکن شده در بین کالاهای مرجع نیست
              showFlushBar(
                  "کالاهای این سند شامل کالای اسکن شده نیستند یا برای این کالا بارکدی در سیستم تعریف نشده است",
                  Colors.red[300],
                  5, () {
                Navigator.popUntil(
                  context,
                  ModalRoute.withName('/saleHome'),
                );
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => SaleVoucherFrm(
                            initialIndex: 0,
                          )),
                );
              });
            } else {
              double newQuantity = vd.quantity + 1;
              if (newQuantity > vd.quantityReserved) {
                showFlushBar(
                    "تعداد اسکن بیشتر از مانده مرجع است", Colors.red[300], 5,
                    () {
                  Navigator.popUntil(
                    context,
                    ModalRoute.withName('/saleHome'),
                  );
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => SaleVoucherFrm(
                              initialIndex: 0,
                            )),
                  );
                });
              } else {
                // همه چیز اوکی است و تعداد کالا یکی بالا میرود
                showFlushBar(
                    vd.goods.goodsDesc +
                        " به تعداد " +
                        newQuantity.toString() +
                        " بار اسکن شد",
                    Colors.green[300],
                    1, () {
                  vd.quantity = newQuantity;
                  Navigator.popUntil(
                    context,
                    ModalRoute.withName('/saleHome'),
                  );
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => SaleVoucherFrm(
                              initialIndex: 0,
                            )),
                  );
                });
              }
            }
          } else {
            showGoodsList(barcode);
          }
        } else {
          // یعنی مرجع ردیفی نیست
          showGoodsList(barcode);
        }
      },
    );
  }

  void showGoodsList(String barcode) {
    Navigator.push(
      context,
      MaterialPageRoute(
          settings: RouteSettings(name: "/goods"),
          builder: (BuildContext context) => SaleGoods(
                barcode: barcode,
                customerID: voucher.voucherHeader.customer.customerID,
                goodsGroup: null,
                storeID: voucher.voucherHeader.store.storeID,
                voucherTypeID: voucher.voucherHeader.voucherType.voucherTypeID,
              )),
    );
  }

  void showStoreList() {
    Navigator.push(
      context,
      MaterialPageRoute(
          settings: RouteSettings(name: "/stores"),
          builder: (BuildContext context) => InvStore()),
    );
  }

  showFlushBar(
      String messageText, Color color, int time, Function onDISMISSED) {
    Flushbar(
      leftBarIndicatorColor: color,
      margin: EdgeInsets.all(8),
      // borderRadius: 8,
      messageText: Text(messageText,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'IRANSansBlack',
            color: Colors.white,
            fontSize: 14.0,
          )),
      duration: Duration(seconds: time),
    )..show(context).then((result) {
        onDISMISSED();
      });
  }

  void showDetailsReferGoodsList() {
    Navigator.push(
      context,
      MaterialPageRoute(
          settings: RouteSettings(name: "/detailsRefer"),
          builder: (BuildContext context) => DetailReferGoods(
                storeID: voucher.voucherHeader.store.storeID,
                voucherTypeID: voucher.voucherHeader.voucherType.voucherTypeID,
                refTypeID: voucher.voucherHeader.voucherTypeRefer.voucherTypeID,
                systemID: voucher.voucherHeader.voucherTypeRefer.systemID,
                voucherTypeDesc:
                    voucher.voucherHeader.voucherTypeRefer.voucherTypeDesc,
                goodsGroup: null,
              )),
    );
  }

  void showGoodsListForPromotion() {
    //جایزه دستی
    List<QueryParam> queryParams = [];
    queryParams.add(QueryParam(
        queryParamType: QueryParamType.TEXT,
        name: "voucherDate",
        value: Jalali.now().formatter.y +
            "/" +
            Jalali.now().formatter.mm +
            "/" +
            Jalali.now().formatter.dd));
    queryParams.add(QueryParam(
        queryParamType: QueryParamType.NUMBER,
        name: "voucherTypeID",
        value: voucher.voucherHeader.voucherType.voucherTypeID.toString()));
    queryParams.add(QueryParam(
        queryParamType: QueryParamType.NUMBER,
        name: "customerID",
        value: voucher.voucherHeader.customer.customerID.toString()));

    queryParams.add(QueryParam(
        queryParamType: QueryParamType.NUMBER,
        name: "storeID",
        value: voucher.voucherHeader.store.storeID == null
            ? ""
            : voucher.voucherHeader.store.storeID.toString()));

    queryParams.add(QueryParam(
        queryParamType: QueryParamType.BOOL,
        name: "hasPrice",
        value: setting.sale.saleSettingClient.hasPrice.toString()));

    queryParams.add(QueryParam(
        queryParamType: QueryParamType.BOOL,
        name: "isOnlyRemain",
        value: setting.sale.saleSettingClient.hasRemain.toString()));

    Navigator.push(
      context,
      MaterialPageRoute(
          settings: RouteSettings(name: "/goodsForPromotion"),
          builder: (BuildContext context) {
            return WillPopScope(
              onWillPop: () {
                this.goodsGroup = GoodsGroup();
                Navigator.popUntil(
                  context,
                  ModalRoute.withName('/saleHome'),
                );
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => SaleVoucherFrm(
                            initialIndex: 0,
                          )),
                );
                return new Future.value(false);
              },
              child: MultipleView<Goods>(
                initialIndex: 0,
                title: "جایزه ها",
                subtitle: this.goodsGroup.groupID == null
                    ? "همه گروه کالا ها"
                    : this.goodsGroup.groupDesc,
                value: Goods(),
                filterWidgets: <Widget>[
                  TTextField(
                    title: "نام کالا",
                    onValue: () {
                      return goodsDesc;
                    },
                    onChanged: (txt) {
                      goodsDesc = txt;
                    },
                  ),
                  TTextField(
                    title: "کد کالا",
                    onValue: () {
                      return goodsCode;
                    },
                    onChanged: (txt) {
                      goodsCode = txt;
                    },
                  ),
                ],
                onFiltered: () {
                  List<QueryParam> queryParams = [];
                  if (this.goodsDesc != null && this.goodsDesc != "") {
                    queryParams.add(QueryParam(
                        queryParamType: QueryParamType.TEXT,
                        name: "goodsDesc",
                        value: this.goodsDesc));
                  }
                  if (this.goodsCode != null && this.goodsCode != "") {
                    queryParams.add(QueryParam(
                        queryParamType: QueryParamType.TEXT,
                        name: "goodsCode",
                        value: this.goodsCode));
                  }
                  return queryParams;
                },
                onDeleteFilter: () {
                  goodsDesc = null;
                  goodsCode = null;
                  goodsGroup = GoodsGroup();
                },
                on204: () {},
                onTabChanged: (tabIndex) {
                  goodsDesc = null;
                  goodsCode = null;
                  goodsGroup = GoodsGroup();
                },
                queryParams: queryParams,
                resource: "sale/goods",
                listViewItemBuilder: (item, index) {
                  return GoodsListTilePlusMinusSale(
                      goods: item,
                      onTapPlus: () {
                        // audioCache.play("sounds/plus.mp3", volume: 1.0);
                        //TODO این کالا رو به جایزه اضافه کن
                      },
                      onTapMinus: () {
                        // audioCache.play("sounds/minus.mp3", volume: 1.0);
                        //TODO این کالا رو از جایزه کم کن
                      });
                },
                mainSectionBuilder: (item, index) {
                  return GoodsGridTilePlusMinus(
                      goods: item,
                      onTapPlus: () {
                        // audioCache.play("sounds/plus.mp3", volume: 1.0);
                        //TODO این کالا رو به جایزه اضافه کن
                      },
                      onTapMinus: () {
                        // audioCache.play("sounds/minus.mp3", volume: 1.0);
                        //TODO این کالا رو از جایزه کم کن
                      });
                },
                detailSectionBuilder: (item, index) {
                  return GridTile(
                      child: ClipRect(
                    child: Container(
                        color: Colors.white10,
                        child: Column(
                          children: <Widget>[
                            ButtonBar(
                              alignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  item.goodsDesc,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontFamily: 'IRANSansBlack',
                                    fontSize: 15.0,
                                    color: Colors.black38,
                                  ),
                                ),
                              ],
                            ),
                            ButtonBar(
                              alignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  item.goodsCode,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontFamily: 'IRANSans',
                                    fontSize: 14.0,
                                    color: Colors.black38,
                                  ),
                                ),
                                Text(
                                  "کد کالا",
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                    fontFamily: 'IRANSans',
                                    fontSize: 14.0,
                                    color: Colors.black38,
                                  ),
                                ),
                              ],
                            ),
                            ButtonBar(
                              alignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  item.price == null
                                      ? "0"
                                      : TLib.commaSeparate(
                                          item.price.toString()),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontFamily: 'IRANSans',
                                    fontSize: 14.0,
                                    color: Colors.black38,
                                  ),
                                ),
                                Text(
                                  "قیمت",
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                    fontFamily: 'IRANSans',
                                    fontSize: 14.0,
                                    color: Colors.black38,
                                  ),
                                ),
                              ],
                            ),
                            ButtonBar(
                              alignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  item.storeName ?? "",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontFamily: 'IRANSans',
                                    fontSize: 14.0,
                                    color: Colors.black38,
                                  ),
                                ),
                                Text(
                                  "انبار",
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                    fontFamily: 'IRANSans',
                                    fontSize: 14.0,
                                    color: Colors.black38,
                                  ),
                                ),
                              ],
                            ),
                            ButtonBar(
                              alignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  item.goodsUnit.defaultSecUnitID == null
                                      ? item.goodsUnit.unitDesc
                                      : item.goodsUnit.secUnitDesc,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontFamily: 'IRANSans',
                                    fontSize: 14.0,
                                    color: Colors.black38,
                                  ),
                                ),
                                Text(
                                  "واحد پیش فرض",
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                    fontFamily: 'IRANSans',
                                    fontSize: 14.0,
                                    color: Colors.black38,
                                  ),
                                ),
                              ],
                            ),
                            ButtonBar(
                              alignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  item.remain == null
                                      ? "0"
                                      : TLib.commaSeparate(
                                          item.remain.toString()),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontFamily: 'IRANSans',
                                    fontSize: 14.0,
                                    color: Colors.black38,
                                  ),
                                ),
                                Text(
                                  "موجودی",
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                    fontFamily: 'IRANSans',
                                    fontSize: 14.0,
                                    color: Colors.black38,
                                  ),
                                ),
                              ],
                            ),
                            ButtonBar(
                              alignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  item.goodsUnit.computeValue == null
                                      ? ""
                                      : item.goodsUnit.computeValue.toString(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontFamily: 'IRANSans',
                                    fontSize: 14.0,
                                    color: Colors.black38,
                                  ),
                                ),
                                Text(
                                  "ضریب",
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                    fontFamily: 'IRANSans',
                                    fontSize: 14.0,
                                    color: Colors.black38,
                                  ),
                                ),
                              ],
                            ),
                            ButtonBar(
                              alignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  item.techInfo ?? "",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontFamily: 'IRANSans',
                                    fontSize: 14.0,
                                    color: Colors.black38,
                                  ),
                                ),
                                Text(
                                  "اطلاعات فنی",
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                    fontFamily: 'IRANSans',
                                    fontSize: 14.0,
                                    color: Colors.black38,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )),
                  ));
                },
                tableViewCellBuilder: null,
                tableViewColumns: <TColumn>[],
                onTap: (goods) {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => GoodsDetailsFrm(
                              showPrice: false,
                              showRemain: true,
                              goods: goods,
                              onPressedPlusButton: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          AddingGoodsUnit(
                                            addingGoodsDestination:
                                                AddingGoodsDestination
                                                    .PROMOTION,
                                            index: -1,
                                            goods: goods,
                                            goodsUnit: null,
                                            quantity: 1,
                                            price:
                                                null, //قیمت برای جایزه مهم نیست
                                            parentWidget: () {
                                              return SaleVoucherFrm(
                                                initialIndex: 0,
                                              );
                                            },
                                          )),
                                );
                              },
                            )),
                  );
                },
              ),
            );
          }),
    );
  }

  void onTapPlus(Goods goods) {
    if (goods.price == null || goods.price == 0) {
      TAlertDialog(context: context).show(
          title: "قیمت مجاز نیست", content: "قیمت باید بیشتر از صفر باشد");
    } else if (goods.goodsUnit.computeValue == null ||
        goods.goodsUnit.computeValue == 0) {
      TAlertDialog(context: context).show(
          title: "ضریب واحد کالا مجاز نیست",
          content: "ضریب واحد کالا باید بیشتر از صفر باشد");
    } else {
      Voucher voucher = TIW.getVoucher(context);
      bool isExistInVoucherDetail =
          false; //آیا این کالا در حال حاضر وجود دارد در ووچر دیتیل ؟
      for (var i = 0; i < voucher.voucherDetails.length; i++) {
        if (goods.goodsID == voucher.voucherDetails[i].goods.goodsID &&
            goods.goodsUnit.unitID ==
                voucher.voucherDetails[i].goodsUnit.unitID &&
            goods.storeID == voucher.voucherDetails[i].goods.storeID) {
          isExistInVoucherDetail = true;
          voucher.voucherDetails[i].quantity =
              voucher.voucherDetails[i].quantity + 1;
          break;
        }
      }
      if (!isExistInVoucherDetail) {
        GoodsUnit goodsUnit;
        if (goods.goodsUnit.computeValue.toInt() == 1) {
          //یعنی واحد پیش فرض همان واحد اصلی است
          goodsUnit = GoodsUnit(
            computeValue: goods.goodsUnit.computeValue,
            secUnitID: goods.goodsUnit.secUnitID,
            secUnitDesc: goods.goodsUnit.secUnitDesc,
            unitDesc: goods.goodsUnit.unitDesc,
          );
        } else {
          //یعنی واحد پیش فرض  واحد فرعی است
          goodsUnit = GoodsUnit(
            computeValue: goods.goodsUnit.computeValue,
            secUnitID: goods.goodsUnit.defaultSecUnitID,
            secUnitDesc: goods.goodsUnit.secUnitDesc,
            unitDesc: goods.goodsUnit.secUnitDesc,
          );
        }
        goods.goodsUnit.secUnitID = goods.goodsUnit.defaultSecUnitID;
        voucher.voucherDetails.add(VoucherDetail(
          -1,
          status: VoucherDetailStatus.DEFAULT_ACTIVE,
          goods: goods,
          goodsUnit: goodsUnit,
          quantity: 1,
          fee: goods.price,
        ));
      }
      TIW.setVoucher(context, voucher);
    }
  }

  void onTapMinus(Goods goods) {
    if (goods.price == null || goods.price == 0) {
      TAlertDialog(context: context).show(
          title: "قیمت مجاز نیست", content: "قیمت باید بیشتر از صفر باشد");
    } else if (goods.goodsUnit.computeValue == null ||
        goods.goodsUnit.computeValue == 0) {
      TAlertDialog(context: context).show(
          title: "ضریب واحد کالا مجاز نیست",
          content: "ضریب واحد کالا باید بیشتر از صفر باشد");
    } else {
      Voucher voucher = TIW.getVoucher(context);
      for (var i = 0; i < voucher.voucherDetails.length; i++) {
        if (goods.goodsID == voucher.voucherDetails[i].goods.goodsID &&
            goods.goodsUnit.unitID ==
                voucher.voucherDetails[i].goodsUnit.unitID &&
            goods.storeID == voucher.voucherDetails[i].goods.storeID &&
            voucher.voucherDetails[i].quantity > 1) {
          voucher.voucherDetails[i].quantity =
              voucher.voucherDetails[i].quantity - 1;
          break;
        }
      }
      TIW.setVoucher(context, voucher);
    }
  }

  Future<Response> _futureResponseSaleComputePromotion() {
    Voucher voucher = TIW.getVoucher(context);
    return TRestful.post(
      "sale/promotionelements",
      setting: setting,
      body: Voucher.jsonEncodeSale(voucher),
      timeout: 100,
    );
  }

  void insertCoordinate() {
    WebService.callModalMode<Coordinate>(
      context,
      value: Coordinate(),
      futureResponse: _futureResponseInsertCoordinate,
      waitingMsg: "ذخیره موقعیت جغرافیایی",
      doneMsg: "ذخیره موقعیت جغرافیایی",
      on200object: (coordinate) {
        computePromotionAndInsertVoucher();
      },
    );
  }

  void computePromotionAndInsertVoucher() {
    WebService.callModalMode<PromotionElement>(
      context,
      value: PromotionElement(),
      futureResponse: _futureResponseSaleComputePromotion,
      waitingMsg: "جایزه و عامل",
      doneMsg: "جایزه و عامل",
      on204: () {
        Voucher voucher = TIW.getVoucher(context);
        voucher.tElements = null;
        TIW.setVoucher(context, voucher);
        insertVoucher();
      },
      on200object: (pe) {
        Voucher voucher = TIW.getVoucher(context);

        List<TPromotion> tPromotionsManual =
            voucher.tPromotions.where((tPromotion) {
          return tPromotion.type == 'دستی';
        }).toList();

        List<TPromotion> tPromotion = pe.tPromotions;
        tPromotion.addAll(tPromotionsManual);
        voucher.tPromotions = tPromotion;
        voucher.tElements = pe.tElements;
        TIW.setVoucher(context, voucher);
        insertVoucher();
      },
    );
  }

  void insertVoucher() {
    WebService.callModalMode<Voucher>(
      context,
      value: Voucher(),
      futureResponse: _futureResponseInsertVoucher,
      waitingMsg: "ذخیره سند",
      doneMsg: "ذخیره سند",
      on200object: (voucher) {
        TIW.setVoucher(context, null);
        TAlertDialog(
                barrierDismissible: false,
                context: context,
                close: () {
                  Navigator.popUntil(
                    context,
                    ModalRoute.withName('/saleHome'),
                  );
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => SaleVoucherFrm(
                              initialIndex: 0,
                            )),
                  );
                })
            .show(
                title: voucher.voucherHeader.voucherNumber.toString(),
                content: "سند با موفقیت ذخیره شد!");
      },
    );
  }

  Future<Response> _futureResponseInsertVoucher() {
    Voucher voucher = TIW.getVoucher(context);
    List<TPromotion> tPromotionsManual =
        voucher.tPromotions.where((tPromotion) {
      return tPromotion.type == 'دستی';
    }).toList();

    if (tPromotionsManual.length > 0) {
      voucher.voucherHeader.isManualPromotion = true;
    } else {
      voucher.voucherHeader.isManualPromotion = false;
    }

    if (voucher.voucherHeader.saleType == null) {
      // از تنظیم پیش فرش اسناد میخواند
    } else {
      //  اگر پر شود از کیوکی میخواند
      voucher.voucherHeader.saleTypeID =
          voucher.voucherHeader.saleType.saleTypeID;
    }

    return TRestful.post(
      "sale/vouchers",
      setting: setting,
      body: Voucher.jsonEncodeSale(voucher),
      timeout: 250,
    );
  }

  Future<Response> _futureResponseInsertCoordinate() {
    Voucher voucher = TIW.getVoucher(context);
    return TRestful.post(
      "sale/coordinates",
      setting: setting,
      body: json.encode({
        "customerID": voucher.voucherHeader.customer.customerID,
        "latitude": locationData.latitude,
        "longitude": locationData.longitude,
      }),
      timeout: 250,
    );
  }

  Future<Response> _futureResponseXfieldssetups() {
    // this.voucher.voucherHeader.voucherType.voucherTypeID
    List<QueryParam> queryParams = [];
    queryParams.add(QueryParam(
        name: "voucherTypeID",
        queryParamType: QueryParamType.NUMBER,
        title: "آی دی نوع سند",
        value:
            this.voucher.voucherHeader.voucherType.voucherTypeID.toString()));
    return TRestful.get(
      "pub/xfieldssetups",
      setting: setting,
      queryParams: queryParams,
      timeout: 30,
    );
  }
}
