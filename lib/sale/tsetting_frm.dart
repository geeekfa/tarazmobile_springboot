import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TSettingFrm extends StatefulWidget {
  @override
  _TSettingFrmState createState() => _TSettingFrmState();
}

class _TSettingFrmState extends State<TSettingFrm> {
  Setting setting;
  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: setting.currentSystem.themeData,
      child: Scaffold(
        appBar: AppBar(
          title: ListTile(
            title: Text("تنظیمات فروش",
                textAlign: TextAlign.right,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                  color: Colors.white,
                )),
          ),
        ),
        body: ListView(
          children: <Widget>[
            SwitchListTile(
                value: setting.currentSystem.saleSettingClient.mustSendLocation,
                title: Text(
                  "ثبت موقعیت بازاریاب همراه با سند",
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  ),
                ),
                onChanged: (state) {
                  setState(() {
                    setting.currentSystem.saleSettingClient.mustSendLocation =
                        state;
                  });
                  TIW.setSetting(context, setting);
                  SharedPreferences.getInstance().then((sp) {
                    sp.setBool("mustSendLocation", state);
                  });
                }),
            SwitchListTile(
                value: setting.currentSystem.saleSettingClient.hasPrice,
                title: Text(
                  "نمایش کالاهای قیمت دار",
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  ),
                ),
                onChanged: (state) {
                  setState(() {
                    setting.currentSystem.saleSettingClient.hasPrice =
                        state;
                  });
                  TIW.setSetting(context, setting);
                  SharedPreferences.getInstance().then((sp) {
                    sp.setBool("hasPrice", state);
                  });
                }),
                 SwitchListTile(
                value: setting.currentSystem.saleSettingClient.hasRemain,
                title: Text(
                  "نمایش کالاهای مانده دار",
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  ),
                ),
                onChanged: (state) {
                  setState(() {
                    setting.currentSystem.saleSettingClient.hasRemain =
                        state;
                  });
                  TIW.setSetting(context, setting);
                  SharedPreferences.getInstance().then((sp) {
                    sp.setBool("hasRemain", state);
                  });
                }),
          ],
        ),
      ),
    );
  }
}
