import 'package:com_tarazgroup/models/adding_goods_destination.dart';
import 'package:com_tarazgroup/models/goods.dart';
import 'package:com_tarazgroup/models/goods_unit.dart';
import 'package:com_tarazgroup/models/query_param.dart';
import 'package:com_tarazgroup/models/system.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/modules/multipleview/multiple_view.dart';
import 'package:com_tarazgroup/modules/textfield/ttextfield.dart';
import 'package:com_tarazgroup/modules/view/list_tile_view.dart';
import 'package:com_tarazgroup/sale/adding_2_quantity.dart';
import 'package:flutter/material.dart';

class AddingGoodsUnit extends StatefulWidget {
  final AddingGoodsDestination addingGoodsDestination;
  final int index;
  final Goods goods;
  final GoodsUnit goodsUnit;
  final double quantity;
  final double price;
  final Widget Function()
      parentWidget; //یعنی وقتی کار افزودن یا ویرایش تعداد کالا به سند تمام شد به  کدام صفحه بر گردیم

  const AddingGoodsUnit({
    @required this.addingGoodsDestination,
    @required this.index,
    @required this.goods,
    @required this.goodsUnit,
    @required this.quantity,
    @required this.price,
    @required this.parentWidget,
  });
  @override
  _AddingGoodsUnitState createState() => _AddingGoodsUnitState();
}

class _AddingGoodsUnitState extends State<AddingGoodsUnit> {
  System currentSystem;
  String unitDesc;
  GoodsUnit goodsUnit;
  @override
  void initState() {
    this.goodsUnit = widget.goodsUnit;
    currentSystem = TIW.getSetting(context, initState: true).currentSystem;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: currentSystem.themeData,
      child: Scaffold(
        body: MultipleView<GoodsUnit>(
          initialIndex: 0,
          title: "واحد کالا",
          subtitle: widget.goods.goodsDesc,
          value: GoodsUnit(),
          resource:
              "inv/goods/" + widget.goods.goodsID.toString() + "/goodsunits",
          queryParams: <QueryParam>[],
          onTap: null,
          onTabChanged: (tabIndex) {},
          listViewItemBuilder: (item, index) {
            bool value = false;
            String secondary = "";

            if (this.goodsUnit == null) {
              // if (item.isDefaultSecUnitID) {
              //   value = true;
              //   widget.voucherDetail.goodsUnit = item;
              // }
            } else if (this.goodsUnit.secUnitID == item.secUnitID) {
              value = true;
            }

            secondary = item.computeValue.toString();
            return ListTileView.checkboxListTile(
                value: value,
                title: item.unitDesc,
                secondary: secondary,
                subTitle: "",
                onChanged: (value1) {
                  this.goodsUnit = item;
                  setState(() {});
                });
          },
          tableViewCellBuilder: null,
          tableViewColumns: null,
          mainSectionBuilder: null,
          detailSectionBuilder: null,
          on204: () {
            this.goodsUnit = null;
            setState(() {});
          },
          filterWidgets: <Widget>[
            TTextField(
              title: "واحد کالا",
              onValue: () {
                return unitDesc;
              },
              onChanged: (txt) {
                unitDesc = txt;
              },
            ),
          ],
          onFiltered: () {
            this.goodsUnit = null;
            setState(() {});
            List<QueryParam> queryParams = [];
            if (this.unitDesc != null && this.unitDesc != "") {
              queryParams.add(QueryParam(
                  queryParamType: QueryParamType.TEXT,
                  name: "unitDesc",
                  value: this.unitDesc));
            }
            return queryParams;
          },
          onDeleteFilter: () {
            this.goodsUnit = null;
            unitDesc = null;
            setState(() {});
          },
        ),
        floatingActionButton: this.goodsUnit != null
            ? FloatingActionButton(
                tooltip: "ادامه",
                child: Icon(
                  Icons.arrow_forward,
                  color: currentSystem.themeData.primaryColorDark,
                ),
                onPressed: () {
                  GoodsUnit goodsUnit = GoodsUnit(
                    unitID: this.goodsUnit.unitID,
                    unitDesc: this.goodsUnit.unitDesc,
                    secUnitID: this.goodsUnit.secUnitID,
                    defaultSecUnitID: this.goodsUnit.defaultSecUnitID,
                    secUnitDesc: this.goodsUnit.unitDesc,
                    computeValue: this.goodsUnit.computeValue,
                    isQuantitative: this.goodsUnit.isQuantitative,
                    isQuantitativeDesc: this.goodsUnit.isQuantitativeDesc,
                  );
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => AddingQuantity(
                              addingGoodsDestination:
                                  widget.addingGoodsDestination,
                              index: widget.index,
                              goods: widget.goods,
                              goodsUnit: goodsUnit,
                              quantity: widget.quantity,
                              price: widget.price,
                              parentWidget: widget.parentWidget,
                            )),
                  );
                })
            : Container(),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      ),
    );
  }
}
