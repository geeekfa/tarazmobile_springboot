import 'package:com_tarazgroup/models/goods.dart';
import 'package:com_tarazgroup/models/goods_unit.dart';
import 'package:com_tarazgroup/models/system.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher.dart';
import 'package:com_tarazgroup/models/voucher_detail.dart';
import 'package:com_tarazgroup/modules/dialog/alert_dialog.dart';
import 'package:com_tarazgroup/modules/numpad.dart/numpad.dart';
import 'package:flutter/material.dart';
// import 'package:flushbar/flushbar.dart';
import 'package:another_flushbar/flushbar.dart';

class AddingPrice extends StatefulWidget {
  final int index;
  final Goods goods;
  final GoodsUnit goodsUnit;
  final double quantity;
  final double price;
  final Widget Function()
      parentWidget; //یعنی وقتی کار افزودن یا ویرایش تعداد کالا به سند تمام شد به  کدام صفحه بر گردیم

  const AddingPrice({
    @required this.index,
    @required this.goods,
    @required this.goodsUnit,
    @required this.quantity,
    @required this.price,
    @required this.parentWidget,
  });
  @override
  _AddingPriceState createState() => _AddingPriceState();
}

class _AddingPriceState extends State<AddingPrice> {
  double price;
  System currentSystem;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  void initState() {
    this.price = widget.price;
    currentSystem = TIW.getSetting(context, initState: true).currentSystem;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: currentSystem.themeData,
      child: Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            title: ListTile(
              subtitle: Text(widget.goods.goodsDesc,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontFamily: 'IRANSansUltraLight',
                    fontSize: 10.0,
                    color: Colors.white,
                  )),
              title: Text("قیمت",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                    color: Colors.white,
                  )),
            ),
          ),
          body: Numpad<int>(
            value: this.price.toInt() ?? 0,
            onChanged: (price) {
              this.price = price.toDouble();
            },
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerFloat,
          floatingActionButton: FloatingActionButton(
              tooltip: "افزودن به سند",
              child: Icon(Icons.done),
              onPressed: () {
                if (this.price == null || this.price == 0) {
                  TAlertDialog(context: context).show(
                      title: "قیمت مجاز نیست",
                      content: "قیمت باید بیشتر از صفر باشد");
                } else {
                  VoucherDetail voucherDetail = VoucherDetail(
                    widget.index,
                    status: VoucherDetailStatus.DEFAULT_ACTIVE,
                    goods: widget.goods,
                    goodsUnit: widget.goodsUnit,
                    quantity: widget.quantity,
                    fee: this.price,
                  );
                  Voucher voucher = TIW.getVoucher(context);
                  switch (widget.index) {
                    case -1: //افزودن کالا به سند
                      voucher.voucherDetails.add(voucherDetail);
                      TIW.setVoucher(context, voucher);
                      showFlushBar("کالا به سند اضافه شد", () {
                        Navigator.popUntil(
                          context,
                          ModalRoute.withName("/goods"),
                        );
                      });
                      break;
                    default: //ویرایش کالای فعلی سند
                      voucher.voucherDetails[widget.index] = voucherDetail;
                      TIW.setVoucher(context, voucher);
                      showFlushBar("کالا در سند ویرایش شد", () {
                        Navigator.popUntil(
                          context,
                          ModalRoute.withName("/saleHome"),
                        );
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  widget.parentWidget()),
                        );
                      });
                  }
                }
              })),
    );
  }

  showFlushBar(String messageText, Function onDISMISSED) {
    Flushbar(
      leftBarIndicatorColor: Colors.green[300],
      margin: EdgeInsets.all(8),
      // borderRadius: 8,
      messageText: Text(messageText,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'IRANSansBlack',
            color: Colors.white,
            fontSize: 14.0,
          )),
      duration: Duration(seconds: 2),
    )
      // ..onStatusChanged = (FlushbarStatus status) {
      //   switch (status) {
      //     case FlushbarStatus.SHOWING:
      //     case FlushbarStatus.IS_APPEARING:
      //     case FlushbarStatus.IS_HIDING:
      //       break;
      //     case FlushbarStatus.DISMISSED:
      //       {
      //         onDISMISSED();

      //         break;
      //       }
      //   }
      // }
      ..show(context).then((result) {
        onDISMISSED();
      });
  }
}
