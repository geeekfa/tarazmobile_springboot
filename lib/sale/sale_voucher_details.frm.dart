import 'package:com_tarazgroup/models/query_param.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher_detail.dart';
import 'package:com_tarazgroup/models/voucher_header.dart';
import 'package:com_tarazgroup/modules/dialog/report_dialog.dart';
import 'package:com_tarazgroup/modules/multipleview/multiple_view.dart';
import 'package:com_tarazgroup/modules/tableview/tcolumn.dart';
import 'package:com_tarazgroup/modules/textfield/ttextfield.dart';
import 'package:com_tarazgroup/modules/tlib/tlib.dart';
import 'package:flutter/material.dart';

class SaleVoucherDetailsFrm extends StatefulWidget {
  final VoucherHeader voucherHeader;

  const SaleVoucherDetailsFrm({
    @required this.voucherHeader,
  });
  @override
  _SaleVoucherDetailsFrmState createState() => _SaleVoucherDetailsFrmState();
}

class _SaleVoucherDetailsFrmState extends State<SaleVoucherDetailsFrm> {
  Setting setting;
  String goodsDesc;

  @override
  void initState() {
    deleteFilters();
    setting = TIW.getSetting(context, initState: true);
    super.initState();
  }

  void deleteFilters() {
    goodsDesc = null;
  }

  @override
  Widget build(BuildContext context) {
    return MultipleView<VoucherDetail>(
      initialIndex: 0,
      title: "جزییات سند",
      subtitle: widget.voucherHeader.voucherNumber.toString(),
      value: VoucherDetail(
        0,
        goods: null,
        goodsUnit: null,
        quantity: null,
        status: null,
        tSerials: null,
        voucherDetailID: null,
        voucherHeaderID: null,
      ),
      filterWidgets: <Widget>[
        TTextField(
          title: "نام کالا",
          keyboardType: TextInputType.text,
          onValue: () {
            return goodsDesc;
          },
          onChanged: (txt) {
            goodsDesc = txt;
          },
        ),
      ],
      onFiltered: () {
        List<QueryParam> queryParams = [];
        return queryParams;
      },
      onDeleteFilter: () {},
      onReport: () {
        TReportDialog(
            context: context,
            title: 'گزارش سند_' +
                widget.voucherHeader.voucherNumber.toString() +
                '_' +
                widget.voucherHeader.customer.customerName,
            pdfSetting: PdfSetting(
                resource: "sale/salecrystalrepvoucher/pdf",
                queryParams: <QueryParam>[
                  QueryParam(
                    queryParamType: QueryParamType.NUMBER,
                    name: "voucherHeaderID",
                    value: widget.voucherHeader.voucherHeaderID.toString(),
                  ),
                ]),
            excelSetting: ExcelSetting(
                resource: "sale/salecrystalrepvoucher/excel",
                queryParams: <QueryParam>[
                  QueryParam(
                    queryParamType: QueryParamType.NUMBER,
                    name: "voucherHeaderID",
                    value: widget.voucherHeader.voucherHeaderID.toString(),
                  ),
                ])).show();
      },
      on204: () {},
      onTabChanged: (tabIndex) {
        deleteFilters();
      },
      queryParams: <QueryParam>[
        QueryParam(
          queryParamType: QueryParamType.NUMBER,
          name: "voucherHeaderID",
          value: widget.voucherHeader.voucherHeaderID.toString(),
        )
      ],
      resource: "sale/vouchers/headers/" +
          widget.voucherHeader.voucherHeaderID.toString() +
          "/details",
      listViewItemBuilder: null,
      detailSectionBuilder: null,
      mainSectionBuilder: null,
      tableViewCellBuilder: (tColumn, voucherDetail) {
        switch (tColumn.name) {
          case "GOODSDESC":
            return Text(voucherDetail.goods.goodsDesc,
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));
            break;
          case "FEE":
            return Text(
                voucherDetail.goods.price == null
                    ? "-"
                    : TLib.commaSeparate(voucherDetail.goods.price.toString()),
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));
            break;
          case "QUANTITY":
            return Text(TLib.commaSeparate(voucherDetail.quantity.toString()),
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));
            break;
          case "UNITNAME":
            return Text(voucherDetail.goodsUnit.unitDesc,
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                textDirection: TextDirection.ltr,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));
            break;
          case "COMPUTEVALUE":
            return Text(
                TLib.commaSeparate(
                    voucherDetail.goodsUnit.computeValue.toString()),
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));
            break;
          case "ROWPRICE":
            return Text(
                voucherDetail.rowPrice == null
                    ? "-"
                    : TLib.commaSeparate(voucherDetail.rowPrice.toString()),
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));
            break;
          default:
            return Text("");
        }
      },
      tableViewColumns: <TColumn>[
        TColumn(
          name: "GOODSDESC",
          title: "کالا",
          width: 220,
        ),
        TColumn(
          name: "FEE",
          title: "فی",
          width: 100,
        ),
        TColumn(
          name: "QUANTITY",
          title: "تعداد",
          width: 80,
        ),
        TColumn(
          name: "UNITNAME",
          title: "واحد",
          width: 60,
        ),
        TColumn(
          name: "COMPUTEVALUE",
          title: "ضریب",
          width: 50,
        ),
        TColumn(
          name: "ROWPRICE",
          title: "قیمت",
          width: 200,
        ),
      ],
      onTap: (voucherDetail) {},
    );
  }
}
