import 'package:com_tarazgroup/models/query_param.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher_header.dart';
import 'package:com_tarazgroup/models/voucher_type.dart';
import 'package:com_tarazgroup/modules/multipleview/multiple_view.dart';
import 'package:com_tarazgroup/modules/qkey/qkey_filter.dart';
import 'package:com_tarazgroup/modules/tableview/tcolumn.dart';
import 'package:com_tarazgroup/modules/textfield/ttextfield.dart';
import 'package:com_tarazgroup/modules/textfield/ttextfield_date.dart';
import 'package:com_tarazgroup/modules/tlib/tlib.dart';
import 'package:com_tarazgroup/modules/view/list_tile_view.dart';
import 'package:com_tarazgroup/sale/sale_voucher_details.frm.dart';
import 'package:flutter/material.dart';
import 'package:shamsi_date/shamsi_date.dart';

class SaleVoucherHeadersFrm extends StatefulWidget {
  @override
  _SaleVoucherHeadersFrmState createState() => _SaleVoucherHeadersFrmState();
}

class _SaleVoucherHeadersFrmState extends State<SaleVoucherHeadersFrm> {
  Setting setting;
  String voucherNumber;
  VoucherType voucherType;
  List<QueryParam> queryParamsVoucherType = [];
  //Filter
  String voucherTypeDesc;
  String fromDate;
  String toDate;
  String filter;

  @override
  void initState() {
    deleteFilters();
    setting = TIW.getSetting(context, initState: true);
    filter = "creatorID=" + setting.perComID.toString();
    super.initState();
  }

  void deleteFilters() {
    voucherNumber = null;
    voucherType = VoucherType();
    fromDate = null;
    toDate = null;
  }

  @override
  Widget build(BuildContext context) {
    return MultipleView<VoucherHeader>(
      initialIndex: 0,
      title: "اسناد ثبت شده",
      subtitle: "",
      value: VoucherHeader(
          voucherNumber: null,
          tafsili: null,
          customer: null,
          provider: null,
          store: null,
          voucherType: null,
          voucherTypeRefer: null,
          voucherDesc: null,
          voucherDate: null,
          deliverCenterID: null,
          saleTypeID: null,
          marketingManID: null,
          saleCenterID: null,
          salesManID: null,
          isManualPromotion: null,
          center1ID: null,
          center2ID: null,
          center3ID: null,
          paymentWayID: null,
          voucherHeaderID: null,
          saleType: null),
      filterWidgets: <Widget>[
        TTextField(
          title: "شماره سند",
          keyboardType: TextInputType.number,
          onValue: () {
            return voucherNumber;
          },
          onChanged: (txt) {
            voucherNumber = txt;
          },
        ),
        TTextFieldDate(
          title: "از تاریخ",
          onValue: () {
            return fromDate;
          },
          onChanged: (txt) {
            fromDate = txt;
          },
        ),
        TTextFieldDate(
          title: "تا تاریخ",
          onValue: () {
            return toDate;
          },
          onChanged: (txt) {
            toDate = txt;
          },
        ),
        QKeyFilter<VoucherType>(
          onValue: () {
            return this.voucherType;
          },
          title: "نوع سند",
          resource: "pub/vouchertypes",
          // recCnt: 50,
          queryParams: List<QueryParam>.generate(2, (index) {
            if (index == 0) {
              return QueryParam(
                  queryParamType: QueryParamType.NUMBER,
                  name: "systemID",
                  value: setting.currentSystem.systemID.toString());
            } else {
              String voucherTypeIDs = "";
              setting.currentSystem.saleSettingServer.saleDefaultVouchers
                  .forEach((saleDefaultVoucher) {
                voucherTypeIDs +=
                    saleDefaultVoucher.voucherTypeID.toString() + ",";
              });
              voucherTypeIDs =
                  voucherTypeIDs.substring(0, voucherTypeIDs.length - 1);
              return QueryParam(
                  queryParamType: QueryParamType.TEXT,
                  name: "voucherTypeIDs",
                  value: voucherTypeIDs);
            }
          }),
          itemBuilder: (item, index) {
            return ListTileView.view1_1(
              index,
              item.voucherTypeDesc,
            );
          },
          onChangePermission: () {
            return Future.value(true);
          },
          onSelect: (voucherType) {
            this.voucherType = voucherType;
          },
          filterWidgets: <Widget>[
            TTextField(
              title: "نوع سند",
              onValue: () {
                return voucherTypeDesc;
              },
              onChanged: (txt) {
                voucherTypeDesc = txt;
              },
            ),
          ],
          onFiltered: () {
            List<QueryParam> queryParams = [];
            if (this.voucherTypeDesc != null && this.voucherTypeDesc != "") {
              queryParams.add(QueryParam(
                  queryParamType: QueryParamType.TEXT,
                  name: "voucherTypeDesc",
                  value: this.voucherTypeDesc));
            }
            return queryParams;
          },
          onDeleteFilter: () {
            voucherTypeDesc = null;
          },
        ),
      ],
      onFiltered: () {
        List<QueryParam> queryParams = [];
        if (this.voucherNumber != null && this.voucherNumber != "") {
          // queryParams.add(QueryParam(
          //   queryParamType: QueryParamType.NUMBER,
          //   name: "voucherNumber",
          //   value: this.voucherNumber,
          // ));
          this.filter += " AND voucherNumber=" + this.voucherNumber;
        }
        if (this.voucherType.voucherTypeID != null) {
          // queryParams.add(QueryParam(
          //   queryParamType: QueryParamType.NUMBER,
          //   name: "voucherTypeID",
          //   value: this.voucherType.voucherTypeID.toString(),
          // ));
          this.filter +=
              " AND voucherTypeID=" + this.voucherType.voucherTypeID.toString();
        }

        if (this.fromDate != null &&
            this.fromDate != "" &&
            this.toDate != null &&
            this.toDate != "") {
          Pattern pattern =
              r'([12]\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\d|3[01]))';
          RegExp regex = new RegExp(pattern);
          if (regex.hasMatch(fromDate) && regex.hasMatch(toDate)) {
            int fromDateYear = int.parse(fromDate.split("/")[0]);
            int fromDateMonth = int.parse(fromDate.split("/")[1]);
            int fromDateDay = int.parse(fromDate.split("/")[2]);
            Jalali jalaliFrom =
                Jalali(fromDateYear, fromDateMonth, fromDateDay);
            Gregorian gregorianFrom = Gregorian.fromJalali(jalaliFrom);

            int toDateYear = int.parse(toDate.split("/")[0]);
            int toDateMonth = int.parse(toDate.split("/")[1]);
            int toDateDay = int.parse(toDate.split("/")[2]);
            Jalali jalaliTo = Jalali(toDateYear, toDateMonth, toDateDay);
            Gregorian gregorianTo = Gregorian.fromJalali(jalaliTo);

            this.filter += " AND (VDateG between '" +
                gregorianFrom.toDateTime().toString() +
                "' AND '" +
                gregorianTo.toDateTime().toString() +
                "')";
            // queryParams.add(QueryParam(
            //   queryParamType: QueryParamType.DATE,
            //   name: "fromDate",
            //   value: this.fromDate,
            // ));
            // queryParams.add(QueryParam(
            //   queryParamType: QueryParamType.DATE,
            //   name: "toDate",
            //   value: this.toDate,
            // ));
          }
        }
        queryParams.add(QueryParam(
          queryParamType: QueryParamType.TEXT,
          name: "filter",
          value: filter,
        ));
        return queryParams;
      },
      onDeleteFilter: () {
        deleteFilters();
      },
      on204: () {},
      onTabChanged: (tabIndex) {
        deleteFilters();
      },
      queryParams: <QueryParam>[
        QueryParam(
          queryParamType: QueryParamType.TEXT,
          name: "serverIds",
          value: setting.serverID.toString(),
        ),
        QueryParam(
          queryParamType: QueryParamType.TEXT,
          name: "yearIds",
          value: setting.activeYearID.toString(),
        ),
        // QueryParam(
        //   queryParamType: QueryParamType.NUMBER,
        //   name: "creatorID",
        //   value: setting.perComID.toString(),
        // ),
        QueryParam(
          queryParamType: QueryParamType.TEXT,
          name: "filter",
          value: this.filter,
        ),
        QueryParam(
          queryParamType: QueryParamType.TEXT,
          name: "sortField.name",
          value: "voucherDateG",
        ),
        QueryParam(
          queryParamType: QueryParamType.BOOL,
          name: "sortField.desc",
          value: "true",
        )
      ],
      resource: "sale/vouchers/headers",
      listViewItemBuilder: null,
      detailSectionBuilder: null,
      mainSectionBuilder: null,
      tableViewCellBuilder: (tColumn, voucherHeader) {
        switch (tColumn.name) {
          case "VOUCHERNUMBER":
            return Text(
                TLib.commaSeparate(voucherHeader.voucherNumber.toString()),
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));
            break;
          case "VOUCHERTYPEDESC":
            return Text(voucherHeader.voucherType.voucherTypeDesc,
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));
            break;
          case "STORENAME":
            return Text(voucherHeader.store.storeName ?? "",
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));

            break;
          case "VOUCHERDATE":
            return Text(voucherHeader.voucherDate,
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                textDirection: TextDirection.ltr,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));

            break;
          case "CUSTOMERNAME":
            return Text(voucherHeader.customer.customerName,
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));

            break;
          case "VOUCHERDESC":
            return Text(voucherHeader.voucherDesc ?? "",
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.end,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                ));

            break;
          default:
            return Text("");
        }
      },
      tableViewColumns: <TColumn>[
        TColumn(
          name: "VOUCHERNUMBER",
          title: "شماره سند",
          width: 100,
        ),
        TColumn(
          name: "VOUCHERTYPEDESC",
          title: "نوع سند",
          width: 120,
        ),
        TColumn(
          name: "STORENAME",
          title: "انبار",
          width: 170,
        ),
        TColumn(
          name: "VOUCHERDATE",
          title: "تاریخ",
          width: 80,
        ),
        TColumn(
          name: "CUSTOMERNAME",
          title: "مشتری",
          width: 220,
        ),
        TColumn(
          name: "VOUCHERDESC",
          title: "توضیحات",
          width: 300,
        ),
      ],
      onTap: (voucherHeader) {
        Navigator.of(context).push(
          PageRouteBuilder(
            transitionDuration: Duration(milliseconds: 0),
            pageBuilder: (context, animation1, animation2) {
              return SaleVoucherDetailsFrm(
                voucherHeader: voucherHeader,
              );
            },
          ),
        );
      },
    );
  }
}
