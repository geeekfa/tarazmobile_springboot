import 'package:com_tarazgroup/models/goods.dart';
import 'package:com_tarazgroup/models/goods_unit.dart';
import 'package:com_tarazgroup/models/sale_setting.dart';
import 'package:com_tarazgroup/models/system.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher.dart';
import 'package:com_tarazgroup/models/voucher_detail.dart';
import 'package:com_tarazgroup/modules/dialog/alert_dialog.dart';
import 'package:com_tarazgroup/modules/numpad.dart/numpad.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';

// این کلاس برای حالت مرجع ردیفی است
class AddingQuantityRefer extends StatefulWidget {
  final int voucherDetailID;
  final int index;
  final double fee;
  final Goods goods;
  final GoodsUnit goodsUnit;
  final double quantity;
  final Widget Function()
      parentWidget; //یعنی وقتی کار افزودن یا ویرایش تعداد کالا به سند تمام شد به  کدام صفحه بر گردیم

  const AddingQuantityRefer({
    @required this.voucherDetailID,
    @required this.index,
    @required this.fee,
    @required this.goods,
    @required this.goodsUnit,
    @required this.quantity,
    @required this.parentWidget,
  });
  @override
  _AddingQuantityReferState createState() => _AddingQuantityReferState();
}

class _AddingQuantityReferState extends State<AddingQuantityRefer> {
  System currentSystem;
  double quantity;

  /// تنظیمات فروش برای این نوع سند خاص
  SaleDefaultVoucher saleDefaultVoucher;
  @override
  void initState() {
    quantity = widget.quantity;
    int voucherTypeID = TIW
        .getVoucher(context, initState: true)
        .voucherHeader
        .voucherType
        .voucherTypeID;
    currentSystem = TIW.getSetting(context, initState: true).currentSystem;

    List<SaleDefaultVoucher> saleDefaultVouchers = currentSystem
        .saleSettingServer.saleDefaultVouchers
        .where((saleDefaultVoucher) {
      return saleDefaultVoucher.voucherTypeID == voucherTypeID;
    }).toList();
    saleDefaultVoucher = saleDefaultVouchers[0];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: currentSystem.themeData,
      child: Scaffold(
        appBar: AppBar(
            title: ListTile(
          subtitle: Text(widget.goods.goodsDesc,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              softWrap: true,
              textAlign: TextAlign.right,
              style: TextStyle(
                fontFamily: 'IRANSansUltraLight',
                fontSize: 10.0,
                color: Colors.white,
              )),
          title: Text("تعداد",
              textAlign: TextAlign.right,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 12.0,
                color: Colors.white,
              )),
        )),
        body: Numpad<double>(
          value: widget.quantity,
          onChanged: (quantity) {
            this.quantity = quantity;
          },
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: _bottomNavigationBar(),
      ),
    );
  }

  Widget _bottomNavigationBar() {
    IconData iconData;
    if (saleDefaultVoucher.isFixedFee) {
      iconData = Icons.done;
    } else {
      iconData = Icons.arrow_forward;
    }

    Function() onPressed = () {
      if (this.quantity == null || this.quantity == 0) {
        TAlertDialog(context: context).show(
            title: "تعداد مجاز نیست",
            content: "تعداد اقلام باید بیشتر از صفر باشد");
      } else if (this.quantity > widget.quantity) {
        TAlertDialog(context: context).show(
            title: "تعداد مجاز نیست",
            content: "تعداد اقلام نباید بیشتر از مقدار اولیه " +
                widget.quantity.toString() +
                " باشد");
      } else {
        Voucher voucher = TIW.getVoucher(context);
        VoucherDetail voucherDetail = VoucherDetail(
          widget.index,
          status: VoucherDetailStatus.DEFAULT_ACTIVE,
          goods: widget.goods,
          goodsUnit: widget.goodsUnit,
          quantity: this.quantity,
          fee: widget.fee,
          baseDReferID: widget.voucherDetailID,
          dReferID: widget.voucherDetailID,
        );
        if (saleDefaultVoucher.isFixedFee) {
          /// فی ثابت در سند است و درنتیجه نمیشود قیمت را تغییر داد و نباید صفحه قیمت نمایش یابد
          switch (widget.index) {
            case -1: //افزودن کالا به سند
              voucher.voucherDetails.add(voucherDetail);
              TIW.setVoucher(context, voucher);
              showFlushBar("کالا به سند اضافه شد", () {
                Navigator.popUntil(
                  context,
                  ModalRoute.withName("/detailsRefer"),
                );
              });
              break;
            default: //ویرایش کالای فعلی سند
              voucher.voucherDetails[widget.index] = voucherDetail;
              TIW.setVoucher(context, voucher);
              showFlushBar("کالا در سند ویرایش شد", () {
                Navigator.popUntil(
                  context,
                  ModalRoute.withName("/saleHome"),
                );
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => widget.parentWidget()),
                );
              });
          }
        } else {
          // در حال حاضر فارمد نمی تواند فی را تغییر دهد و این قسمت را پیاده سازی نکردم
          // Navigator.push(
          //   context,
          //   MaterialPageRoute(
          //       builder: (BuildContext context) => AddingPrice(
          //             index: widget.index,
          //             goods: widget.goods,
          //             goodsUnit: widget.goodsUnit,
          //             quantity: this.quantity,
          //             price: widget.price,
          //             parentWidget: widget.parentWidget,
          //           )),
          // );
        }
      }
    };
    return FloatingActionButton(
        tooltip: "ادامه", child: Icon(iconData), onPressed: onPressed);
  }

  showFlushBar(String messageText, Function onDISMISSED) {
    Flushbar(
      leftBarIndicatorColor: Colors.green[300],
      margin: EdgeInsets.all(8),
      // borderRadius: 8,
      messageText: Text(messageText,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'IRANSansBlack',
            color: Colors.white,
            fontSize: 14.0,
          )),
      duration: Duration(seconds: 2),
    )..show(context).then((result) {
        onDISMISSED();
      });
  }
}
