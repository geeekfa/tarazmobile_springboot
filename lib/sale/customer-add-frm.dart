import 'dart:convert';

import 'package:com_tarazgroup/models/customer.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/modules/dialog/alert_dialog.dart';
import 'package:com_tarazgroup/modules/future/webservice.dart';
import 'package:com_tarazgroup/modules/rest/restful.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart';

typedef OnSaved = void Function(String val);

class CustomerAddFrm extends StatefulWidget {
  @override
  _CustomerAddFrmState createState() => _CustomerAddFrmState();
}

class _CustomerAddFrmState extends State<CustomerAddFrm> {
  Setting setting;
  final formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  String perComFName;
  String perComLName;
  String percomTel;
  String percomMobile;
  String perComAddress;

  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    super.initState();
  }

  ListTile _item(
      {String labelText,
      String validatorErrMsg,
      int maxLength,
      int maxLines,
      OnSaved onSaved}) {
    return ListTile(
      title: Directionality(
        textDirection: TextDirection.rtl,
        child: TextFormField(
          controller: TextEditingController(text: perComFName),
          maxLength: maxLength,
          maxLines: maxLines,
          decoration: new InputDecoration(
            labelText: labelText,
            labelStyle: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 11.0,
              color: Colors.black,
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
          keyboardType: TextInputType.text,
          validator: (value) {
            if (value == null || value == "") {
              return validatorErrMsg;
            }
            return null;
          },
          onSaved: onSaved,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    String perComFNameTitle = "نام";
    String perComLNameTitle = "نام خانوادگی";
    String percomTelTitle = "تلفن";
    String percomMobileTitle = "موبایل";
    String perComAddressTitle = "آدرس";

    String perComFNameErrMsg = "فیلد نام را وارد کنید!";
    String perComLNameErrMsg = "فیلد نام خانوادگی را وارد کنید!";
    String percomTelErrMsg = "فیلد تلفن را وارد کنید!";
    String percomMobileErrMsg = "فیلد موبایل را وارد کنید!";
    String perComAddressErrMsg = "فیلد آدرس را وارد کنید!";
    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title: ListTile(
            title: Text("افزودن مشتری",
                textAlign: TextAlign.right,
                style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                    color: Colors.white)),
          ),
          actions: <Widget>[
            IconButton(
                icon: Icon(
                  FontAwesomeIcons.save,
                  size: 16,
                ),
                tooltip: 'ذخیره',
                onPressed: () async {
                  if (formKey.currentState.validate()) {
                    formKey.currentState.save();
                    WebService.callModalMode<Customer>(
                      context,
                      value: Customer(),
                      futureResponse: _futureResponseInsertCustomer,
                      waitingMsg: "ذخیره مشتری",
                      doneMsg: "ذخیره مشتری",
                      on200object: (customer) {
                        setState(() {
                          this.perComFName = null;
                          this.perComLName = null;
                          this.percomTel = null;
                          this.percomMobile = null;
                          this.perComAddress = null;
                        });
                        TAlertDialog(context: context).show(
                            title: "مشتری اضافه شد",
                            content:
                                "مشتری با موفقیت اضافه شد");
                      },
                    );
                  }
                }),
            IconButton(
                icon: Icon(
                  FontAwesomeIcons.trashAlt,
                  size: 16,
                ),
                tooltip: 'خالی کردن فرم',
                onPressed: () {
                  setState(() {
                    this.perComFName = null;
                    this.perComLName = null;
                    this.percomTel = null;
                    this.percomMobile = null;
                    this.perComAddress = null;
                  });
                })
          ],
        ),
        body: Form(
            key: formKey,
            child: ListView(
              children: <Widget>[
                _item(
                    labelText: perComFNameTitle,
                    validatorErrMsg: perComFNameErrMsg,
                    maxLength: 15,
                    maxLines: 1,
                    onSaved: (String val) {
                      perComFName = val;
                    }),
                _item(
                    labelText: perComLNameTitle,
                    validatorErrMsg: perComLNameErrMsg,
                    maxLength: 15,
                    maxLines: 1,
                    onSaved: (String val) {
                      perComLName = val;
                    }),
                _item(
                    labelText: percomTelTitle,
                    validatorErrMsg: percomTelErrMsg,
                    maxLength: 10,
                    maxLines: 1,
                    onSaved: (String val) {
                      percomTel = val;
                    }),
                _item(
                    labelText: percomMobileTitle,
                    validatorErrMsg: percomMobileErrMsg,
                    maxLength: 10,
                    maxLines: 1,
                    onSaved: (String val) {
                      percomMobile = val;
                    }),
                _item(
                    labelText: perComAddressTitle,
                    validatorErrMsg: perComAddressErrMsg,
                    maxLength: 200,
                    maxLines: 5,
                    onSaved: (String val) {
                      perComAddress = val;
                    }),
              ],
            )));
  }

  Future<Response> _futureResponseInsertCustomer() {
    return TRestful.post(
      "acc/percoms",
      setting: setting,
      body: jsonEncode({
        "accPerCom": {
          "perComCode": null,
          "perComTitle": null,
          "perComFName": this.perComFName,
          "perComLName": this.perComLName,
          "perComNameLatin": null,
          "sexID": false,
          "perComAddress": this.perComAddress,
          "tafsiliTypeID": null,
          "perComTypeID": 0,
          "isMPerson": false,
          "fatherName": null,
          "isNotActive": false,
          "isMpp": false,
          "percomName": null,
          "mpp": false
        },
        "accPerComOther": {
          "percomTel": this.percomTel,
          "percomMobile": this.percomMobile,
          "nationalCode": null,
          "customerCode": null,
          "salesManCode": null,
          "marketingManCode": null,
          "providerCode": null,
          "customerClassID": null,
          "marketingManClassID": null,
          "customerGeoID": null,
          "salesManGeoID": null,
          "marketingManGeoID": null,
          "providerGeoID": null,
          "iSProviderManNotActive": false,
          "iSStockerNotActive": false,
          "birthDateStocker": null,
          "numberMemberStocker": null,
          "identityCodeStocker": null,
          "webUserType": null,
          "isSaleCustomer": true,
          "isSaleMan": false,
          "isMarketingMan": false,
          "providerMan": false,
          "isPerson": false,
          "isStocker": false,
          "salesManClassID": null,
          "isForeignProvider": false,
          "isCustomerNotActive": false,
          "isSalesManNotActive": false,
          "isMarketingManNotActive": false,
          "isAllowDuplicate": false
        }
      }),
      timeout: 200,
    );
  }
}
