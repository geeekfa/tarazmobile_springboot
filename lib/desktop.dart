import 'package:com_tarazgroup/models/sale_setting.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/system.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/modules/dialog/confirm_dialog.dart';
import 'package:com_tarazgroup/modules/future/webservice.dart';
import 'package:com_tarazgroup/modules/rest/restful.dart';
import 'package:com_tarazgroup/modules/view/grid_tile_view.dart';
import 'package:com_tarazgroup/sale/home.dart' as saleHome;
import 'package:com_tarazgroup/inv/home.dart' as invHome;
// import 'package:com_tarazgroup/sale/home.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Desktop extends StatefulWidget {
  @override
  _DesktopState createState() => _DesktopState();
}

class _DesktopState extends State<Desktop> {
  Setting setting;
  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          title: ListTile(
            title: Text("نرم افزار جامع همراه تراز سامانه",
                textAlign: TextAlign.right,
                style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                    color: Colors.white)),
          ),
        ),
        drawer: _Drawer(
          setting: setting,
        ),
        body: _Body(
          setting: setting,
        ),
      ),
    );
  }
}

class _Drawer extends StatelessWidget {
  final Setting setting;

  const _Drawer({this.setting});
  @override
  Widget build(BuildContext context) {
    String name = setting.name ?? "";
    String family = setting.family ?? "";
    return Drawer(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: ListTile(
            title: Text(name + " " + family,
                textAlign: TextAlign.right,
                style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                    color: Colors.white)),
          ),
        ),
        body: ListView(
          children: <Widget>[
            // ListTile(
            //   leading: Icon(Icons.settings),
            //   title: Text("تنظیمات",
            //       style: TextStyle(
            //         fontFamily: 'IRANSans',
            //         fontSize: 12.0,
            //       )),
            // ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text("خروج",
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  )),
              onTap: () async {
                bool yesNo = await TConfirmDialog(
                  context: context,
                ).show("خروج", "آیا از نرم افزار خارج می شوید?");

                if (yesNo) {
                  SharedPreferences sp = await SharedPreferences.getInstance();
                  String ip = sp.getString("ip");
                  String port = sp.getString("port");
                  bool mustSendLocation =
                      sp.getBool("mustSendLocation") ?? false;
                  bool hasPrice = sp.getBool("hasPrice") ?? true;
                  bool hasRemain = sp.getBool("hasRemain") ?? true;
                  await sp.clear();
                  sp.setString("ip", ip);
                  sp.setString("port", port);
                  sp.setBool("token", null);
                  sp.setBool("mustSendLocation", mustSendLocation);
                  sp.setBool("hasPrice", hasPrice);
                  sp.setBool("hasRemain", hasRemain);
                  Navigator.pushNamedAndRemoveUntil(context, '/', (_) => false);
                }
              },
            )
          ],
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  final Setting setting;
  const _Body({this.setting});
  @override
  Widget build(BuildContext context) {
    List<String> apps = setting.apps;
    bool isActiveSale = apps.contains("sale");
    bool isActiveInv = apps.contains("inv");
    setting.sale.isActive = isActiveSale;
    setting.sale.onTap = () {
      WebService.callModalMode<SaleSettingServer>(
        context,
        value: SaleSettingServer(),
        futureResponse: _futureResponseApplicationinfoSale,
        waitingMsg: "دریافت تنظیمات فروش",
        doneMsg: "دریافت تنظیمات فروش",
        on200object: (saleSetting) {
          setting.sale.saleSettingServer = saleSetting;
          Navigator.of(context).push(
            PageRouteBuilder(
              transitionDuration: Duration(milliseconds: 0),
              settings: RouteSettings(name: "/saleHome"),
              pageBuilder: (context, animation1, animation2) {
                return saleHome.Home();
              },
            ),
          );
        },
        // on200list: (saleSettings) {},
      );
    };
    setting.inv.isActive = isActiveInv;
    setting.inv.onTap = () {
      Navigator.of(context).push(
        PageRouteBuilder(
          transitionDuration: Duration(milliseconds: 0),
          settings: RouteSettings(name: "/invHome"),
          pageBuilder: (context, animation1, animation2) {
            return invHome.Home();
          },
        ),
      );
    };
    List<System> systems = [];
    systems.add(setting.sale);
    systems.add(setting.inv);
    return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 20.0,
          mainAxisSpacing: 20.0,
        ),
        primary: false,
        itemCount: systems.length,
        padding: const EdgeInsets.all(10.0),
        itemBuilder: (BuildContext context1, int index) {
          return GridTileView.defaultView(
            systems[index].iconData,
            systems[index].title,
            systems[index].isActive
                ? systems[index].themeData.primaryColor
                : Colors.grey[300],
            systems[index].isActive ? systems[index].onTap : null,
          );
        });
  }

  Future<Response> _futureResponseApplicationinfoSale() async {
    return TRestful.get(
      "applicationinfo/sale",
      setting: setting,
      timeout: 25,
    );
  }
}
