import 'package:com_tarazgroup/desktop.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/token.dart';
import 'package:com_tarazgroup/models/user.dart';
import 'package:com_tarazgroup/modules/future/webservice.dart';
import 'package:com_tarazgroup/modules/rest/restful.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  String username, password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: Center(
        child: Form(
          key: formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTileTheme(
                iconColor: Theme.of(context).primaryColor,
                child: ListTile(
                  leading: Icon(FontAwesomeIcons.lock),
                ),
              ),
              ListTile(
                title: TextFormField(
                  // initialValue: "majidi",
                  decoration: new InputDecoration(
                    labelText: "Username",
                    contentPadding: EdgeInsets.all(4.0),
                  ),
                  validator: (value) {
                    if (value == null || value == "")
                      return 'نام کاربری وارد نشده است';
                    else
                      return null;
                  },
                  onSaved: (String val) {
                    username = val;
                  },
                ),
              ),
              ListTile(
                title: TextFormField(
                  // initialValue: "1",
                  obscureText: true,
                  decoration: new InputDecoration(
                    labelText: "Password",
                    contentPadding: EdgeInsets.all(4.0),
                  ),
                  validator: (value) {
                    if (value == null || value == "")
                      return 'پسورد وارد نشده است';
                    else
                      return null;
                  },
                  onSaved: (String val) {
                    password = val;
                  },
                ),
              ),
              ListTile(
                title: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Theme.of(context).primaryColor,
                  ),
                  child: Text("ورود",
                      style: TextStyle(
                        fontFamily: 'IRANSans',
                        fontSize: 16.0,
                      )),
                  onPressed: () {
                    if (formKey.currentState.validate()) {
                      formKey.currentState.save();
                      WebService.callModalMode<Token>(
                        context,
                        value: Token(),
                        futureResponse: _authenticate,
                        waitingMsg: "ورود به سیستم",
                        doneMsg: "ورود به سیستم",
                        on200object: (token) {
                          SharedPreferences.getInstance().then((sp) {
                            Setting setting = TIW.getSetting(context);
                            setting.token = token.token;
                            sp.setString("token", token.token);
                            WebService.callModalMode<User>(
                              context,
                              value: User(),
                              futureResponse:
                                  _futureResponseApplicationinfoDefault,
                              waitingMsg: "اطلاعات پیش فرض کاربر",
                              doneMsg: "اطلاعات پیش فرض کاربر",
                              on200object: (user) {
                                sp.setInt("perComID", user.perComID);
                                sp.setString("name", user.name);
                                sp.setString("family", user.family);
                                sp.setBool("isLogin", true);
                                sp.setBool("isAdmin", user.isAdmin);
                                sp.setInt("activeYearID", user.activeYearID);
                                sp.setInt("serverID", user.serverID);
                                sp.setString("serverIDs", user.serverIDs);
                                setting.perComID = user.perComID;
                                setting.name = user.name;
                                setting.family = user.family;
                                setting.isAdmin = user.isAdmin;
                                setting.activeYearID = user.activeYearID;
                                setting.serverID = user.serverID;
                                setting.serverIDs = user.serverIDs;
                                TIW.setSetting(context, setting);
                                Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Desktop()),
                                );
                              },
                            );
                          });
                        },
                      );
                    }
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<Response> _futureResponseApplicationinfoDefault() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setString("androidId", androidInfo.androidId);
    sp.setString("userName", username);
    sp.setString("password", password);
    Setting setting = TIW.getSetting(context);
    setting.androidId = androidInfo.androidId;
    setting.userName = username;
    setting.password = password;
    TIW.setSetting(context, setting);
    return TRestful.get(
      "applicationinfo/default",
      setting: setting,
      timeout: 25,
    );
  }

  Future<Response> _authenticate() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setString("androidId", androidInfo.androidId);
    sp.setString("userName", username);
    sp.setString("password", password);
    Setting setting = TIW.getSetting(context);
    setting.androidId = androidInfo.androidId;
    setting.userName = username;
    setting.password = password;
    return TRestful.authenticate(setting: setting);
  }
}
