import 'package:flutter/material.dart';

class TTextField extends StatefulWidget {
  final String title;
  final void Function(String t) onChanged;
  final String Function() onValue;
  final int maxLines;
  final TextInputType keyboardType;

  const TTextField({
    this.title,
    this.onChanged,
    this.onValue,
    this.maxLines,
    this.keyboardType,
  });
  @override
  _TTextFieldState createState() => _TTextFieldState();
}

class _TTextFieldState extends State<TTextField> {
  final textEditingController = TextEditingController();
  @override
  void initState() {
    textEditingController.text = widget.onValue();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Directionality(
        textDirection: TextDirection.rtl,
        child: TextField(
          controller: textEditingController,
          maxLines: widget.maxLines??1,
          keyboardType: widget.keyboardType??TextInputType.text,
          style: TextStyle(
            fontFamily: 'IRANSansLight',
            color: Colors.black,
            fontSize: 12.0,
          ),
          decoration: new InputDecoration(
            labelText: widget.title,
            labelStyle: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 11.0,
              color: Colors.black,
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
          onChanged: (txt) {
            widget.onChanged(txt);
          },
        ),
      ),
    );
  }
}
