import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class TTextFieldDate extends StatefulWidget {
  final String title;
  final void Function(String t) onChanged;
  final String Function() onValue;

  const TTextFieldDate({
    this.title,
    this.onChanged,
    this.onValue,
  });
  @override
  _TTextFieldDateState createState() => _TTextFieldDateState();
}

class _TTextFieldDateState extends State<TTextFieldDate> {
  final textEditingController = MaskedTextController(mask: '0000/00/00');
  
  @override
  void initState() {
    textEditingController.text = widget.onValue();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Directionality(
        textDirection: TextDirection.rtl,
        child: TextField(
          controller: textEditingController,
          keyboardType:TextInputType.number,
          textAlign: TextAlign.left,
          style: TextStyle(
            fontFamily: 'IRANSansLight',
            color: Colors.black,
            fontSize: 12.0,
          ),
          decoration: new InputDecoration(
            labelText: widget.title,
            labelStyle: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 11.0,
              color: Colors.black,
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
          onChanged: (txt) {
            widget.onChanged(txt);
          },
        ),
      ),
    );
  }
}
