import 'dart:convert';
import 'package:com_tarazgroup/models/query_param.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class TRestful {
  static Future<Response> get(
    String resource, {
    Setting setting,
    List<QueryParam> queryParams,
    String pathParam,
    int timeout,
    bool needToken = true,
  }) {
    String ip = setting.ip;
    String port = setting.port;

    String queryParameters = "";

    if (queryParams != null) {
      for (var i = 0; i < queryParams.length; i++) {
        switch (i) {
          case 0:
            queryParameters +=
                "?" + queryParams[i].name + "=" + queryParams[i].value;
            break;
          default:
            queryParameters +=
                "&" + queryParams[i].name + "=" + queryParams[i].value;
            break;
        }
      }
    }

    String url = 'http://' +
        ip +
        ':' +
        port +
        '/tws/' +
        resource +
        (pathParam == null ? "" : "/" + pathParam) +
        queryParameters;

    Future<Response> futureResponse;
    var uri = Uri.parse(url);
    if (needToken) {
      futureResponse = http.get(uri, headers: {
        "accept": "application/json",
        "Authorization": "Bearer " + setting.token
      }).timeout(Duration(seconds: timeout));
    } else {
      futureResponse = http.get(uri).timeout(Duration(seconds: timeout));
    }
    return futureResponse;
  }

  static Future<Response> file(
    String resource, {
    Setting setting,
    List<QueryParam> queryParams,
    String pathParam,
    int timeout,
    bool needToken = true,
  }) {
    String ip = setting.ip;
    String port = setting.port;

    String queryParameters = "";

    if (queryParams != null) {
      for (var i = 0; i < queryParams.length; i++) {
        switch (i) {
          case 0:
            queryParameters +=
                "?" + queryParams[i].name + "=" + queryParams[i].value;
            break;
          default:
            queryParameters +=
                "&" + queryParams[i].name + "=" + queryParams[i].value;
            break;
        }
      }
    }

    String url = 'http://' +
        ip +
        ':' +
        port +
        '/tws/' +
        resource +
        (pathParam == null ? "" : "/" + pathParam) +
        queryParameters;

    Future<Response> futureResponse;
    var uri = Uri.parse(url);
    futureResponse = http.get(uri, headers: {
      "Authorization": "Bearer " + setting.token
    }).timeout(Duration(seconds: timeout));
    return futureResponse;
  }

  static Future<Response> post(
    String resource, {
    Setting setting,
    String body,
    int timeout,
  }) {
    String ip = setting.ip;
    String port = setting.port;
    String url = 'http://' + ip + ':' + port + '/tws/' + resource;
    var uri = Uri.parse(url);
    Future<Response> futureResponse;
    futureResponse = http
        .post(uri,
            body: body,
            headers: {
              "Authorization": "Bearer " + setting.token,
              "content-type": "application/json"
            },
            encoding: Encoding.getByName("utf-8"))
        .timeout(Duration(seconds: timeout));
    return futureResponse;
  }

  static Future<Response> authenticate({Setting setting}) {
    String ip = setting.ip;
    String port = setting.port;
    String username = setting.userName;
    String password = setting.password;
    String url = 'http://' + ip + ':' + port + '/tws/authenticate';
    var uri = Uri.parse(url);
    Future<Response> futureResponse = http
        .post(uri,
            body: '{"username": "' +
                username +
                '", "password": "' +
                password +
                '"}',
            headers: {"content-type": "application/json"},
            encoding: Encoding.getByName("utf-8"))
        .timeout(Duration(seconds: 10));
    return futureResponse;
  }

  static Future<Response> refreshtoken({Setting setting}) {
    String ip = setting.ip;
    String port = setting.port;
    String url = 'http://' + ip + ':' + port + '/tws/authenticate/refreshtoken';
    var uri = Uri.parse(url);
    Future<Response> futureResponse = http.get(
      uri,
      headers: {
        "accept": "application/json",
        "Authorization": "Bearer " + setting.token
      },
    ).timeout(Duration(seconds: 10));
    return futureResponse;
  }
}
