import 'package:com_tarazgroup/inv/inv_goods.dart';
import 'package:com_tarazgroup/inv/inv_voucher_frm.dart';
import 'package:com_tarazgroup/models/goods_group.dart';
import 'package:com_tarazgroup/models/progressbar.dart';
import 'package:com_tarazgroup/models/query_param.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher.dart';
import 'package:com_tarazgroup/models/voucher_detail.dart';
import 'package:com_tarazgroup/modules/dialog/alert_dialog.dart';
import 'package:com_tarazgroup/modules/dialog/confirm_dialog.dart';
import 'package:com_tarazgroup/modules/dialog/goods_group_filter_dialog.dart';
import 'package:com_tarazgroup/modules/future/webservice.dart';
import 'package:com_tarazgroup/modules/rest/restful.dart';
import 'package:com_tarazgroup/modules/view/grid_tile_view.dart';
import 'package:com_tarazgroup/sale/detail_refer_goods.dart';
import 'package:com_tarazgroup/sale/sale_goods.dart';
import 'package:com_tarazgroup/sale/sale_voucher_frm.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:math' as math;
import 'package:barcode_scan/barcode_scan.dart';
import 'package:http/http.dart';

class VoucherFrm extends StatefulWidget {
  final int initialIndex;
  final int
      voucherType; // voucherType==1 -> وارده  voucherType==1 -> صادره   voucherType==1 -> انبار به انبار
  final String title;
  final String routeName;
  final void Function() onSend;
  final void Function() onDelete;
  final List<Widget> defaultTab;
  final List<Widget> referTab;
  final List<Widget> otherTab;
  final List<Widget> sliverPersistentHeaderDelegateActions;
  final List<Widget> Function() buildVoucherDetails;
  final Widget Function() onFooter;
  final void Function() onTapGoodsList;
  final void Function(GoodsGroup goodsGroup) onSelectGoodsGroup;
  final void Function(String barcode) onBarcodeScanned;

  final bool Function() onPermissionOpenGoodsList;

  const VoucherFrm({
    @required this.initialIndex,
    @required this.voucherType,
    @required this.title,
    @required this.routeName,
    @required this.onSend,
    @required this.onDelete,
    @required this.defaultTab,
    @required this.referTab,
    @required this.otherTab,
    @required this.sliverPersistentHeaderDelegateActions,
    @required this.buildVoucherDetails,
    @required this.onFooter,
    @required this.onTapGoodsList,
    @required this.onSelectGoodsGroup,
    @required this.onBarcodeScanned,
    @required this.onPermissionOpenGoodsList,
  });
  @override
  _VoucherFrmState createState() => _VoucherFrmState();
}

class _VoucherFrmState extends State<VoucherFrm> {
  Setting setting;
  List<Widget> voucherDetailWidgets;
  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    voucherDetailWidgets = widget.buildVoucherDetails();
    widget.defaultTab.removeWhere((item) {
      if (item is Container) {
        return true;
      }
      return false;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: setting.currentSystem.themeData,
      child: WillPopScope(
        onWillPop: () async {
          Voucher voucher = TIW.getVoucher(context);
          if (voucher.voucherHeader.voucherType.voucherTypeID == null) {
            return new Future.value(true);
          } else {
            bool yesNo = await TConfirmDialog(
              context: context,
            ).show("خروج از سند",
                "با خروج از سند ، اطلاعات وارد شده فعلی پاک خواهد. آیا ادامه می دهید ؟");

            if (yesNo) {
              TIW.setVoucher(context, null);
              return new Future.value(true);
            }
          }
          return new Future.value(false);
        },
        child: Scaffold(
            appBar: AppBar(
              title: ListTile(
                title: Text(widget.title,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        fontFamily: 'IRANSans',
                        fontSize: 12.0,
                        color: Colors.white)),
              ),
              actions: <Widget>[
                IconButton(
                    icon: Icon(
                      FontAwesomeIcons.trash,
                      size: 16,
                    ),
                    tooltip: 'حذف اطلاعات جاری',
                    onPressed: () async {
                      bool yesNo = await TConfirmDialog(
                        context: context,
                      ).show(
                          "حذف اطلاعات جاری", "آیا اطلاعات سند جاری حذف شود؟");

                      if (yesNo) {
                        TIW.setVoucher(context, null);
                        widget.onDelete();
                      }
                    }),
                IconButton(
                    icon: Icon(
                      FontAwesomeIcons.solidPaperPlane,
                      size: 16,
                    ),
                    tooltip: 'ارسال سند به سرور',
                    onPressed: () async {
                      List<VoucherDetail> voucherDetails =
                          TIW.getVoucher(context).voucherDetails;
                      if (voucherDetails == null ||
                          voucherDetails.length == 0) {
                        TAlertDialog(context: context).show(
                            title: "کالایی به سند اضافه نشده است",
                            content:
                                "برای ذخیره سند ، ابتدا به این سند کالا اضافه کنید");
                        return;
                      }

                      bool yesNo = await TConfirmDialog(
                        context: context,
                      ).show("ذخیره سند",
                          "پس از ذخیره سند دیگر قادر به تغییر آن نخواهید بود . آیا از ذخیره سند اطمینان دارید؟");

                      if (yesNo) {
                        widget.onSend();
                      }
                    }),
              ],
            ),
            body: CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  // backgroundColor: setting.currentSystem.color400,
                  automaticallyImplyLeading: false,
                  expandedHeight: 210.0,
                  floating: true,
                  pinned: false,
                  flexibleSpace: FlexibleSpaceBar(
                    background: _FlexibleSpaceBarBackgroundHeader(
                      initialIndex: widget.initialIndex,
                      defaultTab: widget.defaultTab,
                      referTab: widget.referTab,
                      otherTab: widget.otherTab,
                    ),
                  ),
                ),
                SliverPersistentHeader(
                  delegate: _SliverPersistentHeaderDelegate(
                      actions: widget.sliverPersistentHeaderDelegateActions),
                  pinned: true,
                ),
                voucherDetailWidgets.length == 0
                    ? SliverFillRemaining(
                        child: Center(
                            child: Text(
                          "کالایی به این سند اضافه نشده است",
                          style: TextStyle(
                            fontFamily: 'IRANSansUltraLight',
                            fontSize: 14.0,
                            color: Colors.black26,
                          ),
                        )),
                      )
                    : SliverList(
                        delegate: SliverChildBuilderDelegate(
                          (BuildContext context, int index) {
                            final int itemIndex = index ~/ 2;
                            if (index.isEven) {
                              return voucherDetailWidgets[itemIndex];
                            }
                            return Divider(thickness: 4);
                          },
                          semanticIndexCallback:
                              (Widget widget, int localIndex) {
                            if (localIndex.isEven) {
                              return localIndex ~/ 2;
                            }
                            return null;
                          },
                          childCount:
                              math.max(0, voucherDetailWidgets.length * 2 - 1),
                        ),
                      )
              ],
            ),
            bottomNavigationBar: SizedBox(
              height: 107,
              child: Column(
                children: <Widget>[
                  Container(
                      height: 44,
                      padding: EdgeInsets.only(
                        left: 5,
                        right: 5,
                      ),
                      color: setting.currentSystem.themeData.accentColor,
                      child: widget.onFooter()),
                  BottomNavigationBar(
                    currentIndex: 0,
                    items: <BottomNavigationBarItem>[
                      BottomNavigationBarItem(
                        icon: Column(
                          children: <Widget>[
                            Icon(
                              Icons.bubble_chart,
                              size: 25,
                              color: setting
                                  .currentSystem.themeData.primaryColorDark,
                            ),
                            // Text(
                            //   "کالاها",
                            //   style: TextStyle(
                            //     fontFamily: 'IRANSansUltraLight',
                            //     fontSize: 10.0,
                            //     color: Colors.black,
                            //   ),
                            // )
                          ],
                        ),
                        label: "کالاها",
                      ),
                      BottomNavigationBarItem(
                        icon: Column(
                          children: <Widget>[
                            Icon(
                              Icons.category,
                              size: 25,
                              color: setting
                                  .currentSystem.themeData.primaryColorDark,
                            ),
                            // Text(
                            //   "گروه های کالا",
                            //   style: TextStyle(
                            //     fontFamily: 'IRANSansUltraLight',
                            //     fontSize: 10.0,
                            //     color: Colors.black,
                            //   ),
                            // )
                          ],
                        ),
                        label: "گروه های کالا",
                      ),
                      BottomNavigationBarItem(
                        icon: Column(
                          children: <Widget>[
                            Icon(
                              FontAwesomeIcons.qrcode,
                              size: 25,
                              color: setting
                                  .currentSystem.themeData.primaryColorDark,
                            ),
                            // Text(
                            //   "اسکن بارکد",
                            //   style: TextStyle(
                            //     fontFamily: 'IRANSansUltraLight',
                            //     fontSize: 10.0,
                            //     color: Colors.black,
                            //   ),
                            // )
                          ],
                        ),
                        label: "اسکن بارکد",
                      ),
                    ],
                    onTap: (int index) async {
                      switch (index) {
                        case 0: //goods
                          if (widget.onPermissionOpenGoodsList()) {
                            widget.onTapGoodsList();
                          }
                          break;
                        case 1: //goodsGroups
                          if (widget.onPermissionOpenGoodsList()) {
                            GoodsGroup goodsGroup =
                                await Navigator.push<GoodsGroup>(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      _GoodsGroupPage(
                                        routeName: widget.routeName,
                                        voucherType: widget.voucherType,
                                      )),
                            );
                            if (goodsGroup != null) {
                              widget.onSelectGoodsGroup(goodsGroup);
                            }
                          }
                          break;
                        case 2: //qrcode
                          if (widget.onPermissionOpenGoodsList()) {
                            // var result = await BarcodeScanner.scan();
                            // print(result);
                            BarcodeScanner.scan().then((barCode) {
                              widget.onBarcodeScanned(barCode);
                            });
                          }
                          break;
                        default:
                      }
                    },
                  )
                ],
              ),
            )),
      ),
    );
  }
}

class _FlexibleSpaceBarBackgroundHeader extends StatelessWidget {
  final int initialIndex;
  final List<Widget> defaultTab;
  final List<Widget> referTab;
  final List<Widget> otherTab;

  const _FlexibleSpaceBarBackgroundHeader({
    @required this.initialIndex,
    @required this.defaultTab,
    @required this.referTab,
    @required this.otherTab,
  });

  @override
  Widget build(BuildContext context) {
    // final System currentSystem = TIW.getSetting(context).currentSystem;
    return DefaultTabController(
      length: 3,
      initialIndex: initialIndex,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(47.0),
          child: AppBar(
            automaticallyImplyLeading: false,
            // backgroundColor: currentSystem.color400,
            title: TabBar(
              indicatorWeight: 2,
              indicatorColor: Colors.white,
              tabs: <Widget>[
                Tab(
                  child: Text(
                    "پیش فرض",
                    style: TextStyle(
                      fontFamily: 'IRANSans',
                      fontSize: 10.0,
                      color: Colors.white,
                    ),
                  ),
                ),
                Tab(
                  child: Text(
                    "مرجع",
                    style: TextStyle(
                      fontFamily: 'IRANSans',
                      fontSize: 10.0,
                      color: Colors.white,
                    ),
                  ),
                ),
                Tab(
                  child: Text(
                    "سایر",
                    style: TextStyle(
                      fontFamily: 'IRANSans',
                      fontSize: 10.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: defaultTab,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: referTab,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: otherTab,
            ),
          ],
        ),
      ),
    );
  }
}

class _SliverPersistentHeaderDelegate extends SliverPersistentHeaderDelegate {
  final double height = 86;
  final List<Widget> actions;

  _SliverPersistentHeaderDelegate({@required this.actions});
  @override
  double get minExtent => height;
  @override
  double get maxExtent => height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    // final System currentSystem = TIW.getSetting(context).currentSystem;

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: AppBar(
          automaticallyImplyLeading: false,
          // backgroundColor: currentSystem.color400,
          actions: actions,
        ),
      ),
    );
  }

  @override
  bool shouldRebuild(_SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}

class _GoodsGroupPage extends StatefulWidget {
  final String routeName;
  final int
      voucherType; // voucherType==1 -> وارده  voucherType==1 -> صادره   voucherType==1 -> انبار به انبار
  const _GoodsGroupPage({
    @required this.routeName,
    @required this.voucherType,
  });
  @override
  __GoodsGroupPageState createState() => __GoodsGroupPageState();
}

class __GoodsGroupPageState extends State<_GoodsGroupPage> {
  Setting setting;
  int fatherID;
  List<String> parentIDs;
  String groupDesc;

  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          if (groupDesc != null) {
            Navigator.popUntil(
              context,
              ModalRoute.withName(widget.routeName),
            );
            if (widget.routeName == '/saleHome') {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => SaleVoucherFrm(
                          initialIndex: 0,
                        )),
              );
            } else if (widget.routeName == '/invHome') {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => InvVoucherFrm(
                          initialIndex: 0,
                          voucherType: widget.voucherType,
                        )),
              );
            }

            return new Future.value(true);
          }
          if (parentIDs == null) {
            Navigator.popUntil(
              context,
              ModalRoute.withName(widget.routeName),
            );
            if (widget.routeName == '/saleHome') {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => SaleVoucherFrm(
                          initialIndex: 0,
                        )),
              );
            } else if (widget.routeName == '/invHome') {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => InvVoucherFrm(
                          initialIndex: 0,
                          voucherType: widget.voucherType,
                        )),
              );
            }

            return new Future.value(true);
          }
          if (parentIDs.length == 1) {
            Navigator.popUntil(
              context,
              ModalRoute.withName(widget.routeName),
            );
            if (widget.routeName == '/saleHome') {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => SaleVoucherFrm(
                          initialIndex: 0,
                        )),
              );
            } else if (widget.routeName == '/invHome') {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => InvVoucherFrm(
                          initialIndex: 0,
                          voucherType: widget.voucherType,
                        )),
              );
            }
            return new Future.value(true);
          }
          String grandFatherID = parentIDs[parentIDs.length - 2];
          setState(() {
            fatherID = int.parse(grandFatherID);
          });
          return new Future.value(false);
        },
        child: Theme(
          data: setting.currentSystem.themeData,
          child: Scaffold(
            appBar: AppBar(
              title: ListTile(
                title: Text("گروه های کالا",
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontFamily: 'IRANSans',
                      fontSize: 12.0,
                      color: Colors.white,
                    )),
              ),
              actions: <Widget>[
                IconButton(
                    icon: Icon(
                      FontAwesomeIcons.filter,
                      size: 16,
                      color: Colors.white,
                    ),
                    tooltip: 'فیلتر گروه کالا',
                    onPressed: () {
                      GoodsGroupFilterDialog(context: context)
                          .show("جستجوی گروه کالا", groupDesc)
                          .then((groupDesc) {
                        setState(() {
                          this.groupDesc = groupDesc;
                        });
                      });
                    }),
                IconButton(
                    icon: Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        Icon(
                          FontAwesomeIcons.filter,
                          size: 12,
                        ),
                        Icon(
                          Icons.not_interested,
                          size: 26,
                        ),
                      ],
                    ),
                    tooltip: 'حذف فیلتر جاری',
                    onPressed: () {
                      setState(() {
                        this.groupDesc = null;
                      });
                    })
              ],
            ),
            body: WebService.callInlineMode<GoodsGroup>(
              value: GoodsGroup(),
              futureResponse: _futureResponse,
              onWaiting: () {
                return Center(
                  child: SingleChildScrollView(
                    child: ProgressBar(
                      label: "... " + "گروه های کالا",
                      value: null,
                      size: 100,
                    ),
                  ),
                );
              },
              on200objectWidget: (goodsGroup) {
                return null;
              },
              on200listWidget: (goodsGroups) {
                List<String> _parentIDs = goodsGroups[0].parentIDs.split(",");
                _parentIDs.removeLast();
                parentIDs = _parentIDs;
                return GridView.builder(
                  padding: EdgeInsets.all(10),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 20.0,
                      mainAxisSpacing: 20.0),
                  primary: false,
                  itemCount: goodsGroups.length,
                  itemBuilder: (BuildContext context1, int index) {
                    return GridTileView.goodsGroupView(goodsGroups[index],
                        setting.currentSystem.themeData.primaryColor, () {
                      if (goodsGroups[index].isLeaf) {
                        // Navigator.pop(context, goodsGroups[index]);
                        Voucher voucher = TIW.getVoucher(context);
                        if (voucher
                                .voucherHeader.voucherTypeRefer.voucherTypeID !=
                            null) {
                          // یعنی مرجع ردیفی است
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                settings: RouteSettings(name: "/detailsRefer"),
                                builder: (BuildContext context) =>
                                    DetailReferGoods(
                                      storeID:
                                          voucher.voucherHeader.store.storeID,
                                      voucherTypeID: voucher.voucherHeader
                                          .voucherType.voucherTypeID,
                                      refTypeID: voucher.voucherHeader
                                          .voucherTypeRefer.voucherTypeID,
                                      systemID: voucher.voucherHeader
                                          .voucherTypeRefer.systemID,
                                      voucherTypeDesc: voucher.voucherHeader
                                          .voucherTypeRefer.voucherTypeDesc,
                                      goodsGroup: goodsGroups[index],
                                    )),
                          );
                        } else {
                          // یعنی مرجع ردیفی نیست
                          if (widget.routeName == '/saleHome') {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  settings: RouteSettings(name: "/goods"),
                                  builder: (BuildContext context) => SaleGoods(
                                        barcode: null,
                                        customerID: voucher
                                            .voucherHeader.customer.customerID,
                                        goodsGroup: goodsGroups[index],
                                        storeID:
                                            voucher.voucherHeader.store.storeID,
                                        voucherTypeID: voucher.voucherHeader
                                            .voucherType.voucherTypeID,
                                      )),
                            );
                          } else if (widget.routeName == '/invHome') {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  settings: RouteSettings(name: "/goods"),
                                  builder: (BuildContext context) => InvGoods(
                                        barcode: null,
                                        goodsGroup: goodsGroups[index],
                                        storeID:
                                            voucher.voucherHeader.store.storeID,
                                        voucherTypeID: voucher.voucherHeader
                                            .voucherType.voucherTypeID,
                                        voucherType: widget.voucherType,
                                      )),
                            );
                          }
                        }
                      } else {
                        setState(() {
                          fatherID = goodsGroups[index].groupID;
                        });
                      }
                    });
                  },
                );
              },
              on204Widget: () {
                return Container();
              },
            ),
          ),
        ));
  }

  final List<IconData> iconData = <IconData>[
    Icons.change_history,
    Icons.donut_large,
    Icons.extension,
    Icons.grade,
    Icons.invert_colors,
    Icons.opacity,
    Icons.rounded_corner,
    Icons.settings_backup_restore,
    Icons.toll,
    Icons.work,
    Icons.fiber_manual_record,
    Icons.fiber_smart_record,
    Icons.gamepad,
    Icons.stop,
    Icons.web_asset,
    Icons.swap_calls,
    Icons.whatshot,
    Icons.widgets,
    Icons.group_work,
    Icons.filter_b_and_w,
    Icons.filter_vintage,
  ];
  final math.Random r = math.Random();
  IconData randomIcon2() => iconData[r.nextInt(iconData.length)];

  Future<Response> _futureResponse() {
    List<QueryParam> queryParams = [];
    if (groupDesc != null && groupDesc != "") {
      // برای وقتی که گروه کالا سرچ می شود
      queryParams.add(QueryParam(
          queryParamType: QueryParamType.NUMBER,
          name: "filter",
          value: "IsLeaf = 1 AND GroupDesc like N'%" + this.groupDesc + "%'"));
    } else {
      if (fatherID != null) {
        queryParams.add(QueryParam(
            queryParamType: QueryParamType.NUMBER,
            name: "fatherID",
            value: fatherID.toString()));
      }
    }

    return TRestful.get(
      "inv/goodsgroups",
      setting: setting,
      queryParams: queryParams,
      timeout: 30,
    );
  }
}
