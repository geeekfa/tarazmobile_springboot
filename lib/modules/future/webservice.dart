import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:com_tarazgroup/models/progressbar.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/modules/rest/restful.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

typedef FutureResponse = Future<Response> Function();
typedef On200Object<T> = void Function(T t);
typedef On200File = void Function(Uint8List uint8List);
typedef On204File = void Function();
typedef On200List<T> = void Function(List<T> t);
typedef On204<T> = void Function();

typedef OnWaiting = Widget Function();

typedef On200ListWidget<T> = Widget Function(List<T> t);
typedef On200ObjectWidget<T> = Widget Function(T t);
typedef On204Widget<T> = Widget Function();

// typedef Done200Inline = Widget Function(dynamic mapOrList);
// typedef Done204Inline = Widget Function();

class WebService {
  static void callModalModeFile(
    BuildContext context, {
    @required FutureResponse futureResponse,
    @required String waitingMsg,
    @required String doneMsg,
    On200File on200File,
    On204File on204File
  }) {
    showGeneralDialog(
      context: context,
      pageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation) {
        return _TFutureResponseFile(
            doneMsg: doneMsg,
            waitingMsg: waitingMsg,
            futureResponse: futureResponse,
            on200File: on200File,
            on204File:on204File);
      },
      barrierColor: Color.fromRGBO(255, 255, 255, 0.9),
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      transitionDuration: const Duration(milliseconds: 200),
    );
  }

  static void callModalMode<T>(
    BuildContext context, {
    @required T value,
    @required FutureResponse futureResponse,
    @required String waitingMsg,
    @required String doneMsg,
    On200Object<T> on200object,
    On200List<T> on200list,
    On204<T> on204,
  }) {
    showGeneralDialog(
      context: context,
      pageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation) {
        return _TFutureResponseModal(
          value: value,
          futureResponse: futureResponse,
          on200object: on200object,
          on200list: on200list,
          waitingMsg: waitingMsg,
          doneMsg: doneMsg,
          on204: on204,
        );
      },
      barrierColor: Color.fromRGBO(255, 255, 255, 0.9),
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      transitionDuration: const Duration(milliseconds: 200),
    );
  }

  static Widget callInlineMode<T>({
    @required T value,
    @required FutureResponse futureResponse,
    @required OnWaiting onWaiting,
    @required On200ListWidget<T> on200listWidget,
    @required On204Widget<T> on204Widget,
    @required On200ObjectWidget<T> on200objectWidget,
  }) {
    return _TFutureResponseInline(
      value: value,
      futureResponse: futureResponse,
      onWaiting: onWaiting,
      on200listWidget: on200listWidget,
      on204Widget: on204Widget,
      on200objectWidget: on200objectWidget,
    );
  }
}

class _TFutureResponseModal<T> extends StatefulWidget {
  final T value;
  final FutureResponse futureResponse;
  final String waitingMsg;
  final String doneMsg;
  final On200Object<T> on200object;
  final On200List<T> on200list;
  final On204<T> on204;

  const _TFutureResponseModal({
    @required this.value,
    @required this.futureResponse,
    @required this.waitingMsg,
    @required this.doneMsg,
    @required this.on200object,
    @required this.on200list,
    @required this.on204,
  });

  @override
  _TFutureResponseModalState createState() => _TFutureResponseModalState<T>();
}

class _TFutureResponseModalState<T> extends State<_TFutureResponseModal<T>> {
  @override
  Widget build(BuildContext context) {
    bool canPop = true;
    return WillPopScope(
        onWillPop: () {
          return Future.value(canPop);
        },
        child: Scaffold(
          body: FutureBuilder<Response>(
              future: widget.futureResponse(),
              builder: (BuildContext context,
                  AsyncSnapshot<Response> asyncSnapshot) {
                if (asyncSnapshot.hasError) {
                  canPop = true;
                  return _ResponseWidget.onHasError(asyncSnapshot);
                }
                switch (asyncSnapshot.connectionState) {
                  case ConnectionState.none:
                    canPop = true;
                    return _ResponseWidget.onNone();
                  case ConnectionState.waiting:
                    canPop = false;
                    return _ResponseWidget.onWaiting(widget.waitingMsg);
                  case ConnectionState.active:
                    canPop = false;
                    return _ResponseWidget.onActive();
                  case ConnectionState.done:
                    canPop = true;
                    switch (asyncSnapshot.data.statusCode) {
                      case 200:
                        return _ResponseWidget.on200<T>(
                          context,
                          asyncSnapshot,
                          value: widget.value,
                          on200object: widget.on200object,
                          on200list: widget.on200list,
                          doneMsg: widget.doneMsg,
                        );
                      case 204: //no body
                        return _ResponseWidget.on204(
                          context,
                          asyncSnapshot,
                          widget.on204,
                          widget.doneMsg,
                        );
                      case 401:
                        canPop = false;
                        return _TFutureResponseToken(
                          onGetToken: (String token) {
                            setState(() {});
                          },
                          onError: () {
                            canPop = true;
                          },
                        );
                      case 403:
                        return _ResponseWidget.on403(asyncSnapshot);
                      case 404:
                        return _ResponseWidget.on404();
                      case 409:
                        return _ResponseWidget.on409(asyncSnapshot);
                      case 500:
                        return _ResponseWidget.on500(asyncSnapshot);
                    }
                    return Container();
                  default:
                    return Container();
                }
              }),
        ));
  }
}

class _TFutureResponseFile<Uint8List> extends StatefulWidget {
  final FutureResponse futureResponse;
  final String waitingMsg;
  final String doneMsg;
  final On200File on200File;
  final On204File on204File;

  const _TFutureResponseFile({
    @required this.futureResponse,
    @required this.waitingMsg,
    @required this.doneMsg,
    @required this.on200File,
    @required this.on204File,
  });

  @override
  _TFutureResponseFileState createState() =>
      _TFutureResponseFileState<Uint8List>();
}

class _TFutureResponseFileState<Uint8List>
    extends State<_TFutureResponseFile> {
  @override
  Widget build(BuildContext context) {
    bool canPop = true;
    return WillPopScope(
        onWillPop: () {
          return Future.value(canPop);
        },
        child: Scaffold(
          body: FutureBuilder<Response>(
              future: widget.futureResponse(),
              builder: (BuildContext context,
                  AsyncSnapshot<Response> asyncSnapshot) {
                if (asyncSnapshot.hasError) {
                  canPop = true;
                  return _ResponseWidget.onHasError(asyncSnapshot);
                }
                switch (asyncSnapshot.connectionState) {
                  case ConnectionState.none:
                    canPop = true;
                    return _ResponseWidget.onNone();
                  case ConnectionState.waiting:
                    canPop = false;
                    return _ResponseWidget.onWaiting(widget.waitingMsg);
                  case ConnectionState.active:
                    canPop = false;
                    return _ResponseWidget.onActive();
                  case ConnectionState.done:
                    canPop = true;
                    switch (asyncSnapshot.data.statusCode) {
                      case 200:
                        return _ResponseWidget.on200File(
                          context,
                          asyncSnapshot,
                          on200File: widget.on200File,
                          doneMsg: widget.doneMsg,
                        );
                      case 204: //no body
                        return _ResponseWidget.on204(
                          context,
                          asyncSnapshot,
                          widget.on204File,
                          widget.doneMsg,
                        );
                      case 401:
                        canPop = false;
                        return _TFutureResponseToken(
                          onGetToken: (String token) {
                            setState(() {});
                          },
                          onError: () {
                            canPop = true;
                          },
                        );
                      case 403:
                        return _ResponseWidget.on403(asyncSnapshot);
                      case 404:
                        return _ResponseWidget.on404();
                      case 409:
                        return _ResponseWidget.on409(asyncSnapshot);
                      case 500:
                        return _ResponseWidget.on500(asyncSnapshot);
                    }
                    return Container();
                  default:
                    return Container();
                }
              }),
        ));
  }
}

class _TFutureResponseInline<T> extends StatefulWidget {
  final T value;
  final FutureResponse futureResponse;
  final OnWaiting onWaiting;
  final On200ListWidget<T> on200listWidget;
  final On204Widget<T> on204Widget;
  final On200ObjectWidget<T> on200objectWidget;

  const _TFutureResponseInline({
    @required this.value,
    @required this.futureResponse,
    @required this.onWaiting,
    @required this.on200listWidget,
    @required this.on204Widget,
    @required this.on200objectWidget,
  });

  @override
  _TFutureResponseInlineState createState() => _TFutureResponseInlineState<T>();
}

class _TFutureResponseInlineState<T> extends State<_TFutureResponseInline<T>> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Response>(
        future: widget.futureResponse(),
        builder: (BuildContext context, AsyncSnapshot<Response> asyncSnapshot) {
          if (asyncSnapshot.hasError) {
            return _ResponseWidget.onHasError(asyncSnapshot);
          }
          switch (asyncSnapshot.connectionState) {
            case ConnectionState.none:
              return _ResponseWidget.onNone();
            case ConnectionState.waiting:
              return widget.onWaiting();
              break;
            case ConnectionState.active:
              return _ResponseWidget.onActive();
            case ConnectionState.done:
              switch (asyncSnapshot.data.statusCode) {
                case 200:
                  dynamic decode = json.decode(asyncSnapshot.data.body);
                  if (decode is List) {
                    return widget.on200listWidget(
                        List<T>.generate(decode.length, (index) {
                      dynamic newValue =
                          (widget.value as dynamic).fromMap(decode[index]);
                      return newValue as T;
                    }));
                  }
                  dynamic newValue = (widget.value as dynamic).fromMap(decode);
                  return widget.on200objectWidget(newValue as T);
                case 204: //no body
                  return widget.on204Widget();
                case 401:
                  return _TFutureResponseToken(
                    onGetToken: (String token) {
                      setState(() {});
                    },
                    onError: () {},
                  );
                case 400:
                  return _ResponseWidget.on400(asyncSnapshot);
                case 403:
                  return _ResponseWidget.on403(asyncSnapshot);
                case 404:
                  return _ResponseWidget.on404();
                case 409:
                  return _ResponseWidget.on409(asyncSnapshot);
                case 500:
                  return _ResponseWidget.on500(asyncSnapshot);
              }
              return Container();
            default:
              return Container();
          }
        });
  }
}

class _TFutureResponseToken extends StatelessWidget {
  final void Function(String token) onGetToken;
  final void Function() onError;

  const _TFutureResponseToken(
      {@required this.onGetToken, @required this.onError});
  @override
  Widget build(BuildContext context) {
    Setting setting = TIW.getSetting(context);
    return Scaffold(
      body: FutureBuilder<Response>(
          future: TRestful.refreshtoken(setting: setting),
          builder:
              (BuildContext context, AsyncSnapshot<Response> asyncSnapshot) {
            if (asyncSnapshot.hasError) {
              onError();
              return _ResponseWidget.onHasError(asyncSnapshot);
            }
            switch (asyncSnapshot.connectionState) {
              case ConnectionState.none:
                onError();
                return _ResponseWidget.onNone();
              case ConnectionState.waiting:
                return _ResponseWidget.onWaiting("Refresh Token ...");
              case ConnectionState.active:
                return _ResponseWidget.onActive();
              case ConnectionState.done:
                switch (asyncSnapshot.data.statusCode) {
                  case 200:
                    var tokeObject = json.decode(asyncSnapshot.data.body);
                    SharedPreferences.getInstance().then((sp) {
                      Setting setting = TIW.getSetting(context);
                      sp.setString("token", tokeObject["token"]);
                      setting.token = tokeObject["token"];
                      TIW.setSetting(context, setting);
                      onGetToken(tokeObject["token"]);
                    });
                    return Center(
                      child: ProgressBar(
                        label: "Refresh Token ...",
                        value: null,
                        size: 100,
                      ),
                    );
                    break;
                  case 204: //no body
                    return _ResponseWidget.on204(
                        context, asyncSnapshot, null, "Refresh Token ...");
                  case 401:
                    return _ResponseWidget.on401(asyncSnapshot);
                    break;
                  case 403:
                    onError();
                    return _ResponseWidget.on403(asyncSnapshot);
                  case 409:
                    onError();
                    return _ResponseWidget.on409(asyncSnapshot);
                  case 500:
                    return _ResponseWidget.on500(asyncSnapshot);
                }
                return Container();
              default:
                return Container();
            }
          }),
    );
  }
}

class _ResponseWidget {
  static Widget onHasError(AsyncSnapshot<Response> asyncSnapshot) {
    String code = "";
    String message = "";
    List<String> details = [];
    if (asyncSnapshot.error is PlatformException) {
      PlatformException err = asyncSnapshot.error;
      code = err.code;
      message = err.message;
    } else if (asyncSnapshot.error is TimeoutException) {
      TimeoutException err = asyncSnapshot.error;
      code = "TimeoutException";
      message = err.message;
      details.add("سرور در دسترس نیست");
      details.add("وب سرویس غیر فعال است");
      details.add("آی پی یا پورت نادرست وارد شده است");
      details.add("اگر از فیلتر شکن استفاده می کنید آن را غیر فعال کنید");
    } else if (asyncSnapshot.error is SocketException) {
      SocketException err = asyncSnapshot.error;
      code = err.osError.message;
      message = err.message;
      details.add("وای فای خاموش است");
      details.add("موبایل دیتا غیر فعال است");
      details.add("آی پی یا پورت نادرست وارد شده است");
      details.add("اگر از فیلتر شکن استفاده می کنید آن را غیر فعال کنید");
    } else if (asyncSnapshot.error is NoSuchMethodError) {
      NoSuchMethodError err = asyncSnapshot.error;
      code = "-1";
      message = err.toString();
      details.add("NoSuchMethodError");
    } else if (asyncSnapshot.error is ClientException) {
      ClientException err = asyncSnapshot.error;
      code = "-1";
      message = err.message;
      details.add(message);
    } else if (asyncSnapshot.error is ArgumentError) {
      code = "ArgumentError";
      details.add("Invalid argument(s)");
    }
    String details1 = "";
    details.forEach((detail) {
      details1 += "*  " + detail + "\n";
    });
    return _errWidget(code, details1);
  }

  static Widget onNone() {
    return _errWidget("ConnectionState.none", "internal error");
  }

  static Widget onWaiting(String waitingMsg) {
    return ProgressBar(
      label: waitingMsg ?? "Waiting ...",
      value: null,
      size: 100,
    );
  }

  static Widget onActive() {
    return ProgressBar(
      label: "Active",
      value: null,
      size: 100,
    );
  }

  static Widget on200<T>(
    BuildContext context,
    AsyncSnapshot<Response> asyncSnapshot, {
    T value,
    On200Object<T> on200object,
    On200List<T> on200list,
    String doneMsg,
  }) {
    dynamic decode = json.decode(asyncSnapshot.data.body);
    if (decode is List) {
      Timer(const Duration(milliseconds: 100), () {
        Navigator.pop(context);
        on200list(List<T>.generate(decode.length, (index) {
          dynamic newValue = (value as dynamic).fromMap(decode[index]);
          return newValue as T;
        }));
      });
      return Center(
        child: ProgressBar(
          label: doneMsg ?? "",
          value: null,
          size: 100,
        ),
      );
    } else {
      dynamic newValue = (value as dynamic).fromMap(decode);
      Timer(const Duration(milliseconds: 100), () {
        Navigator.pop(context);
        on200object(newValue as T);
      });
      return Center(
        child: ProgressBar(
          label: doneMsg ?? "",
          value: null,
          size: 100,
        ),
      );
    }
  }

  static Widget on200File(
    BuildContext context,
    AsyncSnapshot<Response> asyncSnapshot, {
    On200File on200File,
    String doneMsg,
  }) {
    Uint8List imageBytes = asyncSnapshot.data.bodyBytes;
    Timer(const Duration(milliseconds: 100), () {
      Navigator.pop(context);
      on200File(imageBytes);
    });
    return Center(
      child: ProgressBar(
        label: doneMsg ?? "",
        value: null,
        size: 100,
      ),
    );
  }

  static Widget on204(
    BuildContext context,
    AsyncSnapshot<Response> asyncSnapshot,
    On204 on204,
    String doneMsg,
  ) {
    if (on204 == null) {
      return _errWidget(
        "204",
        "برای این درخواست اطلاعاتی در سرور وجود ندارد",
      );
    } else {
      Timer(const Duration(milliseconds: 100), () {
        Navigator.pop(context);
        on204();
      });
      return Center(
        child: ProgressBar(
          label: doneMsg ?? "",
          value: null,
          size: 100,
        ),
      );
    }
  }

  static Widget on401(AsyncSnapshot<Response> asyncSnapshot) {
    return _errWidget(
      "401",
      "مهلت استفاده از سیستم به پایان رسیده است!",
    );
  }

  static Widget on400(AsyncSnapshot<Response> asyncSnapshot) {
    Map mapBody = json.decode(asyncSnapshot.data.body);
    return _errWidget(
      mapBody["error"],
      mapBody["errors"][0]["value"],
    );
  }

  static Widget on403(AsyncSnapshot<Response> asyncSnapshot) {
    Map mapBody = json.decode(asyncSnapshot.data.body);
    return _errWidget(
      mapBody["error"],
      mapBody["errors"][1]["value"],
    );
  }

  static Widget on404() {
    return _errWidget(
      "404",
      "Resource Not Found",
    );
  }

  static Widget on409(AsyncSnapshot<Response> asyncSnapshot) {
    Map mapBody = json.decode(asyncSnapshot.data.body);
    return _errWidget(
      mapBody["error"],
      mapBody["message"],
    );
  }

  static Widget on500(AsyncSnapshot<Response> asyncSnapshot) {
    Map mapBody = json.decode(asyncSnapshot.data.body);
    return _errWidget(
      mapBody["error"],
      mapBody["message"],
    );
  }

  static Widget _errWidget(String title, String desc) {
    return Center(
      child: ListTile(
        isThreeLine: true,
        dense: true,
        leading: Icon(Icons.error),
        title: Text(title,
            style: TextStyle(
              fontFamily: 'IRANSans',
              color: Colors.black54,
              fontSize: 12.0,
            )),
        subtitle: Text(desc,
            style: TextStyle(
              fontFamily: 'IRANSans',
              color: Colors.black45,
              fontSize: 10.0,
            )),
      ),
    );
  }
}
