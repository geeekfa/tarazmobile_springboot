import 'dart:async';
import 'package:com_tarazgroup/models/progressbar.dart';
import 'package:flutter/material.dart';
// import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart';
// import 'package:latlong/latlong.dart';
// import 'package:latlng/latlng.dart';

enum LocationState { NO_PERMISSION, NO_SERVICE, OK }
typedef OnDoneLocationData = void Function(LocationData locationData);
typedef OnDoneLocationDataWidget = Widget Function(LocationData locationData);
// typedef OnDonePlacemark = void Function(Placemark placemark);
// typedef OnDonePlacemarkWidget = Widget Function(Placemark placemark);

class TLocation {
  static void callLocationDataModalMode(
    BuildContext context, {
    OnDoneLocationData onDoneLocationData,
  }) {
    showGeneralDialog(
      context: context,
      pageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation) {
        return _TFutureLocationDataModal(
          onDoneLocationData: onDoneLocationData,
        );
      },
      barrierColor: Color.fromRGBO(255, 255, 255, 0.9),
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      transitionDuration: const Duration(milliseconds: 200),
    );
  }

  // static void callPlacemarkModalMode(
  //   BuildContext context, {
  //   LatLng latLng,
  //   OnDonePlacemark onDonePlacemark,
  // }) {
  //   showGeneralDialog(
  //     context: context,
  //     pageBuilder: (BuildContext context, Animation<double> animation,
  //         Animation<double> secondaryAnimation) {
  //       return _TFuturePlacemarkModal(
  //         latLng: latLng,
  //         onDonePlacemark: onDonePlacemark,
  //       );
  //     },
  //     barrierColor: Color.fromRGBO(255, 255, 255, 0.9),
  //     barrierDismissible: true,
  //     barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
  //     transitionDuration: const Duration(milliseconds: 200),
  //   );
  // }

  // static Widget callLocationDataInlineMode({
  //   @required OnDoneLocationDataWidget onDoneLocationDataWidget,
  // }) {
  //   return _TFutureLocationDataInline(
  //     onDoneLocationDataWidget: onDoneLocationDataWidget,
  //   );
  // }

  // static Widget callPlacemarkInlineMode({
  //   LatLng latLng,
  //   OnDonePlacemarkWidget onDonePlacemarkWidget,
  // }) {
  //   return _TFuturePlacemarkInline(
  //     latLng: latLng,
  //     onDonePlacemarkWidget: onDonePlacemarkWidget,
  //   );
  // }
}

class _TFutureLocationDataModal extends StatefulWidget {
  final OnDoneLocationData onDoneLocationData;

  const _TFutureLocationDataModal({
    @required this.onDoneLocationData,
  });

  @override
  _TFutureLocationDataModalState createState() =>
      _TFutureLocationDataModalState();
}

class _TFutureLocationDataModalState extends State<_TFutureLocationDataModal> {
  @override
  Widget build(BuildContext context) {
    bool canPop = true;
    return WillPopScope(
        onWillPop: () {
          return Future.value(canPop);
        },
        child: Scaffold(
          body: FutureBuilder<LocationData>(
              future: _Method.futureLocationData(),
              builder: (BuildContext context,
                  AsyncSnapshot<LocationData> asyncSnapshot) {
                if (asyncSnapshot.hasError) {
                  canPop = true;
                  return _StateWidget.onHasError();
                }
                switch (asyncSnapshot.connectionState) {
                  case ConnectionState.none:
                    canPop = true;
                    return _StateWidget.onNone();
                  case ConnectionState.waiting:
                  // canPop = false;
                    canPop = true;
                    return _StateWidget.onWaiting();
                  case ConnectionState.active:
                    canPop = false;
                    return _StateWidget.onActive();
                  case ConnectionState.done:
                    canPop = true;
                    switch (_Method.locationState) {
                      case LocationState.NO_PERMISSION:
                        return Center(
                            child: ListTile(
                          title: Text(
                            "Gps permission is denied!",
                            style: TextStyle(
                              fontFamily: 'NotoSans-Regular',
                              fontSize: 12.0,
                              color: Colors.blueGrey,
                            ),
                          ),
                          trailing: IconButton(
                            icon: Icon(Icons.refresh),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        ));
                        break;
                      case LocationState.NO_SERVICE:
                        return Center(
                          child: ListTile(
                            title: Text(
                              "Turning on device location is denied!",
                              style: TextStyle(
                                fontFamily: 'NotoSans-Regular',
                                fontSize: 12.0,
                                color: Colors.blueGrey,
                              ),
                            ),
                            trailing: IconButton(
                              icon: Icon(Icons.refresh),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                          ),
                        );
                        break;
                      default:
                        Timer(const Duration(milliseconds: 100), () {
                          Navigator.pop(context);
                          widget.onDoneLocationData(asyncSnapshot.data);
                        });
                        return ProgressBar(
                          label: "",
                          value: 1,
                          size: 100,
                        );
                    }
                    break;
                  default:
                    return Container();
                }
              }),
        ));
  }
}

// class _TFutureLocationDataInline extends StatefulWidget {
//   final OnDoneLocationDataWidget onDoneLocationDataWidget;

//   const _TFutureLocationDataInline({
//     @required this.onDoneLocationDataWidget,
//   });

//   @override
//   _TFutureLocationDataInlineState createState() =>
//       _TFutureLocationDataInlineState();
// }

// class _TFutureLocationDataInlineState
//     extends State<_TFutureLocationDataInline> {
//   @override
//   Widget build(BuildContext context) {
//     return FutureBuilder<LocationData>(
//         future: _Method.futureLocationData(),
//         builder:
//             (BuildContext context, AsyncSnapshot<LocationData> asyncSnapshot) {
//           if (asyncSnapshot.hasError) {
//             return _StateWidget.onHasError();
//           }
//           switch (asyncSnapshot.connectionState) {
//             case ConnectionState.none:
//               return _StateWidget.onNone();
//             case ConnectionState.waiting:
//               return _StateWidget.onWaiting();
//               break;
//             case ConnectionState.active:
//               return _StateWidget.onActive();
//             case ConnectionState.done:
//               switch (_Method.locationState) {
//                 case LocationState.NO_PERMISSION:
//                   return Center(
//                       child: ListTile(
//                     title: Text(
//                       "Gps permission is denied!",
//                       style: TextStyle(
//                         fontFamily: 'NotoSans-Regular',
//                         fontSize: 12.0,
//                         color: Colors.blueGrey,
//                       ),
//                     ),
//                     trailing: IconButton(
//                       icon: Icon(Icons.refresh),
//                       onPressed: () {
//                         setState(() {});
//                       },
//                     ),
//                   ));
//                   break;
//                 case LocationState.NO_SERVICE:
//                   return Center(
//                     child: ListTile(
//                       title: Text(
//                         "Turning on device location is denied!",
//                         style: TextStyle(
//                           fontFamily: 'NotoSans-Regular',
//                           fontSize: 12.0,
//                           color: Colors.blueGrey,
//                         ),
//                       ),
//                       trailing: IconButton(
//                         icon: Icon(Icons.refresh),
//                         onPressed: () {
//                           setState(() {});
//                         },
//                       ),
//                     ),
//                   );
//                   break;
//                 default:
//                   return widget.onDoneLocationDataWidget(asyncSnapshot.data);
//               }
//               break;
//             default:
//               return Container();
//           }
//         });
//   }
// }

// class _TFuturePlacemarkModal extends StatefulWidget {
//   final LatLng latLng;
//   final OnDonePlacemark onDonePlacemark;

//   const _TFuturePlacemarkModal({
//     @required this.onDonePlacemark,
//     @required this.latLng,
//   });

//   @override
//   _TFuturePlacemarkModalState createState() => _TFuturePlacemarkModalState();
// }

// class _TFuturePlacemarkModalState extends State<_TFuturePlacemarkModal> {
//   @override
//   Widget build(BuildContext context) {
//     bool canPop = true;
//     return WillPopScope(
//         onWillPop: () {
//           return Future.value(canPop);
//         },
//         child: Scaffold(
//           body: FutureBuilder<Placemark>(
//               future: _Method.futurePlacemark(widget.latLng),
//               builder: (BuildContext context,
//                   AsyncSnapshot<Placemark> asyncSnapshot) {
//                 if (asyncSnapshot.hasError) {
//                   canPop = true;
//                   return _StateWidget.onHasError();
//                 }
//                 switch (asyncSnapshot.connectionState) {
//                   case ConnectionState.none:
//                     canPop = true;
//                     return _StateWidget.onNone();
//                   case ConnectionState.waiting:
//                     canPop = true;
//                     return _StateWidget.onWaiting();
//                   case ConnectionState.active:
//                     canPop = false;
//                     return _StateWidget.onActive();
//                   case ConnectionState.done:
//                     canPop = true;
//                     Timer(const Duration(milliseconds: 100), () {
//                       Navigator.pop(context);
//                       widget.onDonePlacemark(asyncSnapshot.data);
//                     });
//                     return ProgressBar(
//                       label: "",
//                       value: 1,
//                       size: 100,
//                     );
//                     break;
//                   default:
//                     return Container();
//                 }
//               }),
//         ));
//   }
// }

// class _TFuturePlacemarkInline extends StatefulWidget {
//   final LatLng latLng;
//   final OnDonePlacemarkWidget onDonePlacemarkWidget;

//   const _TFuturePlacemarkInline({
//     @required this.onDonePlacemarkWidget,
//     @required this.latLng,
//   });

//   @override
//   _TFuturePlacemarkInlineState createState() => _TFuturePlacemarkInlineState();
// }

// class _TFuturePlacemarkInlineState extends State<_TFuturePlacemarkInline> {
//   @override
//   Widget build(BuildContext context) {
//     return FutureBuilder<Placemark>(
//         future: _Method.futurePlacemark(widget.latLng),
//         builder:
//             (BuildContext context, AsyncSnapshot<Placemark> asyncSnapshot) {
//           if (asyncSnapshot.hasError) {
//             return _StateWidget.onHasError();
//           }
//           switch (asyncSnapshot.connectionState) {
//             case ConnectionState.none:
//               return _StateWidget.onNone();
//             case ConnectionState.waiting:
//               return _StateWidget.onWaiting();
//               break;
//             case ConnectionState.active:
//               return _StateWidget.onActive();
//             case ConnectionState.done:
//               return widget.onDonePlacemarkWidget(asyncSnapshot.data);
//               break;
//             default:
//               return Container();
//           }
//         });
//   }
// }

class _StateWidget {
  static Widget onHasError() {
    return _onError("Error", "Try Again.");
  }

  static Widget onNone() {
    return _onError("None", "Try Again.");
  }

  static Widget onWaiting() {
    return ProgressBar(
      label: "دریافت مختصات جغرافیایی",
      value: null,
      size: 100,
    );
  }

  static Widget onActive() {
    return ProgressBar(
      label: "Active",
      value: null,
      size: 100,
    );
  }

  static Widget _onError(String title, String desc) {
    return Center(
      child: ListTile(
        isThreeLine: true,
        dense: true,
        leading: Icon(Icons.error),
        title: Text(title,
            style: TextStyle(
              fontFamily: 'NotoSans-Regular',
              color: Colors.black54,
              fontSize: 12.0,
            )),
        subtitle: Text(desc,
            style: TextStyle(
              fontFamily: 'NotoSans-Regular',
              color: Colors.black45,
              fontSize: 10.0,
            )),
      ),
    );
  }
}

class _Method {
  static LocationState locationState;

  static Future<LocationData> futureLocationData() async {
    Location location = Location();
    PermissionStatus permissionStatus1 = await location.hasPermission();

    if (permissionStatus1 == PermissionStatus.granted) {
      return _checkService(location);
    } else {
      PermissionStatus permissionStatus2 = await location.requestPermission();
      if (permissionStatus2 == PermissionStatus.granted) {
        return _checkService(location);
      } else {
        locationState = LocationState.NO_PERMISSION;
        return null;
      }
    }
  }

  static Future<LocationData> _checkService(Location location) async {
    bool serviceEnabled = await location.serviceEnabled();
    if (serviceEnabled) {
      locationState = LocationState.OK;
      LocationData locationData = await location.getLocation();
      return locationData;
    } else {
      bool isRequestServiceAccepted = await location.requestService();
      if (isRequestServiceAccepted) {
        locationState = LocationState.OK;
        LocationData locationData = await location.getLocation();
        return locationData;
      } else {
        locationState = LocationState.NO_SERVICE;
        return null;
      }
    }
  }

  // static Future<Placemark> futurePlacemark(LatLng latLng) async {
  //   List<Placemark> placemarks = await Geolocator()
  //       .placemarkFromCoordinates(latLng.latitude, latLng.longitude);
  //   Placemark placemark = Placemark(
  //     administrativeArea: placemarks.first.administrativeArea,
  //     country: placemarks.first.country,
  //     locality: placemarks.first.locality,
  //     position: Position(
  //       latitude: latLng.latitude,
  //       longitude: latLng.longitude,
  //     ),
  //     postalCode: placemarks.first.postalCode,
  //     subAdministrativeArea: placemarks.first.subAdministrativeArea,
  //     subLocality: placemarks.first.subLocality,
  //     subThoroughfare: placemarks.first.subThoroughfare,
  //     thoroughfare: placemarks.first.thoroughfare,
  //   );

  //   return placemark;
  // }
}
