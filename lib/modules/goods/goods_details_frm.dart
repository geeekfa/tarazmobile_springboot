import 'package:cached_network_image/cached_network_image.dart';
import 'package:com_tarazgroup/models/goods.dart';
import 'package:com_tarazgroup/models/system.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/modules/tlib/tlib.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:math' as math;

class GoodsDetailsFrm extends StatefulWidget {
  final bool showPrice;
  final bool showRemain;
  final Goods goods;
  final void Function() onPressedPlusButton;

  const GoodsDetailsFrm({
    @required this.showPrice,
    @required this.showRemain,
    @required this.goods,
    @required this.onPressedPlusButton,
  });

  @override
  _GoodsFrmState createState() => _GoodsFrmState();
}

class _GoodsFrmState extends State<GoodsDetailsFrm> {
  System currentSystem;
  List<Widget> fields = [];
  @override
  void initState() {
    currentSystem = TIW.getSetting(context, initState: true).currentSystem;
    fields.add(
      ButtonBar(
        alignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            widget.goods.goodsCode,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 14.0,
              color: currentSystem.themeData.primaryColor,
            ),
          ),
          Text(
            "کد کالا",
            textAlign: TextAlign.right,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 14.0,
              color: currentSystem.themeData.primaryColor,
            ),
          ),
        ],
      ),
    );
    fields.add(
      ButtonBar(
        alignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            widget.goods.storeName ?? "",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 14.0,
              color: currentSystem.themeData.primaryColor,
            ),
          ),
          Text(
            "انبار",
            textAlign: TextAlign.right,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 14.0,
              color: currentSystem.themeData.primaryColor,
            ),
          ),
        ],
      ),
    );
    fields.add(
      ButtonBar(
        alignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            widget.goods.goodsUnit.defaultSecUnitID == null
                ? widget.goods.goodsUnit.unitDesc
                : widget.goods.goodsUnit.secUnitDesc,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 14.0,
              color: currentSystem.themeData.primaryColor,
            ),
          ),
          Text(
            "واحد پیش فرض",
            textAlign: TextAlign.right,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 14.0,
              color: currentSystem.themeData.primaryColor,
            ),
          ),
        ],
      ),
    );
    fields.add(
      ButtonBar(
        alignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            widget.goods.goodsUnit.computeValue == null
                ? ""
                : widget.goods.goodsUnit.computeValue.toString(),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 14.0,
              color: currentSystem.themeData.primaryColor,
            ),
          ),
          Text(
            "ضریب",
            textAlign: TextAlign.right,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 14.0,
              color: currentSystem.themeData.primaryColor,
            ),
          ),
        ],
      ),
    );
    fields.add(
      ButtonBar(
        alignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            widget.goods.techInfo ?? "",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 14.0,
              color: currentSystem.themeData.primaryColor,
            ),
          ),
          Text(
            "اطلاعات فنی",
            textAlign: TextAlign.right,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 14.0,
              color: currentSystem.themeData.primaryColor,
            ),
          ),
        ],
      ),
    );
    fields.add(
      ButtonBar(
        alignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            widget.goods.goodsUnit.isQuantitativeDesc,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 14.0,
              color: currentSystem.themeData.primaryColor,
            ),
          ),
          Text(
            "نحوه شمارش",
            textAlign: TextAlign.right,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 14.0,
              color: currentSystem.themeData.primaryColor,
            ),
          ),
        ],
      ),
    );

    if (widget.showPrice) {
      fields.add(
        ButtonBar(
          alignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              widget.goods.price == null
                  ? "0"
                  : TLib.commaSeparate(widget.goods.price.toString()),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 14.0,
                color: currentSystem.themeData.primaryColor,
              ),
            ),
            Text(
              "قیمت",
              textAlign: TextAlign.right,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 14.0,
                color: currentSystem.themeData.primaryColor,
              ),
            ),
          ],
        ),
      );
    }
    if (widget.showRemain) {
      fields.add(
        ButtonBar(
          alignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              widget.goods.remain == null
                  ? "0"
                  : TLib.commaSeparate(widget.goods.remain.toString()),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 14.0,
                color: currentSystem.themeData.primaryColor,
              ),
            ),
            Text(
              "موجودی",
              textAlign: TextAlign.right,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 14.0,
                color: currentSystem.themeData.primaryColor,
              ),
            ),
          ],
        ),
      );
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: currentSystem.themeData,
      child: ChangeNotifierProvider(
        create: (context) {
          return _ProviderGoodsFrm(
              currentSystem: currentSystem, goods: widget.goods);
        },
        child: Consumer<_ProviderGoodsFrm>(builder: (context, provider, child) {
          return SafeArea(
            child: Scaffold(
              appBar: AppBar(),
              body: CustomScrollView(
                slivers: <Widget>[
                  SliverAppBar(
                    backgroundColor: currentSystem.themeData.accentColor,
                    automaticallyImplyLeading: false,
                    expandedHeight: 300.0,
                    floating: false,
                    pinned: false,
                    elevation: 1,
                    forceElevated: true,
                    flexibleSpace: FlexibleSpaceBar(
                      background: _FlexibleSpaceBarBackgroundHeader(),
                    ),
                  ),
                  SliverPersistentHeader(
                    delegate: _SliverPersistentHeaderDelegate(),
                    pinned: true,
                  ),
                  SliverList(
                    delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) {
                        final int itemIndex = index ~/ 2;
                        if (index.isEven) {
                          return fields[itemIndex];
                        }
                        return Divider();
                      },
                      semanticIndexCallback: (Widget widget, int localIndex) {
                        if (localIndex.isEven) {
                          return localIndex ~/ 2;
                        }
                        return null;
                      },
                      childCount: math.max(0, fields.length * 2 - 1),
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Divider(),
                  ),
                ],
              ),
              floatingActionButton: FloatingActionButton(
                  tooltip: widget.showPrice && !widget.showRemain
                      ? "افزودن به سند"
                      : "افزودن به جایزه",
                  child: Icon(Icons.add),
                  onPressed: () {
                    widget.onPressedPlusButton();
                  }),
              floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
            ),
          );
        }),
      ),
    );
  }
}

class _FlexibleSpaceBarBackgroundHeader extends StatelessWidget {
  final PageController pageController = PageController();
  @override
  Widget build(BuildContext context) {
    final _ProviderGoodsFrm providerGoodsFrm =
        Provider.of<_ProviderGoodsFrm>(context, listen: false);
    return providerGoodsFrm.goods.imageUrls == null
        ? Container(
            padding: const EdgeInsets.all(5.0),
            child: Icon(
              Icons.image,
              color: providerGoodsFrm.currentSystem.themeData.primaryColor,
              size: 200,
            ),
          )
        : PageView.builder(
            itemCount: providerGoodsFrm.goods.imageUrls.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                padding: const EdgeInsets.all(5.0),
                child: CachedNetworkImage(
                  fit: BoxFit.contain,
                  imageUrl: providerGoodsFrm.goods.imageUrls[index],
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              );
            });
  }
}

class _SliverPersistentHeaderDelegate extends SliverPersistentHeaderDelegate {
  final double height = 50;
  @override
  double get minExtent => height;
  @override
  double get maxExtent => height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    final _ProviderGoodsFrm providerGoodsFrm =
        Provider.of<_ProviderGoodsFrm>(context, listen: false);

    return Container(
        height: height,
        child: ClipRect(
          child: Container(
            color: providerGoodsFrm.currentSystem.themeData.primaryColorDark,
            child: ListTile(
              title: Text(
                providerGoodsFrm.goods.goodsDesc,
                textAlign: TextAlign.right,
                style: TextStyle(
                  fontFamily: 'IRANSansBold',
                  fontSize: 18.0,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ));
  }

  @override
  bool shouldRebuild(_SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}

class _ProviderGoodsFrm with ChangeNotifier {
  System currentSystem;
  Goods goods;

  _ProviderGoodsFrm({
    @required this.currentSystem,
    @required this.goods,
  });
}
