import 'package:com_tarazgroup/models/query_param.dart';
import 'package:com_tarazgroup/models/system.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/modules/listview/listview_filter.dart';
import 'package:flutter/material.dart';

// typedef ItemBuilder<T> = Widget Function(T t, int index);

class QKeyFilter<T> extends StatefulWidget {
  final String title;
  final String resource;
  // final int recCnt;
  // final T value;
  final T Function() onValue;

  final List<QueryParam> queryParams;
  final Widget Function(T t, int index) itemBuilder;

  final void Function(T t) onSelect;

  final Future<bool> Function() onChangePermission;
  final List<Widget> filterWidgets;
  final List<QueryParam> Function() onFiltered;
  final void Function() onDeleteFilter;

  QKeyFilter({
    this.title,
    this.itemBuilder,
    this.resource,
    this.queryParams,
    // this.recCnt,
    this.onSelect,
    this.onValue,
    @required this.onChangePermission,
    @required this.filterWidgets,
    @required this.onFiltered,
    @required this.onDeleteFilter,
  });

  @override
  _QKeyFilterState createState() => _QKeyFilterState<T>();
}

class _QKeyFilterState<T> extends State<QKeyFilter<T>> {
  System currentSystem;
  T value;
  @override
  void initState() {
    currentSystem = TIW.getSetting(context, initState: true).currentSystem;
    value = widget.onValue();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: ListTile(
        isThreeLine: true,
        dense: true,
        trailing: SizedBox(
          height: 30,
          width: 30,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Colors.grey[400], padding: EdgeInsets.all(0)),
            child: Icon(Icons.more_horiz),
            onPressed: () async {
              if (await widget.onChangePermission()) {
                T item = await showGeneralDialog<T>(
                  context: context,
                  pageBuilder: (BuildContext context,
                      Animation<double> animation,
                      Animation<double> secondaryAnimation) {
                    return ListViewFilter<T>(
                      filterWidget: FilterWidget(
                        title: widget.title,
                        widgets: widget.filterWidgets,
                        onFiltered: () {
                          List<QueryParam> queryParams = widget.onFiltered();
                          Navigator.pop<List<QueryParam>>(context, queryParams);
                        },
                        onCanceled: () {},
                      ),
                      value: value,
                      title: widget.title,
                      queryParams: widget.queryParams,
                      // recCnt: widget.recCnt,
                      resource: widget.resource,
                      itemBuilder: widget.itemBuilder,
                      onTap: (item) {
                        Navigator.pop<T>(context, item);
                      },
                      onDeleteFilter: widget.onDeleteFilter,
                    );
                  },
                  barrierColor: Color.fromRGBO(255, 255, 255, 0.9),
                  barrierDismissible: true,
                  barrierLabel: MaterialLocalizations.of(context)
                      .modalBarrierDismissLabel,
                  transitionDuration: const Duration(milliseconds: 200),
                );
                if (item != null) {
                  setState(() {
                    value = item;
                  });
                  widget.onSelect(item);
                }
              }
            },
          ),
        ),
        title: Text(
          widget.title,
          textAlign: TextAlign.right,
          style: TextStyle(
            fontFamily: 'IRANSans',
            fontSize: 11.0,
            color: Colors.black,
          ),
        ),
        subtitle: Text(
          (value as dynamic).desc ?? "",
          textAlign: TextAlign.right,
          style: TextStyle(
            fontFamily: 'IRANSans',
            fontSize: 10.0,
            color: Colors.black,
          ),
        ),
      ),
    );
  }
}
