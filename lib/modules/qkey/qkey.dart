import 'package:com_tarazgroup/models/system.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/modules/listview/listview_search.dart';
import 'package:flutter/material.dart';

class QKey<T> extends StatefulWidget {
  final String title;
  final ItemBuilder<T> itemBuilder;
  final String resource;
  final QueryParams queryParams;
  final int recCnt;
  final FilterFields filterFields;
  final void Function(T t) onSelect;
  final T Function() onValue;
  final Future<bool> Function() onChangePermission;

  QKey({
    this.title,
    this.itemBuilder,
    this.resource,
    this.queryParams,
    this.recCnt,
    this.filterFields,
    this.onSelect,
    this.onValue,
    @required this.onChangePermission,
  });

  @override
  _QKeyState createState() => _QKeyState<T>();
}

class _QKeyState<T> extends State<QKey<T>> {
  System currentSystem;
  T value;
  @override
  void initState() {
    currentSystem = TIW.getSetting(context, initState: true).currentSystem;
    value = widget.onValue();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: ListTile(
        isThreeLine: true,
        dense: true,
        trailing: SizedBox(
          height: 30,
          width: 30,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Colors.grey[400], padding: EdgeInsets.all(0)),
            child: Icon(Icons.more_horiz),
            onPressed: () async {
              if (await widget.onChangePermission()) {
                T item = await showGeneralDialog<T>(
                  context: context,
                  pageBuilder: (BuildContext context,
                      Animation<double> animation,
                      Animation<double> secondaryAnimation) {
                    return Scaffold(
                      appBar: AppBar(
                        // backgroundColor: currentSystem.color,
                        title: ListTile(
                          title: Text(widget.title,
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                  fontFamily: 'IRANSans',
                                  fontSize: 12.0,
                                  color: Colors.white)),
                        ),
                      ),
                      body: ListViewSearch<T>(
                        value: value,
                        title: widget.title,
                        queryParams: widget.queryParams,
                        recCnt: widget.recCnt,
                        resource: widget.resource,
                        filterFields: widget.filterFields,
                        itemBuilder: widget.itemBuilder,
                        onTap: (item) {
                          Navigator.pop<T>(context, item);
                        },
                      ),
                    );
                  },
                  barrierColor: Color.fromRGBO(255, 255, 255, 0.9),
                  barrierDismissible: true,
                  barrierLabel: MaterialLocalizations.of(context)
                      .modalBarrierDismissLabel,
                  transitionDuration: const Duration(milliseconds: 200),
                );
                if (item != null) {
                  setState(() {
                    value = item;
                  });
                  widget.onSelect(item);
                }
              }
            },
          ),
        ),
        title: Text(
          widget.title,
          textAlign: TextAlign.right,
          style: TextStyle(
            fontFamily: 'IRANSans',
            fontSize: 11.0,
            color: Colors.black,
          ),
        ),
        subtitle: Text(
          (value as dynamic).desc ?? "",
          textAlign: TextAlign.right,
          style: TextStyle(
            fontFamily: 'IRANSans',
            fontSize: 10.0,
            color: Colors.black,
          ),
        ),
      ),
    );
  }
}
