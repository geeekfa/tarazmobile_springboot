import 'package:flutter/material.dart';

class TConfirmDialog {
  final BuildContext context;

  const TConfirmDialog({
    @required this.context,
  });

  Future<bool> show(String title, String content) async {
    return await showDialog<bool>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding:
              EdgeInsets.only(top: 5, left: 25, right: 25, bottom: 30),
          title: Container(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                child: Text(title,
                    textAlign: TextAlign.center,
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                      fontFamily: 'IRANSansBold',
                      color: Colors.black54,
                      fontSize: 16.0,
                    )),
              ),
              Divider(
                color: Colors.black87,
              ),
            ],
          )),
          content: Text(
            content,
            textDirection: TextDirection.rtl,
            style: TextStyle(
              fontFamily: 'IRANSans',
              color: Colors.black45,
              fontSize: 12.0,
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                "خیر",
                style: TextStyle(
                  fontFamily: 'IRANSansBlack',
                  color: Colors.black45,
                  fontSize: 12.0,
                ),
              ),
              onPressed: () {
                Navigator.pop<bool>(context, false);
              },
            ),
            TextButton(
              child: Text(
                "بله",
                style: TextStyle(
                  fontFamily: 'IRANSansBlack',
                  color: Colors.black45,
                  fontSize: 12.0,
                ),
              ),
              onPressed: () {
                Navigator.pop<bool>(context, true);
              },
            )
          ],
        );
      },
    );
  }
}
