import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/xFieldsSetup.dart';
import 'package:flutter/material.dart';
import 'package:com_tarazgroup/modules/textfield/ttextfield.dart';

class TCustomFieldsDialog {
  final BuildContext context;

  const TCustomFieldsDialog({
    @required this.context,
  });

  List<Widget> customFieldWidgets(List<String> customFields, double fee) {
    List<XFieldsSetup> xFields = TIW.getXFieldsSetups(context);
    List<Widget> widgets = [];
    for (var i = 0; i < xFields.length; i++) {
      widgets.add(TTextField(
          title: xFields[i].customCaption,
          keyboardType: (xFields[i].fieldType == 3 || xFields[i].fieldType == 4)
              ? TextInputType.number
              : TextInputType.text,
          onValue: () {
            if (xFields[i].isShowFee) {
              if (customFields[xFields[i].fieldNumber - 1] == null) {
                return fee.toInt().toString();
              }
            }
            return customFields[xFields[i].fieldNumber - 1];
          },
          onChanged: (txt) {
            customFields[xFields[i].fieldNumber - 1] = txt;
          }));
    }
    return widgets;
  }

  Future<List<String>> show(
      String title, List<String> customFields, double fee) {
    return showDialog<List<String>>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding:
              EdgeInsets.only(top: 5, left: 25, right: 25, bottom: 30),
          title: Container(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                child: Text(title,
                    textAlign: TextAlign.center,
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                      fontFamily: 'IRANSansBold',
                      color: Colors.black54,
                      fontSize: 16.0,
                    )),
              ),
              Divider(
                color: Colors.black87,
              ),
            ],
          )),
          content: ListView(
            children: customFieldWidgets(customFields, fee),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                "بستن",
                style: TextStyle(
                  fontFamily: 'IRANSansBlack',
                  color: Colors.black45,
                  fontSize: 12.0,
                ),
              ),
              onPressed: () {
                Navigator.pop<String>(context, null);
              },
            ),
            TextButton(
              child: Text(
                "ثبت",
                style: TextStyle(
                  fontFamily: 'IRANSansBlack',
                  color: Colors.black45,
                  fontSize: 12.0,
                ),
              ),
              onPressed: () {
                Navigator.pop<List<String>>(context, customFields);
              },
            )
          ],
        );
      },
    );
  }
}
