import 'dart:typed_data';

import 'package:com_tarazgroup/models/query_param.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/modules/future/webservice.dart';
import 'package:com_tarazgroup/modules/rest/restful.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:printing/printing.dart';
import 'package:pdf/pdf.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:share/share.dart';

class TReportDialog {
  final BuildContext context;
  final String title;
  final PdfSetting pdfSetting;
  final ExcelSetting excelSetting;

  TReportDialog(
      {@required this.context,
      @required this.title,
      @required this.pdfSetting,
      @required this.excelSetting});

  void show() {
    final Setting setting = TIW.getSetting(context);
    showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () {
            return new Future.value(true);
          },
          child: AlertDialog(
            contentPadding:
                EdgeInsets.only(top: 5, left: 25, right: 25, bottom: 30),
            title: Container(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Text(title,
                      textAlign: TextAlign.center,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(
                        fontFamily: 'IRANSansBold',
                        color: Colors.black54,
                        fontSize: 16.0,
                      )),
                ),
                Divider(
                  color: Colors.black87,
                ),
              ],
            )),
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    ElevatedButton(
                        onPressed: () {
                          WebService.callModalModeFile(context,
                              futureResponse: () {
                                return TRestful.file(
                                  this.excelSetting.resource,
                                  setting: setting,
                                  queryParams: this.excelSetting.queryParams,
                                  timeout: 200,
                                );
                              },
                              waitingMsg: "آماده سازی گزارش",
                              doneMsg: "",
                              on200File: (bytes) async {
                                final tempDir = await getTemporaryDirectory();
                                File file =
                                    await File('${tempDir.path}/$title.xlsx')
                                        .create();
                                file.writeAsBytesSync(bytes);
                                Share.shareFiles([file.path], text: title);
                              });
                        },
                        child: Icon(FontAwesomeIcons.solidFileExcel,
                            color: Colors.white),
                        style: ElevatedButton.styleFrom(
                          shape: CircleBorder(),
                          padding: EdgeInsets.all(10),
                          primary: setting.currentSystem.themeData.primaryColor,
                        )),
                    Text("اکسل",
                        textAlign: TextAlign.center,
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                          fontFamily: 'IRANSans',
                          color: Colors.black54,
                          fontSize: 12.0,
                        )),
                  ],
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    ElevatedButton(
                      child: Icon(FontAwesomeIcons.solidFilePdf,
                          color: Colors.white),
                      style: ElevatedButton.styleFrom(
                        shape: CircleBorder(),
                        padding: EdgeInsets.all(10),
                        primary: setting.currentSystem.themeData.primaryColor,
                      ),
                      onPressed: () {
                        WebService.callModalModeFile(context,
                            futureResponse: () {
                              return TRestful.file(
                                this.pdfSetting.resource,
                                setting: setting,
                                queryParams: this.pdfSetting.queryParams,
                                timeout: 200,
                              );
                            },
                            waitingMsg: "آماده سازی گزارش",
                            doneMsg: "",
                            on200File: (bytes) {
                              _TPdfOptionsDialog(
                                      context: context,
                                      title: title,
                                      bytes: bytes)
                                  .show();
                            });
                      },
                    ),
                    Text("پی دی اف",
                        textAlign: TextAlign.center,
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                          fontFamily: 'IRANSans',
                          color: Colors.black54,
                          fontSize: 12.0,
                        )),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class PdfSetting {
  final String resource;
  final List<QueryParam> queryParams;

  const PdfSetting({
    this.resource,
    this.queryParams,
  });
}

class ExcelSetting {
  final String resource;
  final List<QueryParam> queryParams;

  const ExcelSetting({
    this.resource,
    this.queryParams,
  });
}

class _TPdfOptionsDialog {
  final BuildContext context;
  final String title;
  final Uint8List bytes;
  _TPdfOptionsDialog(
      {@required this.context, @required this.title, @required this.bytes});

  void show() {
    final Setting setting = TIW.getSetting(context);
    showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () {
            return new Future.value(true);
          },
          child: AlertDialog(
            contentPadding:
                EdgeInsets.only(top: 5, left: 25, right: 25, bottom: 30),
            title: Container(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Text("گزینه ها",
                      textAlign: TextAlign.center,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(
                        fontFamily: 'IRANSansBold',
                        color: Colors.black54,
                        fontSize: 16.0,
                      )),
                ),
                Divider(
                  color: Colors.black87,
                ),
              ],
            )),
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    ElevatedButton(
                      child: Icon(FontAwesomeIcons.print, color: Colors.white),
                      style: ElevatedButton.styleFrom(
                        shape: CircleBorder(),
                        padding: EdgeInsets.all(10),
                        primary: setting.currentSystem.themeData.primaryColor,
                      ),
                      onPressed: () {
                        Navigator.of(context).push(
                          PageRouteBuilder(
                            transitionDuration: Duration(milliseconds: 100),
                            pageBuilder: (context, animation1, animation2) {
                              return PdfPreview(
                                build: (format) => bytes,
                              );
                            },
                          ),
                        );
                      },
                    ),
                    Text("پرینت",
                        textAlign: TextAlign.center,
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                          fontFamily: 'IRANSans',
                          color: Colors.black54,
                          fontSize: 12.0,
                        )),
                  ],
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    ElevatedButton(
                      child:
                          Icon(FontAwesomeIcons.shareAlt, color: Colors.white),
                      style: ElevatedButton.styleFrom(
                        shape: CircleBorder(),
                        padding: EdgeInsets.all(10),
                        primary: setting.currentSystem.themeData.primaryColor,
                      ),
                      onPressed: () async {
                        await Printing.sharePdf(
                            bytes: bytes, filename: title + '.pdf');
                      },
                    ),
                    Text("اشتراک",
                        textAlign: TextAlign.center,
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                          fontFamily: 'IRANSans',
                          color: Colors.black54,
                          fontSize: 12.0,
                        )),
                  ],
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    ElevatedButton(
                        onPressed: () async {
                          await Printing.layoutPdf(
                              onLayout: (PdfPageFormat format) async => bytes);
                        },
                        child: Icon(FontAwesomeIcons.eye, color: Colors.white),
                        style: ElevatedButton.styleFrom(
                          shape: CircleBorder(),
                          padding: EdgeInsets.all(10),
                          primary: setting.currentSystem.themeData.primaryColor,
                        )),
                    Text("نمایش",
                        textAlign: TextAlign.center,
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                          fontFamily: 'IRANSans',
                          color: Colors.black54,
                          fontSize: 12.0,
                        )),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
