import 'package:flutter/material.dart';

class TNumpadDialog {
  BuildContext context;
  Function close;
  double number;
  TNumpadDialog({@required this.context, @required this.number, this.close});

  void show() {
    showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(
            onWillPop: () {
              return new Future.value(true);
            },
            child: _NumpadDialog(
              number: number,
              close: close,
            ));
      },
    );
  }
}

class _NumpadDialog extends StatefulWidget {
  final double number;
  final Function close;
  _NumpadDialog({@required this.number, this.close});
  @override
  __NumpadDialogState createState() => __NumpadDialogState();
}

class __NumpadDialogState extends State<_NumpadDialog> {
  List<ElevatedButton> raisedButtons;
  String _number;
  bool
      textIsSelected; // برای وقتی نام پد برای بار اول باز می شودو مثلا عدد بالا در حالت سلکت است

  @override
  void initState() {
    textIsSelected = true;
    _number = widget.number.toString();
    raisedButtons = [];
    raisedButtons.add(ElevatedButton(
        style: ElevatedButton.styleFrom(
            primary: Theme.of(context).primaryColor,
            padding: EdgeInsets.all(20)),
        child: Text(
          "1",
          style: TextStyle(
              fontFamily: 'IRANSansBlack', color: Colors.black, fontSize: 25.0),
        ),
        onPressed: () {
          setState(() {
            if (textIsSelected) {
              _number = "1";
              textIsSelected = false;
            } else {
              _number = _number + "1";
            }
          });
        }));
    raisedButtons.add(ElevatedButton(
        child: Text("2",
            style: TextStyle(
                fontFamily: 'IRANSansBlack',
                color: Colors.black,
                fontSize: 25.0)),
        onPressed: () {
          setState(() {
            if (textIsSelected) {
              _number = "2";
              textIsSelected = false;
            } else {
              _number = _number + "2";
            }
          });
        }));
    raisedButtons.add(ElevatedButton(
        child: Text("3",
            style: TextStyle(
                fontFamily: 'IRANSansBlack',
                color: Colors.black,
                fontSize: 25.0)),
        onPressed: () {
          setState(() {
            if (textIsSelected) {
              _number = "3";
              textIsSelected = false;
            } else {
              _number = _number + "3";
            }
          });
        }));
    raisedButtons.add(ElevatedButton(
        child: Text("4",
            style: TextStyle(
                fontFamily: 'IRANSansBlack',
                color: Colors.black,
                fontSize: 25.0)),
        onPressed: () {
          setState(() {
            if (textIsSelected) {
              _number = "4";
              textIsSelected = false;
            } else {
              _number = _number + "4";
            }
          });
        }));
    raisedButtons.add(ElevatedButton(
        child: Text("5",
            style: TextStyle(
                fontFamily: 'IRANSansBlack',
                color: Colors.black,
                fontSize: 25.0)),
        onPressed: () {
          setState(() {
            if (textIsSelected) {
              _number = "5";
              textIsSelected = false;
            } else {
              _number = _number + "5";
            }
          });
        }));
    raisedButtons.add(ElevatedButton(
        child: Text("6",
            style: TextStyle(
                fontFamily: 'IRANSansBlack',
                color: Colors.black,
                fontSize: 25.0)),
        onPressed: () {
          setState(() {
            if (textIsSelected) {
              _number = "6";
              textIsSelected = false;
            } else {
              _number = _number + "6";
            }
          });
        }));
    raisedButtons.add(ElevatedButton(
        child: Text("7",
            style: TextStyle(
                fontFamily: 'IRANSansBlack',
                color: Colors.black,
                fontSize: 25.0)),
        onPressed: () {
          setState(() {
            if (textIsSelected) {
              _number = "7";
              textIsSelected = false;
            } else {
              _number = _number + "7";
            }
          });
        }));
    raisedButtons.add(ElevatedButton(
        child: Text("8",
            style: TextStyle(
                fontFamily: 'IRANSansBlack',
                color: Colors.black,
                fontSize: 25.0)),
        onPressed: () {
          setState(() {
            if (textIsSelected) {
              _number = "8";
              textIsSelected = false;
            } else {
              _number = _number + "8";
            }
          });
        }));
    raisedButtons.add(ElevatedButton(
        child: Text("9",
            style: TextStyle(
                fontFamily: 'IRANSansBlack',
                color: Colors.black,
                fontSize: 25.0)),
        onPressed: () {
          setState(() {
            if (textIsSelected) {
              _number = "9";
              textIsSelected = false;
            } else {
              _number = _number + "9";
            }
          });
        }));
    raisedButtons.add(ElevatedButton(
        child: Text(" ",
            style: TextStyle(
                fontFamily: 'IRANSansBlack',
                color: Colors.black,
                fontSize: 25.0)),
        onPressed: () {}));
    raisedButtons.add(ElevatedButton(
        child: Text("0",
            style: TextStyle(
                fontFamily: 'IRANSansBlack',
                color: Colors.black,
                fontSize: 25.0)),
        onPressed: () {
          setState(() {
            if (textIsSelected) {
              if (_number == "0") {
                _number = "0";
              } else {
                _number = _number + "0";
                textIsSelected = false;
              }
            } else {
              if (_number == "0") {
                _number = "0";
              } else {
                _number = _number + "0";
              }
            }
          });
        }));
    raisedButtons.add(ElevatedButton(
        child: Text("C",
            style: TextStyle(
                fontFamily: 'IRANSansBlack',
                color: Colors.black,
                fontSize: 25.0)),
        onPressed: () {
          setState(() {
            _number = "0";
            textIsSelected = true;
          });
        }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      backgroundColor: Colors.grey[200],
      content: Column(
        children: <Widget>[
          Text(
            _number,
            style: TextStyle(
                fontFamily: 'IRANSansBlack',
                color: Colors.black,
                fontSize: 25.0),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 20),
          ),
          Expanded(
            flex: 1,
            child: GridView.builder(
              gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 20.0,
                  mainAxisSpacing: 20.0),
              primary: false,
              itemCount: raisedButtons.length,
              itemBuilder: (BuildContext context1, int index) {
                return raisedButtons[index];
              },
            ),
          )
        ],
      ),
      actions: <Widget>[
        new TextButton(
          child: new Text("تایید"),
          onPressed: () {
            Navigator.of(context).pop();
            if (widget.close != null) {
              widget.close(double.parse(_number));
            }
          },
        ),
      ],
    );
  }
}
