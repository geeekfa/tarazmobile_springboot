import 'package:flutter/material.dart';
import 'package:com_tarazgroup/modules/textfield/ttextfield.dart';

class GoodsGroupFilterDialog {
  final BuildContext context;

  const GoodsGroupFilterDialog({
    @required this.context,
  });

  Future<String> show(String title, String groupDesc) {
    return showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding:
              EdgeInsets.only(top: 5, left: 25, right: 25, bottom: 30),
          title: Container(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                child: Text(title,
                    textAlign: TextAlign.center,
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                      fontFamily: 'IRANSansBold',
                      color: Colors.black54,
                      fontSize: 16.0,
                    )),
              ),
              Divider(
                color: Colors.black87,
              ),
            ],
          )),
          content: ListView(
            children: [
              TTextField(
                title: "گروه کالا",
                onValue: () {
                  return groupDesc;
                },
                onChanged: (text1) {
                  groupDesc = text1;
                },
              ),
            ],
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                "بستن",
                style: TextStyle(
                  fontFamily: 'IRANSansBlack',
                  color: Colors.black45,
                  fontSize: 12.0,
                ),
              ),
              onPressed: () {
                Navigator.pop<String>(context, null);
              },
            ),
            TextButton(
              child: Text(
                "ثبت",
                style: TextStyle(
                  fontFamily: 'IRANSansBlack',
                  color: Colors.black45,
                  fontSize: 12.0,
                ),
              ),
              onPressed: () {
                Navigator.pop<String>(context, groupDesc);
              },
            )
          ],
        );
      },
    );
  }
}
