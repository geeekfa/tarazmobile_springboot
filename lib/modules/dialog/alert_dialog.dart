import 'package:flutter/material.dart';

class TAlertDialog {
  BuildContext context;
  Function close;
  final bool barrierDismissible;
  TAlertDialog(
      {@required this.context, this.close, this.barrierDismissible = true});

  void show({String title, String content}) {
    showDialog(
      barrierDismissible: barrierDismissible,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () {
            return new Future.value(barrierDismissible);
          },
          child: AlertDialog(
            contentPadding:
                EdgeInsets.only(top: 5, left: 25, right: 25, bottom: 30),
            title: Container(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Text(title,
                      textAlign: TextAlign.center,
                      textDirection: TextDirection.rtl,
                      style: TextStyle(
                        fontFamily: 'IRANSansBold',
                        color: Colors.black54,
                        fontSize: 16.0,
                      )),
                ),
                Divider(
                  color: Colors.black87,
                ),
              ],
            )),
            content: Text(
              content,
              textDirection: TextDirection.rtl,
              style: TextStyle(
                fontFamily: 'IRANSans',
                color: Colors.black45,
                fontSize: 12.0,
              ),
            ),
            actions: <Widget>[
              new TextButton(
                child: new Text(
                  "بستن",
                  style: TextStyle(
                    fontFamily: 'IRANSansBlack',
                    color: Colors.black,
                    fontSize: 12.0,
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  if (close != null) {
                    close();
                  }
                },
              ),
            ],
          ),
        );
      },
    );
  }
}
