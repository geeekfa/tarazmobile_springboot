import 'package:flutter/material.dart';

class TRadioListDialog<T> {
  final BuildContext context;
  final List<TRadioItem<T>> tRadioItems;
  T value;

  TRadioListDialog({
    @required this.context,
    @required this.tRadioItems,
    @required this.value,
  });

  Future<T> show(String title) {
    return showDialog<T>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding:
              EdgeInsets.only(top: 5, left: 25, right: 25, bottom: 30),
          title: Container(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                child: Text(title,
                    textAlign: TextAlign.center,
                    textDirection: TextDirection.rtl,
                    style: TextStyle(
                      fontFamily: 'IRANSansBold',
                      color: Colors.black54,
                      fontSize: 16.0,
                    )),
              ),
              Divider(
                color: Colors.black87,
              ),
            ],
          )),
          content: _TRadioList<T>(
            tRadioItems: tRadioItems,
            value: value,
            onSelect: (value) {
              this.value = value;
            },
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                "ثبت",
                style: TextStyle(
                  fontFamily: 'IRANSansBlack',
                  color: Colors.black45,
                  fontSize: 12.0,
                ),
              ),
              onPressed: () {
                Navigator.pop<T>(context, value);
              },
            )
          ],
        );
      },
    );
  }
}

typedef OnSelect<T> = void Function(T value);

class _TRadioList<T> extends StatefulWidget {
  final List<TRadioItem<T>> tRadioItems;
  final T value;
  final OnSelect onSelect;
  _TRadioList({
    @required this.tRadioItems,
    @required this.value,
    @required this.onSelect,
  });

  @override
  __TRadioListState createState() => __TRadioListState();
}

class __TRadioListState<T> extends State<_TRadioList> {
  List<RadioListTile<T>> radioListTiles = [];
  T value;

  @override
  void initState() {
    this.value = widget.value;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: List.generate(widget.tRadioItems.length, (index) {
        return RadioListTile<T>(
          controlAffinity: ListTileControlAffinity.trailing,
          title: Text(
            widget.tRadioItems[index].desc,
            textDirection: TextDirection.rtl,
            style: TextStyle(
              fontFamily: 'IRANSans',
              color: Colors.black45,
              fontSize: 14.0,
              
            ),
          ),
          value: widget.tRadioItems[index].value,
          groupValue: value,
          onChanged: (T value) {
            setState(() {
              this.value = value;
            });
            widget.onSelect(value);
          },
        );
      }),
    );
  }
}

class TRadioItem<T> {
  final String desc;
  final T value;

  TRadioItem({
    this.desc,
    this.value,
  });
}
