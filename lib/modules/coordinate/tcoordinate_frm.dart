// import 'package:com.tarazgroup/models/coordinate.dart';
// import 'package:com.tarazgroup/models/marketingman.dart';
// import 'package:com.tarazgroup/models/progressbar.dart';
// import 'package:com.tarazgroup/models/query_param.dart';
// import 'package:com.tarazgroup/models/setting.dart';
// import 'package:com.tarazgroup/models/tiw.dart';
// import 'package:com.tarazgroup/modules/future/webservice.dart';
// import 'package:com.tarazgroup/modules/listview/listview_filter.dart';
// import 'package:com.tarazgroup/modules/qkey/qkey_filter.dart';
// import 'package:com.tarazgroup/modules/rest/restful.dart';
// import 'package:com.tarazgroup/modules/textfield/ttextfield_date.dart';
// import 'package:com.tarazgroup/modules/view/list_tile_view.dart';
// import 'package:flutter/material.dart';
// // import 'package:flutter_map/flutter_map.dart';
// import 'package:flutter_svg/svg.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:http/http.dart';
// // import 'package:latlong/latlong.dart';
// import 'package:latlng/latlng.dart';
// import 'package:shamsi_date/shamsi_date.dart';

// class TCoordinateFrm extends StatefulWidget {
//   @override
//   _TCoordinateFrmState createState() => _TCoordinateFrmState();
// }

// class _TCoordinateFrmState extends State<TCoordinateFrm> {
//   Setting setting;
//   MarketingMan marketingMan;
//   String date;

//   @override
//   void initState() {
//     setting = TIW.getSetting(context, initState: true);
//     marketingMan =
//         MarketingMan(marketingManName: setting.name + " " + setting.family);
//     date = Jalali.now().formatter.y +
//         "/" +
//         Jalali.now().formatter.mm +
//         "/" +
//         Jalali.now().formatter.dd;

//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Theme(
//       data: setting.currentSystem.themeData,
//       child: Scaffold(
//         appBar: AppBar(
//           title: ListTile(
//             title: Text(date,
//                 textAlign: TextAlign.right,
//                 style: TextStyle(
//                     fontFamily: 'IRANSans',
//                     fontSize: 12.0,
//                     color: Colors.white)),
//             subtitle: Text(marketingMan.marketingManName,
//                 maxLines: 1,
//                 overflow: TextOverflow.ellipsis,
//                 softWrap: true,
//                 textAlign: TextAlign.right,
//                 style: TextStyle(
//                   fontFamily: 'IRANSansUltraLight',
//                   fontSize: 10.0,
//                   color: Colors.white,
//                 )),
//           ),
//           actions: <Widget>[
//             IconButton(
//                 icon: Icon(
//                   FontAwesomeIcons.filter,
//                   size: 16,
//                 ),
//                 tooltip: 'فیلتر',
//                 onPressed: () async {
//                   List<Widget> widgets = []
//                   widgets.add(TTextFieldDate(
//                     title: "تاریخ",
//                     onValue: () {
//                       return date;
//                     },
//                     onChanged: (txt) {
//                       date = txt;
//                     },
//                   ));
//                   if (setting.isAdmin) {
//                     widgets.add(QKeyFilter<MarketingMan>(
//                       onValue: () {
//                         return marketingMan;
//                       },
//                       title: "بازاریاب",
//                       resource: "acc/person/marketingMan",
//                       // recCnt: 100,
//                       queryParams: <QueryParam>[],
//                       itemBuilder: (item, index) {
//                         return ListTileView.view1_1(
//                           index,
//                           item.marketingManName,
//                         );
//                       },
//                       onSelect: (marketingMan) {
//                         this.marketingMan = marketingMan;
//                       },
//                       onChangePermission: () {
//                         return Future.value(true);
//                       },
//                       filterWidgets: <Widget>[],
//                       onFiltered: () {
//                         return []
//                       },
//                       onDeleteFilter: () {},
//                     ));
//                   }

//                   await showGeneralDialog(
//                     context: context,
//                     pageBuilder: (BuildContext context,
//                         Animation<double> animation,
//                         Animation<double> secondaryAnimation) {
//                       return FilterWidget(
//                         title: "مختصات جغرافیایی",
//                         widgets:widgets,
//                         onFiltered: () {
//                           if (this.date != null && this.date != "") {
//                             Pattern pattern =
//                                 r'([12]\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\d|3[01]))';
//                             RegExp regex = new RegExp(pattern);
//                             if (regex.hasMatch(date)) {
//                             } else {
//                               //اگر تاریخ معتبر نباشد تاریخ امروز را قرار میدهد
//                               this.date =
//                                   "${Jalali.now().formatter.y}/${Jalali.now().formatter.mm}/${Jalali.now().formatter.dd}";
//                             }
//                           }
//                           Navigator.pop(context);
//                         },
//                         onCanceled: () {},
//                       );
//                     },
//                     barrierColor: Color.fromRGBO(255, 255, 255, 0.9),
//                     barrierDismissible: true,
//                     barrierLabel: MaterialLocalizations.of(context)
//                         .modalBarrierDismissLabel,
//                     transitionDuration: const Duration(milliseconds: 200),
//                   );
//                   setState(() {});
//                 }),
//           ],
//         ),
//         body: WebService.callInlineMode<Coordinate>(
//           value: Coordinate(),
//           futureResponse: _futureResponseCoordinates,
//           onWaiting: () {
//             return Center(
//               child: SingleChildScrollView(
//                 child: ProgressBar(
//                   label: "... " + "مختصات جغرافیایی",
//                   value: null,
//                   size: 100,
//                 ),
//               ),
//             );
//           },
//           on200listWidget: (coordinates) {
//             return _TMap(
//               coordinates: coordinates,
//             );
//           },
//           on204Widget: () {
//             return Center(
//                 child: SvgPicture.asset(
//               "assets/images/spider_web.svg",
//               height: 150,
//               color: Colors.black12,
//               semanticsLabel: 'nothing to show',
//             ));
//           },
//           on200objectWidget: (coordinate) {
//             return null;
//           },
//         ),
//       ),
//     );
//   }

//   Future<Response> _futureResponseCoordinates() {
//     List<QueryParam> queryParams = []
//     queryParams.add(QueryParam(
//         queryParamType: QueryParamType.TEXT, name: "date", value: date));
//     if (marketingMan.marketingManID != null) {
//       queryParams.add(QueryParam(
//           queryParamType: QueryParamType.NUMBER,
//           name: "marketingManID",
//           value: marketingMan.marketingManID.toString()));
//     }
//     return TRestful.get(
//       "sale/coordinates",
//       setting: setting,
//       queryParams: queryParams,
//       timeout: 10,
//     );
//   }
// }

// class _TMap extends StatefulWidget {
//   final List<Coordinate> coordinates;

//   const _TMap({
//     @required this.coordinates,
//   });
//   @override
//   __TMapState createState() => __TMapState();
// }

// class __TMapState extends State<_TMap> {
//   bool isOpenedBottomSheet = false;
//   Coordinate coordinate;
//   int index;
//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//       child: Scaffold(
//         body: _FlutterMap(
//           coordinates: widget.coordinates,
//           onTap: (latLng) {
//             setState(() {
//               isOpenedBottomSheet = false;
//             });
//           },
//           onClear: () {
//             setState(() {
//               isOpenedBottomSheet = false;
//             });
//           },
//           onTapMarker: (index, coordinate) {
//             setState(() {
//               isOpenedBottomSheet = true;
//               this.coordinate = coordinate;
//               this.index = index;
//             });
//           },
//         ),
//         bottomSheet: isOpenedBottomSheet
//             ? Container(
//                 height: 150,
//                 child: Column(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: <Widget>[
//                     ButtonBar(
//                       alignment: MainAxisAlignment.spaceBetween,
//                       children: <Widget>[
//                         Text(
//                           (index + 1).toString(),
//                           textAlign: TextAlign.center,
//                           style: TextStyle(
//                             fontFamily: 'IRANSans',
//                             fontSize: 14.0,
//                           ),
//                         ),
//                         Icon(
//                           Icons.flag,
//                           size: 30,
//                         ),
//                       ],
//                     ),
//                     ButtonBar(
//                       alignment: MainAxisAlignment.spaceBetween,
//                       children: <Widget>[
//                         Text(
//                           coordinate.customerName,
//                           textAlign: TextAlign.center,
//                           style: TextStyle(
//                             fontFamily: 'IRANSans',
//                             fontSize: 14.0,
//                           ),
//                         ),
//                         Icon(
//                           Icons.person_pin,
//                           size: 30,
//                         ),
//                       ],
//                     ),
//                     ButtonBar(
//                       alignment: MainAxisAlignment.spaceBetween,
//                       children: <Widget>[
//                         Text(
//                           coordinate.startTime,
//                           textAlign: TextAlign.center,
//                           style: TextStyle(
//                             fontFamily: 'IRANSans',
//                             fontSize: 14.0,
//                           ),
//                         ),
//                         Icon(
//                           Icons.timer,
//                           size: 30,
//                         ),
//                       ],
//                     )
//                   ],
//                 ),
//               )
//             : Container(
//                 height: 0,
//               ),
//       ),
//     );
//   }
// }

// class _FlutterMap extends StatefulWidget {
//   final List<Coordinate> coordinates;
//   final void Function(LatLng latLng) onTap;
//   final void Function(int index, Coordinate coordinate) onTapMarker;
//   final void Function() onClear;

//   const _FlutterMap({
//     this.coordinates,
//     this.onTap,
//     this.onTapMarker,
//     this.onClear,
//   });
//   @override
//   __FlutterMapState createState() => __FlutterMapState();
// }

// class __FlutterMapState extends State<_FlutterMap> {
//   List<Marker> markers = []

//   @override
//   void initState() {
//     createMarkers();
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return FlutterMap(
//       options: MapOptions(
//         onPositionChanged: (mapPosition, isNotFirstTime) {
//           if (isNotFirstTime) {
//             widget.onClear();
//           }
//         },
//         onTap: (latLng) {
//           widget.onTap(latLng);
//         },
//         center: widget.coordinates.first.latLng,
//         zoom: 16.0,
//       ),
//       layers: [
//         TileLayerOptions(
//           urlTemplate: "https://api.tiles.mapbox.com/v4/"
//               "{id}/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoiZ2VlZWtmYSIsImEiOiJjazVwdHMxeGowMGR1M2twMHg4ZTQwdTlpIn0.jnuAHrMOpjD9vL1jgbn-Uw",
//           additionalOptions: {
//             'accessToken':
//                 'pk.eyJ1IjoiZ2VlZWtmYSIsImEiOiJjazVwdHMxeGowMGR1M2twMHg4ZTQwdTlpIn0.jnuAHrMOpjD9vL1jgbn-Uw',
//             'id': 'mapbox.streets',
//           },
//         ),
//         MarkerLayerOptions(
//           markers: markers,
//         ),
//       ],
//     );
//   }

//   void createMarkers() {
//     markers.clear();
//     for (var i = 0; i < widget.coordinates.length; i++) {
//       markers.add(Marker(
//         width: 35.0,
//         height: 35.0,
//         point: widget.coordinates[i].latLng,
//         builder: (ctx) => RawMaterialButton(
//           elevation: 5.0,
//           child: Text((i + 1).toString(),
//               style: TextStyle(
//                 fontFamily: 'IRANSans',
//                 fontSize: 12.0,
//                 color: Colors.white,
//               )),
//           onPressed: () {
//             widget.onTapMarker(i, widget.coordinates[i]);
//           },
//           shape: CircleBorder(),
//           fillColor: Colors.blueGrey,
//         ),
//       ));
//     }
//   }
// }
