import 'package:cached_network_image/cached_network_image.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher_detail.dart';
import 'package:com_tarazgroup/modules/tlib/tlib.dart';
import 'package:flutter/material.dart';

class VoucherDetailListTilePlusMinus extends StatefulWidget {
  final VoucherDetail voucherDetail;
  final void Function() onTapPlus;
  final void Function() onTapMinus;

  VoucherDetailListTilePlusMinus({
    @required this.voucherDetail,
    @required this.onTapPlus,
    @required this.onTapMinus,
  });

  @override
  _VoucherDetailListTilePlusMinusState createState() =>
      _VoucherDetailListTilePlusMinusState();
}

class _VoucherDetailListTilePlusMinusState
    extends State<VoucherDetailListTilePlusMinus> {
  Setting setting;
  Widget image;
  int counter = 0;
  List<ButtonBar> buttonBars = [];
  List<PopupMenuItem<int>> popupMenuItems = [];

  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    if (!!(widget.voucherDetail.goods.imageUrls == null ||
        widget.voucherDetail.goods.imageUrls.length == 0)) {
      ///به این کالا عکسی اختصاص نشده است
      image = Icon(
        Icons.wallpaper,
        color: setting.currentSystem.themeData.primaryColorLight,
        size: 100,
      );
    } else {
      image = CachedNetworkImage(
        width: 100.0,
        height: 100.0,
        fit: BoxFit.cover,
        imageUrl: widget.voucherDetail.goods.imageUrls.first.toString(),
        errorWidget: (context, url, error) => Icon(Icons.error),
      );
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++
    buttonBars.add(ButtonBar(
      buttonPadding: EdgeInsets.all(2),
      alignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          widget.voucherDetail.goods.goodsCode,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'IRANSans',
            fontSize: 10.0,
            color: setting.currentSystem.themeData.primaryColorLight,
          ),
        ),
        Text(
          "کد کالا",
          textAlign: TextAlign.right,
          style: TextStyle(
            fontFamily: 'IRANSans',
            fontSize: 7.0,
            color: setting.currentSystem.themeData.primaryColorLight,
          ),
        ),
      ],
    ));
    // buttonBars.add(ButtonBar(
    //   buttonPadding: EdgeInsets.all(2),
    //   alignment: MainAxisAlignment.spaceBetween,
    //   children: <Widget>[
    //     Text(
    //       widget.voucherDetail.goods.storeName ?? "",
    //       textAlign: TextAlign.center,
    //       style: TextStyle(
    //         fontFamily: 'IRANSans',
    //         fontSize: 10.0,
    //         color: setting.currentSystem.themeData.primaryColorLight,
    //       ),
    //     ),
    //     Text(
    //       "انبار",
    //       textAlign: TextAlign.right,
    //       style: TextStyle(
    //         fontFamily: 'IRANSans',
    //         fontSize: 7.0,
    //         color: setting.currentSystem.themeData.primaryColorLight,
    //       ),
    //     ),
    //   ],
    // ));
    buttonBars.add(ButtonBar(
      buttonPadding: EdgeInsets.all(2),
      alignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          widget.voucherDetail.goodsUnit.unitDesc,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'IRANSans',
            fontSize: 10.0,
            color: setting.currentSystem.themeData.primaryColorLight,
          ),
        ),
        Text(
          "واحد",
          textAlign: TextAlign.right,
          style: TextStyle(
            fontFamily: 'IRANSans',
            fontSize: 7.0,
            color: setting.currentSystem.themeData.primaryColorLight,
          ),
        ),
      ],
    ));
    // بخاطر فارمد فیلد زیر برداشته شده است وگرنه باید درست شود در آینده
    // buttonBars.add(ButtonBar(
    //   buttonPadding: EdgeInsets.all(2),
    //   alignment: MainAxisAlignment.spaceBetween,
    //   children: <Widget>[
    //     Text(
    //       widget.voucherDetail.fee == null
    //           ? "0"
    //           : TLib.commaSeparate(widget.voucherDetail.fee.toString()),
    //       textAlign: TextAlign.center,
    //       style: TextStyle(
    //         fontFamily: 'IRANSans',
    //         fontSize: 10.0,
    //         color: setting.currentSystem.themeData.primaryColorLight,
    //       ),
    //     ),
    //     Text(
    //       "فی",
    //       textAlign: TextAlign.right,
    //       style: TextStyle(
    //         fontFamily: 'IRANSans',
    //         fontSize: 7.0,
    //         color: setting.currentSystem.themeData.primaryColorLight,
    //       ),
    //     ),
    //   ],
    // ));
    buttonBars.add(ButtonBar(
      buttonPadding: EdgeInsets.all(2),
      alignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          widget.voucherDetail.feeAgreement == null
              ? "0"
              : TLib.commaSeparate(widget.voucherDetail.feeAgreement.toString()),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'IRANSans',
            fontSize: 10.0,
            color: setting.currentSystem.themeData.primaryColorLight,
          ),
        ),
        Text(
          "فی برآوردی",
          textAlign: TextAlign.right,
          style: TextStyle(
            fontFamily: 'IRANSans',
            fontSize: 7.0,
            color: setting.currentSystem.themeData.primaryColorLight,
          ),
        ),
      ],
    ));
    buttonBars.add(ButtonBar(
      buttonPadding: EdgeInsets.all(2),
      alignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          widget.voucherDetail.remainQ == null
              ? "0"
              : TLib.commaSeparate(widget.voucherDetail.remainQ.toString()),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'IRANSans',
            fontSize: 10.0,
            color: setting.currentSystem.themeData.primaryColorLight,
          ),
        ),
        Text(
          "مانده مرجع",
          textAlign: TextAlign.right,
          style: TextStyle(
            fontFamily: 'IRANSans',
            fontSize: 7.0,
            color: setting.currentSystem.themeData.primaryColorLight,
          ),
        ),
      ],
    ));
    buttonBars.add(ButtonBar(
      buttonPadding: EdgeInsets.all(2),
      alignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          widget.voucherDetail.invRemain == null
              ? "0"
              : TLib.commaSeparate(widget.voucherDetail.invRemain.toString()),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'IRANSans',
            fontSize: 10.0,
            color: setting.currentSystem.themeData.primaryColorLight,
          ),
        ),
        Text(
          "موجودی",
          textAlign: TextAlign.right,
          style: TextStyle(
            fontFamily: 'IRANSans',
            fontSize: 7.0,
            color: setting.currentSystem.themeData.primaryColorLight,
          ),
        ),
      ],
    ));

    //++++++++++++++++++++++++++++++++++++++++++++++++++
    popupMenuItems.add(PopupMenuItem<int>(
      value: 0,
      child: ButtonBar(
        alignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            widget.voucherDetail.goods.goodsCode,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 14.0,
              color: setting.currentSystem.themeData.primaryColorLight,
            ),
          ),
          Text(
            "کد کالا",
            textAlign: TextAlign.right,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 8.0,
              color: setting.currentSystem.themeData.primaryColorLight,
            ),
          )
        ],
      ),
    ));
    popupMenuItems.add(
      PopupMenuItem<int>(
        value: 1,
        child: ButtonBar(
          alignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              widget.voucherDetail.goods.storeName ?? "",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 14.0,
                color: setting.currentSystem.themeData.primaryColorLight,
              ),
            ),
            Text(
              "انبار",
              textAlign: TextAlign.right,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 8.0,
                color: setting.currentSystem.themeData.primaryColorLight,
              ),
            ),
          ],
        ),
      ),
    );
    popupMenuItems.add(
      PopupMenuItem<int>(
        value: 2,
        child: ButtonBar(
          alignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              widget.voucherDetail.goodsUnit.unitDesc,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 14.0,
                color: setting.currentSystem.themeData.primaryColorLight,
              ),
            ),
            Text(
              "واحد",
              textAlign: TextAlign.right,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 8.0,
                color: setting.currentSystem.themeData.primaryColorLight,
              ),
            ),
          ],
        ),
      ),
    );
    popupMenuItems.add(PopupMenuItem<int>(
      value: 3,
      child: ButtonBar(
        alignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            widget.voucherDetail.goodsUnit.computeValue == null
                ? ""
                : widget.voucherDetail.goodsUnit.computeValue.toString(),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 14.0,
              color: setting.currentSystem.themeData.primaryColorLight,
            ),
          ),
          Text(
            "ضریب",
            textAlign: TextAlign.right,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 8.0,
              color: setting.currentSystem.themeData.primaryColorLight,
            ),
          ),
        ],
      ),
    ));
    popupMenuItems.add(PopupMenuItem<int>(
      value: 4,
      child: ButtonBar(
        alignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            widget.voucherDetail.fee == null
                ? "0"
                : TLib.commaSeparate(
                    widget.voucherDetail.fee.toString()),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 14.0,
              color: setting.currentSystem.themeData.primaryColorLight,
            ),
          ),
          Text(
            "فی",
            textAlign: TextAlign.right,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 8.0,
              color: setting.currentSystem.themeData.primaryColorLight,
            ),
          ),
        ],
      ),
    ));
    popupMenuItems.add(PopupMenuItem<int>(
      value: 5,
      child: ButtonBar(
        alignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            widget.voucherDetail.feeAgreement == null
                ? "0"
                : TLib.commaSeparate(
                    widget.voucherDetail.feeAgreement.toString()),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 14.0,
              color: setting.currentSystem.themeData.primaryColorLight,
            ),
          ),
          Text(
            "فی برآوردی",
            textAlign: TextAlign.right,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 8.0,
              color: setting.currentSystem.themeData.primaryColorLight,
            ),
          ),
        ],
      ),
    ));
    popupMenuItems.add(PopupMenuItem<int>(
      value: 6,
      child: ButtonBar(
        alignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            widget.voucherDetail.remainQ == null
                ? "0"
                : TLib.commaSeparate(widget.voucherDetail.remainQ.toString()),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 14.0,
              color: setting.currentSystem.themeData.primaryColorLight,
            ),
          ),
          Text(
            "مانده مرجع",
            textAlign: TextAlign.right,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 8.0,
              color: setting.currentSystem.themeData.primaryColorLight,
            ),
          ),
        ],
      ),
    ));

    popupMenuItems.add(PopupMenuItem<int>(
      value: 6,
      child: ButtonBar(
        alignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            widget.voucherDetail.invRemain == null
                ? "0"
                : TLib.commaSeparate(widget.voucherDetail.invRemain.toString()),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 14.0,
              color: setting.currentSystem.themeData.primaryColorLight,
            ),
          ),
          Text(
            "موجودی",
            textAlign: TextAlign.right,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 8.0,
              color: setting.currentSystem.themeData.primaryColorLight,
            ),
          ),
        ],
      ),
    ));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: PopupMenuButton<int>(
        icon: Icon(Icons.more_vert),
        itemBuilder: (BuildContext context) {
          return popupMenuItems;
        },
      ),
      subtitle: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
              child: Container(
                  height: 100,
                  margin: EdgeInsets.only(
                    right: 5,
                  ),
                  decoration: BoxDecoration(
                      border: Border(
                    right: BorderSide(
                      color: Colors.grey[400],
                      width: 0.3,
                    ),
                  )),
                  padding: EdgeInsets.only(right: 5),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: buttonBars,
                  ))),
          Container(
            decoration: BoxDecoration(
                color: Colors.grey[100],
                border: Border.all(
                  width: 0.3,
                  color: Colors.grey[400],
                )),
            width: 100,
            child: image,
          ),
          Container(
            margin: EdgeInsets.only(
              left: 5,
            ),
            width: 60,
            height: 100,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                // ElevatedButton(
                //     style: ElevatedButton.styleFrom(
                //         primary: Colors.grey[400], padding: EdgeInsets.all(0)),
                //     onPressed: () {
                //       counter++;
                //       setState(() {});
                //       widget.onTapPlus();
                //     },
                //     child: Icon(Icons.add)),
                // ElevatedButton(
                //     style: ElevatedButton.styleFrom(
                //         primary: Colors.grey[400], padding: EdgeInsets.all(0)),
                //     onPressed: () {
                //       if (counter > 0) {
                //         counter--;
                //       } else {
                //         counter = 0;
                //       }
                //       setState(() {});
                //       widget.onTapMinus();
                //     },
                //     child: Icon(Icons.remove))
              ],
            ),
          )
        ],
      ),
      title: Container(
        decoration: BoxDecoration(
            border: Border(
          bottom: BorderSide(
            color: Colors.grey[400],
            width: 0.3,
          ),
        )),
        margin: EdgeInsets.only(bottom: 5),
        padding: EdgeInsets.only(
          top: 5,
          bottom: 5,
          right: 5,
        ),
        child: ButtonBar(
          alignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              counter.toString(),
              style: TextStyle(
                  fontFamily: 'IRANSansBlack',
                  fontSize: 20.0,
                  color: setting.currentSystem.themeData.primaryColorLight),
            ),
            Text(
              widget.voucherDetail.goods.goodsDesc,
              textDirection: TextDirection.rtl,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              softWrap: true,
              textAlign: TextAlign.right,
              style: TextStyle(
                fontFamily: 'IRANSansBold',
                fontSize: 15.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
