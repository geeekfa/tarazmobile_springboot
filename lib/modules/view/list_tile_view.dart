import 'package:another_flushbar/flushbar.dart';
import 'package:com_tarazgroup/inv/inv_goods_barcode_confirmation.dart';
import 'package:com_tarazgroup/inv/inv_goods_serial_entrance.dart';
import 'package:com_tarazgroup/inv/inv_goods_serial_select.dart';
import 'package:com_tarazgroup/models/adding_goods_destination.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/models/voucher.dart';
import 'package:com_tarazgroup/models/voucher_detail.dart';
import 'package:com_tarazgroup/modules/dialog/custom_fields_dialog.dart';
import 'package:com_tarazgroup/modules/dialog/input_dialog.dart';
import 'package:com_tarazgroup/modules/tlib/tlib.dart';
import 'package:com_tarazgroup/sale/adding_1_goods_unit.dart'
    as SaleAddTOVoucherGoodsUnit;
import 'package:com_tarazgroup/inv/adding_1_goods_unit.dart'
    as InvAddTOVoucherGoodsUnit;
import 'package:com_tarazgroup/sale/adding_1_quantity_refer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ListTileView {
  static Widget view1_1(int index, String value) {
    return Container(
      height: 50,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
              child: Text(
            value,
            textAlign: TextAlign.right,
            style: TextStyle(
              fontFamily: 'IRANSans',
              fontSize: 12.0,
              color: Colors.black45,
            ),
          )),
          VerticalDivider(),
          Container(
            width: 50,
            child: Text(
              (index + 1).toString(),
              textAlign: TextAlign.left,
              style: TextStyle(
                fontFamily: 'IRANSansUltraLight',
                fontSize: 20.0,
                color: Colors.black45,
              ),
            ),
          )
        ],
      ),
    );
  }

  static Widget view1_2_1(int index, String value1, String value2) {
    return ListTile(
      trailing: Text(
        (index + 1).toString(),
        textAlign: TextAlign.left,
        style: TextStyle(
          fontFamily: 'IRANSansUltraLight',
          fontSize: 20.0,
          color: Colors.black45,
        ),
      ),
      title: Text(
        value1,
        textAlign: TextAlign.right,
        style: TextStyle(
          fontFamily: 'IRANSans',
          fontSize: 12.0,
          color: Colors.black45,
        ),
      ),
      subtitle: Text(
        value2,
        maxLines: 4,
        overflow: TextOverflow.ellipsis,
        softWrap: true,
        textAlign: TextAlign.right,
        textDirection: TextDirection.rtl,
        style: TextStyle(
          fontFamily: 'IRANSans',
          fontSize: 10.0,
          color: Colors.black45,
        ),
      ),
    );
  }

  static Widget view1_2(int index, String value1, String value2) {
    return Container(
      height: 50,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(
                value1,
                textAlign: TextAlign.right,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 12.0,
                  color: Colors.black45,
                ),
              ),
              Text(
                value2,
                maxLines: 4,
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                textAlign: TextAlign.right,
                textDirection: TextDirection.rtl,
                style: TextStyle(
                  fontFamily: 'IRANSans',
                  fontSize: 10.0,
                  color: Colors.black45,
                ),
              )
            ],
          ),
          VerticalDivider(),
          Container(
            width: 50,
            child: Text(
              (index + 1).toString(),
              textAlign: TextAlign.left,
              style: TextStyle(
                fontFamily: 'IRANSansUltraLight',
                fontSize: 20.0,
                color: Colors.black45,
              ),
            ),
          )
        ],
      ),
    );
  }

  static Widget checkboxListTile({
    bool value,
    String title,
    String secondary,
    String subTitle,
    Function(bool) onChanged,
  }) {
    return CheckboxListTile(
      value: value,
      dense: true,
      secondary: Text(
        secondary,
        textAlign: TextAlign.left,
        style: TextStyle(
          fontFamily: 'IRANSans',
          fontSize: 10.0,
          color: Colors.black45,
        ),
      ),
      title: Text(
        title,
        textAlign: TextAlign.right,
        style: TextStyle(
          fontFamily: 'IRANSansBold',
          fontSize: 12.0,
          color: Colors.black45,
        ),
      ),
      subtitle: Text(
        subTitle,
        textAlign: TextAlign.right,
        style: TextStyle(
          fontFamily: 'IRANSans',
          fontSize: 8.0,
          color: Colors.black45,
        ),
      ),
      onChanged: onChanged,
    );
  }

  static Widget voucherDetailListTileSale({
    BuildContext context,
    int index,
    VoucherDetail voucherDetail,
    Widget Function() parentWidget,
  }) {
    Setting setting = TIW.getSetting(context);
    bool isFeeAgrToFeeInRefer =
        setting.sale.saleSettingServer.systemSetup.isFeeAgrToFeeInRefer;
    return ListTile(
      leading: PopupMenuButton<int>(
        icon: Icon(Icons.more_vert),
        onSelected: (_index) {
          switch (_index) {
            case 0: //delete
              Voucher voucher = TIW.getVoucher(context);
              voucher.voucherDetails.removeAt(index);
              TIW.setVoucher(context, voucher);
              Navigator.popUntil(
                context,
                ModalRoute.withName('/saleHome'),
              );
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => parentWidget()),
              );
              break;
            case 1: //edit
              if (voucherDetail.dReferID != null &&
                  voucherDetail.dReferID != -1) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => AddingQuantityRefer(
                            index: index,
                            fee: voucherDetail.fee,
                            goods: voucherDetail.goods,
                            goodsUnit: voucherDetail.goodsUnit,
                            quantity: voucherDetail.quantity,
                            voucherDetailID: voucherDetail.dReferID,
                            parentWidget: parentWidget,
                          )),
                );
              } else {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          SaleAddTOVoucherGoodsUnit.AddingGoodsUnit(
                            addingGoodsDestination:
                                AddingGoodsDestination.VOUCHER,
                            index: index,
                            goods: voucherDetail.goods,
                            goodsUnit: voucherDetail.goodsUnit,
                            quantity: voucherDetail.quantity,
                            price: voucherDetail.fee,
                            parentWidget: parentWidget,
                          )),
                );
              }

              break;
            case 2: //شرح ردیف
              Voucher voucher = TIW.getVoucher(context);
              VoucherDetail voucherDetailTemp = voucher.voucherDetails[index];
              TInputDialog(context: context)
                  .show("شرح ردیف", voucherDetailTemp.detailXDesc)
                  .then((detailXDesc) {
                if (detailXDesc != null) {
                  voucherDetailTemp.detailXDesc = detailXDesc;
                  TIW.setVoucher(context, voucher);
                  Navigator.popUntil(
                    context,
                    ModalRoute.withName('/saleHome'),
                  );
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => parentWidget()),
                  );
                }
              });
              break;
            case 3: //فیلد های اختیاری
              Voucher voucher = TIW.getVoucher(context);
              VoucherDetail voucherDetailTemp = voucher.voucherDetails[index];
              List<String> customFields = []..length = 25;
              customFields[0] = voucherDetailTemp.customField1;
              customFields[1] = voucherDetailTemp.customField2;
              customFields[2] = voucherDetailTemp.customField3;
              customFields[3] = voucherDetailTemp.customField4;
              customFields[4] = voucherDetailTemp.customField5;
              customFields[5] = voucherDetailTemp.customField6;
              customFields[6] = voucherDetailTemp.customField7;
              customFields[7] = voucherDetailTemp.customField8;
              customFields[8] = voucherDetailTemp.customField9;
              customFields[9] = voucherDetailTemp.customField10;

              customFields[10] = voucherDetailTemp.customField11;
              customFields[11] = voucherDetailTemp.customField12;
              customFields[12] = voucherDetailTemp.customField13;
              customFields[13] = voucherDetailTemp.customField14;
              customFields[14] = voucherDetailTemp.customField15;
              customFields[15] = voucherDetailTemp.customField16;
              customFields[16] = voucherDetailTemp.customField17;
              customFields[17] = voucherDetailTemp.customField18;
              customFields[18] = voucherDetailTemp.customField19;
              customFields[19] = voucherDetailTemp.customField20;

              customFields[20] = voucherDetailTemp.customField21;
              customFields[21] = voucherDetailTemp.customField22;
              customFields[22] = voucherDetailTemp.customField23;
              customFields[23] = voucherDetailTemp.customField24;
              customFields[24] = voucherDetailTemp.customField25;

              TCustomFieldsDialog(context: context)
                  .show("فیلد های اختیاری", customFields, voucherDetailTemp.fee)
                  .then((customFields) {
                if (customFields != null) {
                  voucherDetailTemp.customField1 = customFields[0];
                  voucherDetailTemp.customField2 = customFields[1];
                  voucherDetailTemp.customField3 = customFields[2];
                  voucherDetailTemp.customField4 = customFields[3];
                  voucherDetailTemp.customField5 = customFields[4];
                  voucherDetailTemp.customField6 = customFields[5];
                  voucherDetailTemp.customField7 = customFields[6];
                  voucherDetailTemp.customField8 = customFields[7];
                  voucherDetailTemp.customField9 = customFields[8];
                  voucherDetailTemp.customField10 = customFields[9];

                  voucherDetailTemp.customField11 = customFields[10];
                  voucherDetailTemp.customField12 = customFields[11];
                  voucherDetailTemp.customField13 = customFields[12];
                  voucherDetailTemp.customField14 = customFields[13];
                  voucherDetailTemp.customField15 = customFields[14];
                  voucherDetailTemp.customField16 = customFields[15];
                  voucherDetailTemp.customField17 = customFields[16];
                  voucherDetailTemp.customField18 = customFields[17];
                  voucherDetailTemp.customField19 = customFields[18];
                  voucherDetailTemp.customField20 = customFields[19];

                  voucherDetailTemp.customField21 = customFields[20];
                  voucherDetailTemp.customField22 = customFields[21];
                  voucherDetailTemp.customField23 = customFields[22];
                  voucherDetailTemp.customField24 = customFields[23];
                  voucherDetailTemp.customField25 = customFields[24];

                  TIW.setVoucher(context, voucher);
                  Navigator.popUntil(
                    context,
                    ModalRoute.withName('/saleHome'),
                  );
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => parentWidget()),
                  );
                }
              });
              break;
            default:
          }
        },
        itemBuilder: (BuildContext context) {
          List<PopupMenuItem<int>> pmes = [];
          pmes.add(
            PopupMenuItem<int>(
              value: 0,
              child: ListTile(
                  trailing: Icon(
                    Icons.delete,
                    size: 16,
                  ),
                  title: Container(
                    width: 80,
                    child: Text(
                      "حذف کالا",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontFamily: 'IRANSans',
                        fontSize: 10.0,
                        color: Colors.black54,
                      ),
                    ),
                  )),
            ),
          );
          pmes.add(
            PopupMenuItem<int>(
              value: 1,
              child: ListTile(
                  trailing: Icon(
                    Icons.edit,
                    size: 16,
                  ),
                  title: Container(
                    width: 80,
                    child: Text(
                      "ویرایش کالا",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontFamily: 'IRANSans',
                        fontSize: 10.0,
                        color: Colors.black54,
                      ),
                    ),
                  )),
            ),
          );
          pmes.add(
            PopupMenuItem<int>(
              value: 2,
              child: ListTile(
                  trailing: Icon(
                    Icons.chat,
                    size: 16,
                  ),
                  title: Container(
                    width: 80,
                    child: Text(
                      "شرح ردیف",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontFamily: 'IRANSans',
                        fontSize: 10.0,
                        color: Colors.black54,
                      ),
                    ),
                  )),
            ),
          );
          pmes.add(
            PopupMenuItem<int>(
              value: 3,
              child: ListTile(
                  trailing: Icon(
                    Icons.accessibility,
                    size: 16,
                  ),
                  title: Container(
                    width: 80,
                    child: Text(
                      "فیلد های اختیاری",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontFamily: 'IRANSans',
                        fontSize: 10.0,
                        color: Colors.black54,
                      ),
                    ),
                  )),
            ),
          );
          return pmes;
        },
      ),
      trailing: Text(
        (index + 1).toString(),
        textAlign: TextAlign.left,
        style: TextStyle(
          fontFamily: 'IRANSansUltraLight',
          fontSize: 20.0,
        ),
      ),
      title: Container(
        decoration: BoxDecoration(
            border: Border(
          bottom: BorderSide(
            color: Colors.grey[400],
            width: 0.3,
          ),
          right: BorderSide(
            color: Colors.grey[400],
            width: 0.3,
          ),
        )),
        padding: EdgeInsets.only(
          top: 5,
          bottom: 5,
          right: 5,
        ),
        child: Text(
          voucherDetail.goods.goodsDesc,
          textDirection: TextDirection.rtl,
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          softWrap: true,
          textAlign: TextAlign.right,
          style: TextStyle(
            fontFamily: 'IRANSansBold',
            fontSize: 15.0,
            // color: currentSystem.themeData.primaryColorDark,
          ),
        ),
      ),
      subtitle: Container(
        decoration: BoxDecoration(
            border: Border(
          right: BorderSide(
            color: Colors.grey[400],
            width: 0.3,
          ),
        )),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  TLib.commaSeparate(voucherDetail.fee.toString()),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  ),
                ),
                Text("قیمت",
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontFamily: 'IRANSansUltraLight',
                      fontSize: 9.0,
                    ))
                // Icon(
                //   Icons.local_offer,
                //   size: 15,
                // ),
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  TLib.commaSeparate(voucherDetail.quantity.toString()),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  ),
                ),
                Text("تعداد سفارش به " + voucherDetail.goodsUnit.unitDesc,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontFamily: 'IRANSansUltraLight',
                      fontSize: 9.0,
                    ))
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  TLib.commaSeparate(
                      voucherDetail.goodsUnit.computeValue.toString()),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  ),
                ),
                Text("ضریب به " + voucherDetail.goods.goodsUnit.unitDesc,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontFamily: 'IRANSansUltraLight',
                      fontSize: 9.0,
                    ))
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  voucherDetail.detailXDesc ?? "",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  ),
                ),
                Text("شرح ردیف",
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontFamily: 'IRANSansUltraLight',
                      fontSize: 9.0,
                    ))
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  voucherDetail.goods.storeName ?? "",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  ),
                ),
                Text("انبار",
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontFamily: 'IRANSansUltraLight',
                      fontSize: 9.0,
                    ))
                // Icon(
                //   Icons.store,
                //   size: 15,
                // ),
              ],
            ),
            Divider(
              thickness: 1.0,
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  TLib.commaSeparate((voucherDetail.quantity *
                          voucherDetail.goodsUnit.computeValue *
                          voucherDetail.fee)
                      .toString()),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'IRANSansBold',
                    fontSize: 12.0,
                  ),
                ),
                Text(
                  "\$",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'IRANSansBold',
                    fontSize: 12.0,
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  static Widget voucherDetailListTileSaleUnlockByBarcode({
    BuildContext context,
    int index,
    VoucherDetail voucherDetail,
    Widget Function() parentWidget,
  }) {
    // منظور این است که مقدار این کالای سند فقط با بارکد تغییر میکند
    // Setting setting = TIW.getSetting(context);
    // bool isFeeAgrToFeeInRefer =
    //     setting.sale.saleSettingServer.systemSetup.isFeeAgrToFeeInRefer;
    return ListTile(
      leading: PopupMenuButton<int>(
        icon: Icon(Icons.more_vert),
        onSelected: (_index) {
          switch (_index) {
            case 0: //delete
              Voucher voucher = TIW.getVoucher(context);
              voucher.voucherDetails[index].quantity = 0;
              TIW.setVoucher(context, voucher);
              Navigator.popUntil(
                context,
                ModalRoute.withName('/saleHome'),
              );
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => parentWidget()),
              );
              break;
            case 1: //شرح ردیف
              Voucher voucher = TIW.getVoucher(context);
              VoucherDetail voucherDetailTemp = voucher.voucherDetails[index];
              TInputDialog(context: context)
                  .show("شرح ردیف", voucherDetailTemp.detailXDesc)
                  .then((detailXDesc) {
                if (detailXDesc != null) {
                  voucherDetailTemp.detailXDesc = detailXDesc;
                  TIW.setVoucher(context, voucher);
                  Navigator.popUntil(
                    context,
                    ModalRoute.withName('/saleHome'),
                  );
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => parentWidget()),
                  );
                }
              });
              break;
            case 2: //فیلد های اختیاری
              Voucher voucher = TIW.getVoucher(context);
              VoucherDetail voucherDetailTemp = voucher.voucherDetails[index];
              List<String> customFields = []..length = 25;
              customFields[0] = voucherDetailTemp.customField1;
              customFields[1] = voucherDetailTemp.customField2;
              customFields[2] = voucherDetailTemp.customField3;
              customFields[3] = voucherDetailTemp.customField4;
              customFields[4] = voucherDetailTemp.customField5;
              customFields[5] = voucherDetailTemp.customField6;
              customFields[6] = voucherDetailTemp.customField7;
              customFields[7] = voucherDetailTemp.customField8;
              customFields[8] = voucherDetailTemp.customField9;
              customFields[9] = voucherDetailTemp.customField10;

              customFields[10] = voucherDetailTemp.customField11;
              customFields[11] = voucherDetailTemp.customField12;
              customFields[12] = voucherDetailTemp.customField13;
              customFields[13] = voucherDetailTemp.customField14;
              customFields[14] = voucherDetailTemp.customField15;
              customFields[15] = voucherDetailTemp.customField16;
              customFields[16] = voucherDetailTemp.customField17;
              customFields[17] = voucherDetailTemp.customField18;
              customFields[18] = voucherDetailTemp.customField19;
              customFields[19] = voucherDetailTemp.customField20;

              customFields[20] = voucherDetailTemp.customField21;
              customFields[21] = voucherDetailTemp.customField22;
              customFields[22] = voucherDetailTemp.customField23;
              customFields[23] = voucherDetailTemp.customField24;
              customFields[24] = voucherDetailTemp.customField25;

              TCustomFieldsDialog(context: context)
                  .show("فیلد های اختیاری", customFields, voucherDetailTemp.fee)
                  .then((customFields) {
                if (customFields != null) {
                  voucherDetailTemp.customField1 = customFields[0];
                  voucherDetailTemp.customField2 = customFields[1];
                  voucherDetailTemp.customField3 = customFields[2];
                  voucherDetailTemp.customField4 = customFields[3];
                  voucherDetailTemp.customField5 = customFields[4];
                  voucherDetailTemp.customField6 = customFields[5];
                  voucherDetailTemp.customField7 = customFields[6];
                  voucherDetailTemp.customField8 = customFields[7];
                  voucherDetailTemp.customField9 = customFields[8];
                  voucherDetailTemp.customField10 = customFields[9];

                  voucherDetailTemp.customField11 = customFields[10];
                  voucherDetailTemp.customField12 = customFields[11];
                  voucherDetailTemp.customField13 = customFields[12];
                  voucherDetailTemp.customField14 = customFields[13];
                  voucherDetailTemp.customField15 = customFields[14];
                  voucherDetailTemp.customField16 = customFields[15];
                  voucherDetailTemp.customField17 = customFields[16];
                  voucherDetailTemp.customField18 = customFields[17];
                  voucherDetailTemp.customField19 = customFields[18];
                  voucherDetailTemp.customField20 = customFields[19];

                  voucherDetailTemp.customField21 = customFields[20];
                  voucherDetailTemp.customField22 = customFields[21];
                  voucherDetailTemp.customField23 = customFields[22];
                  voucherDetailTemp.customField24 = customFields[23];
                  voucherDetailTemp.customField25 = customFields[24];
                  TIW.setVoucher(context, voucher);
                  Navigator.popUntil(
                    context,
                    ModalRoute.withName('/saleHome'),
                  );
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => parentWidget()),
                  );
                }
              });
              break;
            default:
          }
        },
        itemBuilder: (BuildContext context) {
          List<PopupMenuItem<int>> pmes = [];
          pmes.add(
            PopupMenuItem<int>(
              value: 0,
              child: ListTile(
                  trailing: Icon(
                    Icons.delete,
                    size: 16,
                  ),
                  title: Container(
                    width: 80,
                    child: Text(
                      "صفر کردن تعداد",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontFamily: 'IRANSans',
                        fontSize: 10.0,
                        color: Colors.black54,
                      ),
                    ),
                  )),
            ),
          );
          pmes.add(
            PopupMenuItem<int>(
              value: 1,
              child: ListTile(
                  trailing: Icon(
                    Icons.chat,
                    size: 16,
                  ),
                  title: Container(
                    width: 80,
                    child: Text(
                      "شرح ردیف",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontFamily: 'IRANSans',
                        fontSize: 10.0,
                        color: Colors.black54,
                      ),
                    ),
                  )),
            ),
          );
          pmes.add(
            PopupMenuItem<int>(
              value: 2,
              child: ListTile(
                  trailing: Icon(
                    Icons.accessibility,
                    size: 16,
                  ),
                  title: Container(
                    width: 80,
                    child: Text(
                      "فیلد های اختیاری",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontFamily: 'IRANSans',
                        fontSize: 10.0,
                        color: Colors.black54,
                      ),
                    ),
                  )),
            ),
          );

          return pmes;
        },
      ),
      trailing: Text(
        (index + 1).toString(),
        textAlign: TextAlign.left,
        style: TextStyle(
          fontFamily: 'IRANSansUltraLight',
          fontSize: 20.0,
        ),
      ),
      title: Container(
        decoration: BoxDecoration(
            border: Border(
          bottom: BorderSide(
            color: Colors.grey[400],
            width: 0.3,
          ),
          right: BorderSide(
            color: Colors.grey[400],
            width: 0.3,
          ),
        )),
        padding: EdgeInsets.only(
          top: 5,
          bottom: 5,
          right: 5,
        ),
        child: Text(
          voucherDetail.goods.goodsDesc,
          textDirection: TextDirection.rtl,
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          softWrap: true,
          textAlign: TextAlign.right,
          style: TextStyle(
            fontFamily: 'IRANSansBold',
            fontSize: 15.0,
            // color: currentSystem.themeData.primaryColorDark,
          ),
        ),
      ),
      subtitle: Container(
        decoration: BoxDecoration(
            border: Border(
          right: BorderSide(
            color: Colors.grey[400],
            width: 0.3,
          ),
        )),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  TLib.commaSeparate(voucherDetail.fee.toString()),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  ),
                ),
                Text("قیمت",
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontFamily: 'IRANSansUltraLight',
                      fontSize: 9.0,
                    ))
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  TLib.commaSeparate(voucherDetail.quantityReserved.toString()),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  ),
                ),
                Text(
                    "تعداد سفارش در سند مرجع به " +
                        voucherDetail.goodsUnit.unitDesc,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontFamily: 'IRANSansUltraLight',
                      fontSize: 9.0,
                    ))
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  TLib.commaSeparate(voucherDetail.quantity.toString()),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'IRANSans',
                      fontSize: 12.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.red),
                ),
                Text("تعداد سفارش به " + voucherDetail.goodsUnit.unitDesc,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        fontFamily: 'IRANSansUltraLight', fontSize: 9.0))
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  TLib.commaSeparate(
                      voucherDetail.goodsUnit.computeValue.toString()),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  ),
                ),
                Text("ضریب به " + voucherDetail.goods.goodsUnit.unitDesc,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontFamily: 'IRANSansUltraLight',
                      fontSize: 9.0,
                    ))
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  voucherDetail.detailXDesc ?? "",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  ),
                ),
                Text("شرح ردیف",
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontFamily: 'IRANSansUltraLight',
                      fontSize: 9.0,
                    ))
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  voucherDetail.goods.storeName ?? "",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  ),
                ),
                Text("انبار",
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontFamily: 'IRANSansUltraLight',
                      fontSize: 9.0,
                    ))
                // Icon(
                //   Icons.store,
                //   size: 15,
                // ),
              ],
            ),
            Divider(
              thickness: 1.0,
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  TLib.commaSeparate((voucherDetail.quantity *
                          voucherDetail.goodsUnit.computeValue *
                          voucherDetail.fee)
                      .toString()),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'IRANSansBold',
                    fontSize: 12.0,
                  ),
                ),
                Text(
                  "\$",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'IRANSansBold',
                    fontSize: 12.0,
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  static Widget voucherDetailListTileInv({
    BuildContext context,
    int index,
    VoucherDetail voucherDetail,
    Widget Function() parentWidget,
  }) {
    return ListTile(
      leading: PopupMenuButton<int>(
        icon: Icon(Icons.more_vert),
        onSelected: (_index) {
          switch (_index) {
            case 0: //delete
              Voucher voucher = TIW.getVoucher(context);
              voucher.voucherDetails.removeAt(index);
              TIW.setVoucher(context, voucher);
              Navigator.popUntil(
                context,
                ModalRoute.withName('/invHome'),
              );
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => parentWidget()),
              );
              break;
            case 1: //edit
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          InvAddTOVoucherGoodsUnit.AddingGoodsUnit(
                            index: index,
                            goods: voucherDetail.goods,
                            goodsUnit: voucherDetail.goodsUnit,
                            quantity: voucherDetail.quantity,
                            parentWidget: parentWidget,
                          )));
              break;
            case 2: //confirm تایید کالا
              VoucherDetail voucherDetailTemp = VoucherDetail(index,
                  status: VoucherDetailStatus.DEFAULT_ACTIVE,
                  goodsUnit: voucherDetail.goodsUnit,
                  goods: voucherDetail.goods,
                  quantity: voucherDetail.quantity,
                  voucherHeaderID: voucherDetail.voucherHeaderID,
                  voucherDetailID: voucherDetail.voucherDetailID);
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        InvGoodsBarcodeConfirmation(
                          voucherDetail: voucherDetailTemp,
                        )),
              );
              break;
            case 3: //ورود سریال کالا
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => InvGoodsSerialEntrance(
                          rowIndex: index,
                        )),
              );
              break;
            case 4: //انتخاب سریال کالا
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => InvGoodsSerialSelect(
                          rowIndex: index,
                        )),
              );
              break;
            default:
          }
        },
        itemBuilder: (BuildContext context) {
          Voucher voucher = TIW.getVoucher(context);
          List<PopupMenuItem<int>> pmes = [];
          pmes.add(
            PopupMenuItem<int>(
              value: 0,
              child: ListTile(
                  trailing: Icon(
                    Icons.delete,
                    size: 16,
                  ),
                  title: Container(
                    width: 80,
                    child: Text(
                      "حذف کالا",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontFamily: 'IRANSans',
                        fontSize: 10.0,
                        color: Colors.black54,
                      ),
                    ),
                  )),
            ),
          );
          pmes.add(
            PopupMenuItem<int>(
              value: 1,
              child: ListTile(
                  trailing: Icon(
                    Icons.edit,
                    size: 16,
                  ),
                  title: Container(
                    width: 80,
                    child: Text(
                      "ویرایش کالا",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontFamily: 'IRANSans',
                        fontSize: 10.0,
                        color: Colors.black54,
                      ),
                    ),
                  )),
            ),
          );
          pmes.add(PopupMenuItem<int>(
            value: 2,
            child: ListTile(
                trailing: Icon(
                  Icons.done,
                  size: 16,
                ),
                title: Container(
                  width: 80,
                  child: Text(
                    "تایید کالا",
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontFamily: 'IRANSans',
                      fontSize: 10.0,
                      color: Colors.black54,
                    ),
                  ),
                )),
          ));

          if (voucherDetail.goods.serialType != 0) {
            // تنها کالاهای سریال پذیر اجازه دارند برایشان سربال وارد شود
            switch (
                voucher.voucherHeader.voucherType.serialType.selSerialType) {
              case 0: //این نوع سند سریال پذیر نیست

                break;
              case 1: //برای این نوع سند باید سریال وارد شود

                pmes.add(PopupMenuItem<int>(
                  value: 3,
                  child: ListTile(
                      trailing: Icon(
                        FontAwesomeIcons.qrcode,
                        size: 16,
                      ),
                      title: Container(
                        width: 80,
                        child: Text(
                          "ورود سریال کالا",
                          textAlign: TextAlign.right,
                          style: TextStyle(
                            fontFamily: 'IRANSans',
                            fontSize: 10.0,
                            color: Colors.black54,
                          ),
                        ),
                      )),
                ));

                break;
              case 2: //برای این نوع سند باید سریال انتخاب شود
                pmes.add(PopupMenuItem<int>(
                  value: 4,
                  child: ListTile(
                      trailing: Icon(
                        FontAwesomeIcons.qrcode,
                        size: 16,
                      ),
                      title: Container(
                        width: 80,
                        child: Text(
                          "انتخاب سریال کالا",
                          textAlign: TextAlign.right,
                          style: TextStyle(
                            fontFamily: 'IRANSans',
                            fontSize: 10.0,
                            color: Colors.black54,
                          ),
                        ),
                      )),
                ));
                break;
              default:
            }
          }

          return pmes;
        },
      ),
      trailing: Text(
        (index + 1).toString(),
        textAlign: TextAlign.left,
        style: TextStyle(
          fontFamily: 'IRANSansUltraLight',
          fontSize: 20.0,
        ),
      ),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
              flex: 1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Padding(padding: EdgeInsets.only(left: 5)),
                      Text("×"),
                      Expanded(
                        flex: 2,
                        child: Text(
                          TLib.commaSeparate(voucherDetail.quantity.toString()),
                          textAlign: TextAlign.right,
                          style: TextStyle(
                            fontFamily: 'IRANSans',
                            fontSize: 12.0,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(voucherDetail.goodsUnit.unitDesc,
                            textAlign: TextAlign.right,
                            style: TextStyle(
                              fontFamily: 'IRANSansUltraLight',
                              fontSize: 8.0,
                            )),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: Text(
                          TLib.commaSeparate(
                              voucherDetail.goodsUnit.computeValue.toString()),
                          textAlign: TextAlign.right,
                          style: TextStyle(
                            fontFamily: 'IRANSans',
                            fontSize: 12.0,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(voucherDetail.goods.goodsUnit.unitDesc,
                            textAlign: TextAlign.right,
                            style: TextStyle(
                              fontFamily: 'IRANSansUltraLight',
                              fontSize: 8.0,
                            )),
                      )
                    ],
                  ),
                  Divider(
                    thickness: 1.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: Text(
                          TLib.commaSeparate((voucherDetail.quantity *
                                  voucherDetail.goodsUnit.computeValue)
                              .toString()),
                          textAlign: TextAlign.right,
                          style: TextStyle(
                            fontFamily: 'IRANSansBold',
                            fontSize: 12.0,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text("Σ",
                            textAlign: TextAlign.right,
                            style: TextStyle(
                              fontFamily: 'IRANSansUltraLight',
                              fontSize: 8.0,
                            )),
                      )
                    ],
                  ),
                ],
              )),
          Expanded(
            flex: 1,
            child: Text(
              voucherDetail.goods.goodsDesc,
              textAlign: TextAlign.right,
              style: TextStyle(
                fontFamily: 'IRANSansBold',
                fontSize: 14.0,
              ),
            ),
          ),
        ],
      ),
    );
  }

  static Widget voucherDetailListTileInvUnlockByBarcode1({
    BuildContext context,
    int index,
    VoucherDetail voucherDetail,
    Widget Function() parentWidget,
  }) {
    return ListTile(
      leading: PopupMenuButton<int>(
        icon: Icon(Icons.more_vert),
        onSelected: (_index) {
          switch (_index) {
            case 0: //delete
              Voucher voucher = TIW.getVoucher(context);
              voucher.voucherDetails.removeAt(index);
              TIW.setVoucher(context, voucher);
              Navigator.popUntil(
                context,
                ModalRoute.withName('/invHome'),
              );
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => parentWidget()),
              );
              break;
            case 1: //edit
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          InvAddTOVoucherGoodsUnit.AddingGoodsUnit(
                            index: index,
                            goods: voucherDetail.goods,
                            goodsUnit: voucherDetail.goodsUnit,
                            quantity: voucherDetail.quantity,
                            parentWidget: parentWidget,
                          )));
              break;
            case 2: //confirm تایید کالا
              VoucherDetail voucherDetailTemp = VoucherDetail(index,
                  status: VoucherDetailStatus.DEFAULT_ACTIVE,
                  goodsUnit: voucherDetail.goodsUnit,
                  goods: voucherDetail.goods,
                  quantity: voucherDetail.quantity,
                  voucherHeaderID: voucherDetail.voucherHeaderID,
                  voucherDetailID: voucherDetail.voucherDetailID);
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        InvGoodsBarcodeConfirmation(
                          voucherDetail: voucherDetailTemp,
                        )),
              );
              break;
            case 3: //ورود سریال کالا
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => InvGoodsSerialEntrance(
                          rowIndex: index,
                        )),
              );
              break;
            case 4: //انتخاب سریال کالا
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => InvGoodsSerialSelect(
                          rowIndex: index,
                        )),
              );
              break;
            default:
          }
        },
        itemBuilder: (BuildContext context) {
          Voucher voucher = TIW.getVoucher(context);
          List<PopupMenuItem<int>> pmes = [];
          pmes.add(
            PopupMenuItem<int>(
              value: 0,
              child: ListTile(
                  trailing: Icon(
                    Icons.delete,
                    size: 16,
                  ),
                  title: Container(
                    width: 80,
                    child: Text(
                      "حذف کالا",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontFamily: 'IRANSans',
                        fontSize: 10.0,
                        color: Colors.black54,
                      ),
                    ),
                  )),
            ),
          );
          pmes.add(
            PopupMenuItem<int>(
              value: 1,
              child: ListTile(
                  trailing: Icon(
                    Icons.edit,
                    size: 16,
                  ),
                  title: Container(
                    width: 80,
                    child: Text(
                      "ویرایش کالا",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontFamily: 'IRANSans',
                        fontSize: 10.0,
                        color: Colors.black54,
                      ),
                    ),
                  )),
            ),
          );
          pmes.add(PopupMenuItem<int>(
            value: 2,
            child: ListTile(
                trailing: Icon(
                  Icons.done,
                  size: 16,
                ),
                title: Container(
                  width: 80,
                  child: Text(
                    "تایید کالا",
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontFamily: 'IRANSans',
                      fontSize: 10.0,
                      color: Colors.black54,
                    ),
                  ),
                )),
          ));

          if (voucherDetail.goods.serialType != 0) {
            // تنها کالاهای سریال پذیر اجازه دارند برایشان سربال وارد شود
            switch (
                voucher.voucherHeader.voucherType.serialType.selSerialType) {
              case 0: //این نوع سند سریال پذیر نیست

                break;
              case 1: //برای این نوع سند باید سریال وارد شود

                pmes.add(PopupMenuItem<int>(
                  value: 3,
                  child: ListTile(
                      trailing: Icon(
                        FontAwesomeIcons.qrcode,
                        size: 16,
                      ),
                      title: Container(
                        width: 80,
                        child: Text(
                          "ورود سریال کالا",
                          textAlign: TextAlign.right,
                          style: TextStyle(
                            fontFamily: 'IRANSans',
                            fontSize: 10.0,
                            color: Colors.black54,
                          ),
                        ),
                      )),
                ));

                break;
              case 2: //برای این نوع سند باید سریال انتخاب شود
                pmes.add(PopupMenuItem<int>(
                  value: 4,
                  child: ListTile(
                      trailing: Icon(
                        FontAwesomeIcons.qrcode,
                        size: 16,
                      ),
                      title: Container(
                        width: 80,
                        child: Text(
                          "انتخاب سریال کالا",
                          textAlign: TextAlign.right,
                          style: TextStyle(
                            fontFamily: 'IRANSans',
                            fontSize: 10.0,
                            color: Colors.black54,
                          ),
                        ),
                      )),
                ));
                break;
              default:
            }
          }

          return pmes;
        },
      ),
      trailing: Text(
        (index + 1).toString(),
        textAlign: TextAlign.left,
        style: TextStyle(
          fontFamily: 'IRANSansUltraLight',
          fontSize: 20.0,
        ),
      ),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
              flex: 1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Padding(padding: EdgeInsets.only(left: 5)),
                      Text("×"),
                      Expanded(
                        flex: 2,
                        child: Text(
                          TLib.commaSeparate(voucherDetail.quantity.toString()),
                          textAlign: TextAlign.right,
                          style: TextStyle(
                            fontFamily: 'IRANSans',
                            fontSize: 12.0,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(voucherDetail.goodsUnit.unitDesc,
                            textAlign: TextAlign.right,
                            style: TextStyle(
                              fontFamily: 'IRANSansUltraLight',
                              fontSize: 8.0,
                            )),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: Text(
                          TLib.commaSeparate(
                              voucherDetail.goodsUnit.computeValue.toString()),
                          textAlign: TextAlign.right,
                          style: TextStyle(
                            fontFamily: 'IRANSans',
                            fontSize: 12.0,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(voucherDetail.goods.goodsUnit.unitDesc,
                            textAlign: TextAlign.right,
                            style: TextStyle(
                              fontFamily: 'IRANSansUltraLight',
                              fontSize: 8.0,
                            )),
                      )
                    ],
                  ),
                  Divider(
                    thickness: 1.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: Text(
                          TLib.commaSeparate((voucherDetail.quantity *
                                  voucherDetail.goodsUnit.computeValue)
                              .toString()),
                          textAlign: TextAlign.right,
                          style: TextStyle(
                            fontFamily: 'IRANSansBold',
                            fontSize: 12.0,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text("Σ",
                            textAlign: TextAlign.right,
                            style: TextStyle(
                              fontFamily: 'IRANSansUltraLight',
                              fontSize: 8.0,
                            )),
                      )
                    ],
                  ),
                ],
              )),
          Expanded(
            flex: 1,
            child: Text(
              voucherDetail.goods.goodsDesc,
              textAlign: TextAlign.right,
              style: TextStyle(
                fontFamily: 'IRANSansBold',
                fontSize: 14.0,
              ),
            ),
          ),
        ],
      ),
    );
  }

  static Widget voucherDetailListTileInvUnlockByBarcode({
    BuildContext context,
    int index,
    VoucherDetail voucherDetail,
    Widget Function() parentWidget,
  }) {
    // منظور این است که مقدار این کالای سند فقط با بارکد تغییر میکند
    // Setting setting = TIW.getSetting(context);
    // bool isFeeAgrToFeeInRefer =
    //     setting.sale.saleSettingServer.systemSetup.isFeeAgrToFeeInRefer;
    return ListTile(
      leading: PopupMenuButton<int>(
        icon: Icon(Icons.more_vert),
        onSelected: (_index) {
          switch (_index) {
            case 0: //delete
              Voucher voucher = TIW.getVoucher(context);
              voucher.voucherDetails[index].quantity = 0;
              TIW.setVoucher(context, voucher);
              Navigator.popUntil(
                context,
                ModalRoute.withName('/invHome'),
              );
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => parentWidget()),
              );
              break;
            case 1: //شرح ردیف
              Voucher voucher = TIW.getVoucher(context);
              VoucherDetail voucherDetailTemp = voucher.voucherDetails[index];
              TInputDialog(context: context)
                  .show("شرح ردیف", voucherDetailTemp.detailXDesc)
                  .then((detailXDesc) {
                if (detailXDesc != null) {
                  voucherDetailTemp.detailXDesc = detailXDesc;
                  TIW.setVoucher(context, voucher);
                  Navigator.popUntil(
                    context,
                    ModalRoute.withName('/invHome'),
                  );
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => parentWidget()),
                  );
                }
              });
              break;
            case 2: //فیلد های اختیاری
              Voucher voucher = TIW.getVoucher(context);
              VoucherDetail voucherDetailTemp = voucher.voucherDetails[index];
              List<String> customFields = []..length = 25;
              customFields[0] = voucherDetailTemp.customField1;
              customFields[1] = voucherDetailTemp.customField2;
              customFields[2] = voucherDetailTemp.customField3;
              customFields[3] = voucherDetailTemp.customField4;
              customFields[4] = voucherDetailTemp.customField5;
              customFields[5] = voucherDetailTemp.customField6;
              customFields[6] = voucherDetailTemp.customField7;
              customFields[7] = voucherDetailTemp.customField8;
              customFields[8] = voucherDetailTemp.customField9;
              customFields[9] = voucherDetailTemp.customField10;

              customFields[10] = voucherDetailTemp.customField11;
              customFields[11] = voucherDetailTemp.customField12;
              customFields[12] = voucherDetailTemp.customField13;
              customFields[13] = voucherDetailTemp.customField14;
              customFields[14] = voucherDetailTemp.customField15;
              customFields[15] = voucherDetailTemp.customField16;
              customFields[16] = voucherDetailTemp.customField17;
              customFields[17] = voucherDetailTemp.customField18;
              customFields[18] = voucherDetailTemp.customField19;
              customFields[19] = voucherDetailTemp.customField20;

              customFields[20] = voucherDetailTemp.customField21;
              customFields[21] = voucherDetailTemp.customField22;
              customFields[22] = voucherDetailTemp.customField23;
              customFields[23] = voucherDetailTemp.customField24;
              customFields[24] = voucherDetailTemp.customField25;

              TCustomFieldsDialog(context: context)
                  .show("فیلد های اختیاری", customFields, voucherDetailTemp.fee)
                  .then((customFields) {
                if (customFields != null) {
                  voucherDetailTemp.customField1 = customFields[0];
                  voucherDetailTemp.customField2 = customFields[1];
                  voucherDetailTemp.customField3 = customFields[2];
                  voucherDetailTemp.customField4 = customFields[3];
                  voucherDetailTemp.customField5 = customFields[4];
                  voucherDetailTemp.customField6 = customFields[5];
                  voucherDetailTemp.customField7 = customFields[6];
                  voucherDetailTemp.customField8 = customFields[7];
                  voucherDetailTemp.customField9 = customFields[8];
                  voucherDetailTemp.customField10 = customFields[9];

                  voucherDetailTemp.customField11 = customFields[10];
                  voucherDetailTemp.customField12 = customFields[11];
                  voucherDetailTemp.customField13 = customFields[12];
                  voucherDetailTemp.customField14 = customFields[13];
                  voucherDetailTemp.customField15 = customFields[14];
                  voucherDetailTemp.customField16 = customFields[15];
                  voucherDetailTemp.customField17 = customFields[16];
                  voucherDetailTemp.customField18 = customFields[17];
                  voucherDetailTemp.customField19 = customFields[18];
                  voucherDetailTemp.customField20 = customFields[19];

                  voucherDetailTemp.customField21 = customFields[20];
                  voucherDetailTemp.customField22 = customFields[21];
                  voucherDetailTemp.customField23 = customFields[22];
                  voucherDetailTemp.customField24 = customFields[23];
                  voucherDetailTemp.customField25 = customFields[24];
                  TIW.setVoucher(context, voucher);
                  Navigator.popUntil(
                    context,
                    ModalRoute.withName('/invHome'),
                  );
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => parentWidget()),
                  );
                }
              });
              break;
            case 3: //تایید دستی بارکد
              Voucher voucher = TIW.getVoucher(context);
              VoucherDetail voucherDetailTemp = voucher.voucherDetails[index];
              TInputDialog(context: context)
                  .show("تایید دستی بارکد", voucherDetailTemp.goods.barCode)
                  .then((barCode) {
                if (voucherDetailTemp.goods.barCode != barCode) {
                  showFlushBar(
                      context, "بارکد وارد شده معتبر نیست", Colors.red[300], 7,
                      () {
                    Navigator.popUntil(
                      context,
                      ModalRoute.withName('/invHome'),
                    );
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => parentWidget()),
                    );
                  });
                } else if (barCode != null) {
                  double newQuantity = voucherDetailTemp.quantity + 1;
                  if (newQuantity > voucherDetailTemp.quantityReserved) {
                    showFlushBar(context, "تعداد اسکن بیشتر از مانده مرجع است",
                        Colors.red[300], 5, () {
                      Navigator.popUntil(
                        context,
                        ModalRoute.withName('/invHome'),
                      );
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => parentWidget()),
                      );
                    });
                  } else {
                    // همه چیز اوکی است و تعداد کالا یکی بالا میرود
                    showFlushBar(
                        context,
                        voucherDetailTemp.goods.goodsDesc +
                            " به تعداد " +
                            newQuantity.toString() +
                            " بار اسکن شد",
                        Colors.green[300],
                        1, () {
                      voucherDetailTemp.quantity = newQuantity;
                      Navigator.popUntil(
                        context,
                        ModalRoute.withName('/invHome'),
                      );
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => parentWidget()),
                      );
                    });
                  }
                }
              });
              break;
            default:
          }
        },
        itemBuilder: (BuildContext context) {
          List<PopupMenuItem<int>> pmes = [];
          pmes.add(
            PopupMenuItem<int>(
              value: 0,
              child: ListTile(
                  trailing: Icon(
                    Icons.delete,
                    size: 16,
                  ),
                  title: Container(
                    width: 80,
                    child: Text(
                      "صفر کردن تعداد",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontFamily: 'IRANSans',
                        fontSize: 10.0,
                        color: Colors.black54,
                      ),
                    ),
                  )),
            ),
          );
          pmes.add(
            PopupMenuItem<int>(
              value: 1,
              child: ListTile(
                  trailing: Icon(
                    Icons.chat,
                    size: 16,
                  ),
                  title: Container(
                    width: 80,
                    child: Text(
                      "شرح ردیف",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontFamily: 'IRANSans',
                        fontSize: 10.0,
                        color: Colors.black54,
                      ),
                    ),
                  )),
            ),
          );
          pmes.add(
            PopupMenuItem<int>(
              value: 2,
              child: ListTile(
                  trailing: Icon(
                    Icons.accessibility,
                    size: 16,
                  ),
                  title: Container(
                    width: 80,
                    child: Text(
                      "فیلد های اختیاری",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontFamily: 'IRANSans',
                        fontSize: 10.0,
                        color: Colors.black54,
                      ),
                    ),
                  )),
            ),
          );
          pmes.add(
            PopupMenuItem<int>(
              value: 3,
              child: ListTile(
                  trailing: Icon(
                    Icons.qr_code,
                    size: 16,
                  ),
                  title: Container(
                    width: 80,
                    child: Text(
                      "تایید دستی بارکد",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontFamily: 'IRANSans',
                        fontSize: 10.0,
                        color: Colors.black54,
                      ),
                    ),
                  )),
            ),
          );
          return pmes;
        },
      ),
      trailing: Text(
        (index + 1).toString(),
        textAlign: TextAlign.left,
        style: TextStyle(
          fontFamily: 'IRANSansUltraLight',
          fontSize: 20.0,
        ),
      ),
      title: Container(
        decoration: BoxDecoration(
            border: Border(
          bottom: BorderSide(
            color: Colors.grey[400],
            width: 0.3,
          ),
          right: BorderSide(
            color: Colors.grey[400],
            width: 0.3,
          ),
        )),
        padding: EdgeInsets.only(
          top: 5,
          bottom: 5,
          right: 5,
        ),
        child: Text(
          voucherDetail.goods.goodsDesc,
          textDirection: TextDirection.rtl,
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          softWrap: true,
          textAlign: TextAlign.right,
          style: TextStyle(
            fontFamily: 'IRANSansBold',
            fontSize: 15.0,
            // color: currentSystem.themeData.primaryColorDark,
          ),
        ),
      ),
      subtitle: Container(
        decoration: BoxDecoration(
            border: Border(
          right: BorderSide(
            color: Colors.grey[400],
            width: 0.3,
          ),
        )),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  TLib.commaSeparate(voucherDetail.quantityReserved.toString()),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  ),
                ),
                Text(
                    "تعداد سفارش در سند مرجع به " +
                        voucherDetail.goodsUnit.unitDesc,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontFamily: 'IRANSansUltraLight',
                      fontSize: 9.0,
                    ))
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  TLib.commaSeparate(voucherDetail.quantity.toString()),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'IRANSans',
                      fontSize: 12.0,
                      fontWeight: FontWeight.bold,
                      color: voucherDetail.quantity <
                              voucherDetail.quantityReserved
                          ? Colors.red
                          : Colors.green),
                ),
                Text("تعداد سفارش به " + voucherDetail.goodsUnit.unitDesc,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        fontFamily: 'IRANSansUltraLight', fontSize: 9.0))
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  TLib.commaSeparate(
                      voucherDetail.goodsUnit.computeValue.toString()),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  ),
                ),
                Text("ضریب به " + voucherDetail.goods.goodsUnit.unitDesc,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontFamily: 'IRANSansUltraLight',
                      fontSize: 9.0,
                    ))
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  voucherDetail.detailXDesc ?? "",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  ),
                ),
                Text("شرح ردیف",
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontFamily: 'IRANSansUltraLight',
                      fontSize: 9.0,
                    ))
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  voucherDetail.goods.storeName ?? "",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 12.0,
                  ),
                ),
                Text("انبار",
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontFamily: 'IRANSansUltraLight',
                      fontSize: 9.0,
                    ))
                // Icon(
                //   Icons.store,
                //   size: 15,
                // ),
              ],
            ),
            Divider(
              thickness: 1.0,
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  TLib.commaSeparate((voucherDetail.quantity *
                          voucherDetail.goodsUnit.computeValue *
                          (voucherDetail.fee == null ? 0 : voucherDetail.fee))
                      .toString()),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'IRANSansBold',
                    fontSize: 12.0,
                  ),
                ),
                Text(
                  "\$",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'IRANSansBold',
                    fontSize: 12.0,
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  static showFlushBar(BuildContext context, String messageText, Color color,
      int time, Function onDISMISSED) {
    Flushbar(
      leftBarIndicatorColor: color,
      margin: EdgeInsets.all(8),
      // borderRadius: 8,
      messageText: Text(messageText,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'IRANSansBlack',
            color: Colors.white,
            fontSize: 14.0,
          )),
      duration: Duration(seconds: time),
    )..show(context).then((result) {
        onDISMISSED();
      });
  }
}
