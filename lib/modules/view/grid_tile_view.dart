import 'package:cached_network_image/cached_network_image.dart';
import 'package:com_tarazgroup/models/goods_group.dart';
import 'package:flutter/material.dart';

class GridTileView {
  static Widget defaultView(
    IconData iconData,
    String title,
    Color color,
    void Function() onTap,
  ) {
    return Material(
      borderRadius: BorderRadius.circular(0.0),
      elevation: 1,
      child: InkWell(
        borderRadius: BorderRadius.circular(0.0),
        child: GridTile(
          child: Icon(
            iconData,
            size: 60,
            color: color,
          ),
          footer: Text(title,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'IRANSans',
                fontSize: 14.0,
                color: color,
              )),
        ),
        onTap: onTap,
      ),
    );
  }

  static Widget goodsGroupView(
    GoodsGroup goodsGroup,
    Color color,
    void Function() onTap,
  ) {
    Widget image;
    if (!!(goodsGroup.imageGroup == null)) {
      ///به این  گروه کالا عکسی اختصاص داده نشده است
      image = Icon(
        Icons.image,
        color: color,
        size: 60,
      );
    } else {
      image = CachedNetworkImage(
        fit: BoxFit.cover,
        imageUrl: goodsGroup.imageGroup.toString(),
        errorWidget: (context, url, error) => Icon(Icons.error),
      );
    }
    return Material(
      borderRadius: BorderRadius.circular(0.0),
      elevation: 1,
      child: InkWell(
        borderRadius: BorderRadius.circular(0.0),
        child: GridTile(
          child: image,
          footer: ClipRect(
            child: Container(
              padding: EdgeInsets.all(10),
              color: Colors.black54,
              child: Text(goodsGroup.groupDesc,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'IRANSans',
                    fontSize: 14.0,
                    color: Colors.white,
                  )),
            ),
          ),
        ),
        onTap: onTap,
      ),
    );
  }
}
