import 'package:cached_network_image/cached_network_image.dart';
import 'package:com_tarazgroup/models/goods.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:flutter/material.dart';

class GoodsGridTilePlusMinus extends StatefulWidget {
  final Goods goods;
  final void Function() onTapPlus;
  final void Function() onTapMinus;

  const GoodsGridTilePlusMinus({
    @required this.goods,
    @required this.onTapPlus,
    @required this.onTapMinus,
  });

  @override
  _GoodsGridTilePlusMinusState createState() => _GoodsGridTilePlusMinusState();
}

class _GoodsGridTilePlusMinusState extends State<GoodsGridTilePlusMinus> {
  Setting setting;
  Widget image;
  int counter = 0;
  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    if (!!(widget.goods.imageUrls == null ||
        widget.goods.imageUrls.length == 0)) {
      ///به این کالا عکسی اختصتص نشده است
      image = Icon(
        Icons.image,
        color: setting.currentSystem.themeData.primaryColorLight,
        size: 200,
      );
    } else {
      image = CachedNetworkImage(
        width: 100.0,
        height: 100.0,
        fit: BoxFit.cover,
        imageUrl: widget.goods.imageUrls.first.toString(),
        errorWidget: (context, url, error) => Icon(Icons.error),
      );
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GridTile(
        header: ClipRect(
          child: Container(
            color: Colors.black38,
            child: ListTile(
              trailing: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Colors.grey[400], padding: EdgeInsets.all(0)),
                  onPressed: () {
                    counter++;
                    setState(() {});
                    widget.onTapPlus();
                  },
                  child: Icon(Icons.add)),
              title: Text(
                counter.toString(),
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: 'IRANSansBlack',
                    fontSize: 20.0,
                    color: Colors.white),
              ),
              leading: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Colors.grey[400], padding: EdgeInsets.all(0)),
                  onPressed: () {
                    if (counter > 0) {
                      counter--;
                    } else {
                      counter = 0;
                    }
                    setState(() {});
                    widget.onTapMinus();
                  },
                  child: Icon(Icons.remove)),
            ),
          ),
        ),
        footer: ClipRect(
          child: Container(
            padding: EdgeInsets.all(10),
            color: Colors.black38,
            child: Text(
              widget.goods.goodsDesc,
              textAlign: TextAlign.left,
              style: TextStyle(
                fontFamily: 'IRANSansBlack',
                fontSize: 15.0,
                color: Colors.white,
              ),
            ),
          ),
        ),
        child: image);
  }
}
