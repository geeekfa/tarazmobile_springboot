class TColumn {
  final String name;
  final String title;
  final double width;

  TColumn({
    this.name,
    this.title,
    this.width,
  });
}
