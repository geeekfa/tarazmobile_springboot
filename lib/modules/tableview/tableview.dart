import 'package:com_tarazgroup/modules/tableview/tcolumn.dart';
import 'package:flutter/material.dart';

class TableView<T> extends StatefulWidget {
  final int rowsLength;
  final Widget legendCell;
  final Widget Function(int rowIndex, TColumn tColumn) cellBuilder;
  final List<TColumn> tColumns;
  final List<T> items;
  final void Function(T t) onTap;

  TableView({
    Key key,
    @required this.rowsLength,
    this.legendCell = const Text(' '),
    @required this.cellBuilder,
    @required this.tColumns,
    @required this.items,
    @required this.onTap,
    // this.cellDimensions = CellDimensions.base,//initialize in constractor نکته آموزشی
  });

  @override
  _TableViewState createState() => _TableViewState<T>();
}

class _TableViewState<T> extends State<TableView<T>> {
  final ScrollController _verticalTitleController = ScrollController();
  ScrollController _verticalBodyController = ScrollController();
  final CellDimensions cellDimensions = CellDimensions.base;
  final ScrollController _horizontalBodyController = ScrollController();
  final ScrollController _horizontalTitleController = ScrollController();

  _SyncScrollController _verticalSyncController;
  _SyncScrollController _horizontalSyncController;

  @override
  void initState() {
    super.initState();
    _verticalSyncController = _SyncScrollController(
        [_verticalTitleController, _verticalBodyController]);
    _horizontalSyncController = _SyncScrollController(
        [_horizontalTitleController, _horizontalBodyController]);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(
              width: cellDimensions.stickyLegendWidth,
              height: cellDimensions.stickyLegendHeight,
              decoration: BoxDecoration(
                color: Theme.of(context).accentColor,
                border: Border.all(
                  width: 0.3,
                  color: Theme.of(context).primaryColorLight,
                ),
                borderRadius: BorderRadius.all(Radius.circular(0.0)),
              ),
              padding: EdgeInsets.all(5),
              child: Center(
                child: widget.legendCell,
              ),
            ),
            // STICKY ROW
            Expanded(
              child: NotificationListener<ScrollNotification>(
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: List.generate(
                      widget.tColumns.length,
                      (i) => Container(
                        width: widget.tColumns[i].width,
                        //  width: 300,
                        decoration: BoxDecoration(
                          color: Theme.of(context).accentColor,
                          border: Border.all(
                            width: 0.3,
                            color: Theme.of(context).primaryColorLight,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(0.0)),
                        ),
                        padding: EdgeInsets.all(5),
                        height: cellDimensions.stickyLegendHeight,
                        child: Center(
                          child: Text(
                            widget.tColumns[i].title,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            softWrap: true,
                            style: TextStyle(
                              fontFamily: 'IRANSans',
                              color: Theme.of(context).primaryColorDark,
                              fontSize: 14.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  controller: _horizontalTitleController,
                ),
                onNotification: (ScrollNotification notification) {
                  _horizontalSyncController.processNotification(
                      notification, _horizontalTitleController);
                  return true;
                },
              ),
            )
          ],
        ),
        Expanded(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              // STICKY COLUMN
              NotificationListener<ScrollNotification>(
                child: SingleChildScrollView(
                  child: Column(
                    children: List.generate(
                      widget.rowsLength,
                      (i) => Container(
                        width: cellDimensions.stickyLegendWidth,
                        height: cellDimensions.contentCellHeight,
                        decoration: BoxDecoration(
                          color: Theme.of(context).accentColor,
                          border: Border.all(
                            width: 0.3,
                            color: Theme.of(context).primaryColorLight,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(0.0)),
                        ),
                        padding: EdgeInsets.all(5),
                        child: Center(
                          child: Text((i + 1).toString()),
                        ),
                      ),
                    ),
                  ),
                  controller: _verticalTitleController,
                ),
                onNotification: (ScrollNotification notification) {
                  _verticalSyncController.processNotification(
                      notification, _verticalTitleController);
                  return true;
                },
              ),
              // CONTENT
              Expanded(
                child: NotificationListener<ScrollNotification>(
                  onNotification: (ScrollNotification notification) {
                    _horizontalSyncController.processNotification(
                        notification, _horizontalBodyController);
                    return true;
                  },
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    controller: _horizontalBodyController,
                    child: NotificationListener<ScrollNotification>(
                      child: SingleChildScrollView(
                          controller: _verticalBodyController,
                          child: Column(
                            children: List.generate(widget.rowsLength, (int i) {
                              return Material(
                                color: i % 2 == 0
                                    ? Colors.grey[100]
                                    : Colors.white,
                                child: InkWell(
                                  child: Row(
                                    children: List.generate(
                                      widget.tColumns.length,
                                      (int j) {
                                        return FittedBox(
                                          child: Container(
                                            constraints: BoxConstraints(
                                              maxWidth:
                                                  widget.tColumns[j].width,
                                              maxHeight: cellDimensions
                                                  .contentCellHeight,
                                            ),
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                width: 0.3,
                                                color: Theme.of(context)
                                                    .primaryColorLight,
                                              ),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(0.0)),
                                            ),
                                            padding: EdgeInsets.only(right: 5),
                                            alignment: Alignment.centerRight,
                                            child: widget.cellBuilder(
                                                i, widget.tColumns[j]),
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                  onTap: (){
                                    widget.onTap(widget.items[i]);
                                  },
                                ),
                              );
                            }),
                          )),
                      onNotification: (ScrollNotification notification) {
                        _verticalSyncController.processNotification(
                            notification, _verticalBodyController);
                        return true;
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

/// Dimensions for table
class CellDimensions {
  final double contentCellWidth;
  final double contentCellHeight;
  final double stickyLegendWidth;
  final double stickyLegendHeight;

  const CellDimensions({
    @required this.contentCellWidth,
    @required this.contentCellHeight,
    @required this.stickyLegendWidth,
    @required this.stickyLegendHeight,
  });

  static const CellDimensions base = CellDimensions(
    contentCellWidth: 70.0,
    contentCellHeight: 40.0,//40
    stickyLegendWidth: 45.0,
    stickyLegendHeight: 50.0,
  );
}

class _SyncScrollController {
  _SyncScrollController(List<ScrollController> controllers) {
    controllers
        .forEach((controller) => _registeredScrollControllers.add(controller));
  }

  final List<ScrollController> _registeredScrollControllers = [];

  ScrollController _scrollingController;
  bool _scrollingActive = false;

  processNotification(
      ScrollNotification notification, ScrollController sender) {
    if (notification is ScrollStartNotification && !_scrollingActive) {
      _scrollingController = sender;
      _scrollingActive = true;
      return;
    }

    if (identical(sender, _scrollingController) && _scrollingActive) {
      if (notification is ScrollEndNotification) {
        _scrollingController = null;
        _scrollingActive = false;
        return;
      }

      if (notification is ScrollUpdateNotification) {
        for (ScrollController controller in _registeredScrollControllers) {
          if (identical(_scrollingController, controller)) continue;
          controller.jumpTo(_scrollingController.offset);
        }
      }
    }
  }
}
