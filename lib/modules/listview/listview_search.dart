import 'package:com_tarazgroup/models/filter_field.dart';
import 'package:com_tarazgroup/models/progressbar.dart';
import 'package:com_tarazgroup/models/query_param.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/modules/future/webservice.dart';
import 'package:com_tarazgroup/modules/rest/restful.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:http/http.dart';

typedef QueryParams = List<QueryParam> Function();
typedef FilterFields = List<FilterField> Function();
typedef ItemBuilder<T> = Widget Function(T t, int index);
typedef OnTap<T> = void Function(T t);

class ListViewSearch<T> extends StatefulWidget {
  final T value;
  final String title;
  final String resource;
  final int recCnt;
  final QueryParams queryParams;
  final FilterFields filterFields;
  final ItemBuilder<T> itemBuilder;
  final OnTap<T> onTap;

  const ListViewSearch({
    this.value,
    this.title,
    this.resource,
    this.recCnt,
    this.queryParams,
    this.filterFields,
    this.itemBuilder,
    this.onTap,
  });

  @override
  _ListViewSearchState createState() => _ListViewSearchState<T>();
}

class _ListViewSearchState<T> extends State<ListViewSearch<T>> {
  TextEditingController textEditingControllerFilter;
  ScrollController scrollController;
  int pageNo;
  List<T> items;
  Setting setting;
  bool lock;
  double offset = 0.0;

  void syncDB(bool renew) {
    //if renew = true then resynch else just get next records
    if (renew) {
      pageNo = 1;
      items = [];
    }
  }

  void scrollListener() {
    offset = scrollController.offset;
    if (scrollController.position.extentAfter == 0) {
      setState(() {
        lock = false;
        pageNo++;
        syncDB(false);
      });
    }
  }

  @override
  void initState() {
    lock = false;
    setting = TIW.getSetting(context, initState: true);
    textEditingControllerFilter = TextEditingController();
    syncDB(true);
    super.initState();
  }

  @override
  void dispose() {
    textEditingControllerFilter.dispose();
    scrollController.removeListener(scrollListener);
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: AppBar(
        //   backgroundColor: setting.currentSystem.color,
        //   title: ListTile(
        //     title: Text(widget.title,
        //         textAlign: TextAlign.right,
        //         style: TextStyle(
        //             fontFamily: 'IRANSans',
        //             fontSize: 12.0,
        //             color: Colors.white)),
        //   ),
        // ),
        body: Column(
      children: <Widget>[
        // ListTile(
        //   trailing: PopupMenuButton(
        //       icon: Icon(
        //         FontAwesomeIcons.ellipsisV,
        //         color: Colors.black45,
        //       ),
        //       tooltip: "تنظیمات",
        //       onSelected: (val) async {
        //         switch (val) {
        //           case "addtovoucher":

        //             break;
        //           default:
        //         }
        //       },
        //       itemBuilder: (_) {
        //         List<PopupMenuItem<String>> popupMenuItems = []
        //         popupMenuItems.add(PopupMenuItem<String>(
        //             child: RadioListTile(
        //           controlAffinity: ListTileControlAffinity.trailing,
        //           value: true,
        //           groupValue: true,
        //           title: Text("همه فیلد ها",
        //               textAlign: TextAlign.right,
        //               style: TextStyle(
        //                   fontFamily: 'IRANSans',
        //                   color: Colors.black54,
        //                   fontSize: 10.0)),
        //           onChanged: null,
        //         )));
        //         widget.filterFields().forEach((filterField) {
        //           popupMenuItems.add(PopupMenuItem<String>(
        //               child: RadioListTile(
        //             controlAffinity: ListTileControlAffinity.trailing,
        //             value: false,
        //             groupValue: true,
        //             title: Text(filterField.title,
        //                 textAlign: TextAlign.right,
        //                 style: TextStyle(
        //                     fontFamily: 'IRANSans',
        //                     color: Colors.black54,
        //                     fontSize: 10.0)),
        //             onChanged: null,
        //           )));
        //         });

        //         return popupMenuItems;
        //       }),
        //   title: TextField(
        //     controller: textEditingControllerFilter,
        //     textAlign: TextAlign.right,
        //     textDirection: TextDirection.rtl,
        //     style: TextStyle(
        //       fontFamily: 'IRANSansLight',
        //       color: Colors.black,
        //       fontSize: 12.0,
        //     ),
        //     decoration: new InputDecoration(
        //       hintText: 'جستجو',
        //       hintStyle: TextStyle(
        //         color: Colors.black45,
        //         fontFamily: 'IRANSansLight',
        //         fontStyle: FontStyle.italic,
        //         fontSize: 12.0,
        //       ),
        //       enabledBorder: UnderlineInputBorder(
        //         borderSide: BorderSide(color: Colors.black),
        //       ),
        //     ),
        //   ),
        //   leading: IconButton(
        //     icon: Icon(Icons.search),
        //     onPressed: () {
        //       setState(() {
        //         lock = false;
        //         syncDB(true);
        //       });
        //     },
        //   ),
        // ),
        Container(
          padding: EdgeInsets.only(right: 20),
          child: Row(
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  setState(() {
                    lock = false;
                    syncDB(true);
                  });
                },
              ),
              Expanded(
                child: TextField(
                  controller: textEditingControllerFilter,
                  textAlign: TextAlign.right,
                  textDirection: TextDirection.rtl,
                  style: TextStyle(
                    fontFamily: 'IRANSansLight',
                    color: Colors.black,
                    fontSize: 12.0,
                  ),
                  decoration: new InputDecoration(
                    hintText: 'جستجو',
                    hintStyle: TextStyle(
                      color: Colors.black45,
                      fontFamily: 'IRANSansLight',
                      fontStyle: FontStyle.italic,
                      fontSize: 12.0,
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.black),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Expanded(
            child: lock
                ? _listView(true)
                : WebService.callInlineMode<T>(
                    value: widget.value,
                    futureResponse: _futureResponse,
                    onWaiting: () {
                      if (items.length == 0) {
                        return Center(
                          child: SingleChildScrollView(
                            child: ProgressBar(
                              label: "... " + widget.title,
                              value: null,
                              size: 100,
                            ),
                          ),
                        );
                      } else {
                        return ListView.separated(
                            separatorBuilder: (context, index) => Divider(
                                  // color: setting.currentSystem.color900,
                                  thickness: .25,
                                ),
                            controller: ScrollController(
                                initialScrollOffset: scrollController.offset),
                            itemCount: items.length + 1,
                            itemBuilder: (BuildContext context, int index) {
                              if (index == items.length) {
                                return ProgressBar(
                                  label: "... " + widget.title,
                                  value: null,
                                  size: 100,
                                );
                              } else {
                                return widget.itemBuilder(items[index], index);
                              }
                            });
                        // return ListView.builder(
                        //     controller: ScrollController(
                        //         initialScrollOffset: scrollController.offset),
                        //     itemCount: items.length + 1,
                        //     itemBuilder: (BuildContext context, int index) {
                        //       if (index == items.length) {
                        //         return ProgressBar(
                        //           label: "... " + widget.title,
                        //           value: null,
                        //         );
                        //       } else {
                        //         return widget.itemBuilder(items[index], index);
                        //       }
                        //     });
                      }
                    },
                    on200objectWidget: (obj) {
                      return null;
                    },
                    on200listWidget: (list) {
                      if (lock == true) {
                      } else {
                        items.addAll(list);
                        lock = true;
                      }
                      return _listView(true);
                    },
                    on204Widget: () {
                      scrollController.removeListener(scrollListener);
                      if (items.length == 0) {
                        return Center(
                            child: SvgPicture.asset(
                          "assets/images/spider_web.svg",
                          height: 150,
                          color: Colors.black12,
                          semanticsLabel: 'nothing to show',
                        ));
                      } else {
                        return _listView(false);
                      }
                    },
                  ))
      ],
    ));
  }

  ListView _listView(bool hasScrollListener) {
    scrollController = ScrollController(initialScrollOffset: offset);
    if (hasScrollListener) {
      scrollController..addListener(scrollListener);
    }
    return ListView.separated(
        separatorBuilder: (context, index) => Divider(
              // color: setting.currentSystem.color900,
              thickness: .25,
            ),
        controller: scrollController,
        itemCount: items.length,
        itemBuilder: (BuildContext context, int index) {
          return InkWell(
            borderRadius: BorderRadius.circular(0.0),
            splashColor: Colors.blueGrey[200],
            highlightColor: Colors.blueGrey[50],
            child: widget.itemBuilder(items[index], index),
            onTap: () {
              lock = true;
              widget.onTap(items[index]);
            },
          );
        });

    // return ListView.builder(
    //     controller: scrollController,
    //     itemCount: items.length,
    //     itemBuilder: (BuildContext context, int index) {
    //       return InkWell(
    //         borderRadius: BorderRadius.circular(0.0),
    //         splashColor: Colors.blueGrey[200],
    //         highlightColor: Colors.blueGrey[50],
    //         child: widget.itemBuilder(items[index], index),
    //         onTap: () {
    //           lock = true;
    //           widget.onTap(items[index]);
    //         },
    //       );
    //     });
  }

  Future<Response> _futureResponse() {
    List<QueryParam> queryParams = [];

    queryParams.add(QueryParam(
        queryParamType: QueryParamType.NUMBER,
        name: "pageNo",
        value: pageNo.toString()));
    queryParams.add(QueryParam(
        queryParamType: QueryParamType.NUMBER,
        name: "recCnt",
        value: widget.recCnt.toString()));

    if (textEditingControllerFilter.text != null &&
        textEditingControllerFilter.text != '') {
      List<FilterField> filterFields = widget.filterFields();
      for (var i = 0; i < filterFields.length; i++) {
        queryParams.add(QueryParam(
            queryParamType: QueryParamType.TEXT,
            name: filterFields[i].name,
            value: textEditingControllerFilter.text));
      }
    }
    queryParams.addAll(widget.queryParams());
    return TRestful.get(
      widget.resource,
      setting: setting,
      queryParams: queryParams,
      timeout: 30,
    );
  }
}
