import 'package:com_tarazgroup/models/progressbar.dart';
import 'package:com_tarazgroup/models/query_param.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/modules/future/webservice.dart';
import 'package:com_tarazgroup/modules/rest/restful.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart';

// typedef OnTap<T> = void Function(T t);

class ListViewFilter<T> extends StatefulWidget {
  final T value;
  final String title;
  final String resource;
  final List<QueryParam> queryParams;
  final Widget Function(T t, int index) itemBuilder;
  final void Function(T t) onTap;
  final Widget filterWidget;
  final void Function() onDeleteFilter;

  const ListViewFilter({
    this.value,
    this.title,
    this.resource,
    this.queryParams,
    this.itemBuilder,
    this.onTap,
    @required this.filterWidget,
    @required this.onDeleteFilter,
  });

  @override
  _ListViewFilterState createState() => _ListViewFilterState<T>();
}

class _ListViewFilterState<T> extends State<ListViewFilter<T>> {
  List<T> items;
  Setting setting;
  bool lock;
  List<QueryParam> queryParamsFiltered;

  void clearItems() {
    items = [];
  }

  @override
  void initState() {
    queryParamsFiltered = [];
    lock = false;
    setting = TIW.getSetting(context, initState: true);
    clearItems();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: setting.currentSystem.themeData,
      child: Scaffold(
          appBar: AppBar(
            title: ListTile(
              title: Text(widget.title,
                  textAlign: TextAlign.right,
                  style: TextStyle(
                      fontFamily: 'IRANSans',
                      fontSize: 12.0,
                      color: Colors.white)),
            ),
            actions: <Widget>[
              IconButton(
                  icon: Icon(
                    FontAwesomeIcons.filter,
                    size: 16,
                  ),
                  tooltip: 'فیلتر',
                  onPressed: () async {
                    queryParamsFiltered =
                        await showGeneralDialog<List<QueryParam>>(
                      context: context,
                      pageBuilder: (BuildContext context,
                          Animation<double> animation,
                          Animation<double> secondaryAnimation) {
                        return widget.filterWidget;
                      },
                      barrierColor: Color.fromRGBO(255, 255, 255, 0.9),
                      barrierDismissible: true,
                      barrierLabel: MaterialLocalizations.of(context)
                          .modalBarrierDismissLabel,
                      transitionDuration: const Duration(milliseconds: 200),
                    );
                    if (queryParamsFiltered != null) {
                      setState(() {
                        lock = false;
                        clearItems();
                      });
                    }
                  }),
              IconButton(
                  icon: Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      Icon(
                        FontAwesomeIcons.filter,
                        size: 12,
                      ),
                      Icon(
                        Icons.not_interested,
                        size: 26,
                      ),
                    ],
                  ),
                  tooltip: 'حذف فیلتر جاری',
                  onPressed: () {
                    widget.onDeleteFilter();
                    queryParamsFiltered = [];
                    setState(() {
                      lock = false;
                      clearItems();
                    });
                  })
            ],
          ),
          body: lock
              ? _listView()
              : WebService.callInlineMode<T>(
                  value: widget.value,
                  futureResponse: _futureResponse,
                  onWaiting: () {
                    return Center(
                      child: SingleChildScrollView(
                        child: ProgressBar(
                          label: "... " + widget.title,
                          value: null,
                          size: 100,
                        ),
                      ),
                    );
                  },
                  on200objectWidget: (obj) {
                    return null;
                  },
                  on200listWidget: (list) {
                    if (lock == false) {
                      items.addAll(list);
                      lock = true;
                    }
                    return _listView();
                  },
                  on204Widget: () {
                    return Center(
                        child: SvgPicture.asset(
                      "assets/images/spider_web.svg",
                      height: 150,
                      color: Colors.black12,
                      semanticsLabel: 'nothing to show',
                    ));
                  },
                )),
    );
  }

  ListView _listView() {
    return ListView.separated(
        separatorBuilder: (context, index) => Divider(
              // color: setting.currentSystem.color900,
              thickness: .25,
            ),
        itemCount: items.length,
        itemBuilder: (BuildContext context, int index) {
          return InkWell(
            borderRadius: BorderRadius.circular(0.0),
            child: widget.itemBuilder(items[index], index),
            onTap: () {
              lock = true;
              widget.onTap(items[index]);
            },
          );
        });
  }

  Future<Response> _futureResponse() {
    List<QueryParam> queryParams = [];
    queryParams.addAll(queryParamsFiltered);
    queryParams.addAll(widget.queryParams);
    return TRestful.get(
      widget.resource,
      setting: setting,
      queryParams: queryParams,
      timeout: 120,
    );
  }
}

class FilterWidget extends StatefulWidget {
  final String title;
  final List<Widget> widgets;
  final void Function() onFiltered;
  final void Function() onCanceled;
  const FilterWidget({
    @required this.title,
    @required this.widgets,
    @required this.onFiltered,
    @required this.onCanceled,
  });

  @override
  _FilterWidgetState createState() => _FilterWidgetState();
}

class _FilterWidgetState extends State<FilterWidget> {
  Setting setting;
  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: setting.currentSystem.themeData,
      child: WillPopScope(
        onWillPop: () {
          widget.onCanceled();
          return Future.value(true);
        },
        child: Scaffold(
          appBar: AppBar(
            actions: <Widget>[],
            title: ListTile(
              title: Text(" فیلتر " + widget.title,
                  textAlign: TextAlign.right,
                  style: TextStyle(
                      fontFamily: 'IRANSans',
                      fontSize: 12.0,
                      color: Colors.white)),
            ),
          ),
          body: Container(
            padding: EdgeInsets.all(5),
            child: ListView(
              children: widget.widgets,
            ),
          ),
          floatingActionButton: FloatingActionButton(
              tooltip: "اعمال فیلتر",
              child: Icon(
                FontAwesomeIcons.filter,
                color: setting.currentSystem.themeData.primaryColorDark,
              ),
              onPressed: widget.onFiltered),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerFloat,
        ),
      ),
    );
  }
}
