import 'package:com_tarazgroup/models/system.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:flutter/material.dart';

class Numpad<T> extends StatefulWidget {
  final T value;
  final void Function(T value) onChanged;

  const Numpad({
    @required this.value,
    @required this.onChanged,
  });
  @override
  _NumpadState createState() => _NumpadState<T>();
}

class _NumpadState<T> extends State<Numpad<T>> {
  String value;
  System currentSystem;
  bool isDotClicked = false;
  @override
  void initState() {
    if (widget.value is double) {
      if ((widget.value as double) % 1 == 0) {
        value = (widget.value as double).toInt().toString();
      } else {
        isDotClicked = true;
        value = (widget.value as double).toString();
      }
    } else {
      value = (widget.value as int).toString();
    }
    currentSystem = TIW.getSetting(context, initState: true).currentSystem;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 2),
        ),
        Text(
          value.toString(),
          style: TextStyle(
            fontFamily: 'IRANSansBlack',
            fontSize: 20.0,
            color: Colors.black54,
          ),
        ),
        Expanded(
          child: GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                childAspectRatio: 1.16,
                crossAxisSpacing: 0.0,
                mainAxisSpacing: 0.0,
              ),
              // shrinkWrap: true,
              // primary: false,
              itemCount: 12,
              padding: const EdgeInsets.only(left: 25, right: 25),
              itemBuilder: (BuildContext context1, int index) {
                // return ElevatedButton();
                switch (index) {
                  case 0:
                  case 1:
                  case 2:
                  case 3:
                  case 4:
                  case 5:
                  case 6:
                  case 7:
                  case 8:
                    return _button((index + 1).toString(), () {
                      _onPressedNum(index + 1);
                    });
                  case 9:
                    var func;
                    if (widget.value is double) {
                      func = () {
                        if (!isDotClicked) {
                          if (value == "0") {
                            value = "0.";
                          } else {
                            value = value + ".";
                          }
                          widget.onChanged(double.parse(value) as T);
                          isDotClicked = true;
                        }
                        setState(() {});
                      };
                    }
                    return _button(".", func);

                  case 10:
                    return _button("0", () {
                      if (value == "0") {
                        value = "0";
                      } else {
                        value = value + "0";
                      }
                      if (widget.value is double) {
                        widget.onChanged(double.parse(value) as T);
                      } else {
                        widget.onChanged(int.parse(value) as T);
                      }
                      setState(() {});
                    });
                  case 11:
                    return _button("C", () {
                      isDotClicked = false;
                      value = "0";
                      if (widget.value is double) {
                        widget.onChanged(double.parse(value) as T);
                      } else {
                        widget.onChanged(int.parse(value) as T);
                      }
                      setState(() {});
                    });

                  default:
                    return null;
                }
              }),
        )
      ],
    );
  }

  void _onPressedNum(int number) {
    if (value == "0") {
      value = number.toString();
    } else {
      value = value + number.toString();
    }
    if (widget.value is double) {
      widget.onChanged(double.parse(value) as T);
    } else {
      widget.onChanged(int.parse(value) as T);
    }
    setState(() {});
  }

  Widget _button(String title, Function() onPressed) {
    return SizedBox(
      child: Center(
        child: Container(
          height: 65,
          width: 65,
          child: ElevatedButton(
            // color: Colors.grey[100],
            style: ElevatedButton.styleFrom(
            primary: Colors.grey[200]),
            child: Text(
              title,
              style: TextStyle(
                fontFamily: 'IRANSansBlack',
                fontSize: 25.0,
                color: Colors.black38,
              ),
            ),
            onPressed: onPressed,
          ),
        ),
      ),
    );
  }
}
