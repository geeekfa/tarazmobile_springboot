import 'dart:async';

import 'package:com_tarazgroup/models/progressbar.dart';
import 'package:com_tarazgroup/models/query_param.dart';
import 'package:com_tarazgroup/models/setting.dart';
import 'package:com_tarazgroup/models/tiw.dart';
import 'package:com_tarazgroup/modules/future/webservice.dart';
import 'package:com_tarazgroup/modules/listview/listview_filter.dart';
import 'package:com_tarazgroup/modules/rest/restful.dart';
import 'package:com_tarazgroup/modules/tableview/tableview.dart';
import 'package:com_tarazgroup/modules/tableview/tcolumn.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';

class _Provider<T> with ChangeNotifier {
  final String title;
  final String subtitle;
  final T value;
  final String resource;
  final List<QueryParam> queryParams;
  final void Function(T t) onTap;
  final List<Widget> filterWidgets;
  final List<QueryParam> Function() onFiltered;
  final void Function() onDeleteFilter;
  final void Function() onReport;
  final void Function() on204;

  _Provider({
    @required this.title,
    @required this.subtitle,
    @required this.value,
    @required this.resource,
    @required this.queryParams,
    @required this.onTap,
    @required this.filterWidgets,
    @required this.onFiltered,
    @required this.onDeleteFilter,
    @required this.onReport,
    @required this.on204,
  });
}

class MultipleView<T> extends StatefulWidget {
  final int initialIndex;
  final String title;
  final String subtitle;
  final T value;
  final String resource;
  final List<QueryParam> queryParams;
  final void Function(T t) onTap;
  final Widget Function(T t, int index) listViewItemBuilder;
  final Widget Function(TColumn tColumn, T t) tableViewCellBuilder;
  final List<TColumn> tableViewColumns;
  final Widget Function(T t, int index) mainSectionBuilder;
  final Widget Function(T t, int index) detailSectionBuilder;

  final List<Widget> filterWidgets;
  final List<QueryParam> Function() onFiltered;
  final void Function() onDeleteFilter;
  final void Function() onReport;
  final void Function() on204;
  final void Function(int tabIndex) onTabChanged;

  const MultipleView({
    @required this.initialIndex,
    @required this.title,
    @required this.subtitle,
    @required this.value,
    @required this.resource,
    @required this.queryParams,
    @required this.onTap,
    @required this.listViewItemBuilder,
    @required this.tableViewCellBuilder,
    @required this.tableViewColumns,
    @required this.mainSectionBuilder,
    @required this.detailSectionBuilder,
    @required this.filterWidgets,
    @required this.onFiltered,
    @required this.onDeleteFilter,
    this.onReport,
    @required this.on204,
    @required this.onTabChanged,
  });
  @override
  _MultipleViewState createState() => _MultipleViewState<T>();
}

class _MultipleViewState<T> extends State<MultipleView<T>>
    with SingleTickerProviderStateMixin {
  Setting setting;
  TabController tabController;
  final keyListViewSection = new GlobalKey<__ListViewSectionState>();
  final keyPageViewSection = new GlobalKey<__PageViewSectionState>();
  final keyTableViewSection = new GlobalKey<__TableViewSectionState>();

  final List<Widget> tabBars = [];
  final List<Widget> tabBarViews = [];

  void tabListener() {
    if (!tabController.indexIsChanging) {
      widget.onTabChanged(tabController.index);
    }
  }

  void addTabBars() {
    if (widget.listViewItemBuilder != null) {
      tabBars.add(Tab(
        icon: Icon(Icons.view_list),
      ));
    }
    if (widget.mainSectionBuilder != null) {
      tabBars.add(Tab(
        icon: Icon(Icons.view_carousel),
      ));
    }
    if (widget.tableViewCellBuilder != null) {
      tabBars.add(Tab(
        icon: Icon(Icons.view_column),
      ));
    }
  }

  void addTabViews() {
    tabBarViews.clear();
    if (widget.listViewItemBuilder != null) {
      tabBarViews.add(_ListViewSection<T>(
        key: keyListViewSection,
        itemBuilder: widget.listViewItemBuilder,
      ));
    }
    if (widget.mainSectionBuilder != null) {
      tabBarViews.add(_PageViewSection<T>(
        key: keyPageViewSection,
        mainSectionBuilder: widget.mainSectionBuilder,
        detailSectionBuilder: widget.detailSectionBuilder,
      ));
    }
    if (widget.tableViewCellBuilder != null) {
      tabBarViews.add(_TableViewSection<T>(
        key: keyTableViewSection,
        value: widget.value,
        itemBuilder: widget.tableViewCellBuilder,
        tableViewColumns: widget.tableViewColumns,
      ));
    }
  }

  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    addTabBars();
    tabController = new TabController(
      vsync: this,
      length: tabBars.length,
      initialIndex: widget.initialIndex,
    )..addListener(tabListener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    addTabViews();
    return Theme(
      data: setting.currentSystem.themeData,
      child: ChangeNotifierProvider(create: (context) {
        return _Provider<T>(
            queryParams: widget.queryParams,
            resource: widget.resource,
            value: widget.value,
            title: widget.title,
            subtitle: widget.subtitle,
            onTap: widget.onTap,
            filterWidgets: widget.filterWidgets,
            onFiltered: widget.onFiltered,
            onDeleteFilter: widget.onDeleteFilter,
            onReport: widget.onReport,
            on204: widget.on204);
      }, child: Consumer<_Provider<T>>(builder: (context, provider, child) {
        if (tabBars.length == 1) {
          return Scaffold(
            appBar: AppBar(
              actions: _actions(),
              title: _title(),
            ),
            body: tabBarViews[0],
          );
        }
        return DefaultTabController(
          length: tabBars.length,
          initialIndex: widget.initialIndex,
          child: Scaffold(
            appBar: AppBar(
              actions: _actions(),
              title: _title(),
              bottom: TabBar(
                controller: tabController,
                indicatorWeight: 2,
                indicatorColor: Colors.white,
                tabs: tabBars,
              ),
            ),
            body: TabBarView(
              controller: tabController,
              physics: NeverScrollableScrollPhysics(),
              children: tabBarViews,
            ),
          ),
        );
      })),
    );
  }

  List<Widget> _actions() {
    List<Widget> actions = [];
    actions.add(IconButton(
        icon: Icon(
          FontAwesomeIcons.filter,
          size: 16,
        ),
        tooltip: 'فیلتر',
        onPressed: () {
          if (keyListViewSection.currentState != null) {
            keyListViewSection.currentState.onFilter();
          } else if (keyPageViewSection.currentState != null) {
            keyPageViewSection.currentState.onFilter();
          } else if (keyTableViewSection.currentState != null) {
            keyTableViewSection.currentState.onFilter();
          }
        }));
    actions.add(IconButton(
        icon: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Icon(
              FontAwesomeIcons.filter,
              size: 12,
            ),
            Icon(
              Icons.not_interested,
              size: 26,
            ),
          ],
        ),
        tooltip: 'حذف فیلتر جاری',
        onPressed: () {
          if (keyListViewSection.currentState != null) {
            keyListViewSection.currentState.onDeleteFilter();
          } else if (keyPageViewSection.currentState != null) {
            keyPageViewSection.currentState.onDeleteFilter();
          } else if (keyTableViewSection.currentState != null) {
            keyTableViewSection.currentState.onDeleteFilter();
          }
        }));
    if (widget.onReport != null) {
      actions.insert(
          0,
          IconButton(
              icon: Icon(
                FontAwesomeIcons.chartPie,
                size: 16,
              ),
              tooltip: 'گزارشات',
              onPressed: () {
                if (keyListViewSection.currentState != null) {
                  keyListViewSection.currentState.onReport();
                } else if (keyPageViewSection.currentState != null) {
                  keyPageViewSection.currentState.onReport();
                } else if (keyTableViewSection.currentState != null) {
                  keyTableViewSection.currentState.onReport();
                }
              }));
    }
    return actions;
  }

  Widget _title() {
    return ListTile(
      subtitle: Text(widget.subtitle,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          softWrap: true,
          textAlign: TextAlign.right,
          style: TextStyle(
            fontFamily: 'IRANSansUltraLight',
            fontSize: 10.0,
            color: Colors.white,
          )),
      title: Text(widget.title,
          textAlign: TextAlign.right,
          style: TextStyle(
            fontFamily: 'IRANSans',
            fontSize: 12.0,
            color: Colors.white,
          )),
    );
  }
}

class _ListViewSection<T> extends StatefulWidget {
  final Widget Function(T t, int index) itemBuilder;
  const _ListViewSection({
    Key key,
    this.itemBuilder,
  }) : super(key: key);

  @override
  __ListViewSectionState createState() => __ListViewSectionState<T>();
}

class __ListViewSectionState<T> extends State<_ListViewSection<T>> {
  Setting setting;
  bool lock;
  List<T> items;
  List<QueryParam> queryParamsFiltered;

  Future<void> onFilter() async {
    final _Provider<T> _provider =
        Provider.of<_Provider<T>>(context, listen: false);
    queryParamsFiltered = await showGeneralDialog<List<QueryParam>>(
      context: context,
      pageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation) {
        return FilterWidget(
          title: _provider.title,
          widgets: _provider.filterWidgets,
          onFiltered: () {
            List<QueryParam> queryParams = _provider.onFiltered();
            Navigator.pop<List<QueryParam>>(context, queryParams);
          },
          onCanceled: () {},
        );
      },
      barrierColor: Color.fromRGBO(255, 255, 255, 0.9),
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      transitionDuration: const Duration(milliseconds: 200),
    );
    if (queryParamsFiltered != null) {
      setState(() {
        lock = false;
        clearItems();
      });
    } else {
      queryParamsFiltered = [];
    }
  }

  void onDeleteFilter() {
    final _Provider<T> _provider =
        Provider.of<_Provider<T>>(context, listen: false);
    _provider.onDeleteFilter();
    queryParamsFiltered = [];
    setState(() {
      lock = false;
      clearItems();
    });
  }

  void onReport() {
    final _Provider<T> _provider =
        Provider.of<_Provider<T>>(context, listen: false);
    _provider.onReport();

    // setState(() {
    //   lock = false;
    // });
  }

  void clearItems() {
    items = [];
  }

  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    queryParamsFiltered = [];
    lock = false;
    clearItems();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (lock) {
      if (items.length == 0) {
        return _nothing();
      }
      return _listView(true);
    }
    return _webService();
  }

  Widget _webService() {
    final _Provider<T> _provider =
        Provider.of<_Provider<T>>(context, listen: false);
    return WebService.callInlineMode<T>(
      value: _provider.value,
      futureResponse: _futureResponse,
      onWaiting: () {
        return Center(
          child: SingleChildScrollView(
            child: ProgressBar(
              label: "... " + _provider.title,
              value: null,
              size: 100,
            ),
          ),
        );
      },
      on200objectWidget: (obj) {
        return null;
      },
      on200listWidget: (list) {
        if (lock == true) {
        } else {
          items.addAll(list);
          lock = true;
        }
        return _listView(true);
      },
      on204Widget: () {
        lock = true;
        Timer(const Duration(milliseconds: 200), () {
          _provider.on204();
        });
        return _nothing();
      },
    );
  }

  Widget _nothing() {
    return Center(
        child: SvgPicture.asset(
      "assets/images/spider_web.svg",
      height: 150,
      color: Colors.black12,
      semanticsLabel: 'nothing to show',
    ));
  }

  ListView _listView(bool hasScrollListener) {
    final _Provider<T> _provider =
        Provider.of<_Provider<T>>(context, listen: false);
    return ListView.separated(
        separatorBuilder: (context, index) => Divider(
              thickness: 4,
            ),
        itemCount: items.length,
        itemBuilder: (BuildContext context, int index) {
          return InkWell(
            borderRadius: BorderRadius.circular(0.0),
            child: widget.itemBuilder(items[index], index),
            onTap: () {
              lock = true;
              _provider.onTap(items[index]);
            },
          );
        });
  }

  Future<Response> _futureResponse() {
    final _Provider<T> _provider =
        Provider.of<_Provider<T>>(context, listen: false);
    List<QueryParam> queryParams = [];
    queryParams.addAll(queryParamsFiltered);
    queryParams.addAll(_provider.queryParams);
    return TRestful.get(
      _provider.resource,
      setting: setting,
      queryParams: queryParams,
      timeout: 120,
    );
  }
}

class _PageViewSection<T> extends StatefulWidget {
  final Widget Function(T t, int index) mainSectionBuilder;
  final Widget Function(T t, int index) detailSectionBuilder;

  const _PageViewSection({
    Key key,
    @required this.mainSectionBuilder,
    @required this.detailSectionBuilder,
  }) : super(key: key);

  @override
  __PageViewSectionState createState() => __PageViewSectionState<T>();
}

class __PageViewSectionState<T> extends State<_PageViewSection<T>> {
  PageController pageController;
  Setting setting;
  bool lock;
  double page = 0.0;
  List<T> items;
  List<QueryParam> queryParamsFiltered;

  final GlobalKey key = GlobalKey();

  PersistentBottomSheetController bottomSheetController;
  bool isOpenedBottomSheet = false;

  Future<void> onFilter() async {
    final _Provider<T> _provider =
        Provider.of<_Provider<T>>(context, listen: false);
    queryParamsFiltered = await showGeneralDialog<List<QueryParam>>(
      context: context,
      pageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation) {
        return FilterWidget(
          title: _provider.title,
          widgets: _provider.filterWidgets,
          onFiltered: () {
            List<QueryParam> queryParams = _provider.onFiltered();
            Navigator.pop<List<QueryParam>>(context, queryParams);
          },
          onCanceled: () {},
        );
      },
      barrierColor: Color.fromRGBO(255, 255, 255, 0.9),
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      transitionDuration: const Duration(milliseconds: 200),
    );
    if (queryParamsFiltered != null) {
      setState(() {
        lock = false;
        clearItems();
      });
    } else {
      queryParamsFiltered = [];
    }
  }

  void onDeleteFilter() {
    final _Provider<T> _provider =
        Provider.of<_Provider<T>>(context, listen: false);
    _provider.onDeleteFilter();
    queryParamsFiltered = [];
    setState(() {
      lock = false;
      clearItems();
    });
  }

  void onReport() {
    final _Provider<T> _provider =
        Provider.of<_Provider<T>>(context, listen: false);
    _provider.onReport();

    // setState(() {
    //   lock = false;
    // });
  }

  void clearItems() {
    items = [];
  }

  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    queryParamsFiltered = [];
    lock = false;
    clearItems();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (lock) {
      if (items.length == 0) {
        return _nothing();
      }
      return _pageView(true);
    }
    return _webService();
  }

  Widget _webService() {
    final _Provider<T> _provider =
        Provider.of<_Provider<T>>(context, listen: false);
    return WebService.callInlineMode<T>(
      value: _provider.value,
      futureResponse: _futureResponse,
      onWaiting: () {
        return Center(
          child: SingleChildScrollView(
            child: ProgressBar(
              label: "... " + _provider.title,
              value: null,
              size: 100,
            ),
          ),
        );
      },
      on200objectWidget: (obj) {
        return null;
      },
      on200listWidget: (list) {
        if (lock == true) {
        } else {
          items.addAll(list);
          lock = true;
        }
        return _pageView(true);
      },
      on204Widget: () {
        lock = true;
        Timer(const Duration(milliseconds: 200), () {
          _provider.on204();
        });
        return _nothing();
      },
    );
  }

  Widget _nothing() {
    return Center(
        child: SvgPicture.asset(
      "assets/images/spider_web.svg",
      height: 150,
      color: Colors.black12,
      semanticsLabel: 'nothing to show',
    ));
  }

  Widget _pageView(bool hasScrollListener) {
    final _Provider<T> _provider =
        Provider.of<_Provider<T>>(context, listen: false);
    return PageView.builder(
      key: key,
      physics: AlwaysScrollableScrollPhysics(),
      controller: pageController,
      itemCount: items.length,
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
          child: _PageView(
            pageIndex: index,
            item: items[index],
            mainSectionBuilder: widget.mainSectionBuilder,
            detailSectionBuilder: widget.detailSectionBuilder,
          ),
          onTap: () {
            lock = true;
            _provider.onTap(items[index]);
          },
        );
      },
      onPageChanged: (index) {
        // setState(() {});
        // if (pageIndexOpened != -1) {
        //   Navigator.pop(context);
        //   pageIndexOpened = -1;
        // }
      },
    );
  }

  Future<Response> _futureResponse() {
    final _Provider<T> _provider =
        Provider.of<_Provider<T>>(context, listen: false);
    List<QueryParam> queryParams = [];
    queryParams.addAll(queryParamsFiltered);
    queryParams.addAll(_provider.queryParams);
    return TRestful.get(
      _provider.resource,
      setting: setting,
      queryParams: queryParams,
      timeout: 120,
    );
  }
}

class _PageView<T> extends StatefulWidget {
  final int pageIndex;
  final T item;
  final Widget Function(T t, int index) mainSectionBuilder;
  final Widget Function(T t, int index) detailSectionBuilder;

  const _PageView({
    Key key,
    @required this.pageIndex,
    @required this.item,
    @required this.mainSectionBuilder,
    @required this.detailSectionBuilder,
  }) : super(key: key);

  @override
  __PageViewState createState() => __PageViewState<T>();
}

class __PageViewState<T> extends State<_PageView<T>> {
  bool isOpenedBottomSheet = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Theme.of(context).hoverColor,
            border: Border.all(
              width: 0.3,
              color: Theme.of(context).primaryColorLight,
            ),
            borderRadius: BorderRadius.all(Radius.circular(0.0)),
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: widget.mainSectionBuilder(widget.item, widget.pageIndex),
        ),
        bottomSheet: isOpenedBottomSheet
            ? Container(
                margin: EdgeInsets.only(bottom: 10, left: 10, right: 10),
                width: MediaQuery.of(context).size.width,
                height: 280,
                decoration: BoxDecoration(
                  color: Theme.of(context).hoverColor,
                  border: Border.all(
                    width: 0.3,
                    color: Theme.of(context).primaryColorLight,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(0.0)),
                ),
                child:
                    widget.detailSectionBuilder(widget.item, widget.pageIndex),
              )
            : Container(
                height: 0,
              ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButton: FloatingActionButton(
          heroTag: widget.pageIndex,
          disabledElevation: 0,
          backgroundColor: Theme.of(context).buttonColor,
          child: !isOpenedBottomSheet
              ? Icon(Icons.visibility)
              : Icon(Icons.visibility_off),
          onPressed: () {
            setState(() {
              isOpenedBottomSheet = !isOpenedBottomSheet;
            });
          },
        ));
  }
}

class _TableViewSection<T> extends StatefulWidget {
  final T value;
  final Widget Function(TColumn tColumn, T t) itemBuilder;
  final List<TColumn> tableViewColumns;

  const _TableViewSection({
    Key key,
    @required this.value,
    @required this.itemBuilder,
    @required this.tableViewColumns,
  }) : super(key: key);

  @override
  __TableViewSectionState createState() => __TableViewSectionState<T>();
}

class __TableViewSectionState<T> extends State<_TableViewSection<T>> {
  Setting setting;
  bool lock;
  double offset = 0.0;
  List<T> items;
  List<QueryParam> queryParamsFiltered;

  Future<void> onFilter() async {
    final _Provider<T> _provider =
        Provider.of<_Provider<T>>(context, listen: false);
    queryParamsFiltered = await showGeneralDialog<List<QueryParam>>(
      context: context,
      pageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation) {
        return FilterWidget(
          title: _provider.title,
          widgets: _provider.filterWidgets,
          onFiltered: () {
            List<QueryParam> queryParams = _provider.onFiltered();
            Navigator.pop<List<QueryParam>>(context, queryParams);
          },
          onCanceled: () {},
        );
      },
      barrierColor: Color.fromRGBO(255, 255, 255, 0.9),
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      transitionDuration: const Duration(milliseconds: 200),
    );
    if (queryParamsFiltered != null) {
      setState(() {
        lock = false;
        clearItems();
      });
    } else {
      queryParamsFiltered = [];
    }
  }

  void onDeleteFilter() {
    final _Provider<T> _provider =
        Provider.of<_Provider<T>>(context, listen: false);
    _provider.onDeleteFilter();
    queryParamsFiltered = [];
    setState(() {
      lock = false;
      clearItems();
    });
  }

  void onReport() {
    final _Provider<T> _provider =
        Provider.of<_Provider<T>>(context, listen: false);
    _provider.onReport();

    // setState(() {
    //   lock = false;
    // });
  }

  void clearItems() {
    items = [];
  }

  @override
  void initState() {
    setting = TIW.getSetting(context, initState: true);
    queryParamsFiltered = [];
    lock = false;
    clearItems();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (lock) {
      if (items.length == 0) {
        return _nothing();
      }
      return _tableView(false);
    }
    return _webService();
  }

  Widget _webService() {
    final _Provider<T> _provider =
        Provider.of<_Provider<T>>(context, listen: false);
    return WebService.callInlineMode<T>(
      value: _provider.value,
      futureResponse: _futureResponse,
      onWaiting: () {
        return Center(
          child: SingleChildScrollView(
            child: ProgressBar(
              label: "... " + _provider.title,
              value: null,
              size: 100,
            ),
          ),
        );
      },
      on200objectWidget: (obj) {
        return null;
      },
      on200listWidget: (list) {
        if (lock == true) {
        } else {
          items.addAll(list);
          lock = true;
        }
        return _tableView(false);
      },
      on204Widget: () {
        lock = true;
        Timer(const Duration(milliseconds: 200), () {
          _provider.on204();
        });
        return _nothing();
      },
    );
  }

  Widget _nothing() {
    return Center(
        child: SvgPicture.asset(
      "assets/images/spider_web.svg",
      height: 150,
      color: Colors.black12,
      semanticsLabel: 'nothing to show',
    ));
  }

  Widget _tableView(bool isWating) {
    // scrollController = ScrollController(initialScrollOffset: offset);
    // if (hasScrollListener) {
    //   scrollController..addListener(scrollListener);
    // }
    return Directionality(
      textDirection: TextDirection.rtl,
      child: TableView<T>(
        onTap: (T item) {
          final _Provider<T> _provider =
              Provider.of<_Provider<T>>(context, listen: false);
          lock = true;
          _provider.onTap(item);
        },
        items: items,
        tColumns: widget.tableViewColumns,
        rowsLength: items.length,
        cellBuilder: (
          index,
          tColumn,
        ) {
          return widget.itemBuilder(tColumn, items[index]);
        },
        legendCell: isWating
            ? ProgressBar(
                label: "",
                value: null,
                size: 10,
              )
            : Text("#"),
      ),
    );
  }

  Future<Response> _futureResponse() {
    final _Provider<T> _provider =
        Provider.of<_Provider<T>>(context, listen: false);
    List<QueryParam> queryParams = [];
    queryParams.addAll(_provider.queryParams);

    // کد های زیر برای این است که فیلتر بیس و فیلتری که بعدا از قیف بالا می آید با هم تداخل نکند

    var queryParamsBaseContain = queryParamsFiltered
        .where((queryParamFiltered) => queryParamFiltered.name == "filter");
    if (queryParamsBaseContain.isEmpty) {
      queryParams.addAll(queryParamsFiltered);
    } else {
      var queryParamsBaseContain =
          queryParams.where((queryParam) => queryParam.name == "filter");

      if (queryParamsBaseContain.isEmpty) {
        queryParams.addAll(queryParamsFiltered);
      } else {
        var item = queryParams.firstWhere(
            (queryParam) => queryParam.name == "filter",
            orElse: () => null);

        queryParams.remove(item);
        queryParams.addAll(queryParamsFiltered);
      }
    }

    return TRestful.get(
      _provider.resource,
      setting: setting,
      queryParams: queryParams,
      timeout: 120,
    );
  }
}
